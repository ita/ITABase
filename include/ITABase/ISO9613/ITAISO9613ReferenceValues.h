/*
 * ----------------------------------------------------------------
 *
 *		ITA core libs
 *		(c) Copyright Institute of Technical Acoustics (ITA)
 *		RWTH Aachen University, Germany, 2015-2024
 *
 * ----------------------------------------------------------------
 *				    ____  __________  _______
 *				   //  / //__   ___/ //  _   |
 *				  //  /    //  /    //  /_|  |
 *				 //  /    //  /    //  ___   |
 *				//__/    //__/    //__/   |__|
 *
 * ----------------------------------------------------------------
 *
 */

#ifndef INCLUDE_WATCHER_ITA_ISO_9613_REFERENCE_VALUES
#define INCLUDE_WATCHER_ITA_ISO_9613_REFERENCE_VALUES

#include <ITAThirdOctaveMagnitudeSpectrum.h>
#include <vector>

namespace ITABase
{
	namespace ISO9613
	{
		// Reference data-set taken from Table 1 in ISO 9613-1
		// Stores values for static pressure [kPa], temperature [°C], relative humidity [%] and respective alpha [dB/km] values for 1/3-octave band center frequrencies between 50 HZ and 10 kHz
		// IMPORTANT: Do not use the data given for the 20kHz band, since it is undefined (set to zero dB attenuation).
		class ITA_BASE_API CReferenceDataSet
		{
		private:
			const double dStaticPressueKiloPa = 131.325;
			double dTemperatureDegC;
			double dRelativeHumidityPercent;
			CThirdOctaveDecibelMagnitudeSpectrum oSpectrumAlphaDBperKm;
			
			CReferenceDataSet( double dTemperatureDegC, double dRelativeHumidityPercent, const CThirdOctaveDecibelMagnitudeSpectrum& oSpectrumAlphaDBperKm );
		public:
			static CReferenceDataSet Create( int idxCase );
			static int MinimumCaseID( ) { return 1; }
			static int MaximumCaseID( ) { return 5; }

		public:
			double StaticPressureKiloPa( ) const { return dStaticPressueKiloPa; };
			double StaticPressurePa( ) const { return dStaticPressueKiloPa * 1000; };
			double TemperatureDegC( ) const { return dTemperatureDegC; };
			double TemperatureKelvin( ) const { return dTemperatureDegC + 273.15; };
			double RelativeHumidity( ) const { return dRelativeHumidityPercent; };
			const CThirdOctaveDecibelMagnitudeSpectrum SpectrumDB( float fDistance = 1000.0f ) const;
			const CThirdOctaveGainMagnitudeSpectrum GainSpectrum( float fDistance = 1000.0f ) const;
		};

	} // namespace ISO9613
} // namespace ITABase

#endif // INCLUDE_WATCHER_ITA_ISO_9613_REFERENCE_VALUES
