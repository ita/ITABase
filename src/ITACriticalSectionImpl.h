#ifndef INCLUDE_WATCHER_ITA_CRITICALSECTIONIMPL
#define INCLUDE_WATCHER_ITA_CRITICALSECTIONIMPL

class ITACriticalSectionImpl
{
public:
	virtual ~ITACriticalSectionImpl( ) { };

	virtual bool tryenter( ) const = 0;
	virtual void enter( ) const    = 0;
	virtual void leave( ) const    = 0;
};

#endif // INCLUDE_WATCHER_ITA_CRITICALSECTIONIMPL
