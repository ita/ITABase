#include <ITAAudiofileReader.h>
#include <ITAAudiofileWriter.h>
#include <ITAException.h>
#include <ITAFade.h>
#include <ITAFastMath.h>
#include <ITASampleBuffer.h>
#include <algorithm>
#include <assert.h>
#include <cmath>
#include <math.h>
#include <memory.h>
#include <sstream>


ITASampleBuffer::ITASampleBuffer( ) : m_pParent( NULL ), m_iLength( 0 ), m_pfData( NULL ) {}

ITASampleBuffer::ITASampleBuffer( int iLength, bool bZeroinit ) : m_pParent( NULL ), m_iLength( 0 ), m_pfData( NULL )
{
	Init( iLength, bZeroinit );
}

ITASampleBuffer::ITASampleBuffer( const ITASampleBuffer* pSource ) : m_pParent( NULL ), m_iLength( 0 ), m_pfData( NULL )
{
	*this = *pSource;
}

ITASampleBuffer::ITASampleBuffer( const ITASampleBuffer& sbSource ) : m_pParent( NULL ), m_iLength( 0 ), m_pfData( NULL )
{
	*this = sbSource;
}

ITASampleBuffer::~ITASampleBuffer( )
{
	Free( );
}

void ITASampleBuffer::Init( int iLength, bool bZeroInit )
{
	if( m_pParent )
		ITA_EXCEPT1( MODAL_EXCEPTION, "Init impossible. This sample buffer is a channel of a parent sample frame." );

	assert( iLength >= 0 );

	Free( );

	m_iLength = iLength;

	size_t iBytes = size_t( iLength * sizeof( float ) );
#ifdef __GNUC__
	// TODO: Für den g++ erstmal kein aligned malloc
	m_pfData = (float*)malloc( iBytes );
#else
	m_pfData = (float*)_aligned_malloc( iBytes, 16 );
#endif

	if( bZeroInit )
		fm_zero( m_pfData, m_iLength );
}

void ITASampleBuffer::Free( )
{
#ifdef __GNUC__
	if( m_pfData )
		free( m_pfData ); // unaligned
#else
	if( m_pfData )
		_aligned_free( m_pfData );
#endif
	m_pfData  = NULL;
	m_iLength = 0;
}

bool ITASampleBuffer::IsEmpty( ) const
{
	return ( m_iLength == 0 );
}

int ITASampleBuffer::GetLength( ) const
{
	return m_iLength;
}

const float* ITASampleBuffer::GetData( ) const
{
	return m_pfData;
}

float* ITASampleBuffer::GetData( )
{
	return m_pfData;
}

void ITASampleBuffer::Fill( float fValue )
{
	fm_set( m_pfData, fValue, m_iLength );
}

void ITASampleBuffer::Fill( int iOffset, int iCount, float fValue )
{
	if( ( iOffset < 0 ) || ( iOffset >= m_iLength ) )
		ITA_EXCEPT1( INVALID_PARAMETER, "Offset out of range" );

	if( ( iCount < 0 ) || ( iOffset + iCount > m_iLength ) )
		ITA_EXCEPT1( INVALID_PARAMETER, "Count out of range" );

	// TODO: Schnelle Implementierung?
	for( int i = iOffset; i < iOffset + iCount; i++ )
		m_pfData[i] = fValue;
}

void ITASampleBuffer::Zero( )
{
	Fill( 0.0f );
}

void ITASampleBuffer::Zero( int iOffset, int iCount )
{
	Fill( iOffset, iCount, 0.0f );
}

void ITASampleBuffer::Identity( )
{
	if( m_iLength == 0 )
		ITA_EXCEPT1( INVALID_PARAMETER, "Sample buffer has no length" );

	m_pfData[0] = 1;
	for( int i = 1; i < m_iLength; i++ )
		m_pfData[i] = 0;
}

void ITASampleBuffer::Fade( int iOffset, int iCount, int iFadingSign, int iFadingFunction )
{
	assert( iOffset >= 0 );
	assert( iCount > 0 );
	assert( ( iOffset + iCount ) <= m_iLength );

	::Fade( m_pfData + iOffset, iCount, iFadingSign, iFadingFunction );
}

void ITASampleBuffer::Crossfade( const ITASampleBuffer* psbSrc, int iOffset, int iCount, int iCrossfadeDirection, int iFadeFunction )
{
	assert( psbSrc );
	assert( iOffset >= 0 );
	assert( iCount >= 0 );
	assert( ( iOffset + iCount ) <= m_iLength );
	assert( ( iOffset + iCount ) <= psbSrc->m_iLength );

	switch( iCrossfadeDirection )
	{
		case ITABase::CrossfadeDirection::FROM_SOURCE:
		{
			// Samples am Anfang kopieren
			for( int i = 0; i < iOffset; i++ )
				m_pfData[i] = psbSrc->m_pfData[i];
			::Crossfade( psbSrc->m_pfData + iOffset, m_pfData + iOffset, m_pfData + iOffset, iCount, iFadeFunction );
			return;
		}
		case ITABase::CrossfadeDirection::TO_SOURCE:
		{
			assert( psbSrc->m_iLength >= m_iLength );
			::Crossfade( m_pfData + iOffset, psbSrc->m_pfData + iOffset, m_pfData + iOffset, iCount, iFadeFunction );
			// Samples am Ende kopieren
			for( int i = iOffset + iCount; i < m_iLength; i++ )
				m_pfData[i] = psbSrc->m_pfData[i];
		}
		default:
		{
			// Ungültiger Wert
			assert( false );
		}
	}
}

void ITASampleBuffer::Crossfade( const ITASampleBuffer& sbSrc, int iOffset, int iCount, int iCrossfadeDirection, int iFadeFunction )
{
	Crossfade( &sbSrc, iOffset, iCount, iCrossfadeDirection, iFadeFunction );
}

void ITASampleBuffer::Envelope( float fGain0, float fGain1 )
{
	if( IsEmpty( ) )
		return;

	// Linear interpolation
	float m = ( fGain1 - fGain0 ) / float( m_iLength );
	for( int i = 0; i < m_iLength; i++ )
		m_pfData[i] *= ( fGain0 + m * i );
}

void ITASampleBuffer::read( float* pfDest, int iCount, int iSrcOffset ) const
{
	assert( pfDest != NULL );
	assert( iCount >= 0 );
	assert( ( iSrcOffset >= 0 ) );
	assert( ( iSrcOffset + iCount ) <= m_iLength );

	// TODO: Erkennung von MemoryAlignment und schnelle SSE-Variante?
	memcpy( pfDest, m_pfData + iSrcOffset, iCount * sizeof( float ) );
}

void ITASampleBuffer::write( const float* pfSrc, int iCount, int iDestOffset )
{
	assert( pfSrc != NULL );
	assert( iCount >= 0 );
	assert( ( iDestOffset >= 0 ) );
	assert( ( iDestOffset + iCount ) <= m_iLength );

	// TODO: Erkennung von MemoryAlignment und schnelle SSE-Variante?
	memcpy( m_pfData + iDestOffset, pfSrc, iCount * sizeof( float ) );
}

void ITASampleBuffer::write( const ITASampleBuffer* psbSrc, int iCount, int iSrcOffset, int iDestOffset )
{
	assert( psbSrc != NULL );
	assert( iCount >= 0 );
	assert( ( iSrcOffset >= 0 ) );
	assert( ( iSrcOffset + iCount ) <= psbSrc->GetLength( ) );
	assert( ( iDestOffset + iCount ) <= m_iLength );

	// TODO: Erkennung von MemoryAlignment und schnelle SSE-Variante?
	memcpy( m_pfData + iDestOffset, psbSrc->GetData( ) + iSrcOffset, iCount * sizeof( float ) );
}

void ITASampleBuffer::write( const ITASampleBuffer& sbSrc, int iCount, int iSrcOffset, int iDestOffset )
{
	write( &sbSrc, iCount, iSrcOffset, iDestOffset );
}

void ITASampleBuffer::AddSamples( const float* pfSrc, int iCount, int iDestOffset )
{
	assert( pfSrc != NULL );
	assert( iCount >= 0 );
	assert( ( iDestOffset >= 0 ) );
	assert( ( iDestOffset + iCount ) <= m_iLength );

	for( size_t i = 0; i < (size_t)iCount; i++ )
	{
		m_pfData[i + iDestOffset] += pfSrc[i];
	}
}

void ITASampleBuffer::cyclic_read( float* pfDest, int iCount, int iSrcOffset ) const
{
	assert( pfDest != NULL );
	assert( iCount >= 0 );
	// assert( ( iSrcOffset >= 0 ) && ( iSrcOffset < m_iLength ) ); //This limits wrap around of the buffer to "clockwise" only

	int n = 0; // Anzahl kopierter Samples
	// int p = ( ( iSrcOffset % m_iLength ) + m_iLength ) % m_iLength; // Leseposition
	// int p = (iSrcOffset + (int)ceil(fabs(iSrcOffset/ (float)m_iLength))*m_iLength ) % m_iLength; //work out the read cursor for any source offset (+ or -)
	int p = ( iSrcOffset + ( abs( iSrcOffset / m_iLength ) + 1 ) * m_iLength ) % m_iLength;

	while( n < iCount )
	{
		// Verbleibende Samples berechnen
		int r = std::min( iCount - n, m_iLength - p ); // read until a wrap around is needed, r = n0 samples to read on this iteration
		read( pfDest + n, r, p );                      // read(dest, count, offset)
		p = ( p + r ) % m_iLength;
		n += r; // n = total samples read
	}
}

void ITASampleBuffer::cyclic_write( const float* pfSrc, int iCount, int iDestOffset )
{
	assert( pfSrc != NULL );
	assert( iCount >= 0 );

	int n = 0;                                                       // Anzahl kopierter Samples
	int p = ( ( iDestOffset % m_iLength ) + m_iLength ) % m_iLength; // Schreibposition

	while( n < iCount )
	{
		// Verbleibende Samples berechnen
		int r = std::min( iCount - n, m_iLength - p );
		write( pfSrc + n, r, p );
		p = ( p + r ) % m_iLength;
		n += r;
	}
}

void ITASampleBuffer::cyclic_write( const ITASampleBuffer* psbSrc, int iCount, int iSrcOffset, int iDestOffset )
{
	assert( psbSrc != NULL );
	assert( iCount >= 0 );

	int iSrcLength = psbSrc->GetLength( );

	int n = 0; // Anzahl kopierter Samples
	// Beliebige Offsets werden in das positive Intervall abgebildet
	int p = ( ( iSrcOffset % iSrcLength ) + iSrcLength ) % iSrcLength; // Leseposition
	int q = ( ( iDestOffset % m_iLength ) + m_iLength ) % m_iLength;   // Schreibposition

	while( n < iCount )
	{
		// Verbleibende Samples berechnen
		int rs = iSrcLength - p;
		int rd = m_iLength - q;
		int r  = std::min( iCount - n, std::min( rs, rd ) );
		write( psbSrc, r, p, q );
		p = ( p + r ) % iSrcLength;
		q = ( q + r ) % m_iLength;
		n += r;
	}
}

void ITASampleBuffer::cyclic_write( const ITASampleBuffer& sbSrc, int iCount, int iSrcOffset, int iDestOffset )
{
	cyclic_write( &sbSrc, iCount, iSrcOffset, iDestOffset );
}


void ITASampleBuffer::CyclicShift( int iCount )
{
	if( iCount >= GetLength( ) )
		ITA_EXCEPT1( INVALID_PARAMETER, "Shifting by a count greater than buffer length not allowed" );
	ITASampleBuffer sbTemp( this );
	cyclic_write( sbTemp, GetLength( ), iCount );
}

void ITASampleBuffer::add_scalar( float fValue )
{
	// Invariante
	if( fValue == 0 )
		return;

	// TODO: Schnelle Implementierung?
	for( int i = 0; i < m_iLength; i++ )
		m_pfData[i] += fValue;
}

void ITASampleBuffer::sub_scalar( float fValue )
{
	// Invariante
	if( fValue == 0 )
		return;

	// TODO: Schnelle Implementierung?
	for( int i = 0; i < m_iLength; i++ )
		m_pfData[i] -= fValue;
}

void ITASampleBuffer::mul_scalar( float fValue )
{
	// Invariante
	if( fValue == 1 )
		return;

	// TODO: Schnelle Implementierung?
	for( int i = 0; i < m_iLength; i++ )
		m_pfData[i] *= fValue;
}

void ITASampleBuffer::div_scalar( float fValue )
{
	if( fValue == 0 )
		ITA_EXCEPT1( INVALID_PARAMETER, "Division by zero" );

	// Invariante
	if( fValue == 1 )
		return;

	// TODO: Schnelle Implementierung?
	for( int i = 0; i < m_iLength; i++ )
		m_pfData[i] /= fValue;
}

void ITASampleBuffer::add_buf( const ITASampleBuffer* pSource )
{
	add_buf( pSource, pSource->GetLength( ) );
}

void ITASampleBuffer::add_buf( const ITASampleBuffer* pSource, int iCount )
{
	if( !pSource )
		ITA_EXCEPT1( INVALID_PARAMETER, "Nullpointer passed" );
	if( pSource->GetLength( ) < iCount )
		ITA_EXCEPT1( INVALID_PARAMETER, "Lengths of source buffer too small" );
	if( m_iLength < iCount )
		ITA_EXCEPT1( INVALID_PARAMETER, "Requested lengths of source buffer too big to fit into target buffer" );

	fm_add( m_pfData, pSource->GetData( ), iCount );
}

void ITASampleBuffer::add_buf_pos( const ITASampleBuffer* pSource, int iPos ) // TODO: move this to add_buf(mit offset=0)
{
	if( !pSource )
		ITA_EXCEPT1( INVALID_PARAMETER, "Nullpointer passed" );
	// if (pSource->GetLength() < iCount)  ITA_EXCEPT1(INVALID_PARAMETER, "Lengths of source buffer too small");
	if( ( pSource->m_iLength + iPos ) > m_iLength )
		ITA_EXCEPT1( INVALID_PARAMETER, "Source length + delay exceed length of filter" );

	fm_add( &( m_pfData[iPos] ), pSource->GetData( ), pSource->GetLength( ) );
}

void ITASampleBuffer::add_buf_pos( float* fSource, int iSize, int iPos )
{
	if( !fSource )
		ITA_EXCEPT1( INVALID_PARAMETER, "Nullpointer passed" );
	if( m_iLength < iPos + iSize )
		ITA_EXCEPT1( INVALID_PARAMETER, "Source length + delay exceed length of filter" );

	fm_add( &( m_pfData[iPos] ), fSource, iSize );
}

void ITASampleBuffer::sub_buf( const ITASampleBuffer* pSource )
{
	if( !pSource )
		ITA_EXCEPT1( INVALID_PARAMETER, "Nullpointer passed" );
	if( pSource->GetLength( ) != m_iLength )
		ITA_EXCEPT1( INVALID_PARAMETER, "Block lengths do not match" );

	fm_sub( m_pfData, pSource->GetData( ), m_iLength );
}

void ITASampleBuffer::mul_buf( const ITASampleBuffer* pSource )
{
	if( !pSource )
		ITA_EXCEPT1( INVALID_PARAMETER, "Nullpointer passed" );
	if( pSource->GetLength( ) != m_iLength )
		ITA_EXCEPT1( INVALID_PARAMETER, "Block lengths do not match" );

	// TODO: Schnelle Implementierung?
	const float* pfSourceData = pSource->GetData( );
	for( int i = 0; i < m_iLength; i++ )
		m_pfData[i] *= pfSourceData[i];
}

void ITASampleBuffer::div_buf( const ITASampleBuffer* pSource )
{
	if( !pSource )
		ITA_EXCEPT1( INVALID_PARAMETER, "Nullpointer passed" );
	if( pSource->GetLength( ) != m_iLength )
		ITA_EXCEPT1( INVALID_PARAMETER, "Block lengths do not match" );

	// TODO: Schnelle Implementierung?
	// TODO: Was wenn Division durch Null?
	const float* pfSourceData = pSource->GetData( );
	for( int i = 0; i < m_iLength; i++ )
		m_pfData[i] /= pfSourceData[i];
}

void ITASampleBuffer::add_buf( const ITASampleBuffer& sbSource, int iCount )
{
	add_buf( &sbSource, iCount );
}

void ITASampleBuffer::add_buf( const ITASampleBuffer& sbSource )
{
	add_buf( &sbSource );
}

void ITASampleBuffer::sub_buf( const ITASampleBuffer& sbSource )
{
	sub_buf( &sbSource );
}

void ITASampleBuffer::mul_buf( const ITASampleBuffer& sbSource )
{
	mul_buf( &sbSource );
}

void ITASampleBuffer::div_buf( const ITASampleBuffer& sbSource )
{
	div_buf( &sbSource );
}

void ITASampleBuffer::MulAdd( const ITASampleBuffer* pSource, float fScalar, int iSrcOffset, int iDestOffset, int iCount )
{
	if( !pSource )
		ITA_EXCEPT1( INVALID_PARAMETER, "Nullpointer passed" );

	if( iSrcOffset < 0 )
		ITA_EXCEPT1( INVALID_PARAMETER, "Invalid source offset" );
	if( iDestOffset < 0 )
		ITA_EXCEPT1( INVALID_PARAMETER, "Invalid destination offset" );
	if( iCount < 0 )
		ITA_EXCEPT1( INVALID_PARAMETER, "Invalid count" );

	// Test: Lesen innerhalb des Quellblockes
	if( ( iSrcOffset + iCount ) > pSource->GetLength( ) )
		ITA_EXCEPT1( INVALID_PARAMETER, "Source range exceeds source buffer" );
	if( ( iDestOffset + iCount ) > m_iLength )
		ITA_EXCEPT1( INVALID_PARAMETER, "Destination range exceeds destination buffer" );

	// Spezialfall: Count = 0 => Nichts zu tun
	if( iCount == 0 )
		return;

	// Spezialfall: Skalar = 0 => Nichts zu tun
	if( fScalar == 0 )
		return;

	for( int i = 0; i < iCount; i++ )
		m_pfData[iDestOffset + i] += ( pSource->m_pfData[iSrcOffset + i] * fScalar );
}

void ITASampleBuffer::MulAdd( const ITASampleBuffer& sbSource, float fScalar, int iSrcOffset, int iDestOffset, int iCount )
{
	MulAdd( &sbSource, fScalar, iSrcOffset, iDestOffset, iCount );
}

float ITASampleBuffer::FindPeak( int* piPeakIndex )
{
	if( m_iLength == 0 )
	{
		if( piPeakIndex )
			*piPeakIndex = 0;
		return 0;
	}

	float fPeak    = 0;
	int iPeakIndex = 0;

	// TODO: Schnelle Implementierung? (SSE3?)
	for( int i = 0; i < m_iLength; i++ )
	{
		float x = fabs( m_pfData[i] );
		if( x > fPeak )
		{
			fPeak      = x;
			iPeakIndex = i;
		}
	}

	if( piPeakIndex )
		*piPeakIndex = iPeakIndex;

	return fPeak;
}

void ITASampleBuffer::Negate( )
{
	mul_scalar( -1.0f );
}


void ITASampleBuffer::Normalize( )
{
	mul_scalar( FindPeak( ) );
}


float ITASampleBuffer::RootMeanSquare( bool bPower /* = true */ ) const
{
	float fRMS = 0.0f;

	for( int i = 0; i < m_iLength; i++ )
		fRMS += powf( m_pfData[i], 2 );

	return sqrtf( fRMS ) / ( bPower ? (float)m_iLength : 1.0f );
}

float& ITASampleBuffer::operator[]( int iSample )
{
	return m_pfData[iSample];
}

const float& ITASampleBuffer::operator[]( int iSample ) const
{
	return m_pfData[iSample];
}

ITASampleBuffer& ITASampleBuffer::operator=( const ITASampleBuffer& rhs )
{
	// Selbstzuweisung abfangen
	if( &rhs == this )
		return *this;

	if( m_pParent )
		ITA_EXCEPT1( MODAL_EXCEPTION, "Assignment impossible. Would change length, but sample buffer is part of a parent sample frame." );

	// Schnelle Abkürzung
	if( rhs.IsEmpty( ) )
	{
		Free( );
		return *this;
	}

	if( ( m_iLength != rhs.m_iLength ) )
	{
		// Neu allozieren
		Init( rhs.GetLength( ), false );
	}

	// Samples kopieren
	fm_copy( m_pfData, rhs.GetData( ), m_iLength );

	return *this;
}

ITASampleBuffer& ITASampleBuffer::operator+=( float rhs )
{
	add_scalar( rhs );
	return *this;
}

ITASampleBuffer& ITASampleBuffer::operator-=( float rhs )
{
	sub_scalar( rhs );
	return *this;
}

ITASampleBuffer& ITASampleBuffer::operator*=( float rhs )
{
	mul_scalar( rhs );
	return *this;
}

ITASampleBuffer& ITASampleBuffer::operator/=( float rhs )
{
	div_scalar( rhs );
	return *this;
}

ITASampleBuffer& ITASampleBuffer::operator+=( const ITASampleBuffer& rhs )
{
	add_buf( rhs );
	return *this;
}

ITASampleBuffer& ITASampleBuffer::operator-=( const ITASampleBuffer& rhs )
{
	sub_buf( rhs );
	return *this;
}

ITASampleBuffer& ITASampleBuffer::operator*=( const ITASampleBuffer& rhs )
{
	mul_buf( rhs );
	return *this;
}

ITASampleBuffer& ITASampleBuffer::operator/=( const ITASampleBuffer& rhs )
{
	div_buf( rhs );
	return *this;
}

std::string ITASampleBuffer::toString( ) const
{
	std::stringstream ss;
	ss << "Sample buffer { ";
	if( IsEmpty( ) )
		ss << "emtpy";
	else
		ss << m_iLength << " samples";
	ss << " }";
	return ss.str( );
}

std::string ITASampleBuffer::ValuesToString( ) const
{
	std::stringstream ss;
	ss << "Sample buffer { ";
	for( int i = 0; i < m_iLength; i++ )
		ss << m_pfData[i] << " ";
	ss << "}";
	return ss.str( );
}

void ITASampleBuffer::Load( const std::string& sFilePath )
{
	float fSampleRate;
	Load( sFilePath, fSampleRate );
}

void ITASampleBuffer::Load( const std::string& sFilePath, float& fSampleRate )
{
#ifdef ITABASE_WITH_SNDFILE
	ITAAudiofileReader* pReader( NULL );
	try
	{
		pReader                             = ITAAudiofileReader::create( sFilePath );
		const ITAAudiofileProperties oProps = pReader->getAudiofileProperties( );

		if( oProps.iChannels > 1 )
			ITA_EXCEPT_INVALID_PARAMETER( "Could not load multi channel wav into sample buffer, use sample frame instead." );

		fSampleRate = float( oProps.dSampleRate );
		Init( oProps.iLength, false );

		std::vector<float*> vpfDest( 1 );
		vpfDest[0] = GetData( );
		pReader->read( oProps.iLength, vpfDest );
	}
	catch( ... )
	{
		delete pReader;
		throw;
	}

	delete pReader;
#else
	ITA_EXCEPT1( NOT_IMPLEMENTED, "ITASampleBuffer::Store() function not available without libsndfile" );
#endif
}

void ITASampleBuffer::Store( const std::string& sFilePath, const float fSampleRate /*= 44100.0f */ ) const
{
#ifdef ITABASE_WITH_SNDFILE
	ITAAudiofileWriter* pWriter( NULL );
	try
	{
		ITAAudiofileProperties oProps;
		oProps.dSampleRate   = double( fSampleRate );
		oProps.eDomain       = ITADomain::ITA_TIME_DOMAIN;
		oProps.eQuantization = ITAQuantization::ITA_FLOAT;
		oProps.iChannels     = 1;
		oProps.iLength       = (unsigned int)GetLength( );
		pWriter              = ITAAudiofileWriter::create( sFilePath, oProps );

		std::vector<const float*> vpfDest( 1 );
		vpfDest[0] = GetData( );
		pWriter->write( oProps.iLength, vpfDest );
	}
	catch( ... )
	{
		delete pWriter;
		throw;
	}

	delete pWriter;
#else
	ITA_EXCEPT1( NOT_IMPLEMENTED, "ITASampleBuffer::Store() function not available without libsndfile" );
#endif
}
