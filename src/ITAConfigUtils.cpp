#include <ITAConfigUtils.h>
#include <ITAException.h>
#include <ITAFileSystemUtils.h>
#include <ITAStringUtils.h>
#include <cstdlib>

std::string sCurrentINIFile = "";
std::string sCurrentSection = "";

bool INIFileExists( const std::string& sINIFilename )
{
	// Nur pr�fen ob die Datei existiert
	return doesFileExist( sINIFilename );
}

bool INIFileSectionExists( const std::string& sINIFilename, const std::string& sSection )
{
	std::vector<std::string> vsSects = INIFileGetSections( sINIFilename );
	std::string s                    = toUppercase( sSection );
	for( unsigned int i = 0; i < vsSects.size( ); i++ )
		if( toUppercase( vsSects[i] ).compare( s ) == 0 )
			return true;
	return false;
}

bool INIFileKeyExists( const std::string& sINIFilename, const std::string& sSection, const std::string& sKey )
{
	std::vector<std::string> vsKeys = INIFileGetKeys( sINIFilename, sSection );
	std::string s                   = toUppercase( sKey );
	for( unsigned int i = 0; i < vsKeys.size( ); i++ )
		if( toUppercase( vsKeys[i] ).compare( s ) == 0 )
			return true;
	return false;
}

void INIFileUseFile( const std::string& sINIFilename )
{
	// Sicherstellen das die INI-Datei existiert
	// [fwe 2008-11-07] Bugfix, baby! Das geht beim reinen Schreiben nicht: INIFileRequireINIFile(sINIFilename);

	sCurrentINIFile = sINIFilename;
}

void INIFileUseSection( const std::string& sSection )
{
	// Sicherstellen das die Sektion existiert
	// [fwe 2008-11-07] Bugfix, baby! Das geht beim reinen Schreiben nicht: INIFileRequireSection(sCurrentINIFile, sSection);

	sCurrentSection = sSection;
}

bool INIFileUseSectionIfExists( const std::string& sSection )
{
	if( INIFileSectionExists( sSection ) )
	{
		INIFileUseSection( sSection );
		return true;
	}
	return false;
}


void INIFileUseNothing( )
{
	sCurrentINIFile = "";
	sCurrentSection = "";
}

/* +-----------------------------------------------------------------------------------------+
 * |                                                                                         |
 * |  Assertion-Funktionen: Lösen Ausnahmen aus, falls Bedingungen nicht erfüllt sind.       |
 * |                                                                                         |
 * +-----------------------------------------------------------------------------------------+ */

void INIFileRequireINIFile( const std::string& sINIFilename )
{
	if( sINIFilename.size( ) == 0 )
		ITA_EXCEPT1( INVALID_PARAMETER, "INI file name can not be an empty string" );
	if( !doesPathExist( sINIFilename ) )
		ITA_EXCEPT1( FILE_NOT_FOUND, std::string( "INI file \"" ) + sINIFilename + std::string( "\" not found" ) );
	if( !isFile( sINIFilename ) )
		ITA_EXCEPT1( FILE_NOT_FOUND, std::string( "The path \"" ) + sINIFilename + std::string( "\" does not point to a file" ) );
}

void INIFileRequireSection( const std::string& sINIFilename, const std::string& sSection )
{
	if( !INIFileSectionExists( sINIFilename, sSection ) )
		ITA_EXCEPT1( IO_ERROR, std::string( "INI file \"" ) + sINIFilename + std::string( "\" is missing a [" ) + sSection + std::string( "] section" ) );
}

void INIFileRequireKey( const std::string& sINIFilename, const std::string& sSection, const std::string& sKey )
{
	INIFileRequireSection( sINIFilename, sSection );
	if( !INIFileKeyExists( sINIFilename, sSection, sKey ) )
		ITA_EXCEPT1( IO_ERROR, std::string( "In INI file \"" ) + sINIFilename + std::string( "\" section [" ) + sSection + std::string( "]: Missing a \"" ) + sKey +
		                           std::string( "\" key" ) );
}

void INIFileRequireNonemptyKey( const std::string& sINIFilename, const std::string& sSection, const std::string& sKey )
{
	// Abgebildet �ber die explizite String-Lesefunktion (Ergebnis wegschmeissen)
	INIFileReadStringExplicit( sINIFilename, sSection, sKey );
}

/* +----------------------------------------------------------------------------------------------+
 * |                                                                                              |
 * |  Implizite Lese-Funktionen                                                                   |
 * |                                                                                              |
 * +----------------------------------------------------------------------------------------------+ */

int INIFileReadInt( const std::string& sINIFilename, const std::string& sSection, const std::string& sKey, int iDefaultValue )
{
	std::string s = INIFileReadString( sINIFilename, sSection, sKey );
	return ( s.empty( ) ? iDefaultValue : StringToInt( s ) );
}

unsigned int INIFileReadUInt( const std::string& sINIFilename, const std::string& sSection, const std::string& sKey, unsigned int uiDefaultValue )
{
	std::string s = INIFileReadString( sINIFilename, sSection, sKey );
	return ( s.empty( ) ? uiDefaultValue : StringToUInt( s ) );
}

long INIFileReadLong( const std::string& sINIFilename, const std::string& sSection, const std::string& sKey, long lDefaultValue )
{
	std::string s = INIFileReadString( sINIFilename, sSection, sKey );
	return ( s.empty( ) ? lDefaultValue : (long)StringToInt( s ) );
}

float INIFileReadFloat( const std::string& sINIFilename, const std::string& sSection, const std::string& sKey, float fDefaultValue )
{
	std::string s = INIFileReadString( sINIFilename, sSection, sKey );
	return ( s.empty( ) ? fDefaultValue : StringToFloat( s ) );
}

double INIFileReadDouble( const std::string& sINIFilename, const std::string& sSection, const std::string& sKey, double dDefaultValue )
{
	std::string s = INIFileReadString( sINIFilename, sSection, sKey );
	return ( s.empty( ) ? dDefaultValue : StringToFloat( s ) );
}

bool INIFileReadBool( const std::string& sINIFilename, const std::string& sSection, const std::string& sKey, bool bDefaultValue )
{
	std::string s = toUppercase( INIFileReadString( sINIFilename, sSection, sKey, ( bDefaultValue ? "yes" : "no" ) ) );

	int i = -1;

	if( s == "TRUE" )
		i = 1;
	if( s == "YES" )
		i = 1;
	if( s == "ENABLED" )
		i = 1;
	if( s == "JA" )
		i = 1;
	if( s == "1" )
		i = 1;
	if( s == "JO" )
		i = 1;
	if( s == "SIGGI" )
		i = 1;
	if( s == "I LIKE" )
		i = 1;
	if( s == "MAG ICH" )
		i = 1;
	if( s == "FREILICH" )
		i = 1;
	if( s == "KLAR" )
		i = 1;
	if( s == "JAWOHL" )
		i = 1;
	if( s == "SELBSTVERSTÄNDLICH" )
		i = 1;
	if( s == "TOP" )
		i = 1;

	if( s == "FALSE" )
		i = 0;
	if( s == "NO" )
		i = 0;
	if( s == "DISABLED" )
		i = 0;
	if( s == "NEIN" )
		i = 0;
	if( s == "0" )
		i = 0;
	if( s == "NÖ" )
		i = 0;
	if( s == "NOPE" )
		i = 0;
	if( s == "I DONT LIKE" )
		i = 0;
	if( s == "MAG ICH NICHT" )
		i = 0;
	if( s == "LASS DAS" )
		i = 0;
	if( s == "VETO" )
		i = 1;

	if( i == -1 )
		ITA_EXCEPT1( IO_ERROR, std::string( "In INI file \"" ) + sINIFilename + std::string( "\" section [" ) + sSection + std::string( "] key \"" ) + sKey +
		                           std::string( "\": Invalid value (\"" + s + "\") for a boolean key" ) );

	return ( i == 1 );
}

std::vector<int> INIFileReadIntList( const std::string& sINIFilename, const std::string& sSection, const std::string& sKey, char cSeparator )
{
	std::vector<std::string> vin = INIFileReadStringList( sINIFilename, sSection, sKey, cSeparator );
	std::vector<int> vout;
	for( std::vector<std::string>::const_iterator cit = vin.begin( ); cit != vin.end( ); cit++ )
		vout.push_back( atoi( ( *cit ).c_str( ) ) );
	return vout;
}

std::vector<unsigned int> INIFileReadUIntList( const std::string& sINIFilename, const std::string& sSection, const std::string& sKey, char cSeparator )
{
	std::vector<std::string> vin = INIFileReadStringList( sINIFilename, sSection, sKey, cSeparator );
	std::vector<unsigned int> vout;
	for( std::vector<std::string>::const_iterator cit = vin.begin( ); cit != vin.end( ); cit++ )
	{
		int i = atoi( ( *cit ).c_str( ) );
		vout.push_back( ( i >= 0 ? (unsigned int)i : 0 ) );
	}
	return vout;
}

std::vector<float> INIFileReadFloatList( const std::string& sINIFilename, const std::string& sSection, const std::string& sKey, char cSeparator )
{
	std::vector<std::string> vin = INIFileReadStringList( sINIFilename, sSection, sKey, cSeparator );
	std::vector<float> vout;
	for( std::vector<std::string>::const_iterator cit = vin.begin( ); cit != vin.end( ); cit++ )
		vout.push_back( (float)atof( ( *cit ).c_str( ) ) );
	return vout;
}

std::vector<double> INIFileReadDoubleList( const std::string& sINIFilename, const std::string& sSection, const std::string& sKey, char cSeparator )
{
	std::vector<std::string> vin = INIFileReadStringList( sINIFilename, sSection, sKey, cSeparator );
	std::vector<double> vout;
	for( std::vector<std::string>::const_iterator cit = vin.begin( ); cit != vin.end( ); cit++ )
		vout.push_back( atof( ( *cit ).c_str( ) ) );
	return vout;
}

std::vector<std::string> INIFileReadStringList( const std::string& sINIFilename, const std::string& sSection, const std::string& sKey, char cSeparator )
{
	std::string s = INIFileReadString( sINIFilename, sSection, sKey );
	std::vector<std::string> v;

	int a = 0;
	int b = (int)s.find( cSeparator, 0 );

	// Kein Separator gefunden?
	if( b == (int)s.npos )
	{
		// Falls der String nicht leer ist, kommt er als einziges Element in den Vektor
		std::string t = stripSpaces( s );
		if( !t.empty( ) )
			v.push_back( t );
		return v;
	}

	// Mindestens ein Separator enthalten!
	do
	{
		// Gefundenen Substring von Leerr�umen befreien und speichern
		// Wichtig: Hier wird auch gespeichert, wenn der Substring leer ist
		//          da wir hier zwischen zwei Separatoren sind.
		v.push_back( stripSpaces( s.substr( a, b - a ) ) );
		a = b + 1;
	} while( ( b = (int)s.find( cSeparator, a ) ) != (int)s.npos );

	// Den Rest speichern
	v.push_back( stripSpaces( s.substr( a, s.length( ) - a ) ) );

	return v;
}

/* +-----------------------------------------------------------------------------------------+
 * |                                                                                         |
 * |  Explizite Lese-Funktionen.                                                             |
 * |                                                                                         |
 * +-----------------------------------------------------------------------------------------+ */

int INIFileReadIntExplicit( const std::string& sINIFilename, const std::string& sSection, const std::string& sKey )
{
	std::string s = INIFileReadStringExplicit( sINIFilename, sSection, sKey );
	return StringToInt( s );
}

unsigned int INIFileReadUIntExplicit( const std::string& sINIFilename, const std::string& sSection, const std::string& sKey )
{
	std::string s = INIFileReadStringExplicit( sINIFilename, sSection, sKey );
	return StringToUInt( s );
}

long INIFileReadLongExplicit( const std::string& sINIFilename, const std::string& sSection, const std::string& sKey )
{
	std::string s = INIFileReadStringExplicit( sINIFilename, sSection, sKey );
	return (long)StringToInt( s );
}

float INIFileReadFloatExplicit( const std::string& sINIFilename, const std::string& sSection, const std::string& sKey )
{
	std::string s = INIFileReadStringExplicit( sINIFilename, sSection, sKey );
	return StringToFloat( s );
}

double INIFileReadDoubleExplicit( const std::string& sINIFilename, const std::string& sSection, const std::string& sKey )
{
	std::string s = INIFileReadStringExplicit( sINIFilename, sSection, sKey );
	return StringToDouble( s );
}

bool INIFileReadBoolExplicit( const std::string& sINIFilename, const std::string& sSection, const std::string& sKey )
{
	std::string s = toUppercase( INIFileReadStringExplicit( sINIFilename, sSection, sKey ) );

	int i = -1;

	if( s == "TRUE" )
		i = 1;
	if( s == "YES" )
		i = 1;
	if( s == "JA" )
		i = 1;
	if( s == "1" )
		i = 1;

	if( s == "FALSE" )
		i = 0;
	if( s == "NO" )
		i = 0;
	if( s == "NEIN" )
		i = 0;
	if( s == "0" )
		i = 0;

	if( i == -1 )
		ITA_EXCEPT1( IO_ERROR, std::string( "In INI file \"" ) + sINIFilename + std::string( "\" section [" ) + sSection + std::string( "] key \"" ) + sKey +
		                           std::string( "\": Invalid value (\"" + s + "\") for a boolean key" ) );

	return ( i == 1 );
}

std::string INIFileReadStringExplicit( const std::string& sINIFilename, const std::string& sSection, const std::string& sKey )
{
	INIFileRequireKey( sINIFilename, sSection, sKey );
	std::string s = stripSpaces( INIFileReadString( sINIFilename, sSection, sKey ) );
	if( s.empty( ) )
		ITA_EXCEPT1( IO_ERROR, std::string( "In INI file \"" ) + sINIFilename + std::string( "\" section [" ) + sSection + std::string( "]: The key \"" ) + sKey +
		                           std::string( "\" must have a value" ) );
	return s;
}

std::vector<int> INIFileReadIntListExplicit( const std::string& sINIFilename, const std::string& sSection, const std::string& sKey, char cSeparator )
{
	std::vector<std::string> vin = INIFileReadStringListExplicit( sINIFilename, sSection, sKey, cSeparator );
	std::vector<int> vout;
	for( std::vector<std::string>::const_iterator cit = vin.begin( ); cit != vin.end( ); cit++ )
		vout.push_back( atoi( ( *cit ).c_str( ) ) );
	return vout;
}

std::vector<unsigned int> INIFileReadUIntListExplicit( const std::string& sINIFilename, const std::string& sSection, const std::string& sKey, char cSeparator )
{
	std::vector<std::string> vin = INIFileReadStringListExplicit( sINIFilename, sSection, sKey, cSeparator );
	std::vector<unsigned int> vout;
	for( std::vector<std::string>::const_iterator cit = vin.begin( ); cit != vin.end( ); cit++ )
	{
		int i = atoi( ( *cit ).c_str( ) );
		vout.push_back( ( i >= 0 ? (unsigned int)i : 0 ) );
	}
	return vout;
}

std::vector<float> INIFileReadFloatListExplicit( const std::string& sINIFilename, const std::string& sSection, const std::string& sKey, char cSeparator )
{
	std::vector<std::string> vin = INIFileReadStringListExplicit( sINIFilename, sSection, sKey, cSeparator );
	std::vector<float> vout;
	for( std::vector<std::string>::const_iterator cit = vin.begin( ); cit != vin.end( ); cit++ )
		vout.push_back( (float)atof( ( *cit ).c_str( ) ) );
	return vout;
}

std::vector<double> INIFileReadDoubleListExplicit( const std::string& sINIFilename, const std::string& sSection, const std::string& sKey, char cSeparator )
{
	std::vector<std::string> vin = INIFileReadStringListExplicit( sINIFilename, sSection, sKey, cSeparator );
	std::vector<double> vout;
	for( std::vector<std::string>::const_iterator cit = vin.begin( ); cit != vin.end( ); cit++ )
		vout.push_back( atof( ( *cit ).c_str( ) ) );
	return vout;
}

std::vector<std::string> INIFileReadStringListExplicit( const std::string& sINIFilename, const std::string& sSection, const std::string& sKey, char cSeparator )
{
	std::string s = INIFileReadString( sINIFilename, sSection, sKey );
	std::vector<std::string> v;

	int a = 0;
	int b = (int)s.find( cSeparator, 0 );

	// Kein Separator gefunden?
	if( b == (int)s.npos )
	{
		// Falls der String nicht leer ist, kommt er als einziges Element in den Vektor
		std::string t = stripSpaces( s );
		if( !t.empty( ) )
			v.push_back( t );
		return v;
	}

	// Mindestens ein Separator enthalten!
	do
	{
		// Gefundenen Substring von Leerr�umen befreien und speichern
		// Wichtig: Hier wird auch gespeichert, wenn der Substring leer ist
		//          da wir hier zwischen zwei Separatoren sind.
		v.push_back( stripSpaces( s.substr( a, b - a ) ) );
		a = b + 1;
	} while( ( b = (int)s.find( cSeparator, a ) ) != (int)s.npos );

	// Den Rest speichern
	v.push_back( stripSpaces( s.substr( a, s.length( ) - a ) ) );

	// Nicht-leere Liste erforderlich
	if( v.empty( ) )
		ITA_EXCEPT1( IO_ERROR, std::string( "In INI file \"" ) + sINIFilename + std::string( "\" section [" ) + sSection + std::string( "]: The value of key \"" ) +
		                           sKey + std::string( "\" must be a non-empty list" ) );

	return v;
}


/* +-----------------------------------------------------------------------------------------+
 * |                                                                                         |
 * |  Schreibfunktionen                                                                      |
 * |                                                                                         |
 * |  - Existiert eine INI-Datei nicht, so wird sie angelegt.                                | | |  - Existiert eine Sektion nicht, so wird sie angelegt. | | |  -
 * Existiert ein Schl�ssel bereits, so wird sein Wert �berschrieben                     | | |
 * +-----------------------------------------------------------------------------------------+ */

bool INIFileWriteInt( const std::string& sINIFilename, const std::string& sSection, const std::string& sKey, int iValue )
{
	return INIFileWriteString( sINIFilename, sSection, sKey, IntToString( iValue ) );
}

bool INIFileWriteUInt( const std::string& sINIFilename, const std::string& sSection, const std::string& sKey, unsigned int uiValue )
{
	return INIFileWriteString( sINIFilename, sSection, sKey, UIntToString( uiValue ) );
}

bool INIFileWriteLong( const std::string& sINIFilename, const std::string& sSection, const std::string& sKey, long lValue )
{
	return INIFileWriteString( sINIFilename, sSection, sKey, LongToString( lValue ) );
}

bool INIFileWriteFloat( const std::string& sINIFilename, const std::string& sSection, const std::string& sKey, float fValue, int iDecimalPlaces )
{
	return INIFileWriteString( sINIFilename, sSection, sKey, FloatToString( fValue, iDecimalPlaces ) );
}

bool INIFileWriteDouble( const std::string& sINIFilename, const std::string& sSection, const std::string& sKey, double dValue, int iDecimalPlaces )
{
	return INIFileWriteString( sINIFilename, sSection, sKey, DoubleToString( dValue, iDecimalPlaces ) );
}

bool INIFileWriteBool( const std::string& sINIFilename, const std::string& sSection, const std::string& sKey, bool bValue )
{
	return INIFileWriteString( sINIFilename, sSection, sKey, ( bValue ? "1" : "0" ) );
}

bool INIFileWriteIntList( const std::string& sINIFilename, const std::string& sSection, const std::string& sKey, const std::vector<int>& viValues,
                          std::string sSeparator )
{
	return INIFileWriteString( sINIFilename, sSection, sKey, IntVecToString( viValues, sSeparator ) );
}

bool INIFileWriteUIntList( const std::string& sINIFilename, const std::string& sSection, const std::string& sKey, const std::vector<unsigned int>& vuiValues,
                           std::string sSeparator )
{
	return INIFileWriteString( sINIFilename, sSection, sKey, UIntVecToString( vuiValues, sSeparator ) );
}

bool INIFileWriteFloatList( const std::string& sINIFilename, const std::string& sSection, const std::string& sKey, const std::vector<float>& vfValues,
                            std::string sSeparator )
{
	return INIFileWriteString( sINIFilename, sSection, sKey, FloatVecToString( vfValues, -1, sSeparator ) );
}

bool INIFileWriteDoubleList( const std::string& sINIFilename, const std::string& sSection, const std::string& sKey, const std::vector<double>& vdValues,
                             std::string sSeparator )
{
	return INIFileWriteString( sINIFilename, sSection, sKey, DoubleVecToString( vdValues, -1, sSeparator ) );
}

bool INIFileWriteStringList( const std::string& sINIFilename, const std::string& sSection, const std::string& sKey, const std::vector<std::string>& vsValues,
                             std::string sSeparator )
{
	return INIFileWriteString( sINIFilename, sSection, sKey, StringVecToString( vsValues, sSeparator ) );
}

/* +-----------------------------------------------------------------------------------------+
 * |                                                                                         |
 * |  Alias-Funktionen f�r vorausgew�hlte INI-Dateien und Sektionen                          |
 * |                                                                                         |
 * +-----------------------------------------------------------------------------------------+ */

bool INIFileSectionExists( const std::string& sSection )
{
	return INIFileSectionExists( sCurrentINIFile, sSection );
}

std::vector<std::string> INIFileGetSections( )
{
	return INIFileGetSections( sCurrentINIFile );
}

bool INIFileKeyExists( const std::string& sKey )
{
	return INIFileKeyExists( sCurrentINIFile, sCurrentSection, sKey );
}

std::vector<std::string> INIFileGetKeys( const std::string& sSection )
{
	return INIFileGetKeys( sCurrentINIFile, sSection );
}

void INIFileRequireSection( const std::string& sSection )
{
	return INIFileRequireSection( sCurrentINIFile, sSection );
}

void INIFileRequireKey( const std::string& sKey )
{
	return INIFileRequireKey( sCurrentINIFile, sCurrentSection, sKey );
}

void INIFileRequireNonemptyKey( const std::string& sKey )
{
	return INIFileRequireNonemptyKey( sCurrentINIFile, sCurrentSection, sKey );
}

int INIFileReadInt( const std::string& sKey, int iDefaultValue )
{
	return INIFileReadInt( sCurrentINIFile, sCurrentSection, sKey, iDefaultValue );
}

unsigned int INIFileReadUInt( const std::string& sKey, unsigned int uiDefaultValue )
{
	return INIFileReadUInt( sCurrentINIFile, sCurrentSection, sKey, uiDefaultValue );
}

long INIFileReadLong( const std::string& sKey, long lDefaultValue )
{
	return INIFileReadLong( sCurrentINIFile, sCurrentSection, sKey, lDefaultValue );
}

float INIFileReadFloat( const std::string& sKey, float fDefaultValue )
{
	return INIFileReadFloat( sCurrentINIFile, sCurrentSection, sKey, fDefaultValue );
}

double INIFileReadDouble( const std::string& sKey, double dDefaultValue )
{
	return INIFileReadDouble( sCurrentINIFile, sCurrentSection, sKey, dDefaultValue );
}

bool INIFileReadBool( const std::string& sKey, bool bDefaultValue )
{
	return INIFileReadBool( sCurrentINIFile, sCurrentSection, sKey, bDefaultValue );
}

std::string INIFileReadString( const std::string& sKey, std::string sDefaultValue )
{
	return INIFileReadString( sCurrentINIFile, sCurrentSection, sKey, sDefaultValue );
}

std::vector<int> INIFileReadIntList( const std::string& sKey, char cSeparator )
{
	return INIFileReadIntList( sCurrentINIFile, sCurrentSection, sKey, cSeparator );
}

std::vector<unsigned int> INIFileReadUIntList( const std::string& sKey, char cSeparator )
{
	return INIFileReadUIntList( sCurrentINIFile, sCurrentSection, sKey, cSeparator );
}

std::vector<float> INIFileReadFloatList( const std::string& sKey, char cSeparator )
{
	return INIFileReadFloatList( sCurrentINIFile, sCurrentSection, sKey, cSeparator );
}

std::vector<double> INIFileReadDoubleList( const std::string& sKey, char cSeparator )
{
	return INIFileReadDoubleList( sCurrentINIFile, sCurrentSection, sKey, cSeparator );
}

std::vector<std::string> INIFileReadStringList( const std::string& sKey, char cSeparator )
{
	return INIFileReadStringList( sCurrentINIFile, sCurrentSection, sKey, cSeparator );
}

int INIFileReadIntExplicit( const std::string& sKey )
{
	return INIFileReadIntExplicit( sCurrentINIFile, sCurrentSection, sKey );
}

unsigned int INIFileReadUIntExplicit( const std::string& sKey )
{
	return INIFileReadUIntExplicit( sCurrentINIFile, sCurrentSection, sKey );
}

long INIFileReadLongExplicit( const std::string& sKey )
{
	return INIFileReadLongExplicit( sCurrentINIFile, sCurrentSection, sKey );
}

float INIFileReadFloatExplicit( const std::string& sKey )
{
	return INIFileReadFloatExplicit( sCurrentINIFile, sCurrentSection, sKey );
}

double INIFileReadDoubleExplicit( const std::string& sKey )
{
	return INIFileReadDoubleExplicit( sCurrentINIFile, sCurrentSection, sKey );
}

bool INIFileReadBoolExplicit( const std::string& sKey )
{
	return INIFileReadBoolExplicit( sCurrentINIFile, sCurrentSection, sKey );
}

std::string INIFileReadStringExplicit( const std::string& sKey )
{
	return INIFileReadStringExplicit( sCurrentINIFile, sCurrentSection, sKey );
}

std::vector<int> INIFileReadIntListExplicit( const std::string& sKey, char cSeparator )
{
	return INIFileReadIntListExplicit( sCurrentINIFile, sCurrentSection, sKey, cSeparator );
}

std::vector<unsigned int> INIFileReadUIntListExplicit( const std::string& sKey, char cSeparator )
{
	return INIFileReadUIntListExplicit( sCurrentINIFile, sCurrentSection, sKey, cSeparator );
}

std::vector<float> INIFileReadFloatListExplicit( const std::string& sKey, char cSeparator )
{
	return INIFileReadFloatListExplicit( sCurrentINIFile, sCurrentSection, sKey, cSeparator );
}

std::vector<double> INIFileReadDoubleListExplicit( const std::string& sKey, char cSeparator )
{
	return INIFileReadDoubleListExplicit( sCurrentINIFile, sCurrentSection, sKey, cSeparator );
}

std::vector<std::string> INIFileReadStringListExplicit( const std::string& sKey, char cSeparator )
{
	return INIFileReadStringListExplicit( sCurrentINIFile, sCurrentSection, sKey, cSeparator );
}

bool INIFileWriteInt( const std::string& sKey, int iValue )
{
	return INIFileWriteInt( sCurrentINIFile, sCurrentSection, sKey, iValue );
}

bool INIFileWriteUInt( const std::string& sKey, unsigned int uiValue )
{
	return INIFileWriteUInt( sCurrentINIFile, sCurrentSection, sKey, uiValue );
}

bool INIFileWriteLong( const std::string& sKey, long lValue )
{
	return INIFileWriteLong( sCurrentINIFile, sCurrentSection, sKey, lValue );
}

bool INIFileWriteFloat( const std::string& sKey, float fValue, int iDecimalPlaces )
{
	return INIFileWriteFloat( sCurrentINIFile, sCurrentSection, sKey, fValue, iDecimalPlaces );
}

bool INIFileWriteDouble( const std::string& sKey, double dValue, int iDecimalPlaces )
{
	return INIFileWriteDouble( sCurrentINIFile, sCurrentSection, sKey, dValue, iDecimalPlaces );
}

bool INIFileWriteBool( const std::string& sKey, bool bValue )
{
	return INIFileWriteBool( sCurrentINIFile, sCurrentSection, sKey, bValue );
}

bool INIFileWriteString( const std::string& sKey, const std::string& sValue )
{
	return INIFileWriteString( sCurrentINIFile, sCurrentSection, sKey, sValue );
}

bool INIFileWriteIntList( const std::string& sKey, const std::vector<int>& viValues, std::string sSeparator )
{
	return INIFileWriteIntList( sCurrentINIFile, sCurrentSection, sKey, viValues, sSeparator );
}

bool INIFileWriteUIntList( const std::string& sKey, const std::vector<unsigned int>& vuiValues, std::string sSeparator )
{
	return INIFileWriteUIntList( sCurrentINIFile, sCurrentSection, sKey, vuiValues, sSeparator );
}

bool INIFileWriteFloatList( const std::string& sKey, const std::vector<float>& vfValues, std::string sSeparator )
{
	return INIFileWriteFloatList( sCurrentINIFile, sCurrentSection, sKey, vfValues, sSeparator );
}

bool INIFileWriteDoubleList( const std::string& sKey, const std::vector<double>& vdValues, std::string sSeparator )
{
	return INIFileWriteDoubleList( sCurrentINIFile, sCurrentSection, sKey, vdValues, sSeparator );
}

bool INIFileWriteStringList( const std::string& sKey, const std::vector<std::string>& vsValues, std::string sSeparator )
{
	return INIFileWriteStringList( sCurrentINIFile, sCurrentSection, sKey, vsValues, sSeparator );
}
