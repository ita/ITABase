#include <ITAException.h>
#include <ITAFastMath.h>
#include <ITAHDFTSpectrum.h>
#include <ITANumericUtils.h>
#include <ITASampleBuffer.h>
#include <ITAStringUtils.h>
#include <cmath>
#include <stdio.h>

using namespace ITABase;


CHDFTSpectrum::CHDFTSpectrum( ) : m_iSize( 0 ), m_iDFTSize( 0 ), m_fSampleRate( 0 ) {}

CHDFTSpectrum::CHDFTSpectrum( const float fSampleRate, const int iDFTSize, const bool bZeroInit ) : m_iSize( -1 ), m_iDFTSize( iDFTSize ), m_fSampleRate( fSampleRate )
{
	SetSampleRate( fSampleRate );
	Init( iDFTSize, bZeroInit ); // sets size and resolution
}

CHDFTSpectrum::CHDFTSpectrum( const CHDFTSpectrum* pSource ) : m_iSize( 0 ), m_iDFTSize( 0 ), m_fSampleRate( 0 )
{
	Init( pSource->GetDFTSize( ) );
	SetSampleRate( pSource->GetSampleRate( ) );
	CopyFrom( *pSource );
}

CHDFTSpectrum::CHDFTSpectrum( const CHDFTSpectrum& oSource ) : m_iSize( 0 ), m_iDFTSize( 0 ), m_fSampleRate( 0 )
{
	Init( oSource.GetDFTSize( ) );
	SetSampleRate( oSource.GetSampleRate( ) );
	CopyFrom( oSource );
}

CHDFTSpectrum::~CHDFTSpectrum( ) {}

void CHDFTSpectrum::Init( const int iDFTSize, const bool bZeroinit )
{
	m_iDFTSize = iDFTSize;
	m_iSize    = (int)floor( (float)m_iDFTSize / 2.0 ) + 1;

	// +2 wegen 4-Byte Ausrichtung @jst todo
	if( m_iSize > 0 )
		m_vfData.resize( m_iSize * 2 + 2 );

	if( bZeroinit )
		for( size_t i = 0; i < m_vfData.size( ); i++ )
			m_vfData[i] = 0.0f;
}

int CHDFTSpectrum::GetSize( ) const
{
	return m_iSize;
}

int CHDFTSpectrum::GetDFTSize( ) const
{
	return m_iDFTSize;
}

float CHDFTSpectrum::GetSampleRate( ) const
{
	return m_fSampleRate;
}

void CHDFTSpectrum::SetSampleRate( const float fSampleRate )
{
	m_fSampleRate = fSampleRate;
}

float CHDFTSpectrum::GetFrequencyResolution( ) const
{
	if( m_iDFTSize == 0 || m_fSampleRate <= 0.0f )
		ITA_EXCEPT_INVALID_PARAMETER( "DFT size or sampling rate invalid" );

	return ( m_iDFTSize == 0 ? 0 : ( m_fSampleRate / (float)m_iDFTSize ) );
}

float* CHDFTSpectrum::GetData( ) const
{
	return &m_vfData[0];
}

void CHDFTSpectrum::SetUnity( )
{
	for( int i = 0; i < m_iSize; i++ )
	{
		int iRe       = 2 * i;
		int iIm       = iRe + 1;
		m_vfData[iRe] = 1.0f;
		m_vfData[iIm] = 0.0f;
	}

	return;
}

void CHDFTSpectrum::SetZero( )
{
	for( int i = 0; i < m_iSize; i++ )
	{
		int iRe       = 2 * i;
		int iIm       = iRe + 1;
		m_vfData[iRe] = 0.0f;
		m_vfData[iIm] = 0.0f;
	}

	return;
}

void CHDFTSpectrum::Add( const float fReal, const float fImag )
{
	for( int i = 0; i < 2 * m_iSize; i += 2 )
	{
		m_vfData[i] += fReal;
		m_vfData[i + 1] += fImag;
	}
}

void CHDFTSpectrum::SetCoeffRI( const int iIndex, const float fReal, const float fImag )
{
	SetCoeffsRI( iIndex, 1, fReal, fImag );
}

void CHDFTSpectrum::SetCoeffsRI( const float fReal, const float fImag )
{
	SetCoeffsRI( 0, m_iSize, fReal, fImag );
}

void CHDFTSpectrum::SetCoeffsRI( const int iOffset, const int iCount, const float fReal, const float fImag )
{
	if( ( iOffset < 0 ) || ( iOffset >= m_iSize ) || ( iCount < 0 ) || ( iOffset + iCount > m_iSize ) )
		ITA_EXCEPT1( INVALID_PARAMETER, "Index/count out of bounds" );

	for( int i = iOffset; i < iOffset + iCount; i++ )
	{
		m_vfData[2 * i]     = fReal;
		m_vfData[2 * i + 1] = fImag;
	}
}

void CHDFTSpectrum::SetCoeffMP( const int iIndex, const float fMagnitude, const float fPhase )
{
	SetCoeffsMP( iIndex, 1, fMagnitude, fPhase );
}

void CHDFTSpectrum::SetCoeffsMP( const float fMagnitude, const float fPhase )
{
	SetCoeffsMP( 0, m_iSize, fMagnitude, fPhase );
}

void CHDFTSpectrum::SetCoeffsMP( const int iOffset, const int iCount, const float fMagnitude, const float fPhase )
{
	if( ( iOffset < 0 ) || ( iOffset >= m_iSize ) || ( iCount < 0 ) || ( iOffset + iCount > m_iSize ) )
		ITA_EXCEPT1( INVALID_PARAMETER, "Index/count out of bounds" );

	SetCoeffsRI( fMagnitude * cos( fPhase ), fMagnitude * sin( fPhase ) );
}

void CHDFTSpectrum::SetMagnitudePreservePhase( const int iIndex, const float fMagnitude )
{
	SetMagnitudesPreservePhases( iIndex, 1, fMagnitude );
}

void CHDFTSpectrum::SetMagnitudesPreservePhases( const float fMagnitude )
{
	SetMagnitudesPreservePhases( 0, m_iSize, fMagnitude );
}

void CHDFTSpectrum::SetMagnitudesPreservePhases( const int iOffset, const int iCount, const float fMagnitude )
{
	if( ( iOffset < 0 ) || ( iOffset >= m_iSize ) || ( iCount < 0 ) || ( iOffset + iCount > m_iSize ) )
		ITA_EXCEPT1( INVALID_PARAMETER, "Index/count out of bounds" );

	for( int i = iOffset; i < iOffset + iCount; i++ )
		csabsparg( m_vfData[2 * i], m_vfData[2 * i + 1], fMagnitude, m_vfData[2 * i], m_vfData[2 * i + 1] );
}

void CHDFTSpectrum::SetPhasePreserveMagnitude( const int iIndex, const float fPhase )
{
	SetPhasesPreserveMagnitudes( iIndex, 1, fPhase );
}

void CHDFTSpectrum::SetPhasesPreserveMagnitudes( const float fPhase )
{
	SetPhasesPreserveMagnitudes( 0, m_iSize, fPhase );
}

void CHDFTSpectrum::SetPhasesPreserveMagnitudes( const int iOffset, const int iCount, const float fPhase )
{
	if( ( iOffset < 0 ) || ( iOffset >= m_iSize ) || ( iCount < 0 ) || ( iOffset + iCount > m_iSize ) )
		ITA_EXCEPT1( INVALID_PARAMETER, "Index/count out of bounds" );

	for( int i = iOffset; i < iOffset + iCount; i++ )
		csargpabs( m_vfData[2 * i], m_vfData[2 * i + 1], fPhase, m_vfData[2 * i], m_vfData[2 * i + 1] );
}

void CHDFTSpectrum::Sub( const float fReal, const float fImag )
{
	for( int i = 0; i < 2 * m_iSize; i += 2 )
	{
		m_vfData[i] -= fReal;
		m_vfData[i + 1] -= fImag;
	}
}

void CHDFTSpectrum::Add( const CHDFTSpectrum& s )
{
	if( s.GetSize( ) != m_iSize )
		ITA_EXCEPT1( INVALID_PARAMETER, "Spectra must have the same size" );

	fm_add( GetData( ), s.GetData( ), 2 * m_iSize );
}

void CHDFTSpectrum::Add( const CHDFTSpectrum* pSource )
{
	if( pSource->GetSize( ) != m_iSize )
		ITA_EXCEPT1( INVALID_PARAMETER, "Source spectrum must be of same size" );

	fm_add( GetData( ), pSource->GetData( ), 2 * m_iSize );

	return;
}

void CHDFTSpectrum::Sub( const CHDFTSpectrum& s )
{
	if( s.GetSize( ) != m_iSize )
		ITA_EXCEPT1( INVALID_PARAMETER, "Spectra must have the same size" );

	fm_sub( GetData( ), s.GetData( ), 2 * m_iSize );
}

void CHDFTSpectrum::Sub( const CHDFTSpectrum* pSource )
{
	if( pSource->GetSize( ) != m_iSize )
		ITA_EXCEPT1( INVALID_PARAMETER, "Source spectrum must be of same size" );

	fm_sub( GetData( ), pSource->GetData( ), 2 * m_iSize );

	return;
}

void CHDFTSpectrum::Mul( const CHDFTSpectrum& s )
{
	if( s.GetSize( ) != m_iSize )
		ITA_EXCEPT1( INVALID_PARAMETER, "Spectra must have the same size" );

	fm_cmul( GetData( ), s.GetData( ), m_iSize );

	return;
}

void CHDFTSpectrum::Mul( const CHDFTSpectrum* pSource )
{
	if( pSource->GetSize( ) != m_iSize )
		ITA_EXCEPT1( INVALID_PARAMETER, "Spectra must have the same size" );

	const float* fS( pSource->GetData( ) );
	float* fD( GetData( ) );

	float fR1, fR2, fI1, fI2;
	for( int i = 0; i < m_iSize; i++ )
	{
		const int idxRe = 2 * i;
		const int idxIm = idxRe + 1;

		// numerically stable
		fR1 = fD[idxRe] * fS[idxRe];
		fR2 = fD[idxIm] * fS[idxIm];
		fI1 = fD[idxRe] * fS[idxIm];
		fI2 = fS[idxRe] * fD[idxIm];

		// numerically instable, be aware of your data range
		fD[idxRe] = fR1 - fR2;
		fD[idxIm] = fI1 + fI2;
	}

	return;
}

void CHDFTSpectrum::MulConj( const CHDFTSpectrum* pSource )
{
	if( pSource->GetSize( ) != m_iSize )
		ITA_EXCEPT1( INVALID_PARAMETER, "Source spectrum must be of same size" );

	const float* fS( pSource->GetData( ) );
	float* fD( GetData( ) );

	float fR1, fR2, fI1, fI2;
	for( int i = 0; i < m_iSize; i++ )
	{
		const int idxRe = 2 * i;
		const int idxIm = idxRe + 1;

		// numerically stable
		fR1 = fD[idxRe] * fS[idxRe];
		fR2 = fD[idxIm] * fS[idxIm];
		fI1 = fD[idxRe] * fS[idxIm];
		fI2 = fS[idxRe] * fD[idxIm];

		// numerically instable, be aware of your data range
		fD[idxRe] = fR1 + fR2;
		fD[idxIm] = fI2 - fI1;
	}

	return;
}

void CHDFTSpectrum::Mul( const float fFactor )
{
	fm_mul( GetData( ), fFactor, m_iSize * 2 );
	return;
}

void CHDFTSpectrum::Div( const CHDFTSpectrum& s )
{
	if( s.GetSize( ) != m_iSize )
		ITA_EXCEPT1( INVALID_PARAMETER, "Spectra must have the same size" );

	fm_cdiv( GetData( ), s.GetData( ), m_iSize );

	return;
}

void CHDFTSpectrum::Div( const CHDFTSpectrum* pSource )
{
	if( pSource->GetSize( ) != m_iSize )
		ITA_EXCEPT1( INVALID_PARAMETER, "Source spectrum must be of same size" );

	fm_cdiv( GetData( ), pSource->GetData( ), m_iSize );

	return;
}

float CHDFTSpectrum::CalcMagnitude( const int iIndex ) const
{
	if( iIndex >= m_iDFTSize )
		ITA_EXCEPT1( INVALID_PARAMETER, "Index out of range" );

	return cabsf( m_vfData[2 * iIndex], m_vfData[2 * iIndex + 1] );
}

void CHDFTSpectrum::CalcMagnitudes( float* pfMagnitudes ) const
{
	if( pfMagnitudes )
		for( int i = 0; i < m_iSize; i++ )
			pfMagnitudes[i] = CalcMagnitude( i );
}

void CHDFTSpectrum::CalcPhases( float* pfPhases )
{
	if( pfPhases )
		for( int i = 0; i < m_iSize; i++ )
			pfPhases[i] = canglef( m_vfData[2 * i], m_vfData[2 * i + 1] );
}

void CHDFTSpectrum::Log( const bool bComplex )
{
	for( int i = 0; i < m_iSize; i++ )
	{
		// TODO: Check for (magnitude < eps, -HUGE_VAL), negative value on real log
		if( bComplex )
		{
			//  z = x+i*y returns log(z) = log(abs(z)) + i*atan2(y,x); cp. http://www.mathworks.de/help/techdoc/ref/log.html
			float x             = m_vfData[2 * i];
			m_vfData[2 * i]     = logf( cabsf( x, m_vfData[2 * i + 1] ) );
			m_vfData[2 * i + 1] = atan2f( m_vfData[2 * i + 1], x );
		}
		else
		{
			m_vfData[2 * i]     = logf( cabsf( m_vfData[2 * i], m_vfData[2 * i + 1] ) );
			m_vfData[2 * i + 1] = 0.0f;
		}
	}
}

void CHDFTSpectrum::Exp( const bool bComplex )
{
	for( int i = 0; i < m_iSize; i++ )
	{
		// TODO: Check for (magnitude > thresh, HUGE_VAL)
		if( bComplex )
		{
			// z = x+i*y returns e^z = e^x * (cos(y) + i*sin(y)); cp. http://www.mathworks.de/help/techdoc/ref/exp.html
			float expMag        = std::exp( m_vfData[2 * i] );
			m_vfData[2 * i]     = expMag * cosf( m_vfData[2 * i + 1] );
			m_vfData[2 * i + 1] = expMag * sinf( m_vfData[2 * i + 1] );
		}
		else
		{
			m_vfData[2 * i]     = std::exp( m_vfData[2 * i] );
			m_vfData[2 * i + 1] = 0.0f;
		}
	}
}

void CHDFTSpectrum::Negate( )
{
	for( int i = 0; i < 2 * m_iSize; i++ )
		m_vfData[i] = -m_vfData[i];
}

void CHDFTSpectrum::Conjugate( )
{
	for( int i = 0; i < m_iSize; i++ )
		m_vfData[2 * i + 1] *= -1;
}

float CHDFTSpectrum::FindMax( ) const
{
	int iDummy;
	return FindMax( iDummy );
}

float CHDFTSpectrum::FindMax( int& iMaxIndex ) const
{
	int i   = 0;
	float x = 0;

	for( int j = 0; j < m_iSize; j++ )
	{
		float y = cabsf( m_vfData[2 * j], m_vfData[2 * j + 1] );
		if( y > x )
		{
			x = y;
			i = j;
		}
	}

	iMaxIndex = i;
	return x;
}

void CHDFTSpectrum::CopyFrom( const CHDFTSpectrum& s )
{
	if( s.GetSize( ) != m_iSize )
		ITA_EXCEPT1( INVALID_PARAMETER, "Spectra must have the same size" );

	fm_copy( GetData( ), s.GetData( ), m_iSize * 2 );
}

void CHDFTSpectrum::CopyFrom( const CHDFTSpectrum& s, const int iOffset, const int iCount )
{
	if( s.GetSize( ) != m_iSize )
		ITA_EXCEPT1( INVALID_PARAMETER, "Spectra must have the same size" );

	// TODO: FastMath? Jetzt gibt es Probleme mit SIMD-Alignment?
	for( int i = iOffset * 2; i < ( iOffset + iCount ) * 2; i++ )
		m_vfData[i] = s.GetData( )[i];
}

CHDFTSpectrum& CHDFTSpectrum::operator=( const CHDFTSpectrum& rhs )
{
	// Selbstzuweisung abfangen
	if( &rhs == this )
		return *this;

	// Neu allozieren
	Init( rhs.GetDFTSize( ), false );
	SetSampleRate( rhs.GetSampleRate( ) );

	// Samples kopieren (+2 für 16 Byte alignment)
	fm_copy( GetData( ), rhs.GetData( ), m_iDFTSize * 2 + 2 );

	return *this;
}

std::string CHDFTSpectrum::ToString( )
{
	std::string s = IntToString( m_iSize ) + " complex coefficients; spectrum = (";
	for( int i = 0; i < 2 * m_iSize; i += 2 )
	{
		if( i > 0 )
			s += ", ";
		s += FloatToString( m_vfData[i], 3 ) + ( m_vfData[i + 1] >= 0 ? "+" : "" ) + FloatToString( m_vfData[i + 1], 3 ) + "i";
	}
	s += ")";
	return s;
}

float CHDFTSpectrum::GetEnergy( ) const
{
	float energy = 0;
	for( int i = 0; i < m_iSize; i++ )
	{
		float mags = cabsf( m_vfData[2 * i], m_vfData[2 * i + 1] );
		energy += mags * mags;
	}
	return energy / ( (float)m_iSize );
}

void CHDFTSpectrum::Copy( const CHDFTSpectrum* pSource )
{
	// TODO: FastMath? Jetzt gibt es Probleme mit SIMD-Alignment?
	for( int i = 0; i < 2 * m_iSize; i++ )
		m_vfData[i] = pSource->GetData( )[i];
}

void CHDFTSpectrum::SetCoeff( const int iIndex, std::complex<float>& cfCoeff )
{
	SetCoeffRI( iIndex, cfCoeff.real( ), cfCoeff.imag( ) );
}

float CHDFTSpectrum::GetFrequencyOfBin( const int iBinIndex ) const
{
	return GetFrequencyResolution( ) * (float)iBinIndex;
}

std::complex<float> CHDFTSpectrum::GetCoeff( const int iIndex ) const
{
	if( iIndex < 0 || 2 * iIndex + 1 >= m_vfData.size( ) )
		ITA_EXCEPT_INVALID_PARAMETER( "Coefficient index out of bounds" );
	return { m_vfData[2 * iIndex], m_vfData[2 * iIndex + 1] };
}
