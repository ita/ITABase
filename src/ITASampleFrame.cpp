#include <ITAAudiofileReader.h>
#include <ITAAudiofileWriter.h>
#include <ITAException.h>
#include <ITASampleFrame.h>
#include <assert.h>
#include <sstream>

ITASampleFrame::ITASampleFrame( ) : m_iChannels( 0 ), m_iLength( 0 ) {}

ITASampleFrame::ITASampleFrame( const int iChannels, const int iLength, const bool bZeroinit /* = true */ ) : m_iChannels( 0 ), m_iLength( 0 )
{
	Init( iChannels, iLength, bZeroinit );
}


ITASampleFrame::ITASampleFrame( const ITASampleFrame* pSource ) : m_iChannels( 0 ), m_iLength( 0 )
{
	*this = *pSource;
}

ITASampleFrame::ITASampleFrame( const ITASampleFrame& sbSource ) : m_iChannels( 0 ), m_iLength( 0 )
{
	*this = sbSource;
}

ITASampleFrame::ITASampleFrame( const std::string& sFilePath )
{
	Load( sFilePath );
}

ITASampleFrame::~ITASampleFrame( ) {}

bool ITASampleFrame::empty( ) const
{
	return ( ( m_iChannels == 0 ) || ( m_iLength == 0 ) );
}

int ITASampleFrame::GetNumChannels( ) const
{
	return m_iChannels;
}

int ITASampleFrame::length( ) const
{
	return m_iLength;
}

void ITASampleFrame::Init( int iChannels, int iLength, bool bZeroinit )
{
	assert( iChannels >= 0 );
	assert( iLength >= 0 );

	// TODO: Mit ein wenig cleverness k�nnte dies ohne Vollst�ndiges Abr�umen gehen.
	//       Die Frage ist: Wer's braucht?
	if( ( m_iChannels > 0 ) || ( m_iLength > 0 ) )
		free( );

	m_vChannels.resize( iChannels );
	for( int i = 0; i < iChannels; i++ )
	{
		m_vChannels[i].m_pParent = NULL;
		m_vChannels[i].Init( iLength, bZeroinit );
		m_vChannels[i].m_pParent = this;
	}

	m_iChannels = iChannels;
	m_iLength   = iLength;
}

void ITASampleFrame::Load( const std::string& sFilePath )
{
	double dSampleRate;
	Load( sFilePath, dSampleRate );
}

void ITASampleFrame::Load( const std::string& sFilePath, double& dSampleRate )
{
#ifdef ITABASE_WITH_SNDFILE
	ITAAudiofileReader* pReader( NULL );
	try
	{
		// Datei �ffnen
		pReader                       = ITAAudiofileReader::create( sFilePath );
		ITAAudiofileProperties oProps = pReader->getAudiofileProperties( );
		dSampleRate                   = oProps.dSampleRate;
		init( (int)oProps.iChannels, (int)oProps.iLength, false );

		// Zielzeiger zusammenstellen
		std::vector<float*> vpfDest( oProps.iChannels );
		for( int i = 0; i < oProps.iChannels; i++ )
			vpfDest[i] = m_vChannels[i].GetData( );

		// Daten einlesen
		pReader->read( oProps.iLength, vpfDest );
	}
	catch( ... )
	{
		// Alle Daten verwerfen
		delete pReader;
		free( );
		throw;
	}

	delete pReader;
#else
	ITA_EXCEPT1( NOT_IMPLEMENTED, "ITASampleFrame::Store() function not available without libsndfile" );
#endif
}

void ITASampleFrame::Store( const std::string& sFilePath, double dSamplingRate ) const
{
#ifdef ITABASE_WITH_SNDFILE
	ITAAudiofileWriter* pWriter( NULL );
	try
	{
		ITAAudiofileProperties oProps;
		oProps.dSampleRate   = dSamplingRate;
		oProps.eDomain       = ITADomain::ITA_TIME_DOMAIN;
		oProps.eQuantization = ITAQuantization::ITA_FLOAT;
		oProps.iChannels     = (unsigned int)m_iChannels;
		oProps.iLength       = (unsigned int)m_iLength;
		pWriter              = ITAAudiofileWriter::create( sFilePath, oProps );

		std::vector<const float*> vpfDest( oProps.iChannels );
		for( int i = 0; i < oProps.iChannels; i++ )
			vpfDest[i] = m_vChannels[i].data( );

		pWriter->write( oProps.iLength, vpfDest );
	}
	catch( ... )
	{
		delete pWriter;
		throw;
	}

	delete pWriter;
#else
	ITA_EXCEPT1( NOT_IMPLEMENTED, "ITASampleFrame::Store() function not available without libsndfile" );
#endif
}

void ITASampleFrame::free( )
{
	m_vChannels.clear( );
	m_iChannels = 0;
	m_iLength   = 0;
};

void ITASampleFrame::fill( float fValue )
{
	for( ch_it it = m_vChannels.begin( ); it != m_vChannels.end( ); ++it )
		it->Fill( fValue );
}

void ITASampleFrame::fill( int iOffset, int iCount, float fValue )
{
	for( ch_it it = m_vChannels.begin( ); it != m_vChannels.end( ); ++it )
		it->Fill( iOffset, iCount, fValue );
}

void ITASampleFrame::zero( )
{
	fill( 0 );
}

void ITASampleFrame::zero( int iOffset, int iCount )
{
	fill( iOffset, iCount, 0.0f );
}

void ITASampleFrame::identity( )
{
	for( ch_it it = m_vChannels.begin( ); it != m_vChannels.end( ); ++it )
		it->Identity( );
}

void ITASampleFrame::fade( int iOffset, int iCount, int iFadeDirection, int iFadeFunction )
{
	for( int i = 0; i < m_iChannels; i++ )
		m_vChannels[i].Fade( iOffset, iCount, iFadeDirection, iFadeFunction );
}

void ITASampleFrame::crossfade( const ITASampleFrame* psfSrc, int iOffset, int iCount, int iFadeDirection, int iFadeFunction )
{
	if( !psfSrc )
		ITA_EXCEPT1( INVALID_PARAMETER, "Nullpointer passed" );
	if( psfSrc->channels( ) != m_iChannels )
		ITA_EXCEPT1( INVALID_PARAMETER, "Number of channels do not match" );

	for( int i = 0; i < m_iChannels; i++ )
		m_vChannels[i].Crossfade( ( *psfSrc )[i], iOffset, iCount, iFadeDirection, iFadeFunction );
}

void ITASampleFrame::crossfade( const ITASampleFrame& sfSrc, int iOffset, int iCount, int iFadeDirection, int iFadeFunction )
{
	crossfade( &sfSrc, iOffset, iCount, iFadeDirection, iFadeFunction );
}

void ITASampleFrame::envelope( float fGain0, float fGain1 )
{
	if( empty( ) )
		return;

	for( int i = 0; i < m_iChannels; i++ )
		m_vChannels[i].Envelope( fGain0, fGain1 );
}

void ITASampleFrame::write( const ITASampleFrame* psfSrc, int iCount, int iSrcOffset, int iDestOffset )
{
	if( !psfSrc )
		ITA_EXCEPT1( INVALID_PARAMETER, "Nullpointer passed" );
	if( psfSrc->channels( ) != m_iChannels )
		ITA_EXCEPT1( INVALID_PARAMETER, "Number of channels do not match" );

	for( int i = 0; i < m_iChannels; i++ )
		m_vChannels[i].write( ( *psfSrc )[i], iCount, iSrcOffset, iDestOffset );
}

void ITASampleFrame::write( const ITASampleFrame& sfSrc, int iCount, int iSrcOffset, int iDestOffset )
{
	write( &sfSrc, iCount, iSrcOffset, iDestOffset );
}

void ITASampleFrame::cyclic_write( const ITASampleFrame* psfSrc, int iCount, int iSrcOffset, int iDestOffset )
{
	if( !psfSrc )
		ITA_EXCEPT1( INVALID_PARAMETER, "Nullpointer passed" );
	if( psfSrc->channels( ) != m_iChannels )
		ITA_EXCEPT1( INVALID_PARAMETER, "Number of channels do not match" );

	for( int i = 0; i < m_iChannels; i++ )
		m_vChannels[i].cyclic_write( &( ( *psfSrc )[i] ), iCount, iSrcOffset, iDestOffset );
}

void ITASampleFrame::cyclic_write( const ITASampleFrame& sfSrc, int iCount, int iSrcOffset, int iDestOffset )
{
	cyclic_write( &sfSrc, iCount, iSrcOffset, iDestOffset );
}

void ITASampleFrame::CyclicShift( int iCount )
{
	for( int i = 0; i < m_iChannels; i++ )
		m_vChannels[i].CyclicShift( iCount );
}

void ITASampleFrame::add_scalar( float fValue )
{
	for( ch_it it = m_vChannels.begin( ); it != m_vChannels.end( ); ++it )
		it->add_scalar( fValue );
}

void ITASampleFrame::sub_scalar( float fValue )
{
	for( ch_it it = m_vChannels.begin( ); it != m_vChannels.end( ); ++it )
		it->sub_scalar( fValue );
}

void ITASampleFrame::mul_scalar( float fValue )
{
	for( ch_it it = m_vChannels.begin( ); it != m_vChannels.end( ); ++it )
		it->mul_scalar( fValue );
}

void ITASampleFrame::div_scalar( float fValue )
{
	for( ch_it it = m_vChannels.begin( ); it != m_vChannels.end( ); ++it )
		it->div_scalar( fValue );
}

void ITASampleFrame::add_buf( const ITASampleBuffer* psbSource )
{
	if( !psbSource )
		ITA_EXCEPT1( INVALID_PARAMETER, "Nullpointer passed" );
	if( psbSource->GetLength( ) != m_iLength )
		ITA_EXCEPT1( INVALID_PARAMETER, "Lengths do not match" );

	for( ch_it it = m_vChannels.begin( ); it != m_vChannels.end( ); ++it )
		it->add_buf( psbSource );
}

void ITASampleFrame::sub_buf( const ITASampleBuffer* psbSource )
{
	if( !psbSource )
		ITA_EXCEPT1( INVALID_PARAMETER, "Nullpointer passed" );
	if( psbSource->GetLength( ) != m_iLength )
		ITA_EXCEPT1( INVALID_PARAMETER, "Lengths do not match" );

	for( ch_it it = m_vChannels.begin( ); it != m_vChannels.end( ); ++it )
		it->sub_buf( psbSource );
}

void ITASampleFrame::mul_buf( const ITASampleBuffer* psbSource )
{
	if( !psbSource )
		ITA_EXCEPT1( INVALID_PARAMETER, "Nullpointer passed" );
	if( psbSource->GetLength( ) != m_iLength )
		ITA_EXCEPT1( INVALID_PARAMETER, "Lengths do not match" );

	for( ch_it it = m_vChannels.begin( ); it != m_vChannels.end( ); ++it )
		it->sub_buf( psbSource );
}

void ITASampleFrame::div_buf( const ITASampleBuffer* psbSource )
{
	if( !psbSource )
		ITA_EXCEPT1( INVALID_PARAMETER, "Nullpointer passed" );
	if( psbSource->GetLength( ) != m_iLength )
		ITA_EXCEPT1( INVALID_PARAMETER, "Lengths do not match" );

	for( ch_it it = m_vChannels.begin( ); it != m_vChannels.end( ); ++it )
		it->div_buf( psbSource );
}

void ITASampleFrame::add_buf( const ITASampleBuffer& sbSource )
{
	add_buf( &sbSource );
}

void ITASampleFrame::sub_buf( const ITASampleBuffer& sbSource )
{
	sub_buf( &sbSource );
}

void ITASampleFrame::mul_buf( const ITASampleBuffer& sbSource )
{
	mul_buf( &sbSource );
}

void ITASampleFrame::div_buf( const ITASampleBuffer& sbSource )
{
	div_buf( &sbSource );
}

void ITASampleFrame::add_frame( const ITASampleFrame* psfSource )
{
	assert( psfSource->GetLength( ) == GetLength( ) );
	add_frame( psfSource, GetLength( ) );
}

void ITASampleFrame::add_frame( const ITASampleFrame* psfSource, int iNumSamples )
{
	if( !psfSource )
		ITA_EXCEPT1( INVALID_PARAMETER, "Nullpointer passed" );

	if( psfSource->m_iChannels != m_iChannels )
		ITA_EXCEPT1( INVALID_PARAMETER, "Number of channels do not match" );

	if( psfSource->m_iLength < iNumSamples )
		ITA_EXCEPT1( INVALID_PARAMETER, "Lengths of given sample frame too short" );

	if( m_iLength < iNumSamples )
		ITA_EXCEPT1( INVALID_PARAMETER, "Too man samples to be added to this sample frame" );

	for( int i = 0; i < m_iChannels; ++i )
		m_vChannels[i].add_buf( &( psfSource->m_vChannels[i] ), iNumSamples );
}

void ITASampleFrame::add_frame_pos( const ITASampleFrame* psfSource, int iPos )
{
	if( !psfSource )
		ITA_EXCEPT1( INVALID_PARAMETER, "Nullpointer passed" );
	if( psfSource->m_iChannels != m_iChannels )
		ITA_EXCEPT1( INVALID_PARAMETER, "Number of channels do not match" );
	if( ( psfSource->m_iLength + iPos ) >= m_iLength )
		ITA_EXCEPT1( INVALID_PARAMETER, "Source length + delay exceed length of filter" );

	for( int i = 0; i < m_iChannels; ++i )
		m_vChannels[i].add_buf_pos( &( psfSource->m_vChannels[i] ), iPos );
}

void ITASampleFrame::sub_frame( const ITASampleFrame* psfSource )
{
	if( !psfSource )
		ITA_EXCEPT1( INVALID_PARAMETER, "Nullpointer passed" );
	if( psfSource->m_iChannels != m_iChannels )
		ITA_EXCEPT1( INVALID_PARAMETER, "Number of channels do not match" );
	if( psfSource->m_iLength != m_iLength )
		ITA_EXCEPT1( INVALID_PARAMETER, "Lengths do not match" );

	for( int i = 0; i < m_iChannels; ++i )
		m_vChannels[i].sub_buf( &( psfSource->m_vChannels[i] ) );
}

void ITASampleFrame::mul_frame( const ITASampleFrame* psfSource )
{
	if( !psfSource )
		ITA_EXCEPT1( INVALID_PARAMETER, "Nullpointer passed" );
	if( psfSource->m_iChannels != m_iChannels )
		ITA_EXCEPT1( INVALID_PARAMETER, "Number of channels do not match" );
	if( psfSource->m_iLength != m_iLength )
		ITA_EXCEPT1( INVALID_PARAMETER, "Lengths do not match" );

	for( int i = 0; i < m_iChannels; ++i )
		m_vChannels[i].mul_buf( &( psfSource->m_vChannels[i] ) );
}

void ITASampleFrame::div_frame( const ITASampleFrame* psfSource )
{
	if( !psfSource )
		ITA_EXCEPT1( INVALID_PARAMETER, "Nullpointer passed" );
	if( psfSource->m_iChannels != m_iChannels )
		ITA_EXCEPT1( INVALID_PARAMETER, "Number of channels do not match" );
	if( psfSource->m_iLength != m_iLength )
		ITA_EXCEPT1( INVALID_PARAMETER, "Lengths do not match" );

	for( int i = 0; i < m_iChannels; ++i )
		m_vChannels[i].div_buf( &( psfSource->m_vChannels[i] ) );
}

void ITASampleFrame::add_frame( const ITASampleFrame& sfSource )
{
	add_frame( &sfSource );
}

void ITASampleFrame::sub_frame( const ITASampleFrame& sfSource )
{
	sub_frame( &sfSource );
}

void ITASampleFrame::mul_frame( const ITASampleFrame& sfSource )
{
	mul_frame( &sfSource );
}

void ITASampleFrame::div_frame( const ITASampleFrame& sfSource )
{
	div_frame( &sfSource );
}

void ITASampleFrame::muladd_frame( const ITASampleFrame* psfSource, float fScalar, int iSrcOffset, int iDestOffset, int iCount )
{
	if( !psfSource )
		ITA_EXCEPT1( INVALID_PARAMETER, "Nullpointer passed" );
	if( psfSource->m_iChannels != m_iChannels )
		ITA_EXCEPT1( INVALID_PARAMETER, "Number of channels do not match" );

	for( int i = 0; i < m_iChannels; i++ )
		m_vChannels[i].MulAdd( ( *psfSource )[i], fScalar, iSrcOffset, iDestOffset, iCount );
}

void ITASampleFrame::muladd_frame( const ITASampleFrame& sfSource, float fScalar, int iSrcOffset, int iDestOffset, int iCount )
{
	muladd_frame( &sfSource, fScalar, iSrcOffset, iDestOffset, iCount );
}

float ITASampleFrame::findPeak( int* piChannel, int* piPeakIndex )
{
	if( ( m_iChannels == 0 ) || ( m_iLength == 0 ) )
	{
		if( piChannel )
			*piChannel = 0;
		if( piPeakIndex )
			*piPeakIndex = 0;

		return 0;
	}

	float fPeak    = 0;
	int iChannel   = 0;
	int iPeakIndex = 0;

	for( int i = 0; i < m_iChannels; i++ )
	{
		int k;
		float x = m_vChannels[i].FindPeak( &k );
		if( x > fPeak )
		{
			fPeak      = x;
			iChannel   = i;
			iPeakIndex = k;
		}
	}

	if( piChannel )
		*piChannel = iChannel;

	if( piPeakIndex )
		*piPeakIndex = iPeakIndex;

	return fPeak;
}

void ITASampleFrame::negate( )
{
	for( ch_it it = m_vChannels.begin( ); it != m_vChannels.end( ); ++it )
		it->Negate( );
};

ITASampleBuffer& ITASampleFrame::operator[]( int iChannel )
{
	return m_vChannels[iChannel];
}

const ITASampleBuffer& ITASampleFrame::operator[]( int iChannel ) const
{
	return m_vChannels[iChannel];
}

ITASampleFrame& ITASampleFrame::operator=( const ITASampleFrame& rhs )
{
	// No self assignment
	if( &rhs == this )
		return *this;

	init( rhs.channels( ), rhs.length( ), false );

	for( int i = 0; i < m_iChannels; i++ )
		( *this )[i].write( rhs[i], this->GetLength( ) );

	return *this;
}

ITASampleFrame& ITASampleFrame::operator+=( const float rhs )
{
	add_scalar( rhs );
	return *this;
}

ITASampleFrame& ITASampleFrame::operator-=( const float rhs )
{
	sub_scalar( rhs );
	return *this;
}

ITASampleFrame& ITASampleFrame::operator*=( const float rhs )
{
	mul_scalar( rhs );
	return *this;
}

ITASampleFrame& ITASampleFrame::operator/=( const float rhs )
{
	div_scalar( rhs );
	return *this;
}

ITASampleFrame& ITASampleFrame::operator+=( const ITASampleBuffer& rhs )
{
	add_buf( rhs );
	return *this;
}

ITASampleFrame& ITASampleFrame::operator-=( const ITASampleBuffer& rhs )
{
	sub_buf( rhs );
	return *this;
}

ITASampleFrame& ITASampleFrame::operator*=( const ITASampleBuffer& rhs )
{
	mul_buf( rhs );
	return *this;
}

ITASampleFrame& ITASampleFrame::operator/=( const ITASampleBuffer& rhs )
{
	div_buf( rhs );
	return *this;
}

ITASampleFrame& ITASampleFrame::operator+=( const ITASampleFrame& rhs )
{
	add_frame( rhs );
	return *this;
}

ITASampleFrame& ITASampleFrame::operator-=( const ITASampleFrame& rhs )
{
	sub_frame( rhs );
	return *this;
}

ITASampleFrame& ITASampleFrame::operator*=( const ITASampleFrame& rhs )
{
	mul_frame( rhs );
	return *this;
}

ITASampleFrame& ITASampleFrame::operator/=( const ITASampleFrame& rhs )
{
	div_frame( rhs );
	return *this;
}

std::string ITASampleFrame::toString( ) const
{
	std::stringstream ss;
	ss << "Sample frame { ";
	if( empty( ) )
		ss << "emtpy";
	else
		ss << m_iChannels << " channels of " << m_iLength << " samples";
	ss << " }";
	return ss.str( );
}

float ITASampleFrame::Normalize( )
{
	int c, s;
	findPeak( &c, &s );
	float fP = m_vChannels[c][s];
	for( int i = 0; i < m_iChannels; i++ )
		m_vChannels[i].div_scalar( std::abs( fP ) );

	return fP;
}
