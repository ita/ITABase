#include <ITAASCIITable.h>
#include <ITAStringUtils.h>
#include <algorithm>
#include <cmath>
#include <iomanip>
#include <iostream>
#include <sstream>

ITAASCIITable::ITAASCIITable( ) {}

ITAASCIITable::ITAASCIITable( unsigned int uiRows, unsigned int uiColumns )
{
	m_vColumns.resize( uiColumns );
	for( unsigned int i = 0; i < uiColumns; i++ )
		m_vColumns[i].vsCells.resize( uiRows );
}

ITAASCIITable::~ITAASCIITable( ) {}

unsigned int ITAASCIITable::rows( ) const
{
	return ( m_vColumns.empty( ) ? 0 : (unsigned int)m_vColumns[0].vsCells.size( ) );
}

unsigned int ITAASCIITable::columns( ) const
{
	return (unsigned int)m_vColumns.size( );
}

void ITAASCIITable::addRow( unsigned int uiPosition )
{
	// Jeweils eine Zelle in alle Spalten einf�gen
	uiPosition = ( std::min )( uiPosition, rows( ) );
	for( unsigned int i = 0; i < (unsigned int)m_vColumns.size( ); i++ )
		m_vColumns[i].vsCells.insert( m_vColumns[i].vsCells.begin( ) + uiPosition, "" );
}

void ITAASCIITable::removeRow( unsigned int uiRow )
{
	// Jeweils eine Zelle in alle Spalten entfernen
	if( uiRow > rows( ) )
		return;
	for( unsigned int i = 0; i < (unsigned int)m_vColumns.size( ); i++ )
		m_vColumns[i].vsCells.erase( m_vColumns[i].vsCells.begin( ) + uiRow );
}

void ITAASCIITable::addColumn( unsigned int uiPosition )
{
	addColumn( "", uiPosition );
}

void ITAASCIITable::addColumn( const std::string& sColumnTitle, unsigned int uiPosition )
{
	uiPosition = std::min( uiPosition, (unsigned int)m_vColumns.size( ) );
	ITAASCIITableColumn c;
	c.sTitle = sColumnTitle;
	m_vColumns.insert( m_vColumns.begin( ) + uiPosition, c );
}

void ITAASCIITable::removeColumn( unsigned int uiColumn )
{
	uiColumn = std::min( uiColumn, (unsigned int)m_vColumns.size( ) );
	m_vColumns.erase( m_vColumns.begin( ) + uiColumn );
}

unsigned int ITAASCIITable::getColumnJustify( unsigned int uiColumn )
{
	if( uiColumn >= m_vColumns.size( ) )
		return LEFT;
	return m_vColumns[uiColumn].uiJustify;
}

void ITAASCIITable::setColumnJustify( unsigned int uiColumn, unsigned int uiJustify )
{
	if( uiColumn < m_vColumns.size( ) )
		m_vColumns[uiColumn].uiJustify = uiJustify;
}

std::string ITAASCIITable::getColumnTitle( unsigned int uiColumn )
{
	if( uiColumn >= m_vColumns.size( ) )
		return "";
	return m_vColumns[uiColumn].sTitle;
}

void ITAASCIITable::setColumnTitle( unsigned int uiColumn, const std::string& sTitle )
{
	if( uiColumn < m_vColumns.size( ) )
		m_vColumns[uiColumn].sTitle = sTitle;
}

unsigned int ITAASCIITable::getColumnTitleJustify( unsigned int uiColumn )
{
	if( uiColumn >= m_vColumns.size( ) )
		return LEFT;
	return m_vColumns[uiColumn].uiTitleJustify;
}

void ITAASCIITable::setColumnTitleJustify( unsigned int uiColumn, unsigned int uiJustify )
{
	if( uiColumn < m_vColumns.size( ) )
		m_vColumns[uiColumn].uiTitleJustify = uiJustify;
}

std::string ITAASCIITable::getContent( unsigned int uiRow, unsigned int uiColumn )
{
	if( uiColumn >= m_vColumns.size( ) )
		return "";
	if( uiRow >= m_vColumns[0].vsCells.size( ) )
		return "";
	return m_vColumns[uiColumn].vsCells[uiRow];
}

void ITAASCIITable::setContent( unsigned int uiRow, unsigned int uiColumn, const std::string& sContent )
{
	if( uiColumn >= m_vColumns.size( ) )
		return;
	if( uiRow >= m_vColumns[0].vsCells.size( ) )
		return;
	m_vColumns[uiColumn].vsCells[uiRow] = sContent;
}

void ITAASCIITable::setContent( unsigned int uiRow, unsigned int uiColumn, const char* pszContent )
{
	setContent( uiRow, uiColumn, std::string( pszContent ) );
}

void ITAASCIITable::setContent( unsigned int uiRow, unsigned int uiColumn, const bool& bContent )
{
	setContent( uiRow, uiColumn, BoolToString( bContent ) );
}

void ITAASCIITable::setContent( unsigned int uiRow, unsigned int uiColumn, const unsigned int& uiContent )
{
	setContent( uiRow, uiColumn, UIntToString( uiContent ) );
}

void ITAASCIITable::setContent( unsigned int uiRow, unsigned int uiColumn, const int& iContent )
{
	setContent( uiRow, uiColumn, IntToString( iContent ) );
}

void ITAASCIITable::setContent( unsigned int uiRow, unsigned int uiColumn, const float& fContent, int iPrecision )
{
	setContent( uiRow, uiColumn, FloatToString( fContent, iPrecision ) );
}

void ITAASCIITable::setContent( unsigned int uiRow, unsigned int uiColumn, const double& dContent, int iPrecision )
{
	setContent( uiRow, uiColumn, DoubleToString( dContent, iPrecision ) );
}

//! Die ASCII-Tabelle rendern
std::ostream& ITAASCIITable::render( std::ostream& os, int iFormat )
{
	switch( iFormat )
	{
		case DAT:
			return renderDAT( os );
		case CSV:
			return renderCSV( os );
		default:
			return renderText( os );
	}
}

std::ostream& ITAASCIITable::renderText( std::ostream& os )
{
	std::stringstream tmp;

	unsigned int n = (unsigned int)m_vColumns.size( );
	unsigned int m = rows( );

	std::vector<std::vector<std::string> > vvsLines( n );

	// Calculate the column widths
	for( unsigned int i = 0; i < n; i++ )
	{
		m_vColumns[i].uiMaxWidth = 0;
		for( unsigned int j = 0; j < (unsigned int)m_vColumns[i].vsCells.size( ); j++ )
			m_vColumns[i].uiMaxWidth = std::max( m_vColumns[i].uiMaxWidth, (unsigned int)m_vColumns[i].vsCells[j].length( ) );
	}

	// Generate the table header

	unsigned int uiLines = 0;
	unsigned int tw      = 0; // Total width

	// Generate a vector of vectors of strings containing each cells lines
	for( unsigned int i = 0; i < n; i++ )
	{
		splitLines( m_vColumns[i].sTitle, vvsLines[i] );
		uiLines = std::max( uiLines, (unsigned int)vvsLines[i].size( ) );
		for( unsigned int j = 0; j < uiLines; j++ )
			if( vvsLines[i].size( ) > j )
				m_vColumns[i].uiMaxWidth = std::max( m_vColumns[i].uiMaxWidth, (unsigned int)vvsLines[i][j].length( ) );
	}

	for( unsigned int j = 0; j < uiLines; j++ )
	{
		tmp.str( "" );
		tmp.clear( );

		for( unsigned int i = 0; i < n; i++ )
		{
			// Left margin
			if( i == 0 )
				tmp << " ";

			unsigned int w = m_vColumns[i].uiMaxWidth;

			switch( m_vColumns[i].uiTitleJustify )
			{
				case RIGHT:
					tmp << std::right;
					break;
				default:
					tmp << std::left;
			}

			if( j < (unsigned int)vvsLines[i].size( ) )
				tmp << std::setw( w ) << vvsLines[i][j];
			else
				tmp << std::setw( w ) << "";

			// Column spacer
			if( i < n - 1 )
				tmp << "  ";

			// Right margin
			if( i == n - 1 )
				tmp << " " << std::endl;
		}

		tw = std::max( tw, (unsigned int)tmp.str( ).length( ) - 1 ); // -1 because of endl
		os << tmp.str( );
	}

	// Generate a horizontal line
	tmp.str( "" );
	tmp.clear( );
	tmp << std::setfill( '-' ) << std::setw( tw ) << '-';
	std::string sLine = tmp.str( );
	os << sLine << std::endl;

	// Render the table lines
	tmp.fill( ' ' );
	for( unsigned int r = 0; r < m; r++ )
	{
		uiLines = 0;

		// Generate a vector of vectors of strings containing each cells lines
		for( unsigned int i = 0; i < n; i++ )
		{
			splitLines( m_vColumns[i].vsCells[r], vvsLines[i] );
			uiLines = std::max( uiLines, (unsigned int)vvsLines[i].size( ) );
		}

		for( unsigned int j = 0; j < uiLines; j++ )
		{
			tmp.str( "" );
			tmp.clear( );

			for( unsigned int i = 0; i < n; i++ )
			{
				// Left margin
				if( i == 0 )
					tmp << " ";
				unsigned int w = m_vColumns[i].uiMaxWidth;

				switch( m_vColumns[i].uiJustify )
				{
					case RIGHT:
						tmp << std::right;
						break;
					default:
						tmp << std::left;
				}

				if( j < (unsigned int)vvsLines[i].size( ) )
					tmp << std::setw( w ) << vvsLines[i][j];
				else
					tmp << std::setw( w ) << "";

				// Column spacer
				if( i < n - 1 )
					tmp << "  ";

				// Right margin
				if( i == n - 1 )
					tmp << " " << std::endl;
			}

			os << tmp.str( );
		}
	}

	// Final line
	os << sLine << std::endl;

	return os;
}

std::ostream& ITAASCIITable::renderDAT( std::ostream& os )
{
	std::stringstream tmp;

	unsigned int n = (unsigned int)m_vColumns.size( );
	unsigned int m = rows( );

	// Calculate the column widths
	for( unsigned int i = 0; i < n; i++ )
	{
		m_vColumns[i].uiMaxWidth = 0;
		for( unsigned int j = 0; j < (unsigned int)m_vColumns[i].vsCells.size( ); j++ )
			m_vColumns[i].uiMaxWidth = std::max( m_vColumns[i].uiMaxWidth, (unsigned int)m_vColumns[i].vsCells[j].length( ) );
	}

	// Output the column titles
	for( unsigned int i = 0; i < n; i++ )
		os << "# [" << i << "] " << m_vColumns[i].sTitle << std::endl;
	os << std::endl;

	// Render the table cells line by line
	for( unsigned int r = 0; r < m; r++ )
	{
		for( unsigned int i = 0; i < n; i++ )
		{
			unsigned int w = m_vColumns[i].uiMaxWidth;
			os << std::left << std::setw( w ) << m_vColumns[i].vsCells[r];
			if( i < ( n - 1 ) )
				os << "  \t";
			else
				os << std::endl;
		}
	}

	return os;
}

std::ostream& ITAASCIITable::renderCSV( std::ostream& os )
{
	std::stringstream tmp;

	unsigned int n = (unsigned int)m_vColumns.size( );
	unsigned int m = rows( );

	/*
	// Output the column titles
	for (unsigned int i=0; i<n; i++)
	    os << "# [" << i << "] " << m_vColumns[i].sTitle << std::endl;
	os << std::endl;
	*/

	// Render the table cells line by line
	for( unsigned int r = 0; r < m; r++ )
	{
		for( unsigned int i = 0; i < n; i++ )
		{
			os << std::left << m_vColumns[i].vsCells[r];
			if( i < ( n - 1 ) )
				os << ";";
			else
				os << std::endl;
		}
	}

	return os;
}

std::string ITAASCIITable::toString( )
{
	std::stringstream ss;
	render( ss );
	return ss.str( );
}

void ITAASCIITable::splitLines( const std::string& sText, std::vector<std::string>& vsDest )
{
	vsDest.clear( );
	if( sText.empty( ) )
		return;

	int i = 0, j;
	do
	{
		j = (int)sText.find( '\n', i );
		if( j != -1 )
		{
			vsDest.push_back( sText.substr( i, j - i ) );
			i = j + 1;
		}
	} while( j != -1 );

	if( i == 0 )
		vsDest.push_back( sText );
	else if( i != (int)sText.length( ) - 1 )
		vsDest.push_back( sText.substr( i, (int)sText.length( ) - i + 1 ) );

	if( sText[sText.length( ) - 1] == '\n' )
		vsDest.erase( vsDest.end( ) - 1 );
}
