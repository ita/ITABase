#include <ITAAudiofileWriter.h>
#include <ITAConstants.h>
#include <ITAFileSystemUtils.h>
#include <ITAHDFTSpectra.h>
#include <ITAHDFTSpectrum.h>
#include <ITASampleFrame.h>
#include <ITAStringUtils.h>

ITABase::CHDFTSpectra::CHDFTSpectra( const float fSampleRate, const int iNumChannels, const int iDFTSize, const bool bZeroInit /*=true*/ )
{
	if( iNumChannels < 1 )
		ITA_EXCEPT1( INVALID_PARAMETER, "At least one DFT channel must be used" );
	if( iDFTSize < 1 )
		ITA_EXCEPT1( INVALID_PARAMETER, "Invalid DFT size" );

	for( int i = 0; i < iNumChannels; i++ )
		m_vpSpectra.push_back( new CHDFTSpectrum( fSampleRate, iDFTSize, bZeroInit ) );
}

ITABase::CHDFTSpectra::CHDFTSpectra( const std::vector<ITABase::CHDFTSpectrum*>& vpSpectrumVec )
{
	if( vpSpectrumVec.size( ) == 0 )
		ITA_EXCEPT1( INVALID_PARAMETER, "At least one DFT channel must be used" );

	for( size_t i = 0; i < vpSpectrumVec.size( ); i++ )
	{
		const ITABase::CHDFTSpectrum* pSpectrum( vpSpectrumVec[i] );
		if( pSpectrum->GetDFTSize( ) <= 0 )
			ITA_EXCEPT1( INVALID_PARAMETER, "Invalid DFT size in spectrum number " + IntToString( int( i ) ) );

		if( pSpectrum->GetSampleRate( ) <= 0 )
			ITA_EXCEPT1( INVALID_PARAMETER, "Invalid sampling rate in spectrum number " + IntToString( int( i ) ) );

		m_vpSpectra.push_back( new ITABase::CHDFTSpectrum( pSpectrum ) ); // copy
	}
}

ITABase::CHDFTSpectra::~CHDFTSpectra( )
{
	for( size_t i = 0; i < m_vpSpectra.size( ); i++ )
		delete m_vpSpectra[i];
}

float ITABase::CHDFTSpectra::GetMaxEnergy( int* piIdx ) const
{
	float fEnergyTemp;
	float fEnergyMax = -1.0f;
	for( size_t n = 0; n < m_vpSpectra.size( ); n++ )
	{
		fEnergyTemp = m_vpSpectra[n]->GetEnergy( );
		if( fEnergyTemp > fEnergyMax )
		{
			fEnergyMax = fEnergyTemp;
			*piIdx     = int( n );
		}
	}

	return fEnergyMax;
}

void ITABase::CHDFTSpectra::CopyFrom( const ITABase::CHDFTSpectra* otherSpectra )
{
	int iNumChannels = otherSpectra->GetNumChannels( );

	m_vpSpectra.clear( );

	for( int i = 0; i < iNumChannels; i++ )
	{
		// ITAHDFTSpectrum* tempSpectrum = new ITAHDFTSpectrum(double(otherSpectra->GetSampleRate()),int(otherSpectra->GetDFTSize()));
		ITABase::CHDFTSpectrum* tempSpectrum = new ITABase::CHDFTSpectrum( ( *otherSpectra )[i] );
		// tempSpectrum->copyFrom((*otherSpectra)[i]);
		m_vpSpectra.push_back( tempSpectrum );
	}
}

void ITABase::CHDFTSpectra::SetUnity( )
{
	for( size_t i = 0; i < m_vpSpectra.size( ); i++ )
	{
		ITABase::CHDFTSpectrum* pSpectrum( m_vpSpectra[i] );
		pSpectrum->SetUnity( );
	}
}

void ITABase::CHDFTSpectra::SetZero( )
{
	for( size_t i = 0; i < m_vpSpectra.size( ); i++ )
	{
		ITABase::CHDFTSpectrum* pSpectrum( m_vpSpectra[i] );
		pSpectrum->Mul( 0.0f );
	}
}

int ITABase::CHDFTSpectra::GetNumChannels( ) const
{
	return int( m_vpSpectra.size( ) );
}

int ITABase::CHDFTSpectra::GetDFTSize( ) const
{
	return m_vpSpectra[0]->GetDFTSize( );
}

double ITABase::CHDFTSpectra::GetSampleRate( ) const
{
	return m_vpSpectra[0]->GetSampleRate( );
}

void ITABase::CHDFTSpectra::add( const ITABase::CHDFTSpectra* pSource )
{
	if( GetNumChannels( ) != pSource->GetNumChannels( ) )
		ITA_EXCEPT1( INVALID_PARAMETER, "Channel number mismatch" );

	for( int i = 0; i < GetNumChannels( ); i++ )
	{
		CHDFTSpectrum* pSpectrum( m_vpSpectra[i] );
		const CHDFTSpectrum* pSourceSpectrum( ( *pSource )[i] );
		pSpectrum->Add( pSourceSpectrum );
	}

	return;
}

void ITABase::CHDFTSpectra::sub( const ITABase::CHDFTSpectra* pSource )
{
	if( GetNumChannels( ) != pSource->GetNumChannels( ) )
		ITA_EXCEPT1( INVALID_PARAMETER, "Channel number mismatch" );

	for( int i = 0; i < GetNumChannels( ); i++ )
	{
		CHDFTSpectrum* pSpectrum( m_vpSpectra[i] );
		const CHDFTSpectrum* pSourceSpectrum( ( *pSource )[i] );
		pSpectrum->Sub( pSourceSpectrum );
	}

	return;
}

void ITABase::CHDFTSpectra::mul( const ITABase::CHDFTSpectra* pSource )
{
	if( GetNumChannels( ) != pSource->GetNumChannels( ) )
		ITA_EXCEPT1( INVALID_PARAMETER, "Channel number mismatch" );

	for( int i = 0; i < GetNumChannels( ); i++ )
	{
		CHDFTSpectrum* pSpectrum( m_vpSpectra[i] );
		const CHDFTSpectrum* pSourceSpectrum( ( *pSource )[i] );
		pSpectrum->Mul( pSourceSpectrum );
	}

	return;
}

void ITABase::CHDFTSpectra::mul_conj( const ITABase::CHDFTSpectra* pSource )
{
	if( GetNumChannels( ) != pSource->GetNumChannels( ) )
		ITA_EXCEPT1( INVALID_PARAMETER, "Channel number mismatch" );

	for( int i = 0; i < GetNumChannels( ); i++ )
	{
		CHDFTSpectrum* pSpectrum( m_vpSpectra[i] );
		const CHDFTSpectrum* pSourceSpectrum( ( *pSource )[i] );
		pSpectrum->Mul( pSourceSpectrum );
	}

	return;
}

void ITABase::CHDFTSpectra::mul_scalar( double )
{
	ITA_EXCEPT0( NOT_IMPLEMENTED );
}

void ITABase::CHDFTSpectra::div( const ITABase::CHDFTSpectra* pSource )
{
	if( GetNumChannels( ) != pSource->GetNumChannels( ) )
		ITA_EXCEPT1( INVALID_PARAMETER, "Channel number mismatch" );

	for( int i = 0; i < GetNumChannels( ); i++ )
	{
		ITABase::CHDFTSpectrum* pSpectrum( m_vpSpectra[i] );
		const ITABase::CHDFTSpectrum* pSourceSpectrum( ( *pSource )[i] );
		pSpectrum->Div( pSourceSpectrum );
	}

	return;
}

const ITABase::CHDFTSpectrum* ITABase::CHDFTSpectra::operator[]( const int iIdx ) const
{
	return m_vpSpectra[iIdx];
}

ITABase::CHDFTSpectrum* ITABase::CHDFTSpectra::operator[]( const int iIdx )
{
	return m_vpSpectra[iIdx];
}

float ITABase::CHDFTSpectra::GetFrequencyOfBin( const int iBinIndex ) const
{
	return m_vpSpectra[0]->GetFrequencyOfBin( iBinIndex );
}

float ITABase::CHDFTSpectra::GetFrequencyResolution( ) const
{
	return m_vpSpectra[0]->GetFrequencyResolution( );
}
