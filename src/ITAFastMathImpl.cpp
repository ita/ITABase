#include <ITAFastMath.h>
#include <assert.h>
#include <malloc.h>
#include <memory.h>

// Makro: Längen auf Vielfaches eines Faktor zu erweitern
#ifdef FM_XSIZE
#	undef FM_XSIZE
#endif

#ifdef FM_NOEXT
#	define FM_XSIZE( V, F )
#else
#	define FM_XSIZE( V, F ) V = F * ( V % F ? ( V / F ) + 1 : V / F );
#endif

namespace CRels
{
	inline void fm_set( float* buf, float value, unsigned int count )
	{
		for( unsigned int i = 0; i < count; i++ )
			buf[i] = value;
	}

	inline void fm_copy( float* dest, const float* src, unsigned int count ) { memcpy( dest, src, count * sizeof( float ) ); }

	inline void fm_add( float* dest, const float* summand, unsigned int count )
	{
		for( unsigned int i = 0; i < count; i++ )
			dest[i] += summand[i];
	}

	inline void fm_sub( float* dest, const float* src, unsigned int count )
	{
		for( unsigned int i = 0; i < count; i++ )
			dest[i] -= src[i];
	}

	inline void fm_mul( float* dest, float factor, unsigned int count )
	{
		for( unsigned int i = 0; i < count; i++ )
			dest[i] *= factor;
	}

	inline void fm_cmul( float* dest, const float* factor, unsigned int count )
	{
		float re, im;
		for( unsigned int i = 0; i < count; i++ )
		{
			// Realteil: Re'[k] = Re{dest[k]}*Re{factor[k]} - Im{dest[k]}*Im{factor[k]}
			re = dest[2 * i] * factor[2 * i] - dest[2 * i + 1] * factor[2 * i + 1];
			// Imaginärteil: Im'[k] = Re{dest[k]}*Im{factor[k]} + Im{dest[k]}*Re{factor[k]}
			im = dest[2 * i] * factor[2 * i + 1] + dest[2 * i + 1] * factor[2 * i];

			dest[2 * i]     = re;
			dest[2 * i + 1] = im;
		}
	}

	inline void fm_cmul_x( float* dest, const float* factor1, const float* factor2, unsigned int count )
	{
		for( unsigned int i = 0; i < count; i++ )
		{
			// Realteil: Re'[k] = Re{dest[k]}*Re{factor[k]} - Im{dest[k]}*Im{factor[k]}
			dest[2 * i] = factor1[2 * i] * factor2[2 * i] - factor1[2 * i + 1] * factor2[2 * i + 1];
			// Imaginärteil: Im'[k] = Re{dest[k]}*Im{factor[k]} + Im{dest[k]}*Re{factor[k]}
			dest[2 * i + 1] = factor1[2 * i] * factor2[2 * i + 1] + factor1[2 * i + 1] * factor2[2 * i];
		}
	}

	inline void fm_cdiv( float* dest, const float* div, unsigned int count )
	{
		float re, im, len;
		for( unsigned int i = 0; i < count; i++ )
		{
			// Nenner im Gesamtergebnis |div[i]| berechnen
			len = div[2 * i] * div[2 * i] + div[2 * i + 1] * div[2 * i + 1];

			// Realteil: Re'[k] = [ Re{dest[k]}*Re{div[k]} + Im{dest[k]}*Im{div[k]} ] / len
			re = ( dest[2 * i] * div[2 * i] + dest[2 * i + 1] * div[2 * i + 1] ) / len;
			// Imaginärteil: Im'[k] = [ Im{dest[k]}*Re{div[k]} - Re{dest[k]}*Im{div[k]} ] / len
			im = ( dest[2 * i + 1] * div[2 * i] - dest[2 * i] * div[2 * i + 1] ) / len;

			dest[2 * i]     = re;
			dest[2 * i + 1] = im;
		}
	}

	// Real-valued addition (in-place)
	// Semantic: srcdest[i] += src[i]
	void fm_add_f32( float* srcdest, const float* src, int count )
	{
		for( int i = 0; i < count; i++ )
			srcdest[i] += src[i];
	}

	// Real-valued multiplication (in-place)
	// Semantic: srcdest[i] *= src[i]
	void fm_mul_f32( float* srcdest, const float* src, int count )
	{
		for( int i = 0; i < count; i++ )
			srcdest[i] *= src[i];
	}

	// Complex-valued multiply
	// Semantic: dest[i] = src1[i]*src2[i]
	void fm_cmul_f32( float* dest, const float* src1, const float* src2, int count )
	{
		for( int i = 0; i < count; i++ )
		{
			// Realteil: Re'[k] = Re{dest[k]}*Re{factor[k]} - Im{dest[k]}*Im{factor[k]}
			dest[2 * i] = src1[2 * i] * src2[2 * i] - src1[2 * i + 1] * src2[2 * i + 1];
			// Imaginärteil: Im'[k] = Re{dest[k]}*Im{src[k]} + Im{dest[k]}*Re{src[k]}
			dest[2 * i + 1] = src1[2 * i] * src2[2 * i + 1] + src1[2 * i + 1] * src2[2 * i];
		}
	}

	// Complex-valued multiply-accumulate
	// Semantic: srcdest[i] += src1[i]*src2[i]
	void fm_cmuladd_f32( float* srcdest, const float* src1, const float* src2, int count )
	{
		for( int i = 0; i < count; i++ )
		{
			// Realteil: Re'[k] = Re{dest[k]}*Re{factor[k]} - Im{dest[k]}*Im{factor[k]}
			srcdest[2 * i] += src1[2 * i] * src2[2 * i] - src1[2 * i + 1] * src2[2 * i + 1];
			// Imaginärteil: Im'[k] = Re{dest[k]}*Im{src[k]} + Im{dest[k]}*Re{src[k]}
			srcdest[2 * i + 1] += src1[2 * i] * src2[2 * i + 1] + src1[2 * i + 1] * src2[2 * i];
		}
	}
} // namespace CRels

void fm_init( ) {}

unsigned int fm_flags( )
{
	return 0;
}

std::string fm_flags_str( )
{
#ifdef DEBUG
	return "No optimization (debug mode)";
#else
	return "Compiler optimization";
#endif
}

float* fm_falloc( unsigned int len, bool zeroinit )
{
#ifdef __GNUC__
	// TODO: Für den g++ erstmal kein aligned malloc
	float* ptr = (float*)malloc( len * sizeof( float ) );
#else
	FM_XSIZE( len, 4 );
	int iBytes = len * sizeof( float );
	float* ptr = (float*)_aligned_malloc( iBytes, 16 );
#endif

	assert( ptr != 0 );

	if( zeroinit )
		fm_zero( ptr, len );
	return ptr;
}

void fm_free( float* ptr )
{
#ifdef __GNUC__
	// TODO: Für den g++ erstmal kein aligned free
	if( ptr )
		free( ptr );
#else
	// Nullzeiger ausschließen
	if( ptr )
		_aligned_free( ptr );
#endif
}


/*
   +----------------------+
   |                      |
   |  Funktionensweichen  |
   |                      |
   +----------------------+
   */

void fm_set( float* buf, float value, unsigned int count )
{
	// Hinweis: Momentan keine optimierte Version
	CRels::fm_set( buf, value, count );
}

void fm_zero( float* buf, unsigned int count )
{
	fm_set( buf, 0, count );
}

void fm_copy( float* dest, const float* src, unsigned int count )
{
	// Hinweis: Hier wird keine SSE-Variante eingesetzt obwohl
	//          diese Implementiert ist (siehe Implementierung)
	CRels::fm_copy( dest, src, count );
}

void fm_add( float* dest, const float* summand, unsigned int count )
{
	CRels::fm_add( dest, summand, count );
}

void fm_sub( float* dest, const float* src, unsigned int count )
{
	CRels::fm_sub( dest, src, count );
}

void fm_mul( float* dest, float factor, unsigned int count )
{
	CRels::fm_mul( dest, factor, count );
}

void fm_cmul( float* dest, const float* factor, unsigned int count )
{
	// TODO: Optimierte Variante. Momentan nur Standard-Code!
	CRels::fm_cmul( dest, factor, count );
}


void fm_cmul_x( float* dest, const float* factor1, const float* factor2, unsigned int count )
{
	CRels::fm_cmul_x( dest, factor1, factor2, count );
}

void fm_cdiv( float* dest, const float* div, unsigned int count )
{
	// TODO: Optimierte Variante. Momentan nur Standard-Code!
	CRels::fm_cdiv( dest, div, count );
}

void fm_cmul_f32( float* dest, const float* src1, const float* src2, int count )
{
	CRels::fm_cmul_f32( dest, src1, src2, count );
}

void fm_cmuladd_f32( float* srcdest, const float* src1, const float* src2, int count )
{
	CRels::fm_cmuladd_f32( srcdest, src1, src2, count );
}

#undef FM_XSIZE
