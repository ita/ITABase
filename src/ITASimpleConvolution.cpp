#include <ITASimpleConvolution.h>
#include <algorithm>

// @todo jst: move to ITADSP

void conv( const float* A, int M, const float* B, int N, float* C, int K )
{
	// TODO: Mehr Geschwindigkeit durch Vektorisierung?!

	/*
	 *  Annahme: Impulsantwort i.A. k�rzer als Signal. Deshalb soll diese
	 *           bevorzugt im Cache gehalten werden. Daher soll die innere
	 *           Schleife �ber die Impulsantwort iterieren. Da die Faltung
	 *           assoziativ ist, wird hier gegenenfalles die Reihenfolge
	 *           der Operanden vertauscht, um diese Bedingungen zu erf�llen.
	 */

	const float* X;
	const float* Y;
	int nx, ny;

	if( M >= N )
	{
		X  = A;
		nx = M;
		Y  = B;
		ny = N;
	}
	else
	{
		X  = B;
		nx = N;
		Y  = A;
		ny = M;
	}

	// �u�ere Schleife �ber die Ausgangssamples
	for( int n = 0; n < std::min( nx + ny - 1, K ); n++ )
	{
		C[n] = 0;

		// Inner Schleife �ber die Filterkoeffizienten der Impulsantwort
		for( int k = std::max( 0, n + 1 - ny ); k < std::min( nx, n + 1 ); k++ )
			C[n] += X[k] * Y[n - k];
	}
}

void conv( const double* A, int M, const double* B, int N, double* C, int K )
{
	// TODO: Mehr Geschwindigkeit durch Vektorisierung?!

	/*
	 *  Annahme: Impulsantwort i.A. k�rzer als Signal. Deshalb soll diese
	 *           bevorzugt im Cache gehalten werden. Daher soll die innere
	 *           Schleife �ber die Impulsantwort iterieren. Da die Faltung
	 *           assoziativ ist, wird hier gegenenfalles die Reihenfolge
	 *           der Operanden vertauscht, um diese Bedingungen zu erf�llen.
	 */

	const double* X;
	const double* Y;
	int nx, ny;

	if( M >= N )
	{
		X  = A;
		nx = M;
		Y  = B;
		ny = N;
	}
	else
	{
		X  = B;
		nx = N;
		Y  = A;
		ny = M;
	}

	// �u�ere Schleife �ber die Ausgangssamples
	for( int n = 0; n < std::min( nx + ny - 1, K ); n++ )
	{
		C[n] = 0;

		// Inner Schleife �ber die Filterkoeffizienten der Impulsantwort
		for( int k = std::max( 0, n + 1 - ny ); k < std::min( nx, n + 1 ); k++ )
			C[n] += X[k] * Y[n - k];
	}
}
