﻿#include <ITABase/UtilsJSON.h>
#include <fstream>
#include <nlohmann/json.hpp>

using namespace nlohmann;
#define JSON_INDENT 3

//-----ITASpectrum-----

void ITABase::Utils::JSON::Export( const CITASpectrum* pSpectrum, const std::string& sTargetFilePath )
{
	json jRoot;
	jRoot["name"]       = pSpectrum->GetName( );
	jRoot["num_bands"]  = pSpectrum->GetNumBands( );
	jRoot["value_unit"] = pSpectrum->GetValueUnit( );

	json jSpectrum;
	for( int i = 0; i < pSpectrum->GetNumBands( ); i++ )
	{
		// TODO: Why use tags for the bin index. Better would be a simple array ( jSpectrum.push_back() )
		jSpectrum["bin" + std::to_string( i )] = { { "center_frequency", pSpectrum->GetCenterFrequencies( )[i] }, { "value", pSpectrum->GetValues( )[i] } };
	}
	jRoot["spectrum"] = jSpectrum;

	std::ofstream fsOut( sTargetFilePath );
	fsOut << jRoot.dump( JSON_INDENT );
	fsOut.close( );
}


//-----Statistics-----

json StatsToJSON( const ITABase::CStatistics& oStats )
{
	json jStats;
	jStats["name"]   = oStats.sName;
	jStats["N"]      = oStats.uiNumSamples;
	jStats["units"]  = oStats.sUnits;
	jStats["min"]    = oStats.dMin;
	jStats["max"]    = oStats.dMax;
	jStats["mean"]   = oStats.dMean;
	jStats["stddev"] = oStats.dStdDev;

	return jStats;
}

void ITABase::Utils::JSON::Export( const ITABase::CStatistics& oStats, const std::string& sTargetFilePath )
{
	std::ofstream fsOut( sTargetFilePath );
	fsOut << StatsToJSON( oStats ).dump( JSON_INDENT );
	fsOut.close( );
}

void ITABase::Utils::JSON::Export( const std::vector<ITABase::CStatistics>& voStats, const std::string& sTargetFilePath )
{
	json jRoot;
	for( auto& oStats: voStats )
	{
		// TODO: What happen if name is identical? Better would be a simple array ( jRoot.push_back() )
		jRoot[oStats.sName] = StatsToJSON( oStats );
	}

	std::ofstream fsOut( sTargetFilePath );
	fsOut << jRoot.dump( JSON_INDENT );
	fsOut.close( );
}
