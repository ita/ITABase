﻿#include <ITABase/ITAProgress.h>

ITABase::IProgressHandler::IProgressHandler( std::string sName /*= "unnamed" */ ) : m_sName( sName ) {}

ITABase::IProgressHandler::~IProgressHandler( ) {}

void ITABase::IProgressHandler::SetName( const std::string& sName )
{
	m_sName = sName;
}

std::string ITABase::IProgressHandler::GetName( ) const
{
	return m_sName;
}

void ITABase::IProgressHandler::SetSection( const std::string& sSection )
{
	m_sSection = sSection;
}

std::string ITABase::IProgressHandler::GetSection( ) const
{
	return m_sSection;
}

void ITABase::IProgressHandler::SetItem( const std::string& sItem )
{
	m_sItem = sItem;
}

std::string ITABase::IProgressHandler::GetItem( ) const
{
	return m_sItem;
}

void ITABase::IProgressHandler::PushProgressUpdate( std::string sSection, std::string sItem, float fProgressPercentage ) const
{
	m_sSection = sSection;
	PushProgressUpdate( sItem, fProgressPercentage );
}

void ITABase::IProgressHandler::PushProgressUpdate( std::string sItem, float fProgressPercentage ) const
{
	m_sItem = sItem;
	PushProgressUpdate( fProgressPercentage );
}
