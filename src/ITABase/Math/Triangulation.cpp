/*
 * ----------------------------------------------------------------
 *
 *		ITA core libs
 *		(c) Copyright Institute of Technical Acoustics (ITA)
 *		RWTH Aachen University, Germany, 2015-2024
 *
 * ----------------------------------------------------------------
 *				    ____  __________  _______
 *				   //  / //__   ___/ //  _   |
 *				  //  /    //  /    //  /_|  |
 *				 //  /    //  /    //  ___   |
 *				//__/    //__/    //__/   |__|
 *
 * ----------------------------------------------------------------
 *
 */

#include <ITABase/Math/Triangulation.h>
#include <ITAException.h>
#include <algorithm>
#include <math.h>

using namespace ITABase;
using namespace ITABase::Math;


/* Constructors */
CTriangulation::CTriangulation( const std::vector<VistaVector3D>& vv3LoudspeakerPos, const VistaVector3D& v3ReproductionCenterPos )
{
	ITA_EXCEPT_NOT_IMPLEMENTED;
}

CTriangulation::CTriangulation( const std::vector<VistaVector3D>& vv3LoudspeakerPos, const std::vector<VistaVector3D>& vv3TriangulationIndices,
                                const VistaVector3D& v3ReproductionCenterPos )
    : m_vv3TriangulationIndices( vv3TriangulationIndices )
    , m_v3CenterPosition( v3ReproductionCenterPos )
{
	VistaVector3D vReferencedLoudspeakerPos;
	for( int i = 0; i < vv3LoudspeakerPos.size( ); i++ )
	{
		vReferencedLoudspeakerPos = vv3LoudspeakerPos[i] - m_v3CenterPosition;
		// vReferencedLoudspeakerPos.Normalize();
		m_vv3LoudspeakerPos.push_back( vReferencedLoudspeakerPos );
	}

	// Construct triangles from given Triangulation
	VistaVector3D currentTriangle;
	for( int i = 0; i < m_vv3TriangulationIndices.size( ); i++ )
	{
		currentTriangle = m_vv3TriangulationIndices[i];
		m_vtTriangulation.push_back(
		    VistaTriangle( m_vv3LoudspeakerPos[currentTriangle[0]], m_vv3LoudspeakerPos[currentTriangle[1]], m_vv3LoudspeakerPos[currentTriangle[2]] ) );
	}
}

CTriangulation::CTriangulation( const std::vector<VistaVector3D>& vv3Loudspeaker, const std::vector<std::string>& vsID, const std::vector<int>& viID,
                                const std::vector<int>& viChannel, const VistaVector3D& vReproductionCenterPos )
    : m_vv3LoudspeakerPos( vv3Loudspeaker )
    , m_v3CenterPosition( vReproductionCenterPos )
{
	for( size_t i = 0; i < vv3Loudspeaker.size( ); i++ )
	{
		VAVec3 v3 { vv3Loudspeaker[i][0], vv3Loudspeaker[i][1], vv3Loudspeaker[i][2] };
		CLoudspeaker ls { vsID[i], viID[i], viChannel[i], v3 };
		m_voLoudspeaker.push_back( ls );
	}
	Triangulate( );
}

/* Destructor */
CTriangulation::~CTriangulation( ) {}


// ---------- Methods ----------
int CTriangulation::NumPoints( ) const
{
	return (int)m_vv3LoudspeakerPos.size( );
}

bool CTriangulation::GetPointIndicesFromTriangle( const VistaTriangle& vtTriangle, VistaVector3D& v3PointIndices ) const
{
	for( size_t i = 0; i < m_vtTriangulation.size( ); i++ )
	{
		if( vtTriangle.GetA( ) == m_vtTriangulation[i].GetA( ) && vtTriangle.GetB( ) == m_vtTriangulation[i].GetB( ) &&
		    vtTriangle.GetC( ) == m_vtTriangulation[i].GetC( ) )
			v3PointIndices = m_vv3TriangulationIndices[i];
		return true;
	}
	return false;
}

bool CTriangulation::IsDelaunayTriangulation( const float epsilon ) const
{
	VistaVector3D vCenter;
	float fRadius;
	bool bBelongsToTriangle;
	for( int i = 0; i < m_vtTriangulation.size( ); ++i )
	{
		fRadius = calculateCircumcenter3D( m_vtTriangulation[i], vCenter );

		for( int j = 0; j < m_vv3LoudspeakerPos.size( ); ++j )
		{
			bBelongsToTriangle = m_vv3TriangulationIndices[i][1] == j || m_vv3TriangulationIndices[i][2] == j || m_vv3TriangulationIndices[i][3] == j;
			if( ~bBelongsToTriangle && ( sphereEquation( vCenter, m_vv3LoudspeakerPos[j] ) - fRadius ) < -epsilon )
				return false;
		}
	}
	return true;
}

const VistaTriangle* CTriangulation::GetTriangleIntersectedByPointDirection( const VistaVector3D& vPoint, VistaVector3D& vBarycentricCoords, int& iTriangleIndex,
                                                                             const bool bIsReferenced, const float epsilon ) const
{
	// Reference point to center position
	VistaVector3D vPointRelative = bIsReferenced ? vPoint : vPoint - m_v3CenterPosition;

	// Get closest speaker
	double dCurrentDistance;
	double dMinDistance;
	int iClosestSpeaker = -1;
	for( size_t i = 0; i < m_vv3LoudspeakerPos.size( ); i++ )
	{
		dCurrentDistance = ( m_vv3LoudspeakerPos[i] - vPointRelative ).GetLength( );
		if( i == 0 || dCurrentDistance < dMinDistance )
		{
			dMinDistance    = dCurrentDistance;
			iClosestSpeaker = i;
		}
	}

	// Get triangles that contain closest speaker
	VistaTriangle* vtIntersectedTriangle;
	VistaVector3D vTriangleNormal;
	for( size_t i = 0; i < m_vtTriangulation.size( ); i++ )
	{
		if( m_vv3TriangulationIndices[i][0] == iClosestSpeaker || m_vv3TriangulationIndices[i][1] == iClosestSpeaker ||
		    m_vv3TriangulationIndices[i][2] == iClosestSpeaker )
		{
			float bary_a = 1, bary_b, bary_c;
			// Scaling factor that projects vPoint (P) onto the plane spanned by the triangle (A, B, C)
			// Assume n to be the plane's normal vector and P' = r * P to be the projection of P onto that plane, then
			// (P' - A) * n = 0  <==>  (r*P - A) * n = 0  <==>  r*P*n - A*n = 0  <==>  r = A*n / P*n  (Also works with B or C)
			vTriangleNormal = m_vtTriangulation[i].Normal( );
			float r         = m_vtTriangulation[i].GetA( ).Dot( vTriangleNormal ) / vPointRelative.Dot( vTriangleNormal );
			if( r < 0 )
				continue;

			m_vtTriangulation[i].IntersectionPoint( r * vPointRelative, bary_b, bary_c, epsilon );
			bary_a -= bary_b + bary_c;
			if( bary_a > -epsilon && bary_b > -epsilon && bary_c > -epsilon )
			{
				bary_a = std::abs( bary_a ) < epsilon ? 0 : std::abs( bary_a );
				bary_b = std::abs( bary_b ) < epsilon ? 0 : std::abs( bary_b );
				bary_c = std::abs( bary_c ) < epsilon ? 0 : std::abs( bary_c );

				vBarycentricCoords.SetValues( bary_a, bary_b, bary_c );
				iTriangleIndex = (int)i;
				return &m_vtTriangulation[i];
			}
		}
	}
	vBarycentricCoords.SetToZeroVector( );
	iTriangleIndex = -1;
	return nullptr;
}

const VistaTriangle* CTriangulation::GetTriangleIntersectedByPointDirection( const VistaVector3D& vPoint, VistaVector3D& vBarycentricCoords, const bool bIsReferenced,
                                                                             const float epsilon ) const
{
	int iTriangleIndex;
	return GetTriangleIntersectedByPointDirection( vPoint, vBarycentricCoords, iTriangleIndex, bIsReferenced, epsilon );
}

const VistaTriangle* CTriangulation::GetTriangleIntersectedByPointDirection( const VistaVector3D& vPoint, const bool bIsReferenced, const float epsilon ) const
{
	VistaVector3D vBarycentricCoords;
	return GetTriangleIntersectedByPointDirection( vPoint, vBarycentricCoords, bIsReferenced, epsilon );
}

// //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// OLD STUFF
std::vector<VistaVector3D> projection3Dto2D( std::vector<VistaVector3D> points3D )
{
	std::vector<VistaVector3D> points2D;
	points2D.reserve( points3D.size( ) );

	for( int i = 0; i < points3D.size( ); i++ )
	{
		points2D.insert( points2D.end( ), VistaVector3D( points3D[i][0] / ( 1 - points3D[i][2] ), points3D[i][1] / ( 1 - points3D[i][2] ), 0, 0 ) );
	}
	return points2D;
}

// returns array with [0] = max x value and [1] with max y value
// searches the max of the absolute value
double* calculateMaximumXandY( std::vector<VistaVector3D> points, int iAmountOfPoints )
{
	double max[2];
	max[0] = 0; // x Value
	max[1] = 0; // y Value

	for( int i = 0; i < iAmountOfPoints; i++ )
	{
		if( max[0] < std::abs( points[i][0] ) )
		{
			max[0] = std::abs( points[i][0] );
		}
		if( max[1] < std::abs( points[i][1] ) )
		{
			max[1] = std::abs( points[i][1] );
		}
	}
	return max;
}

bool isPointInLineSegment( VistaVector3D point, VistaVector3D lineA, VistaVector3D lineB )
{
	float crossproduct = ( point[1] - lineA[1] ) * ( lineB[0] - lineA[0] ) - ( point[0] - lineA[0] ) * ( lineB[1] - lineA[1] );

	// compare versus epsilon for floating point values, or != 0 if using integers
	float epsilon = 0.1;
	if( abs( crossproduct ) > epsilon )
	{
		return false;
	}

	float dotproduct = ( point[0] - lineA[0] ) * ( lineB[0] - lineA[0] ) + ( point[1] - lineA[1] ) * ( lineB[1] - lineA[1] );
	if( dotproduct < 0 )
	{
		return false;
	}

	float squaredlengthba = pow( ( lineB[0] - lineA[0] ), 2 ) + pow( ( lineB[1] - lineA[1] ), 2 );
	if( dotproduct > squaredlengthba )
	{
		return false;
	}

	return true;
}

bool isSameTriangle( VistaTriangle triangle1, VistaTriangle triangle2 )
{
	if( triangle1.GetA( ) == triangle2.GetA( ) )
	{
		if( triangle1.GetB( ) == triangle2.GetB( ) )
		{
			if( triangle1.GetC( ) == triangle2.GetC( ) )
			{
				return true;
			}
			else
				return false;
		}
		else if( triangle1.GetB( ) == triangle2.GetC( ) )
		{
			if( triangle1.GetC( ) == triangle2.GetB( ) )
			{
				return true;
			}
			else
				return false;
		}
		else
			return false;
	}

	else if( triangle1.GetA( ) == triangle2.GetB( ) )
	{
		if( triangle1.GetB( ) == triangle2.GetA( ) )
		{
			if( triangle1.GetC( ) == triangle2.GetC( ) )
			{
				return true;
			}
			else
				return false;
		}
		else if( triangle1.GetB( ) == triangle2.GetC( ) )
		{
			if( triangle1.GetC( ) == triangle2.GetA( ) )
			{
				return true;
			}
			else
				return false;
		}
		else
			return false;
	}

	else if( triangle1.GetA( ) == triangle2.GetC( ) )
	{
		if( triangle1.GetB( ) == triangle2.GetA( ) )
		{
			if( triangle1.GetC( ) == triangle2.GetB( ) )
			{
				return true;
			}
			else
				return false;
		}
		else if( triangle1.GetB( ) == triangle2.GetB( ) )
		{
			if( triangle1.GetC( ) == triangle2.GetA( ) )
			{
				return true;
			}
			else
				return false;
		}
		else
			return false;
	}
}

// returns 0 if there are no two triangles which are the same
VistaVector3D* whichPointIsDifferent( VistaTriangle vtA, VistaTriangle vtB )
{
	VistaVector3D vvDifferingPoint[2];

	if( !( vtA.GetA( ) == vtB.GetA( ) || vtA.GetA( ) == vtB.GetB( ) || vtA.GetA( ) == vtB.GetC( ) ) )
	{
		vvDifferingPoint[0] = vtA.GetA( );
	}
	else if( !( vtA.GetB( ) == vtB.GetA( ) || vtA.GetB( ) == vtB.GetB( ) || vtA.GetB( ) == vtB.GetC( ) ) )
	{
		vvDifferingPoint[0] = vtA.GetB( );
	}
	else if( !( vtA.GetC( ) == vtB.GetA( ) || vtA.GetC( ) == vtB.GetB( ) || vtA.GetC( ) == vtB.GetC( ) ) )
	{
		vvDifferingPoint[0] = vtA.GetC( );
	}


	if( !( vtB.GetA( ) == vtA.GetA( ) || vtB.GetA( ) == vtA.GetB( ) || vtB.GetA( ) == vtA.GetC( ) ) )
	{
		vvDifferingPoint[1] = vtB.GetA( );
	}
	else if( !( vtB.GetB( ) == vtA.GetA( ) || vtB.GetB( ) == vtA.GetB( ) || vtB.GetB( ) == vtA.GetC( ) ) )
	{
		vvDifferingPoint[1] = vtB.GetB( );
	}
	else if( !( vtB.GetC( ) == vtA.GetA( ) || vtB.GetC( ) == vtA.GetB( ) || vtB.GetC( ) == vtA.GetC( ) ) )
	{
		vvDifferingPoint[1] = vtB.GetC( );
	}

	return vvDifferingPoint;

	/*

	if (vtA.GetA() == vtB.GetA()) {
	    if (vtA.GetB() == vtB.GetB()) {
	        return vtB.GetC();
	    }
	    else if (vtA.GetB() == vtB.GetC()) {
	        return vtB.GetC();
	    }
	}
	else if (vtA.GetA() == vtB.GetB()) {
	    if (vtA.GetB() == vtB.GetA()) {
	        return vtB.GetC();
	    }
	    else if (vtA.GetB() == vtB.GetC()) {
	        return vtB.GetA();
	    }
	}
	else if (vtA.GetA() == vtB.GetC()) {
	    if (vtA.GetB() == vtB.GetB()) {
	        return vtB.GetA();
	    }
	    else if (vtA.GetB() == vtB.GetA()) {
	        return vtB.GetB();
	    }
	}
	return VistaVector3D(0, 0, 0); //If triangles dont share 2 points which are the same
	*/
}

double calculateRadiusOfTriangle( VistaTriangle triangle )
{
	VistaVector3D a = triangle.GetA( );
	VistaVector3D b = triangle.GetB( );
	VistaVector3D c = triangle.GetC( );

	VistaLineSegment lsAB = VistaLineSegment( a, b );
	VistaLineSegment lsAC = VistaLineSegment( a, c );
	VistaLineSegment lsBC = VistaLineSegment( b, c );

	double dLengthAB = lsAB.GetLength( );
	double dLengthAC = lsAC.GetLength( );
	double dLengthBC = lsBC.GetLength( );


	double dHelpVar =
	    ( dLengthAB + dLengthAC + dLengthBC ) * ( dLengthAC + dLengthBC - dLengthAB ) * ( dLengthBC + dLengthAB - dLengthAC ) * ( dLengthAB + dLengthAC - dLengthBC );

	double radius = ( dLengthAB * dLengthAC * dLengthBC ) / ( sqrt( dHelpVar ) );
	return radius;
}

// https://de.wikipedia.org/wiki/Umkreis
// Umkreismittelpunkt
// Circumcenter
VistaVector3D calculateCenterOfTriangle( VistaTriangle triangle )
{
	VistaVector3D a = triangle.GetA( );
	VistaVector3D b = triangle.GetB( );
	VistaVector3D c = triangle.GetC( );

	float fd = 2 * ( a[0] * ( b[1] - c[1] ) + b[0] * ( c[1] - a[1] ) + c[0] * ( a[1] - b[1] ) );

	float fXY1 = ( pow( a[0], 2 ) + pow( a[1], 2 ) );
	float fXY2 = ( pow( b[0], 2 ) + pow( b[1], 2 ) );
	float fXY3 = ( pow( c[0], 2 ) + pow( c[1], 2 ) );


	float fXvalue = ( fXY1 * ( b[1] - c[1] ) + fXY2 * ( c[1] - a[1] ) + fXY3 * ( a[1] - b[1] ) ) / fd;


	float fYvalue = ( fXY1 * ( c[0] - b[0] ) + fXY2 * ( a[0] - c[0] ) + fXY3 * ( b[0] - a[0] ) ) / fd;


	return VistaVector3D( fXvalue, fYvalue, 0 );
}

std::vector<VistaTriangle> restoreDelaunay( std::vector<VistaTriangle> triangulation, int iRecentTriangle, std::vector<VistaVector3D> points, int iViolatingPoint )
{
	if( triangulation[iRecentTriangle].GetA( ) == points[iViolatingPoint] || triangulation[iRecentTriangle].GetB( ) == points[iViolatingPoint] ||
	    triangulation[iRecentTriangle].GetC( ) == points[iViolatingPoint] )
	{
		return triangulation;
	}


	// find triangle that includes the violating point and points of longest edge
	//--> edge flip is performed on two triangles
	/*


	    1*----------3*
	    |  \         |
	     \      \     \   violates delaunay. Edge flip to:
	      \        \   \
	       2*----------4*
	    1*-------- 3*
	     \      /  \
	      \    /    \
	       \  /      \
	        2*-------- 4*



*/
	for( int i = 0; i < triangulation.size( ); i++ )
	{
		if( isSameTriangle( VistaTriangle( triangulation[iRecentTriangle].GetA( ), triangulation[iRecentTriangle].GetB( ), points[iViolatingPoint] ), triangulation[i] ) )
		{
			// the found triangle has to be changed now
			triangulation[i].SetA( triangulation[iRecentTriangle].GetA( ) );
			triangulation[i].SetB( triangulation[iRecentTriangle].GetC( ) );
			triangulation[i].SetC( points[iViolatingPoint] );

			// change recent triangle as well to get guilty triangulation
			triangulation[iRecentTriangle].SetA( points[iViolatingPoint] );
			// triangulation[iRecentTriangle].SetB(triangulation[iRecentTriangle].GetB());
			// triangulation[iRecentTriangle].SetC(triangulation[iRecentTriangle].GetC());
			// Edge Flipped. Delaunay restored for these two triangles.

			break; // triangle found. For-loop can be ended and altered triangulation is returned
		}
		else if( isSameTriangle( VistaTriangle( triangulation[iRecentTriangle].GetA( ), triangulation[iRecentTriangle].GetC( ), points[iViolatingPoint] ),
		                         triangulation[i] ) )
		{
			// the found triangle has to be changed now
			triangulation[i].SetA( triangulation[iRecentTriangle].GetA( ) );
			triangulation[i].SetB( triangulation[iRecentTriangle].GetB( ) );
			triangulation[i].SetC( points[iViolatingPoint] );

			// change recent triangle as well to get guilty triangulation
			triangulation[iRecentTriangle].SetA( points[iViolatingPoint] );
			// triangulation[iRecentTriangle].SetB(triangulation[iRecentTriangle].GetB());
			// triangulation[iRecentTriangle].SetC(triangulation[iRecentTriangle].GetC());
			// Edge Flipped. Delaunay restored for these two triangles.

			break; // triangle found. For-loop can be ended and altered triangulation is returned
		}
		else if( isSameTriangle( VistaTriangle( triangulation[iRecentTriangle].GetC( ), triangulation[iRecentTriangle].GetB( ), points[iViolatingPoint] ),
		                         triangulation[i] ) )
		{
			// the found triangle has to be changed now
			triangulation[i].SetA( triangulation[iRecentTriangle].GetA( ) );
			triangulation[i].SetB( triangulation[iRecentTriangle].GetC( ) );
			triangulation[i].SetC( points[iViolatingPoint] );

			// change recent triangle as well to get guilty triangulation
			// triangulation[iRecentTriangle].SetA(triangulation[iRecentTriangle].GetA());
			// triangulation[iRecentTriangle].SetB(triangulation[iRecentTriangle].GetB());
			triangulation[iRecentTriangle].SetC( points[iViolatingPoint] );
			// Edge Flipped. Delaunay restored for these two triangles.

			break; // triangle found. For-loop can be ended and altered triangulation is returned
		}
	}

	return triangulation;
}

std::vector<VistaTriangle> removeDuplicateTriangles( std::vector<VistaTriangle> triangulation )
{
	// check for duplicate triangles.
	for( int q = 0; q < triangulation.size( ); q++ )
	{
		for( int j = 0; j < q; j++ )
		{
			if( isSameTriangle( triangulation[j], triangulation[q] ) )
			{
				triangulation[q].SetA( VistaVector3D( 0, 0, 0 ) );
				triangulation[q].SetB( VistaVector3D( 0, 0, 0 ) );
				triangulation[q].SetC( VistaVector3D( 0, 0, 0 ) );
			}
		}

	} // end for (check all triangles)

	std::vector<VistaTriangle> triangulationClean;
	triangulationClean.reserve( triangulation.size( ) );
	// Shrink list to only contain legit triangles not (0,0,0); --> without nodes of bounding triangle
	for( int r = 0; r < triangulation.size( ); r++ )
	{
		if( isSameTriangle( triangulation[r], VistaTriangle( VistaVector3D( 0, 0, 0 ), VistaVector3D( 0, 0, 0 ), VistaVector3D( 0, 0, 0 ) ) ) )
		{
			continue;
		}

		triangulationClean.insert( triangulationClean.end( ), triangulation[r] );
	}


	return triangulationClean;
}

// BE CAREFUL TO SET THIRD COORDINATE OF POINTS = 0 IN 2D
std::vector<VistaTriangle> calculateDelaunay( std::vector<VistaVector3D> points )
{
	int iAmountOfPoints = points.size( );


	// build a bounding triangle around the pointcloud to build the delaunay trianglulation incrementally

	// calculate the maximum x and the maximum y value
	double iMaxX = calculateMaximumXandY( points, iAmountOfPoints )[0];
	double iMaxY = calculateMaximumXandY( points, iAmountOfPoints )[1];
	double iMax  = std::max( iMaxX, iMaxY );

	// use maximal values to build the three nodes of the bounding triangle
	VistaVector3D vP_x = VistaVector3D( 3 * iMax, 0, 0 );
	VistaVector3D vP_y = VistaVector3D( 0, 3 * iMax, 0 );
	VistaVector3D vP_z = VistaVector3D( -3 * iMax, -3 * iMax, 0 );

	VistaTriangle vBoundingTriangle = VistaTriangle( vP_x, vP_y, vP_z );


	// Datastructure for all traingles of the triangulation
	std::vector<VistaTriangle> triangulation;
	triangulation.reserve( 9 * iAmountOfPoints + 1 );
	triangulation.insert( triangulation.begin( ), vBoundingTriangle );


	// iterate over all points and insert them into triangulation
	for( int i = 0; i < iAmountOfPoints; i++ )
	{
		// iterate over all triangles in triangulation to insert point
		for( int j = 0; j < triangulation.size( ); j++ )
		{
			VistaVector3D ContactPoint; // Will be defined/necessary  in IntersectionTriangle

			// Intersection between point and triangle has to be made with a upwards facing lineSegment.
			VistaLineSegment point = VistaLineSegment( VistaVector3D( points[i][0], points[i][1], 1 ), VistaVector3D( points[i][0], points[i][1], -1 ) );


			if( point.IntersectionTriangle( triangulation[j].GetA( ), triangulation[j].GetB( ), triangulation[j].GetC( ), ContactPoint ) )
			{
				// if the new point lies exactly on the edge between two nodes
				// Then it would be correct to not split into three, but four triangles
				if( isPointInLineSegment( ContactPoint, triangulation[j].GetA( ), triangulation[j].GetB( ) ) ||
				    isPointInLineSegment( ContactPoint, triangulation[j].GetA( ), triangulation[j].GetC( ) ) ||
				    isPointInLineSegment( ContactPoint, triangulation[j].GetB( ), triangulation[j].GetC( ) ) ||

				    isPointInLineSegment( ContactPoint, triangulation[j].GetB( ), triangulation[j].GetA( ) ) ||
				    isPointInLineSegment( ContactPoint, triangulation[j].GetC( ), triangulation[j].GetA( ) ) ||
				    isPointInLineSegment( ContactPoint, triangulation[j].GetC( ), triangulation[j].GetB( ) ) )
				{
					VistaVector3D ContactPoint2; // Will be defined/necessary in IntersectionTriangle
					for( int indexSecondTriangle = 0; indexSecondTriangle < triangulation.size( ); indexSecondTriangle++ )
					{
						if( isSameTriangle( triangulation[j], triangulation[indexSecondTriangle] ) )
						{
							continue;
						}
						if( point.IntersectionTriangle( triangulation[indexSecondTriangle].GetA( ), triangulation[indexSecondTriangle].GetB( ),
						                                triangulation[indexSecondTriangle].GetC( ), ContactPoint2 ) )
						{
							VistaVector3D vvDifferingPointFirstTriangle  = whichPointIsDifferent( triangulation[j], triangulation[indexSecondTriangle] )[0];
							VistaVector3D vvDifferingPointSecondTriangle = whichPointIsDifferent( triangulation[j], triangulation[indexSecondTriangle] )[1];
							if( isPointInLineSegment( ContactPoint, triangulation[j].GetA( ), triangulation[j].GetB( ) ) )
							{
								// Add two new triangles
								triangulation.insert( triangulation.end( ), VistaTriangle( points[i], triangulation[j].GetA( ), vvDifferingPointFirstTriangle ) );
								triangulation.insert( triangulation.end( ), VistaTriangle( points[i], triangulation[j].GetB( ), vvDifferingPointFirstTriangle ) );
								// Change existing
								triangulation[indexSecondTriangle] = VistaTriangle( points[i], triangulation[j].GetA( ), vvDifferingPointSecondTriangle );
								triangulation[j]                   = VistaTriangle( points[i], triangulation[j].GetB( ), vvDifferingPointSecondTriangle );
							}
							else if( isPointInLineSegment( ContactPoint, triangulation[j].GetA( ), triangulation[j].GetC( ) ) )
							{
								// Add two new triangles
								triangulation.insert( triangulation.end( ), VistaTriangle( points[i], triangulation[j].GetA( ), vvDifferingPointFirstTriangle ) );
								triangulation.insert( triangulation.end( ), VistaTriangle( points[i], triangulation[j].GetC( ), vvDifferingPointFirstTriangle ) );
								// Change existing
								triangulation[indexSecondTriangle] = VistaTriangle( points[i], triangulation[j].GetA( ), vvDifferingPointSecondTriangle );
								triangulation[j]                   = VistaTriangle( points[i], triangulation[j].GetC( ), vvDifferingPointSecondTriangle );
							}
							else if( isPointInLineSegment( ContactPoint, triangulation[j].GetB( ), triangulation[j].GetC( ) ) )
							{
								// Add two new triangles
								triangulation.insert( triangulation.end( ), VistaTriangle( points[i], triangulation[j].GetC( ), vvDifferingPointFirstTriangle ) );
								triangulation.insert( triangulation.end( ), VistaTriangle( points[i], triangulation[j].GetB( ), vvDifferingPointFirstTriangle ) );
								// Change existing
								triangulation[indexSecondTriangle] = VistaTriangle( points[i], triangulation[j].GetC( ), vvDifferingPointSecondTriangle );
								triangulation[j]                   = VistaTriangle( points[i], triangulation[j].GetB( ), vvDifferingPointSecondTriangle );
							}


							break;
						}
					}
				}
				else
				{
					// connect point with every outer node of the intersected triangle
					triangulation.insert( triangulation.end( ), VistaTriangle( points[i], triangulation[j].GetA( ), triangulation[j].GetB( ) ) );
					triangulation.insert( triangulation.end( ), VistaTriangle( points[i], triangulation[j].GetA( ), triangulation[j].GetC( ) ) );
					triangulation[j] = VistaTriangle( points[i], triangulation[j].GetB( ), triangulation[j].GetC( ) );
				}

				//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
				// Check if delaunay criteria are fullfilled

				double dRadiusOfCircumcircle;
				VistaVector3D vvCenterOfTriangle;
				bool bDelaunayWasViolated = false;
				int iAmountOfTriangles    = triangulation.size( );


				// loop thorugh the 3 new triangles
				//(the delaunay property can only be violated there, since the structure has to be delaunay in every iteration per definition)
				// first point (m==0) has to be dealt with individually, because the triangle's position is not at the end of the list <triangulation>
				for( int m = 0; m < 3; m++ )
				{
					double dDistanceToPoint;
					if( m == 0 )
					{
						// loop through all three points which have been included so far
						for( int k = 0; k <= i; k++ )
						{
							// skip if point is one of the triangle's nodes
							if( triangulation[j].GetA( ) == points[k] || triangulation[j].GetB( ) == points[k] || triangulation[j].GetC( ) == points[k] )
							{
								continue;
							}

							// build circumcirle of triangle with radius and center of triangle's circumcenter (Umkreismittelpunkt)
							dRadiusOfCircumcircle = calculateRadiusOfTriangle( triangulation[j] );
							vvCenterOfTriangle    = calculateCenterOfTriangle( triangulation[j] );

							dDistanceToPoint = ( vvCenterOfTriangle - points[k] ).GetLength( );

							// Check if delaunay property is violated
							if( ceil( dRadiusOfCircumcircle * 10 ) > ceil( dDistanceToPoint * 10 ) )
							{
								bDelaunayWasViolated = true;
								triangulation        = restoreDelaunay( triangulation, j, points, k );
							}
						}
					}
					else
					{
						for( int k = 0; k <= i; k++ )
						{
							// skip if point is one of the triangle's nodes
							if( triangulation[iAmountOfTriangles - m].GetA( ) == points[k] || triangulation[iAmountOfTriangles - m].GetB( ) == points[k] ||
							    triangulation[iAmountOfTriangles - m].GetC( ) == points[k] )
							{
								continue;
							}

							// build circumcirle of triangle with radius and center of triangle
							dRadiusOfCircumcircle = calculateRadiusOfTriangle( triangulation[iAmountOfTriangles - m] ); // m=2 --> second to last
							vvCenterOfTriangle =
							    calculateCenterOfTriangle( triangulation[iAmountOfTriangles - m] ); // m=1 --> last (both new triangles added with last point)

							dDistanceToPoint = ( vvCenterOfTriangle - points[k] ).GetLength( );

							// Check if delaunay property is violated
							if( ceil( dRadiusOfCircumcircle * 10 ) > ceil( dDistanceToPoint * 10 ) )
							{
								bDelaunayWasViolated = true;
								triangulation        = restoreDelaunay( triangulation, ( iAmountOfTriangles - m ), points, k );
							}
						}
					}
				} // end for (check new triangles)


				if( bDelaunayWasViolated )
				{
					// ToDo: Make it more efficient; Only search neighbouring triangles; At the moment: Iterates thorugh ALL triangles

					for( int n = 0; n < triangulation.size( ); n++ )
					{
						// loop through all points which have been included so far
						for( int p = 0; p <= i; p++ )
						{
							// skip if point is one of the triangle's nodes
							if( triangulation[n].GetA( ) == points[p] || triangulation[n].GetB( ) == points[p] || triangulation[n].GetC( ) == points[p] )
							{
								continue;
							}

							// build circumcirle of triangle with radius and center of triangle
							dRadiusOfCircumcircle = calculateRadiusOfTriangle( triangulation[n] );
							vvCenterOfTriangle    = calculateCenterOfTriangle( triangulation[n] );

							vvCenterOfTriangle[3]          = 0;
							VistaLineSegment centerToPoint = VistaLineSegment( vvCenterOfTriangle, points[p] );
							double dDistanceToPoint        = centerToPoint.GetLength( );

							// Check if delaunay property is violated
							if( ceil( dRadiusOfCircumcircle * 10 ) > ceil( dDistanceToPoint * 10 ) )
							{
								triangulation = restoreDelaunay( triangulation, n, points, p );


								n = 0;
								break;
							}
						}

					} // end for (check all triangles)
					bDelaunayWasViolated = false;
				}
				break; // do not search new triangles if one was already split
			}          // end if (inserted point lays in triangle)
		}
	}


	// remove all triangles with connections to the bounding triangle.
	for( int q = 0; q < triangulation.size( ); q++ )
	{
		if( triangulation[q].GetA( ) == vP_x || triangulation[q].GetB( ) == vP_x || triangulation[q].GetC( ) == vP_x || triangulation[q].GetA( ) == vP_y ||
		    triangulation[q].GetB( ) == vP_y || triangulation[q].GetC( ) == vP_y || triangulation[q].GetA( ) == vP_z || triangulation[q].GetB( ) == vP_z ||
		    triangulation[q].GetC( ) == vP_z )
		{
			triangulation[q].SetA( VistaVector3D( 0, 0, 0 ) );
			triangulation[q].SetB( VistaVector3D( 0, 0, 0 ) );
			triangulation[q].SetC( VistaVector3D( 0, 0, 0 ) );
		}


	} // end for (check all triangles)


	/*
	std::vector<VistaTriangle> triangulationClean;
	triangulationClean.reserve(triangulation.size());
	//Shrink list to only contain legit triangles not (0,0,0); --> without nodes of bounding triangle
	for (int r = 0; r < triangulation.size(); r++) {
	    if (isSameTriangle(triangulation[r], VistaTriangle(VistaVector3D(0, 0, 0), VistaVector3D(0, 0, 0), VistaVector3D(0, 0, 0)))) {
	        continue;
	    }


	    triangulationClean.insert(triangulationClean.end(), triangulation[r]);

	}



	return triangulationClean;
	*/
	return removeDuplicateTriangles( triangulation );
}

// Input: triangulation with speakerpos as coordinates in a triangle <vec>
//		speakerPos as <vec> sorted as speaker 0 = speaker[0]; speaker 1 = speaker[1] etc.
//		IMPORTANT: SpeakerPos still needs to be projected to 2D to match the triangulation's list
// Output: <vec> with VistaVectors. Each vector represents a triangle in the triangulation; stored are the indicies of speakers
std::vector<VistaVector3D> transformTriangulationToSpeakerList( std::vector<VistaVector3D> speaker, std::vector<VistaTriangle> triangulation )
{
	// Datastructure for transformed triangulation
	std::vector<VistaVector3D> speakerTriangulation;
	speakerTriangulation.reserve( triangulation.size( ) );


	// Do the Magic: Map SpeakerPos to SpeakerID
	for( int i = 0; i < triangulation.size( ); i++ )
	{
		speakerTriangulation.insert( speakerTriangulation.end( ), VistaVector3D( 0, 0, 0 ) );
		for( int k = 0; k < 3; k++ )
		{
			for( int l = 0; l < speaker.size( ); l++ )
			{
				switch( k )
				{
					case 0:
						if( triangulation[i].GetA( ) == speaker[l] )
						{
							speakerTriangulation[i][0] = l;
						}
						break;
					case 1:
						if( triangulation[i].GetB( ) == speaker[l] )
						{
							speakerTriangulation[i][1] = l;
						}
						break;
					case 2:
						if( triangulation[i].GetC( ) == speaker[l] )
						{
							speakerTriangulation[i][2] = l;
						}
						break;
					default:
						continue;
				}
			}
		}
	}
	return speakerTriangulation;
}

// hole in Triangulation is at the north pole in a sphere. Seach the four points furthest north (greatest z value). They build two triangles
std::vector<VistaTriangle> fillHole( std::vector<VistaTriangle> triangulation, std::vector<VistaVector3D> points )
{
	if( points.size( ) < 5 )
	{
		return triangulation;
	}


	std::vector<double> zCoordinates;
	zCoordinates.reserve( points.size( ) );

	zCoordinates.insert( zCoordinates.end( ), points[0][2] );
	for( int indexPoints = 0; indexPoints < points.size( ); indexPoints++ )
	{
		zCoordinates.insert( zCoordinates.end( ), points[indexPoints][2] );
	}

	sort( zCoordinates.begin( ), zCoordinates.end( ) );

	// Node 1 is a rotated point to 0,0,1; this has to be connected to the 4 adjacent points
	// Error: fails if there is a faulty order of height in the adjacent points. Check shift in x and y direction to choose points
	VistaVector3D node1;
	VistaVector3D node2;
	VistaVector3D node3;
	VistaVector3D node4;
	VistaVector3D node5;
	for( int indexPoints = 0; indexPoints < points.size( ); indexPoints++ )
	{
		if( points[indexPoints][2] == zCoordinates[zCoordinates.size( ) - 1] )
		{
			node1 = points[indexPoints];
		}
		else if( points[indexPoints][2] == zCoordinates[zCoordinates.size( ) - 2] )
		{
			node2 = points[indexPoints];
		}
		else if( points[indexPoints][2] == zCoordinates[zCoordinates.size( ) - 3] )
		{
			node3 = points[indexPoints];
		}
		else if( points[indexPoints][2] == zCoordinates[zCoordinates.size( ) - 4] )
		{
			node4 = points[indexPoints];
		}
		else if( points[indexPoints][2] == zCoordinates[zCoordinates.size( ) - 5] )
		{
			node5 = points[indexPoints];
		}
	}


	VistaLineSegment vlNode2to3 = VistaLineSegment( node2, node3 );
	VistaLineSegment vlNode2to4 = VistaLineSegment( node2, node4 );
	VistaLineSegment vlNode2to5 = VistaLineSegment( node2, node5 );
	double dDistanceNode2to3    = vlNode2to3.GetLength( );
	double dDistanceNode2to4    = vlNode2to4.GetLength( );
	double dDistanceNode2to5    = vlNode2to5.GetLength( );

	// Check if delaunay property is violated
	if( dDistanceNode2to3 >= dDistanceNode2to4 && dDistanceNode2to3 >= dDistanceNode2to5 )
	{
		triangulation.insert( triangulation.end( ), VistaTriangle( node1, node2, node4 ) );
		triangulation.insert( triangulation.end( ), VistaTriangle( node1, node2, node5 ) );

		triangulation.insert( triangulation.end( ), VistaTriangle( node1, node3, node4 ) );
		triangulation.insert( triangulation.end( ), VistaTriangle( node1, node3, node5 ) );
	}
	else if( dDistanceNode2to4 >= dDistanceNode2to3 && dDistanceNode2to4 >= dDistanceNode2to5 )
	{
		triangulation.insert( triangulation.end( ), VistaTriangle( node1, node2, node3 ) );
		triangulation.insert( triangulation.end( ), VistaTriangle( node1, node2, node5 ) );

		triangulation.insert( triangulation.end( ), VistaTriangle( node1, node4, node3 ) );
		triangulation.insert( triangulation.end( ), VistaTriangle( node1, node4, node5 ) );
	}
	else if( dDistanceNode2to5 >= dDistanceNode2to3 && dDistanceNode2to5 >= dDistanceNode2to4 )
	{
		triangulation.insert( triangulation.end( ), VistaTriangle( node1, node2, node3 ) );
		triangulation.insert( triangulation.end( ), VistaTriangle( node1, node2, node4 ) );

		triangulation.insert( triangulation.end( ), VistaTriangle( node1, node5, node3 ) );
		triangulation.insert( triangulation.end( ), VistaTriangle( node1, node5, node4 ) );
	}

	/*
	//check for duplicate triangles.
	for (int q = 0; q < triangulation.size(); q++) {

	    for (int j = 0; j < q; j++) {
	        if (isSameTriangle(triangulation[j], triangulation[q])) {
	            triangulation[q].SetA(VistaVector3D(0, 0, 0));
	            triangulation[q].SetB(VistaVector3D(0, 0, 0));
	            triangulation[q].SetC(VistaVector3D(0, 0, 0));
	        }
	    }

	} //end for (check all triangles)

	std::vector<VistaTriangle> triangulationClean;
	triangulationClean.reserve(triangulation.size());
	//Shrink list to only contain legit triangles not (0,0,0); --> without nodes of bounding triangle
	for (int r = 0; r < triangulation.size(); r++) {
	    if (isSameTriangle(triangulation[r], VistaTriangle(VistaVector3D(0, 0, 0), VistaVector3D(0, 0, 0), VistaVector3D(0, 0, 0)))) {
	        continue;
	    }
	    triangulationClean.insert(triangulationClean.end(), triangulation[r]);
	}



	return triangulationClean;
	*/
	return removeDuplicateTriangles( triangulation );
}

void CTriangulation::Triangulate( )
{
	// Store Position of Speakers as points to calculate the triangulation

	std::vector<VistaVector3D> vvUnitSpeakerList;
	VistaVector3D speakerHelp;
	for( size_t i = 0; i < m_voLoudspeaker.size( ); i++ )
	{
		// Calculate position of loudspeaker from virtual sound source
		speakerHelp[0] = m_voLoudspeaker[i].pos.x;
		speakerHelp[1] = ( m_voLoudspeaker[i].pos.y - 1.34 ); // center points around (0,0,0) Speakerpos usually center around
		speakerHelp[2] = m_voLoudspeaker[i].pos.z;
		speakerHelp.Normalize( ); // Normalize vector to shift SpeakerPos >> they should now build a unit sphere around (0,0,0)
		vvUnitSpeakerList.insert( vvUnitSpeakerList.end( ), speakerHelp );
	}


	// rotate so that last point in array is on the south pole (0, 0, -1)

	// calculate necessary rotation
	VistaVector3D vvLastSpeaker = vvUnitSpeakerList[0];

	VistaQuaternion qRot( vvLastSpeaker, VistaVector3D( 0, 0, -1 ) );

	// rotate unit sphere so that the last point is in the south pole
	std::vector<VistaVector3D> vvSpeakerList;
	for( size_t indexSpeaker = 0; indexSpeaker < vvUnitSpeakerList.size( ); indexSpeaker++ )
	{
		vvSpeakerList.insert( vvSpeakerList.end( ), qRot.Rotate( vvUnitSpeakerList[indexSpeaker] ) );
	}


	// project speaker sphere (3D) to a 2D circle
	std::vector<VistaVector3D> vvPositionSpeaker2D = projection3Dto2D( vvSpeakerList );


	// perform triangulation of points
	std::vector<VistaTriangle> triangulation = calculateDelaunay( vvPositionSpeaker2D );


	// create triangulation based on speaker id; There will be a hole in the sphere in the north pole
	m_vv3TriangulationIndices = transformTriangulationToSpeakerList( vvPositionSpeaker2D, triangulation );

	// map triangulation to the corresponding speakers
	std::vector<VistaTriangle> projectionTriangulation;
	for( int l = 0; l < m_vv3TriangulationIndices.size( ); l++ )
	{
		projectionTriangulation.insert( projectionTriangulation.end( ), VistaTriangle( VistaVector3D( 0, 0, 0 ), VistaVector3D( 0, 0, 0 ), VistaVector3D( 0, 0, 0 ) ) );

		projectionTriangulation[l].SetA( vvSpeakerList[m_vv3TriangulationIndices[l][0]] );
		projectionTriangulation[l].SetB( vvSpeakerList[m_vv3TriangulationIndices[l][1]] );
		projectionTriangulation[l].SetC( vvSpeakerList[m_vv3TriangulationIndices[l][2]] );
	}

	// due to the construction of the triangle (traingulation in 2d) the sphere will have a hole in the north pole. The missing edges have to be added manually
	projectionTriangulation = fillHole( projectionTriangulation, vvSpeakerList );

	// create triangulation based on speaker id; this time without hole
	m_vv3TriangulationIndices = transformTriangulationToSpeakerList( vvSpeakerList, projectionTriangulation );


	for( int l = 0; l < m_vv3TriangulationIndices.size( ); l++ )
	{
		// create a template for new triangle
		m_vtTriangulation.insert( m_vtTriangulation.end( ), VistaTriangle( VistaVector3D( 0, 0, 0 ), VistaVector3D( 0, 0, 0 ), VistaVector3D( 0, 0, 0 ) ) );

		m_vtTriangulation[l].SetA( vvSpeakerList[m_vv3TriangulationIndices[l][0]] );
		m_vtTriangulation[l].SetB( vvSpeakerList[m_vv3TriangulationIndices[l][1]] );
		m_vtTriangulation[l].SetC( vvSpeakerList[m_vv3TriangulationIndices[l][2]] );
	}

	std::vector<CLoudspeaker> listLoudspeaker;
	listLoudspeaker.reserve( 3 );
	// create a template for three loudspeaker that build a triangle
	listLoudspeaker.insert( listLoudspeaker.end( ), m_voLoudspeaker[0] );
	listLoudspeaker.insert( listLoudspeaker.end( ), m_voLoudspeaker[0] );
	listLoudspeaker.insert( listLoudspeaker.end( ), m_voLoudspeaker[0] );

	for( int l = 0; l < m_vv3TriangulationIndices.size( ); l++ )
	{
		// store the three corresponding loudspeaker in list
		listLoudspeaker[0] = m_voLoudspeaker[m_vv3TriangulationIndices[l][0]];
		listLoudspeaker[1] = m_voLoudspeaker[m_vv3TriangulationIndices[l][1]];
		listLoudspeaker[2] = m_voLoudspeaker[m_vv3TriangulationIndices[l][2]];


		m_voTriangulationLoudspeaker.insert( m_voTriangulationLoudspeaker.end( ), listLoudspeaker );
	}
}

// //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// ---------- Helper functions ----------
float ITABase::Math::calculateCircumcenter3D( const VistaTriangle& tTriangle, VistaVector3D& vCenter )
{
	// Taken from https://gamedev.stackexchange.com/questions/60630/how-do-i-find-the-circumcenter-of-a-triangle-in-3d
	/*
	 * Let A, B, C, M be points in R� and T = {A, B, C} be a Triangle. Then the circumcenter M of T is calculated as
	 *
	 *           ||C-A||� * [(B-A) x (C-A)] x (B-A) + ||B-A||� * (C-A) x [(B-A) x (C-A)]
	 *  M = A + -------------------------------------------------------------------------.
	 *                                  2 * ||(B-A) x (C-A)||�
	 *
	 *
	 */
	VistaVector3D vA2B   = tTriangle.GetB( ) - tTriangle.GetA( );
	VistaVector3D vA2C   = tTriangle.GetC( ) - tTriangle.GetA( );
	VistaVector3D vABxAC = vA2B.Cross( vA2C );

	VistaVector3D vA2M = ( vABxAC.Cross( vA2B ) * vA2C.GetLengthSquared( ) + vA2C.Cross( vABxAC ) * vA2B.GetLengthSquared( ) ) / ( 2 * vABxAC.GetLengthSquared( ) );

	vCenter = tTriangle.GetA( ) + vA2M;
	return vA2M.GetLength( );
}

float ITABase::Math::sphereEquation( const VistaVector3D& vCenter, const VistaVector3D& vPoint )
{
	/*
	 *  r� (<)= (x - m_x)� + (y - m_y)� + (z - m_z)�
	 *        = ||P - M||�
	 */
	return ( vPoint - vCenter ).GetLength( );
}
