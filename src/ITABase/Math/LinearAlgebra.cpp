﻿#include <ITAException.h>
#include <ITABase/Math/LinearAlgebra.h>
#include <string>


using namespace ITABase;

std::vector<double> Math::BandmatrixSolver( const std::vector<double>& vdLower, const std::vector<double>& vdMiddle, const std::vector<double>& vdUpper,
                                            const std::vector<double>& vdExcitation )
{
	// solving bandmatrix with Thomas Algorithm. Implementation according to "numerical recipees in C", William H. Press -2nd edition 1992, ISBN 0-521-43108-5, page 51

	if( vdMiddle[0] == 0 )
		ITA_EXCEPT_INVALID_PARAMETER( "Matrix needs to be semi positiv definit." );
	if( vdMiddle.size( ) != vdExcitation.size( ) || vdLower.size( ) != vdUpper.size( ) || vdExcitation.size( ) != vdLower.size( ) + 1 )
		ITA_EXCEPT_INVALID_PARAMETER( "Dimensions of arguments missmatch." );

	const int iNum = vdMiddle.size( );

	std::vector<double> vdGamma( iNum );
	double fBeta = vdMiddle[0];
	std::vector<double> vdSolution( iNum ); //

	vdSolution[0] = vdExcitation[0] / vdMiddle[0];
	for( int idx = 1; idx < iNum; idx++ )
	{
		vdGamma[idx]    = vdUpper[idx - 1] / fBeta;
		fBeta           = vdMiddle[idx] - vdLower[idx - 1] * vdGamma[idx];
		vdSolution[idx] = ( vdExcitation[idx] - vdLower[idx - 1] * vdSolution[idx - 1] ) / fBeta;
	}
	for( int idx = iNum - 2; idx >= 0; idx-- )
	{ // backsubstitution
		vdSolution[idx] -= vdGamma[idx + 1] * vdSolution[idx + 1];
	}

	return vdSolution;
}
