﻿#include <ITABase/Math/Spline.h>
#include <ITABase/Math/LinearAlgebra.h>
#include <ITAException.h>
#include <string>

using namespace ITABase;
using namespace ITABase::Math;


//---FUNCTIONS CREATING POLYNOMIALS---
//------------------------------------

CPiecewisePolynomial ITABase::Math::CubicSpline( const std::vector<double>& vdSupportingPoints, const std::vector<double>& vdDataPoints )
{
	/*setUp the bandmatrix M with the the h values. The vector with the coefficent b is called b.
	    The vector with the linear approximations on the right hand side is called r.
	    M * b = r
	    Solve the bandmatrix M with the thomas algorithm and reconstruct the other coefficients according to:
	    https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=&cad=rja&uact=8&ved=2ahUKEwjcofWO_IrqAhUux4sKHT52AigQFjABegQIBRAB&url=http%3A%2F
	    out of the b vector.
	    */

	if( vdSupportingPoints.size( ) != vdDataPoints.size( ) )
		ITA_EXCEPT_INVALID_PARAMETER( "vdSupportingPoints and vdDataPoints need to have the same size" );
	if( vdSupportingPoints.size( ) < 4 )
		ITA_EXCEPT_INVALID_PARAMETER( "vdSupportingPoints and vdDataPoints need to contain at least 4 elements" );

	// Calculate h_i, formular 1.27b
	const int iPolynomialOrder = 3;
	const int iPoints          = vdSupportingPoints.size( );
	std::vector<double> vdH( iPoints - 1 );

	for( int i = 0; i < iPoints - 1; i++ )
	{
		vdH[i] = vdSupportingPoints[i + 1] - vdSupportingPoints[i];
		if( vdH[i] <= 0 )
		{
			ITA_EXCEPT_INVALID_PARAMETER( "vdSupportingPoints needs to be sorted in ascending order and not be duplicates" );
		}
	}

	// SetUp the band matrix with lower, middle and upper diagonals. momentary only natural splines
	std::vector<double> vdLower( iPoints - 3 ), vdMiddle( iPoints - 2 ), vdUpper( iPoints - 3 );
	for( int i = 0; i < iPoints - 3; i++ )
	{
		vdLower[i] = vdH[i + 1];
	}
	vdUpper = vdLower; // TODO: non natural splines behave different -> put it in the loop

	for( int i = 0; i < iPoints - 2; i++ )
	{
		vdMiddle[i] = 2 * ( vdH[i] + vdH[i + 1] );
	}

	// excitation vector
	// TODO only for natural splines with S0=S_iPoints = 0
	std::vector<double> vdR( iPoints - 2 );
	for( int i = 0; i < iPoints - 2; i++ )
		vdR[i] = 3 * ( ( vdDataPoints[i + 2] - vdDataPoints[i + 1] ) / vdH[i + 1] - ( vdDataPoints[i + 1] - vdDataPoints[i] ) / vdH[i] );

	// solve bandmatrix and get the other coefficients
	std::vector<double> vdB = BandmatrixSolver( vdLower, vdMiddle, vdUpper, vdR );
	vdB.insert( vdB.begin( ), 0.0 );

	std::vector<double> vdA( iPoints - 1 );
	std::vector<double> vdC( iPoints - 1 );
	std::vector<double> vdD( iPoints - 1 );

	vdB.push_back( 0.0 ); // for ease of loop
	for( int i = 0; i < iPoints - 1; i++ )
	{
		vdC[i] = ( vdDataPoints[i + 1] - vdDataPoints[i] ) / vdH[i] - ( vdH[i] / 3 ) * ( 2 * vdB[i] + vdB[i + 1] );
		vdA[i] = ( vdB[i + 1] - vdB[i] ) / ( 3 * vdH[i] );
	}
	vdB.pop_back( );
	vdD = vdDataPoints;
	vdD.pop_back( );

	// sort for Constructor CPiecewisePolynomial
	int iCoefficients = ( iPoints - 1 ) * ( iPolynomialOrder + 1 );
	std::vector<double> vdCoefficients( iCoefficients );
	for( int k = 0; k < iPoints - 1; k++ )
	{
		vdCoefficients[4 * k]     = vdA[k];
		vdCoefficients[4 * k + 1] = vdB[k];
		vdCoefficients[4 * k + 2] = vdC[k];
		vdCoefficients[4 * k + 3] = vdD[k];
	}

	return CPiecewisePolynomial( vdSupportingPoints, vdCoefficients, iPolynomialOrder );
}
CPiecewisePolynomial ITABase::Math::CubicSpline( const std::vector<float>& vfSupportingPoints, const std::vector<float>& vfDataPoints )
{
	auto vdSupportingPoints = std::vector<double>( vfSupportingPoints.begin( ), vfSupportingPoints.end( ) );
	auto vdDataPoints       = std::vector<double>( vfDataPoints.begin( ), vfDataPoints.end( ) );
	return CubicSpline( vdSupportingPoints, vdDataPoints ); // Calling main function working with doubles
}
CPiecewisePolynomial ITABase::Math::CubicSpline( const std::vector<float>& vfSupportingPoints, const std::vector<double>& vdDataPoints )
{
	auto vdSupportingPoints = std::vector<double>( vfSupportingPoints.begin( ), vfSupportingPoints.end( ) );
	return CubicSpline( vdSupportingPoints, vdDataPoints ); // Calling main function working with doubles
}
CPiecewisePolynomial ITABase::Math::CubicSpline( const std::vector<double>& vdSupportingPoints, const std::vector<float>& vfDataPoints )
{
	auto vdDataPoints = std::vector<double>( vfDataPoints.begin( ), vfDataPoints.end( ) );
	return CubicSpline( vdSupportingPoints, vdDataPoints ); // Calling main function working with doubles
}


//---FUNCTIONS EVALUATING POLYNOMIALS---
//--------------------------------------

std::vector<double> ITABase::Math::CubicSpline( const std::vector<double>& vdSupportingPoints, const std::vector<double>& vdDataPoints,
                                                const std::vector<double>& vdEvalPoints )
{
	CPiecewisePolynomial myPoly = CubicSpline( vdSupportingPoints, vdDataPoints );
	return myPoly.Evaluate( vdEvalPoints );
}

std::vector<double> ITABase::Math::CubicSpline( const std::vector<float>& vfSupportingPoints, const std::vector<float>& vfDataPoints,
                                                const std::vector<float>& vfEvalPoints )
{
	CPiecewisePolynomial myPoly = CubicSpline( vfSupportingPoints, vfDataPoints );
	return myPoly.Evaluate( vfEvalPoints );
}

double ITABase::Math::CubicSpline( const std::vector<double>& vdSupportingPoints, const std::vector<double>& vdDataPoints, double dEvalPoint )
{
	CPiecewisePolynomial myPoly = CubicSpline( vdSupportingPoints, vdDataPoints );
	return myPoly.Evaluate( dEvalPoint );
}
float ITABase::Math::CubicSpline( const std::vector<float>& vfSupportingPoints, const std::vector<float>& vfDataPoints, float fEvalPoint )
{
	CPiecewisePolynomial myPoly = CubicSpline( vfSupportingPoints, vfDataPoints );
	return float( myPoly.Evaluate( fEvalPoint ) );
}
