﻿#include <ITABase/Math/PiecewisePolynomial.h>
#include <ITABase/Math/LinearAlgebra.h>
#include <ITAException.h>
#include <algorithm>
#include <cmath>
#include <string>

using namespace ITABase;
using namespace ITABase::Math;

CPiecewisePolynomial::CPiecewisePolynomial( const std::vector<double>& vdBreakPoints, const std::vector<double>& vdCoeffs, const int iOrder )
    : iOrder( iOrder )
    , vdBreakPoints( vdBreakPoints )
    , vdCoefficients( vdCoeffs )
{
	if( iOrder < 0 )
		ITA_EXCEPT_INVALID_PARAMETER( "Polynom order must be greater or equal zero." );

	const int iMinimumNBreakPoints = 2; // At least one interval
	if( vdBreakPoints.size( ) < iMinimumNBreakPoints )
		ITA_EXCEPT_INVALID_PARAMETER( "Number of break points not sufficient for polynom order: Expected " + std::to_string( iMinimumNBreakPoints ) + " but got " +
		                              std::to_string( vdBreakPoints.size( ) ) + "." );

	const int iNumCoeffs = NumIntervals( ) * ( iOrder + 1 );
	if( vdCoeffs.size( ) != iNumCoeffs )
		ITA_EXCEPT_INVALID_PARAMETER( "Invalid number of coefficients: Expected " + std::to_string( iNumCoeffs ) + " but got " + std::to_string( vdCoeffs.size( ) ) +
		                              "." );
}
CPiecewisePolynomial::CPiecewisePolynomial( const std::vector<float>& vfBreakPoints, const std::vector<float>& vfCoeffs, const int iOrder )
    : CPiecewisePolynomial( std::vector<double>( vfBreakPoints.begin( ), vfBreakPoints.end( ) ), std::vector<double>( vfCoeffs.begin( ), vfCoeffs.end( ) ), iOrder )
{
}


double CPiecewisePolynomial::Evaluate( const double& xValue, bool bExtrapolate ) const
{
	// a(x-x0)^(n) + b(x-x0)^(n-1) + c(x-x0)^(n-2) +...
	const int iInterval = IntervalIndex( xValue, bExtrapolate );


	if( iInterval == -1 )
	{ // out of bounds
		CPiecewisePolynomial ppDerivative = this->Derivation( );
		const int iExtrapolInterval       = ( xValue < vdBreakPoints[0] ) ? 0 : NumIntervals( );

		const double dX0 = vdBreakPoints[iExtrapolInterval];
		return ppDerivative.Evaluate( dX0 ) * ( xValue - dX0 ) + this->Evaluate( dX0 ); // y=m(x-x0)+d
	}

	// inside bounds
	std::vector<double>::const_iterator itCoef = CoefficientsOfInterval( iInterval );
	const double x0                            = vdBreakPoints[iInterval];
	double fResult                             = 0;
	for( int idx = 0; idx <= iOrder; idx++ )
	{ // adding up the parts of the polynomial
		fResult += itCoef[idx] * pow( xValue - x0, iOrder - idx );
	}
	return fResult;
}
std::vector<double> CPiecewisePolynomial::Evaluate( const std::vector<double>& vdXValues, bool bExtrapolate ) const
{
	std::vector<double> result( vdXValues.size( ) );
	for( int i = 0; i < vdXValues.size( ); i++ )
		result[i] = Evaluate( vdXValues[i], bExtrapolate );
	return result;
}
std::vector<double> CPiecewisePolynomial::Evaluate( const std::vector<float>& vfXValues, bool bExtrapolate ) const
{
	std::vector<double> result( vfXValues.size( ) );
	for( int i = 0; i < vfXValues.size( ); i++ )
		result[i] = Evaluate( vfXValues[i], bExtrapolate );
	return result;
}


int CPiecewisePolynomial::IntervalIndex( const double xValue, bool bExtrapolate ) const
{
	if( xValue < vdBreakPoints[0] || xValue > vdBreakPoints[NumIntervals( )] )
	{
		if( bExtrapolate )
			return -1;
		else
			ITA_EXCEPT_INVALID_PARAMETER( "X-value is out of bounds" );
	}

	const auto itUpperBound = std::upper_bound( vdBreakPoints.begin( ), vdBreakPoints.end( ), xValue );
	if( itUpperBound == vdBreakPoints.end( ) ) //xValue = right border of PP => last interval
		return NumIntervals( ) - 1;
	return std::max( 0, (int)std::distance( vdBreakPoints.begin( ), itUpperBound ) - 1 );
}
int CPiecewisePolynomial::IntervalIndex( const float xValue, bool bExtrapolate ) const
{
	return IntervalIndex( double( xValue ), bExtrapolate );
}


CPiecewisePolynomial CPiecewisePolynomial::Derivation( ) const
{
	const int iNewOrder           = std::max( 0, iOrder - 1 );             // Minimum order is zero
	const int iNewNumCoefficients = std::max( 1, NumCoefficients( ) - 1 ); // At least one coefficient

	std::vector<double> vdNewCoeffs( NumIntervals( ) * iNewNumCoefficients, 0.0 );
	for( int idxPiece = 0; idxPiece < NumIntervals( ); idxPiece++ )
	{
		for( int idxCoeff = 0; idxCoeff < iOrder; idxCoeff++ )
		{
			const int iCurrentOrder = iOrder - idxCoeff;
			const int idxOld        = idxPiece * NumCoefficients( ) + idxCoeff;
			const int idxNew        = idxPiece * iNewNumCoefficients + idxCoeff;
			vdNewCoeffs[idxNew]     = vdCoefficients[idxOld] * iCurrentOrder;
		}
	}

	return CPiecewisePolynomial( vdBreakPoints, vdNewCoeffs, iNewOrder );
}

std::vector<double>::const_iterator CPiecewisePolynomial::CoefficientsOfInterval( const int idxInterval ) const
{
	if( idxInterval < 0 || idxInterval >= NumIntervals( ) )
		ITA_EXCEPT_INVALID_PARAMETER( "Index for interval is out of bounds." );

	return vdCoefficients.begin( ) + idxInterval * NumCoefficients( );
}
