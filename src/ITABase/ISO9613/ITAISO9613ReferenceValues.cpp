#include <ITABase/ISO9613/ITAISO9613ReferenceValues.h>
#include <ITAException.h>

#include <string>
#include <cmath>

using namespace ITABase;

float AttenuationDB2SignalGain( float fAttenuationDB )
{
	return std::pow( 10.0f, -fAttenuationDB / 20.0f );
}

CThirdOctaveGainMagnitudeSpectrum AttenuationDB2SignalGain( const CThirdOctaveDecibelMagnitudeSpectrum& oSpectrumDB )
{
	CThirdOctaveGainMagnitudeSpectrum oGainSpectrum;
	for( int idx = 0; idx < oSpectrumDB.GetNumBands( ); idx++ )
	{
		oGainSpectrum.SetMagnitude( idx, AttenuationDB2SignalGain( oSpectrumDB[idx] ) );
	}
	return oGainSpectrum;
}

ITABase::ISO9613::CReferenceDataSet::CReferenceDataSet( double dTemperatureDegC, double dRelativeHumidityPercent,
                                                        const CThirdOctaveDecibelMagnitudeSpectrum& oSpectrumAlphaDBperKm )
    : dTemperatureDegC( dTemperatureDegC )
    , dRelativeHumidityPercent( dRelativeHumidityPercent )
    , oSpectrumAlphaDBperKm( oSpectrumAlphaDBperKm )
{
	
}

 ITABase::ISO9613::CReferenceDataSet ITABase::ISO9613::CReferenceDataSet::Create( int idxCase )
{
	double dTemperatureDegC;
	double dRelativeHumidityPercent;
	std::vector<float> vfAlphaDBperKm( CThirdOctaveGainMagnitudeSpectrum::GetNumBands( ) );
	switch( idxCase )
	{
		case 1:
			dTemperatureDegC         = -20;
			dRelativeHumidityPercent = 20;
			vfAlphaDBperKm           = { 0.0,  0.0,  0.0,  0.0,  4.18e-1, 6.02e-1, 8.46e-1, 1.15, 1.49, 1.83, 2.15,   2.42,   2.63, 2.79, 2.91, 3.0,
                               3.08, 3.16, 3.27, 3.42, 3.65,    4.00,    4.55,    5.42, 6.80, 8.98, 1.24e1, 1.79e1, 0.0,  0.0,  0.0 };
			break;
		case 2:
			dTemperatureDegC         = -20;
			dRelativeHumidityPercent = 50;
			vfAlphaDBperKm           = { 0.0,  0.0,  0.0,  0.0,    1.68e-1, 2.41e-1, 3.52e-1, 5.21e-1, 7.76e-1, 1.16,   1.70,   2.46,   3.43, 4.59, 5.86, 7.10,
                               8.22, 9.14, 9.68, 1.05e1, 1.10e1,  1.16e1,  1.23e1,  1.32e1,  1.46e1,  1.69e1, 2.03e1, 2.58e1, 0.0,  0.0,  0.0 };
			break;
		case 3:
			dTemperatureDegC         = 0;
			dRelativeHumidityPercent = 20;
			vfAlphaDBperKm           = { 0.0,    0.0,    0.0,    0.0,    1.95e-1, 2.56e-1, 3.37e-1, 4.49e-1, 6.14e-1, 8.65e-1, 1.25,   1.85,   2.76, 4.14, 6.16, 9.03,
                               1.29e1, 1.77e1, 2.33e1, 2.91e1, 3.46e1,  3.95e1,  4.36e1,  4.70e1,  5.03e1,  5.37e1,  5.81e1, 6.43e1, 0.0,  0.0,  0.0 };
			break;
		case 4:
			dTemperatureDegC         = 0;
			dRelativeHumidityPercent = 70;
			vfAlphaDBperKm           = { 0.0,  0.0,  0.0,  0.0,    1.03e-1, 1.51e-1, 2.15e-1, 2.96e-1, 3.90e-1, 4.98e-1, 6.19e-1, 7.63e-1, 9.51e-1, 1.21, 1.61, 2.21,
                               3.16, 4.64, 6.96, 1.06e1, 1.61e1,  2.46e1,  3.73e1,  5.55e1,  8.07e1,  1.13e2,  1.53e2,  1.98e2,  0.0,     0.0,  0.0 };
			break;
		case 5:
			dTemperatureDegC         = 20;
			dRelativeHumidityPercent = 20;
			vfAlphaDBperKm           = { 0.0,  0.0,  0.0,  0.0,    1.74e-1, 2.60e-1, 3.77e-1, 5.29e-1, 7.12e-1, 9.19e-1, 1.14,   1.39,   1.69, 2.08, 2.60, 3.39,
                               4.62, 6.53, 9.53, 1.42e1, 2.15e1,  3.26e1,  4.94e1,  7.41e1,  1.09e2,  1.56e2,  2.15e2, 2.84e2, 0.0,  0.0,  0.0 };
			break;
		default:
			ITA_EXCEPT_INVALID_PARAMETER( "Case ID must be an integer between " + std::to_string( MinimumCaseID( ) ) + " and " + std::to_string( MaximumCaseID( ) ) +
			                              "." );
	}

	CThirdOctaveDecibelMagnitudeSpectrum oSpectrumAlphaDBperKm;
	oSpectrumAlphaDBperKm.SetValues( vfAlphaDBperKm );
	return CReferenceDataSet( dTemperatureDegC, dRelativeHumidityPercent, oSpectrumAlphaDBperKm );
}

const CThirdOctaveDecibelMagnitudeSpectrum ITABase::ISO9613::CReferenceDataSet::SpectrumDB( float fDistance ) const
{
	CThirdOctaveDecibelMagnitudeSpectrum oOutputSpectrum = oSpectrumAlphaDBperKm;
	oOutputSpectrum.Multiply( fDistance / 1000.0f );
	return oOutputSpectrum;
}

const CThirdOctaveGainMagnitudeSpectrum ITABase::ISO9613::CReferenceDataSet::GainSpectrum( float fDistance ) const
{
	return AttenuationDB2SignalGain( SpectrumDB( fDistance ) );
}
