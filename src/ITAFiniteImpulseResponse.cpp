#include <ITAFiniteImpulseResponse.h>

using namespace ITABase;

CFiniteImpulseResponse::CFiniteImpulseResponse( ) : m_fSampleRate( 0 ) {}

CFiniteImpulseResponse::CFiniteImpulseResponse( const int iLength, const float fSampleRate, const bool bZeroInit /*= true */ )
    : ITASampleBuffer( iLength, bZeroInit )
    , m_fSampleRate( fSampleRate )
{
}

CFiniteImpulseResponse::CFiniteImpulseResponse( const CFiniteImpulseResponse* pSource )
{
	Init( pSource->GetLength( ), pSource->GetSampleRate( ) );
	ITASampleBuffer::write( pSource, pSource->GetLength( ) );
}

CFiniteImpulseResponse::CFiniteImpulseResponse( const CFiniteImpulseResponse& sbSource )
{
	Init( sbSource.GetLength( ), sbSource.GetSampleRate( ) );
	ITASampleBuffer::write( sbSource, sbSource.GetLength( ) );
}

void CFiniteImpulseResponse::Init( const int iLength, const float fSampleRate, const bool bZeroInit /*= true */ )
{
	m_fSampleRate = fSampleRate;
	ITASampleBuffer::Init( iLength, bZeroInit );
}

float CFiniteImpulseResponse::GetSampleRate( ) const
{
	return m_fSampleRate;
}

float CFiniteImpulseResponse::GetNyquistFrequency( ) const
{
	return GetSampleRate( ) / 2.0f;
}

void ITABase::CFiniteImpulseResponse::SetDirac( float fAmplitude /* = 1.0f */ )
{
	Identity( ); // also validates buffer
	if( fAmplitude != 1.0f )
		ITASampleBuffer::mul_scalar( fAmplitude ); // not efficient, but safe
}

void ITABase::CFiniteImpulseResponse::SetUnitStepFunction( float fAmplitude /* = 1.0f */ )
{
	Fill( fAmplitude );
}
