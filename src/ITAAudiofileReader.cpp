#include "libsndfileAudiofileReader.h"

#include <ITAAudiofileReader.h>
#include <ITAException.h>
#include <VistaTools/VistaFileSystemFile.h>

ITAAudiofileReader* ITAAudiofileReader::create( const std::string& sFilePath )
{
	VistaFileSystemFile oFile( sFilePath );
	// Check if the file exists
	if( oFile.Exists( ) == false )
		ITA_EXCEPT1( FILE_NOT_FOUND, std::string( "Audiofile \"" ) + sFilePath + std::string( "\" not found" ) );

	return new libsndfileAudiofileReader( sFilePath );
}

ITAAudiofileReader::~ITAAudiofileReader( ) { };

ITAAudiofileProperties ITAAudiofileReader::getAudiofileProperties( ) const
{
	ITAAudiofileProperties result;
	result.dSampleRate   = m_dSampleRate;
	result.eDomain       = m_eDomain;
	result.eQuantization = m_eQuantization;
	result.sComment      = m_sComment;
	result.iChannels     = m_iChannels;
	result.iLength       = m_iLength;
	return result;
}

ITAQuantization ITAAudiofileReader::getQuantization( ) const
{
	return m_eQuantization;
}
ITADomain ITAAudiofileReader::getDomain( ) const
{
	return m_eDomain;
}
int ITAAudiofileReader::getNumberOfChannels( ) const
{
	return m_iChannels;
}
double ITAAudiofileReader::getSamplerate( ) const
{
	return m_dSampleRate;
}
int ITAAudiofileReader::getLength( ) const
{
	return m_iLength;
}
int ITAAudiofileReader::getCursor( ) const
{
	return m_iCursor;
}
std::string ITAAudiofileReader::getComment( ) const
{
	return m_sComment;
}

std::vector<float*> ITAAudiofileReader::read( int uiLength )
{
	// Fehler mit der L�nge bereits vor dem Allozieren abfangen!
	if( uiLength > ( m_iLength - m_iCursor ) )
		ITA_EXCEPT1( IO_ERROR, "Versuch �ber das Ende der Datei hinaus zu lesen" );

	// Puffer allozieren
	std::vector<float*> vpfResult;
	for( int i = 0; i < m_iChannels; i++ )
		vpfResult.push_back( new float[uiLength] );

	// Daten lesen
	try
	{
		read( uiLength, vpfResult );
	}
	catch( ... )
	{
		for( unsigned int i = 0; i < vpfResult.size( ); i++ )
			delete[] vpfResult[i];
		throw;
	}

	return vpfResult;
}

void sneakAudiofile( const std::string& sFilePath, ITAAudiofileProperties& props )
{
	ITAAudiofileReader* pReader = ITAAudiofileReader::create( sFilePath );
	props                       = pReader->getAudiofileProperties( );
	delete pReader;
}

std::vector<float*> readAudiofile( const std::string& sFilePath, ITAAudiofileProperties& props )
{
	ITAAudiofileReader* pReader = ITAAudiofileReader::create( sFilePath );
	props                       = pReader->getAudiofileProperties( );
	std::vector<float*> vpfResult;
	try
	{
		vpfResult = pReader->read( pReader->getLength( ) );
	}
	catch( ... )
	{
		delete pReader;
		throw;
	}
	delete pReader;
	return vpfResult;
}

void readAudiofile( const std::string& sFilePath, ITAAudiofileProperties& props, std::vector<float*>& vpfData )
{
	ITAAudiofileReader* pReader = ITAAudiofileReader::create( sFilePath );
	props                       = pReader->getAudiofileProperties( );
	try
	{
		pReader->read( pReader->getLength( ), vpfData );
	}
	catch( ... )
	{
		delete pReader;
		throw;
	}
	delete pReader;
}
