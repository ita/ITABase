#include "ITAConfigUtils.h"

#ifdef USE_SIMPLEINI

#	include "simpleini/SimpleIni.h"

#	include <ITAException.h>
#	include <ITAFileSystemUtils.h>

//
//// Bugfix (fwe): Falls kein Pfad und nur ein Dateinamen angebenen ist, muss .\ vorne angeh�ngt werden
// std::string fixpath(const std::string& sPath) {
//	if (sPath.find_first_of(PATH_SEPARATOR) != -1)
//		return sPath;
//	else
//		return std::string(".") + PATH_SEPARATOR + sPath;
//}

std::vector<std::string> INIFileGetSections( const std::string& sINIFilename )
{
	CSimpleIniA ini;

	SI_Error err = ini.LoadFile( sINIFilename.c_str( ) );
	switch( err )
	{
		case SI_FAIL:
			ini.Reset( );
			ITA_EXCEPT0( UNKNOWN );
			break;

		case SI_NOMEM:
			ini.Reset( );
			ITA_EXCEPT0( OUT_OF_MEMORY );
			break;

		case SI_FILE:
			ini.Reset( );
			ITA_EXCEPT0( IO_ERROR );
			break;
	}

	CSimpleIniA::TNamesDepend sections;
	ini.GetAllSections( sections );

	// Liste von Sektionen im SimpleINI-Kontainer in std::vector< std::string > umsetzen
	std::vector<std::string> vsResults;
	for( CSimpleIniA::TNamesDepend::const_iterator cit = sections.begin( ); cit != sections.end( ); ++cit )
		vsResults.push_back( cit->pItem );

	ini.Reset( );

	return vsResults;
}

std::vector<std::string> INIFileGetKeys( const std::string& sINIFilename, const std::string& sSection )
{
	CSimpleIniA ini;

	SI_Error err = ini.LoadFile( sINIFilename.c_str( ) );
	switch( err )
	{
		case SI_FAIL:
			ini.Reset( );
			ITA_EXCEPT0( UNKNOWN );
			break;

		case SI_NOMEM:
			ini.Reset( );
			ITA_EXCEPT0( OUT_OF_MEMORY );
			break;

		case SI_FILE:
			ini.Reset( );
			ITA_EXCEPT0( IO_ERROR );
			break;
	}

	std::vector<std::string> vsResults;
	CSimpleIniA::TNamesDepend keys;
	if( ini.GetAllKeys( sSection.c_str( ), keys ) )
	{
		for( CSimpleIniA::TNamesDepend::const_iterator cit = keys.begin( ); cit != keys.end( ); ++cit )
			vsResults.push_back( cit->pItem );
	}

	ini.Reset( );

	return vsResults;
}

std::string INIFileReadString( const std::string& sINIFilename, const std::string& sSection, const std::string& sKey, std::string sDefaultValue )
{
	CSimpleIniA ini;

	SI_Error err = ini.LoadFile( sINIFilename.c_str( ) );
	switch( err )
	{
		case SI_FAIL:
			ini.Reset( );
			ITA_EXCEPT0( UNKNOWN );
			break;

		case SI_NOMEM:
			ini.Reset( );
			ITA_EXCEPT0( OUT_OF_MEMORY );
			break;

		case SI_FILE:
			ini.Reset( );
			ITA_EXCEPT0( IO_ERROR );
			break;
	}

	std::string sResult = ini.GetValue( sSection.c_str( ), sKey.c_str( ), sDefaultValue.c_str( ) );

	ini.Reset( );

	return sResult;
}

bool INIFileWriteString( const std::string& sINIFilename, const std::string& sSection, const std::string& sKey, const std::string& sValue )
{
	CSimpleIniA ini;

	SI_Error err;

	if( doesFileExist( sINIFilename ) )
	{
		err = ini.LoadFile( sINIFilename.c_str( ) );
		switch( err )
		{
			case SI_FAIL:
				ini.Reset( );
				ITA_EXCEPT0( UNKNOWN );
				return false;

			case SI_NOMEM:
				ini.Reset( );
				ITA_EXCEPT0( OUT_OF_MEMORY );
				return false;

			case SI_FILE:
				ini.Reset( );
				ITA_EXCEPT0( IO_ERROR );
				return false;
		}
	}

	err = ini.SetValue( sSection.c_str( ), sKey.c_str( ), sValue.c_str( ) );
	switch( err )
	{
		case SI_FAIL:
			ini.Reset( );
			ITA_EXCEPT0( UNKNOWN );
			return false;

		case SI_NOMEM:
			ini.Reset( );
			ITA_EXCEPT0( OUT_OF_MEMORY );
			return false;

		case SI_FILE:
			ini.Reset( );
			ITA_EXCEPT0( IO_ERROR );
			return false;
	}

	// INI-Datei speichern
	err = ini.SaveFile( sINIFilename.c_str( ) );
	switch( err )
	{
		case SI_FAIL:
			ini.Reset( );
			ITA_EXCEPT0( UNKNOWN );
			return false;

		case SI_NOMEM:
			ini.Reset( );
			ITA_EXCEPT0( OUT_OF_MEMORY );
			return false;

		case SI_FILE:
			ini.Reset( );
			ITA_EXCEPT0( IO_ERROR );
			return false;
	}

	ini.Reset( );

	return true;
}

#endif // USE_SIMPLEINI
