#include <ITAFileSystemUtils.h>

#if defined( WIN32 ) || defined( PLATFORM_X64 )
#	include <windows.h>
#endif

#include <sys/stat.h>

bool doesPathExist( const std::string& sPath )
{
	// Inspiration by FOX
#if !defined( WIN32 ) && !defined( PLATFORM_X64 )
	struct stat status;
	return !sPath.empty( ) && ( ::stat( sPath.c_str( ), &status ) == 0 );
#else
	return !sPath.empty( ) && ( GetFileAttributesA( sPath.c_str( ) ) != 0xFFFFFFFF );
#endif
}

bool doesFileExist( const std::string& sPath )
{
	return doesPathExist( sPath ) && isFile( sPath );
}

bool doesDirectoryExist( const std::string& sPath )
{
	return doesPathExist( sPath ) && isDirectory( sPath );
}

bool isFile( const std::string& sPath )
{
	// Inspiration by FOX
#if !defined( WIN32 ) && !defined( PLATFORM_X64 )
	struct stat status;
	return !sPath.empty( ) && ( ::stat( sPath.c_str( ), &status ) == 0 ) && S_ISREG( status.st_mode );
#else
	DWORD atts;
	return !sPath.empty( ) && ( ( atts = GetFileAttributesA( sPath.c_str( ) ) ) != 0xFFFFFFFF ) && !( atts & FILE_ATTRIBUTE_DIRECTORY );
#endif
}

bool isDirectory( const std::string& sPath )
{
#if !defined( WIN32 ) && !defined( PLATFORM_X64 )
	struct stat status;
	return !sPath.empty( ) && ( ::stat( sPath.c_str( ), &status ) == 0 ) && S_ISDIR( status.st_mode );
#else
	DWORD atts;
	return !sPath.empty( ) && ( ( atts = GetFileAttributesA( sPath.c_str( ) ) ) != 0xFFFFFFFF ) && ( atts & FILE_ATTRIBUTE_DIRECTORY );
#endif
}

bool makeDirectory( const std::string& sPath )
{
#if !defined( WIN32 ) && !defined( PLATFORM_X64 )
	// Set permissions to read/write/search permissions for owner and group, and read/search permissions for others
	return mkdir( sPath.c_str( ), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH ) == 0;
#else
	return CreateDirectory( sPath.c_str( ), NULL ) != 0;
#endif
}

std::string getFilenameSuffix( const std::string& sFilename )
{
	int i = (int)sFilename.find_last_of( '.' );
	return ( ( i == -1 ) || ( i == ( (int)sFilename.length( ) - 1 ) ) ? "" : sFilename.substr( i + 1, (int)sFilename.length( ) - i - 1 ) );
}

std::string stripFilenameSuffix( const std::string& sFilename )
{
	size_t i = sFilename.find_last_of( '.' );
	return ( ( i == -1 ) || ( i == ( sFilename.length( ) - 1 ) ) ? "" : sFilename.substr( 0, i ) );
}

std::string getFilenameFromPath( const std::string& sPath )
{
	size_t i = 0;
	if( sPath.find( PATH_SEPARATOR ) != std::string::npos )
	{
		i = sPath.find_last_of( PATH_SEPARATOR ) + 1;
		return sPath.substr( i, ( sPath.length( ) - 1 ) );
	}
	else
	{
		return sPath;
	}
}

std::string getDirectoryFromPath( const std::string& sPath )
{
	size_t i = sPath.find_last_of( PATH_SEPARATOR );
	return ( ( i == -1 ) || ( i == ( sPath.length( ) - 1 ) ) ? "" : sPath.substr( 0, i ) );
}

int64_t getFileSize( const std::string& sFilename )
{
	if( sFilename.empty( ) )
		return -1;
#ifdef LINUX
	// Linux
	struct stat64 statinfo;
	if( stat64( sFilename.c_str( ), &statinfo ) != 0 )
		return -1;
	return (int64_t)statinfo.st_size;
#endif
#ifdef WIN32
	// Windows
	struct _stat64 statinfo;
	if( _stat64( sFilename.c_str( ), &statinfo ) != 0 )
		return -1;
	return (int64_t)statinfo.st_size;
#endif

	// LINUX oder WIN32 nicht definiert
	return -1;
}

std::string combinePath( std::string p1, std::string p2, std::string p3, std::string p4 )
{
	std::string s = p1;
	if( !p2.empty( ) )
	{
		if( !s.empty( ) )
			s += PATH_SEPARATOR;
		s += p2;
	}

	if( !p3.empty( ) )
	{
		if( !s.empty( ) )
			s += PATH_SEPARATOR;
		s += p3;
	}

	if( !p4.empty( ) )
	{
		if( !s.empty( ) )
			s += PATH_SEPARATOR;
		s += p4;
	}

	return correctPath( s );
}

std::string correctPath( const std::string& sPath )
{
	std::string s( sPath );
	if( PATH_SEPARATOR == '/' )
	{
		for( std::string::size_type i = 0; i < s.length( ); i++ )
			// Backslashes in Slashes
			if( s[i] == '\\' )
				s[i] = '/';
	}
	else
	{
		for( std::string::size_type i = 0; i < s.length( ); i++ )
			// Backslashes in Slashes
			if( s[i] == '/' )
				s[i] = '\\';
	}

	// Doppelte Slashes/Backslashes entfernen
	std::string t = std::string( 2, PATH_SEPARATOR );
	size_t pos    = 0;
	while( ( pos = s.find( t, pos ) ) != std::string::npos )
	{
		s.replace( pos, 2, std::string( 1, PATH_SEPARATOR ) );
		pos++;
	}

	return s;
}

bool listFilesInDirectory( const std::string& sPath, std::vector<std::string>& vsFilenames )
{
	vsFilenames.clear( );
	std::string sCorectedPath = correctPath( sPath );

	if( !isDirectory( sCorectedPath ) )
		return false;

	std::string sFilename;
	// Inspiration by FOX
#if !defined( WIN32 ) && !defined( PLATFORM_X64 )
	// Linux implementation

#else
	// Windows implementation
	WIN32_FIND_DATA oData;
	HANDLE h;

	std::string sPathWildcard = sCorectedPath + std::string( "\\*" );
	h                         = FindFirstFile( sPathWildcard.c_str( ), &oData );

	if( h == INVALID_HANDLE_VALUE )
		return false;

	do
	{
		sFilename = oData.cFileName;
		if( !( oData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY ) )
			vsFilenames.push_back( sFilename );
	} while( FindNextFile( h, &oData ) != 0 );

#	ifdef DEBUG
	DWORD dwError = GetLastError( );
#	endif

	FindClose( h );

#endif

	return true;
}

namespace ITA
{
	namespace FileSystemUtils
	{
		std::string GetAbsolutePath( const std::string& sFilePath_RelativeToExecutionFolder )
		{
			// Inspiration by FOX
#if !defined( WIN32 ) && !defined( PLATFORM_X64 )
			ITA_EXCEPT_NOT_IMPLEMENTED;
#else
			char cAbsoluteFilePath[MAX_PATH];
			GetFullPathNameA( sFilePath_RelativeToExecutionFolder.c_str( ), MAX_PATH, cAbsoluteFilePath, nullptr );
			return std::string( cAbsoluteFilePath );
#endif
		}

		std::string GetBasePath( const std::string& sFilePath )
		{
			// Inspiration by FOX
#if !defined( WIN32 ) && !defined( PLATFORM_X64 )
			ITA_EXCEPT_NOT_IMPLEMENTED;
#else
			char cDrive[MAX_PATH];
			char cDir[MAX_PATH];
			_splitpath( sFilePath.c_str( ), cDrive, cDir, nullptr, nullptr );
			return std::string( cDrive ) + std::string( cDir );
#endif
		}
	} // namespace FileSystemUtils
} // namespace ITA
