#include <ITAFastMath.h>

#define WITH_SIMD_OPTIMIZATIONS

// Da kein Inline-Assembler unter 64-Bit Windows zur Verfügung steht, zunächst keine optimierten Funktionen hier
#ifdef _M_X64
#	error "No inline assembler available for 64-bit compiler"
#endif // _M_X64

#include "cpu_info.h" // CAMEL

#include <cassert>
#include <malloc.h>
#include <memory.h>
#include <stdio.h>
#include <stdlib.h>
#include <xmmintrin.h>

// Makro: Längen auf Vielfaches eines Faktor zu erweitern
#ifdef FM_XSIZE
#	undef FM_XSIZE
#endif
#ifdef FM_NOEXT
#	define FM_XSIZE( V, F )
#else
#	define FM_XSIZE( V, F ) V = F * ( V % F ? ( V / F ) + 1 : V / F );
#endif

bool bInit           = false;
unsigned int uiFlags = 0;

// This code is copied from the standard implementation, because
// a lot of FastMath functions do not have SSE implementations or
// are falling back to this implementation if SSE is not available.
// It is highly recommended to use the default implementation on
// modern architectures with current compiler optimization - or the
// IPP variant if the libraries are at hand.
namespace CRels
{
	inline void fm_set( float* buf, float value, unsigned int count )
	{
		for( unsigned int i = 0; i < count; i++ )
			buf[i] = value;
	}

	inline void fm_copy( float* dest, const float* src, unsigned int count ) { memcpy( dest, src, count * sizeof( float ) ); }

	inline void fm_add( float* dest, const float* summand, unsigned int count )
	{
		for( unsigned int i = 0; i < count; i++ )
			dest[i] += summand[i];
	}

	inline void fm_sub( float* dest, const float* src, unsigned int count )
	{
		for( unsigned int i = 0; i < count; i++ )
			dest[i] -= src[i];
	}

	inline void fm_mul( float* dest, float factor, unsigned int count )
	{
		for( unsigned int i = 0; i < count; i++ )
			dest[i] *= factor;
	}

	inline void fm_cmul( float* dest, const float* factor, unsigned int count )
	{
		float re, im;
		for( unsigned int i = 0; i < count; i++ )
		{
			// Realteil: Re'[k] = Re{dest[k]}*Re{factor[k]} - Im{dest[k]}*Im{factor[k]}
			re = dest[2 * i] * factor[2 * i] - dest[2 * i + 1] * factor[2 * i + 1];
			// Imaginärteil: Im'[k] = Re{dest[k]}*Im{factor[k]} + Im{dest[k]}*Re{factor[k]}
			im = dest[2 * i] * factor[2 * i + 1] + dest[2 * i + 1] * factor[2 * i];

			dest[2 * i]     = re;
			dest[2 * i + 1] = im;
		}
	}

	inline void fm_cmul_x( float* dest, const float* factor1, const float* factor2, unsigned int count )
	{
		for( unsigned int i = 0; i < count; i++ )
		{
			// Realteil: Re'[k] = Re{dest[k]}*Re{factor[k]} - Im{dest[k]}*Im{factor[k]}
			dest[2 * i] = factor1[2 * i] * factor2[2 * i] - factor1[2 * i + 1] * factor2[2 * i + 1];
			// Imaginärteil: Im'[k] = Re{dest[k]}*Im{factor[k]} + Im{dest[k]}*Re{factor[k]}
			dest[2 * i + 1] = factor1[2 * i] * factor2[2 * i + 1] + factor1[2 * i + 1] * factor2[2 * i];
		}
	}

	inline void fm_cdiv( float* dest, const float* div, unsigned int count )
	{
		float re, im, len;
		for( unsigned int i = 0; i < count; i++ )
		{
			// Nenner im Gesamtergebnis |div[i]| berechnen
			len = div[2 * i] * div[2 * i] + div[2 * i + 1] * div[2 * i + 1];

			// Realteil: Re'[k] = [ Re{dest[k]}*Re{div[k]} + Im{dest[k]}*Im{div[k]} ] / len
			re = ( dest[2 * i] * div[2 * i] + dest[2 * i + 1] * div[2 * i + 1] ) / len;
			// Imaginärteil: Im'[k] = [ Im{dest[k]}*Re{div[k]} - Re{dest[k]}*Im{div[k]} ] / len
			im = ( dest[2 * i + 1] * div[2 * i] - dest[2 * i] * div[2 * i + 1] ) / len;

			dest[2 * i]     = re;
			dest[2 * i + 1] = im;
		}
	}

	// Real-valued addition (in-place)
	// Semantic: srcdest[i] += src[i]
	void fm_add_f32( float* srcdest, const float* src, int count )
	{
		for( int i = 0; i < count; i++ )
			srcdest[i] += src[i];
	}

	// Real-valued multiplication (in-place)
	// Semantic: srcdest[i] *= src[i]
	void fm_mul_f32( float* srcdest, const float* src, int count )
	{
		for( int i = 0; i < count; i++ )
			srcdest[i] *= src[i];
	}

	// Complex-valued multiply
	// Semantic: dest[i] = src1[i]*src2[i]
	void fm_cmul_f32( float* dest, const float* src1, const float* src2, int count )
	{
		for( int i = 0; i < count; i++ )
		{
			// Realteil: Re'[k] = Re{dest[k]}*Re{factor[k]} - Im{dest[k]}*Im{factor[k]}
			dest[2 * i] = src1[2 * i] * src2[2 * i] - src1[2 * i + 1] * src2[2 * i + 1];
			// Imaginärteil: Im'[k] = Re{dest[k]}*Im{src[k]} + Im{dest[k]}*Re{src[k]}
			dest[2 * i + 1] = src1[2 * i] * src2[2 * i + 1] + src1[2 * i + 1] * src2[2 * i];
		}
	}

	// Complex-valued multiply-accumulate
	// Semantic: srcdest[i] += src1[i]*src2[i]
	void fm_cmuladd_f32( float* srcdest, const float* src1, const float* src2, int count )
	{
		for( int i = 0; i < count; i++ )
		{
			// Realteil: Re'[k] = Re{dest[k]}*Re{factor[k]} - Im{dest[k]}*Im{factor[k]}
			srcdest[2 * i] += src1[2 * i] * src2[2 * i] - src1[2 * i + 1] * src2[2 * i + 1];
			// Imaginärteil: Im'[k] = Re{dest[k]}*Im{src[k]} + Im{dest[k]}*Re{src[k]}
			srcdest[2 * i + 1] += src1[2 * i] * src2[2 * i + 1] + src1[2 * i + 1] * src2[2 * i];
		}
	}
} // namespace CRels


namespace SSERels
{
	inline void fm_copy( float* dest, const float* src, unsigned int count )
	{
		FM_XSIZE( count, 4 );

// Windows implementation
#ifdef WIN32
		// Hinweis: Dieser Assembler-Code mit SSE-Instruktionen ist nur bei wenigen
		//          Counts schneller im allgemeinen ca. 10% langsamer?!

		__asm {
			pusha; // Register auf dem Stack sichern

			mov edi, [dest] // Quell- und Zielzeiger laden
			mov esi, [src]

			mov ecx, count // Floor(count/4) als Zähler in ecx
			shr ecx, 2

			xor ebx, ebx // Offset = 0 in ebx

		begin:
			jecxz end // Schleifen-Abbruchbedingung

			movaps xmm0, [esi+ebx] // Die nächsten 4-floats aus src in xmm0 laden
			movaps [edi+ebx], xmm0 // ... und in dest speichern

			add ebx, 16 // Offset inkrementieren (4*float = 16 Bytes)
			dec ecx // Schleifenzähler dekrementieren
			jmp begin

		end:
			popa; // Register vom Stack holen
		}
#else // Linux implementation
		// TODO: convert Intel Syntax to AT&T Syntax
		// LINK: http://www.ibiblio.org/gferg/ldp/GCC-Inline-Assembly-HOWTO.html
		__asm__(
		    "pusha 				; Save register on stack"
		    "movl [$dest], %edi ; Load source- and targetpointer"
		    "popa 				; Get register from stack" );
/*
		mov %ecx, count				; Floor(count/4) as counter in ecx
		shr %ecx, 2

		xor %ebx, %ebx				; Offset = 0 in ebx

	begin:
		jecxz %end					; Loop break condition

		movaps %xmm0, [%esi+%ebx]	; Load next 4-floats from src in xmm0
		movaps [%edi+%ebx], %xmm0	; ... and save in dest

		add %ebx, 16				; Offset increment (4*float = 16 Bytes)
		dec %ecx					; Loop counter dekrement
		jmp begin

	end:
		popa						; Get register from stack
	");
	*/
#endif // LINUX
	}

	inline void fm_add( float* dest, const float* summand, unsigned int count )
	{
		FM_XSIZE( count, 4 );

// Windows implementation
#ifdef WIN32
		__asm {
			pusha; // Register auf dem Stack sichern

			mov edi, [dest] // Quell- und Zielzeiger laden
			mov esi, [summand]

			mov ecx, count // Floor(count/4) als Zähler in ecx
			shr ecx, 2

			xor ebx, ebx // Offset = 0 in ebx

		begin:
			jecxz end // Schleifen-Abbruchbedingung

			movaps xmm0, [edi+ebx] // 4-floats aus dem Ziel laden
			movaps xmm1, [esi+ebx] // 4-floats aus dem Summand laden
			addps xmm0, xmm1 // Addieren
			movaps [edi+ebx], xmm0 // Ergebnis in dest speichern

			add ebx, 16 // Offset inkrementieren (4*float = 16 Bytes)
			dec ecx // Schleifenzähler dekrementieren
			jmp begin

		end:
			popa; // Register vom Stack holen
		}
#else // Linux implementation
#endif
	}

	inline void fm_sub( float* dest, const float* src, unsigned int count )
	{
		FM_XSIZE( count, 4 );

// Windows implementation
#ifdef WIN32
		__asm {
			pusha; // Register auf dem Stack sichern

			mov edi, [dest] // Quell- und Zielzeiger laden
			mov esi, [src]

			mov ecx, count // Floor(count/4) als Zähler in ecx
			shr ecx, 2

			xor ebx, ebx // Offset = 0 in ebx

		begin:
			jecxz end // Schleifen-Abbruchbedingung

			movaps xmm0, [edi+ebx] // 4-floats aus dem Ziel laden
			movaps xmm1, [esi+ebx] // 4-floats aus dem Summand laden
			subps xmm0, xmm1 // Subtrahieren
			movaps [edi+ebx], xmm0 // Ergebnis in dest speichern

			add ebx, 16 // Offset inkrementieren (4*float = 16 Bytes)
			dec ecx // Schleifenzähler dekrementieren
			jmp begin

		end:
			popa; // Register vom Stack holen
		}
#else // Linux implementation
#endif
	}

	inline void fm_mul( float* dest, float factor, unsigned int count )
	{
		FM_XSIZE( count, 4 );

// Windows implementation
#ifdef WIN32
		__asm {
 			pusha; // Register auf dem Stack sichern

			mov edi, [dest] // Zielzeiger laden

			movss xmm1, factor // Skalar in die Bits 31-0 von xmm1 laden
			shufps xmm1, xmm1, 00000000b // Skalar auf die drei anderen floats in xmm1 kopieren

			mov ecx, count // Floor(count/4) als Zähler in ecx
			shr ecx, 2

		begin:
			jecxz end // Schleifen-Abbruchbedingung

			movups xmm0, [edi] // Die nächsten 4-floats aus src in xmm0 laden
			mulps  xmm0, xmm1 // Multiplizieren
			movups [edi], xmm0 // Ergebnis in dest speichern

			add edi, 16 // Offset inkrementieren (4*float = 16 Bytes)
			dec ecx // Schleifenzähler dekrementieren
			jmp begin

		end:
			popa; // Register vom Stack holen
		}
#else // Linux implementation
#endif
	}

	inline void fm_cmul_x( float* dest, const float* factor1, const float* factor2, unsigned int count )
	{
		FM_XSIZE( count, 2 );

		// Windows implementation
#ifdef WIN32
		__asm {
			// Hähä - hier isser... Sauschnell, dank Assembler

			pusha; // Register auf dem Stack sichern

			mov edi, [dest] // Quell- und Zielzeiger laden
			mov esi, [factor1]
			mov edx, [factor2]

			mov ecx, count // Floor(count/2) als Zähler in ecx
			shr ecx, 1

			xor ebx, ebx // Offset = 0 in ebx

		begin:
			jecxz end // Schleifen-Abbruchbedingung

			    // -= Eigentliche Berechnung =-

			movaps xmm0, [esi+ebx] // xmm0 = (d, c, b, a)
			movaps xmm1, [edx+ebx] // xmm1 = (h, g, f, e)

			movaps xmm2, xmm0
			shufps xmm2, xmm2, 10110001b // xmm2 = (c, d, a, b)

			mulps xmm0, xmm1 // xmm0 = (dh, cg, bf, ae)
			mulps xmm2, xmm1 // xmm2 = (ch, dg, af, be)

			movaps xmm1, xmm0
			shufps xmm1, xmm1, 11110101b // xmm1 = (dh, dh, bf, bf)

			subps xmm0, xmm1 // xmm0 = (dh-dh, cg-dh, bf-bf, bf-ae) = (0, cg-dh, 0, ae-bf)

			movaps xmm1, xmm2
			shufps xmm1, xmm2, 10100000b // xmm1 = (dg, dg, be, be)
			addps xmm1, xmm2 // xmm1 = (ch+dg, dg+dg, af+be, be+be)
			shufps xmm1, xmm0, 11011101b // xmm1 = (  0  ,   0  , ch+dg, af+be)
			shufps xmm1, xmm1, 01100010b // xmm1 = (ch+dg,   0  , af+be,   0  )

			addps xmm0, xmm1; // xmm0 = (ch+dg, cg-dh, af+be, ae-bf)
			movaps [edi+ebx], xmm0; // Ergebnis speichern

			// Diese Variante braucht (ohne Load/Store) 13 Instruktionen

			// -----------------------------

			add ebx, 16 // Offset inkrementieren (4*float = 16 Bytes)
			dec ecx // Schleifenzähler dekrementieren
			jmp begin

		end:
			popa; // Register vom Stack holen
		}
#else // Linux implementation
#endif
	}

} // namespace SSERels

#endif // !PLATFORM_X64


/*
    +-------------------------------------+
    |                                     |
    |  SSE3-Optimierte Implementierungen  |
    |                                     |
    +-------------------------------------+
*/

namespace SSE3Rels
{
	/*
	 *  [fwe 2009-04-08] Neue SSE3-Variante der komplexwertigen Multiplikation.
	 *
	 *  - Code wurde auf korrekte Semantik geprüft.
	 *  - Messergebnisse zeigen: ungefähr um Faktor2 schneller als die SSE1-Variante
	 *
	 */

	inline void fm_cmul_x( float* dest, const float* factor1, const float* factor2, unsigned int count )
	{
		FM_XSIZE( count, 2 );

// Windows implementation
#ifdef WIN32
		__asm {
			pusha; // Register auf dem Stack sichern

			mov edi, [dest] // Quell- und Zielzeiger laden
			mov esi, [factor1]
			mov edx, [factor2]

			mov ecx, count // Floor(count/2) als Zähler in ecx
			shr ecx, 1

			xor ebx, ebx // Offset = 0 in ebx

		begin:
			jecxz end // Schleifen-Abbruchbedingung

			    // -= Eigentliche komplex-wertige Multiplikation =-

			movaps xmm0, [esi+ebx] // xmm0 = (b1, a1, b0, a0)
			movaps xmm1, [edx+ebx] // xmm1 = (d1, c1, d0, c0)

			movaps xmm2, xmm0
			shufps xmm2, xmm0, 10110001b // xmm2 = (a1, b1, a0, b0)

			movsldup xmm3, xmm1 // xmm3 = (c1, c1, c0, c0)
			movshdup xmm4, xmm1 // xmm4 = (d1, d1, d0, d0)

			mulps xmm0, xmm3 // xmm0 = (b1*c1, a1*c1, b0*c0, a0*c0)
			mulps xmm2, xmm4 // xmm2 = (a1*d1, b1*d1, a0*d0, b0*d0)				

			addsubps xmm0, xmm2 // xmm0 = (b1*c1+a1*d1, a1*c1-b1*d1, b0*c0+a0*d0, a0*c0-b0*d0)

			movaps [edi+ebx], xmm0; // Ergebnis speichern

			// Diese Variante braucht (ohne Load/Store) 7 Instruktionen

			// -----------------------------

			add ebx, 16 // Offset inkrementieren (4*float = 16 Bytes)
			dec ecx // Schleifenzähler dekrementieren
			jmp begin

		end:
			popa; // Register vom Stack holen
		}
#else // Linux implementation
#endif
	}

} // end namespace SSE3Rels

#endif // !PLATFORM_X64


void fm_init( )
{
	if( bInit )
		return;

	// Prüfen, was die CPU kann
	CPUInfo ci;
	if( ci.DoesCPUSupportFeature( SSE_FEATURE ) )
		uiFlags |= FM_SSE;
	if( ci.DoesCPUSupportFeature( SSE3_FEATURE ) )
		uiFlags |= FM_SSE3;

	bInit = true;
}

unsigned int fm_flags( )
{
	return uiFlags;
}

std::string fm_flags_str( )
{
	// SSE3 impliziert SSE1
	if( uiFlags & FM_SSE3 )
		return "Explicit SSE, SSE3 optimization";

	if( uiFlags & FM_SSE )
		return "Explicit SSE optimization";

#ifdef DEBUG
	return "No optimization (debug mode)";
#else
	return "Compiler optimization";
#endif
}

float* fm_falloc( unsigned int len, bool zeroinit )
{
#ifdef __GNUC__
	// TODO: Für den g++ erstmal kein aligned malloc
	float* ptr = (float*)malloc( len * sizeof( float ) );
#else
	FM_XSIZE( len, 4 );
	int iBytes = len * sizeof( float );
	float* ptr = (float*)_aligned_malloc( iBytes, 16 );
#endif

	assert( ptr != 0 );

	if( zeroinit )
		fm_zero( ptr, len );
	return ptr;
}

void fm_free( float* ptr )
{
#ifdef __GNUC__
	// TODO: Für den g++ erstmal kein aligned free
	if( ptr )
		free( ptr );
#else
	// Nullzeiger ausschließen
	if( ptr )
		_aligned_free( ptr );
#endif
}

/*
   +----------------------+
   |                      |
   |  Funktionensweichen  |
   |                      |
   +----------------------+
*/


void fm_set( float* buf, float value, unsigned int count )
{
	// Hinweis: Momentan keine optimierte Version
	CRels::fm_set( buf, value, count );
}

void fm_zero( float* buf, unsigned int count )
{
	fm_set( buf, 0, count );
}

void fm_copy( float* dest, const float* src, unsigned int count )
{
	// Hinweis: Hier wird keine SSE-Variante eingesetzt obwohl
	//          diese Implementiert ist (siehe Implementierung)
	CRels::fm_copy( dest, src, count );
}

void fm_add( float* dest, const float* summand, unsigned int count )
{
	if( uiFlags & FM_SSE )
		SSERels::fm_add( dest, summand, count );
	else
		CRels::fm_add( dest, summand, count );
}

void fm_sub( float* dest, const float* src, unsigned int count )
{
	if( uiFlags & FM_SSE )
		SSERels::fm_sub( dest, src, count );
	else
		CRels::fm_sub( dest, src, count );
}

void fm_mul( float* dest, float factor, unsigned int count )
{
	if( uiFlags & FM_SSE )
		SSERels::fm_mul( dest, factor, count );
	else
		CRels::fm_mul( dest, factor, count );
}

void fm_cmul( float* dest, const float* factor, unsigned int count )
{
	// TODO: Optimierte Variante. Momentan nur Standard-Code!
	CRels::fm_cmul( dest, factor, count );
}

void fm_cmul_x( float* dest, const float* factor1, const float* factor2, unsigned int count )
{
	if( uiFlags & FM_SSE3 )
		SSE3Rels::fm_cmul_x( dest, factor1, factor2, count );
	else if( uiFlags & FM_SSE )
		SSERels::fm_cmul_x( dest, factor1, factor2, count );
	else
		CRels::fm_cmul_x( dest, factor1, factor2, count );
}

void fm_cdiv( float* dest, const float* div, unsigned int count )
{
	// TODO: Optimierte Variante. Momentan nur Standard-Code!
	CRels::fm_cdiv( dest, div, count );
}

void fm_cmul_f32( float* dest, const float* src1, const float* src2, int count )
{
	CRels::fm_cmul_f32( dest, src1, src2, count );
}

void fm_cmuladd_f32( float* srcdest, const float* src1, const float* src2, int count )
{
	CRels::fm_cmuladd_f32( srcdest, src1, src2, count );
}


#ifndef FM_NO_AUTOINIT

// Auto-Initialisierer
struct FastMathAutoInitializer
{
	inline FastMathAutoInitializer( ) { fm_init( ); };
};

FastMathAutoInitializer g_oAutoInitializer;

#endif // FM_NO_AUTOINIT

#undef FM_XSIZE
