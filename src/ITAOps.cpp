//--------------------------------------------------------------------------------------
// includes
//--------------------------------------------------------------------------------------
#include "ITAOps.h"

#include "ITABase_instrumentation.h"

#include <ITAConfigUtils.h>
#include <ITAStringUtils.h>
#include <algorithm>
#include <assert.h>
#include <cmath>
#include <fstream>
#include <spline.h>


//--------------------------------------------------------------------------------------
// cartesian2polar
//--------------------------------------------------------------------------------------
void cartesian2polar( float real, float imaginary, float &magnitude, float &phase )
{
	magnitude = (float)sqrt( real * real + imaginary * imaginary );
	phase     = (float)atan2( imaginary, real );
}

//--------------------------------------------------------------------------------------
// polar2cartesian
//--------------------------------------------------------------------------------------
void polar2cartesian( float magnitude, float phase, float &real, float &imaginary )
{
	real      = magnitude * cos( phase );
	imaginary = magnitude * sin( phase );
}

//--------------------------------------------------------------------------------------
// roundIt
//--------------------------------------------------------------------------------------
float roundIt( const float Zahl, const int Stellen )
{
	float tmp = Zahl;
	tmp *= pow( 10.0f, Stellen );
	tmp = floor( tmp + 0.5f );
	tmp *= pow( 10.0f, -Stellen );
	return tmp;
}

//--------------------------------------------------------------------------------------
// getUniqueIDsFromList
//--------------------------------------------------------------------------------------
void getUniqueIDsFromList( const std::list<unsigned int> &inputList, std::list<unsigned int> &outputList )
{
	// returns only unique ids from list
	// example:
	// input: 0 2 0 0 1
	// returns: 0 2 1 // note, ids not sorted, as we don't need that (so far)

	bool idExists = false;

	for( std::list<unsigned int>::const_iterator i = inputList.begin( ); i != inputList.end( ); ++i )
	{
		// check if id is alreay in output list
		for( std::list<unsigned int>::const_iterator j = outputList.begin( ); j != outputList.end( ); ++j )
		{
			if( ( *i ) == ( *j ) )
			{
				idExists = true;
				break;
			}
		} // for all output ids

		if( idExists == true )
		{
			idExists = false;
		}
		else
		{
			outputList.push_back( ( *i ) );
		}
	} // for all input ids

	// TODO: nur zum testen, wenn es laeuft, raus damit
	// print output:
	printf( "------------------------------------------------------------\n" );
	printf( "Input list contains: " );
	for( std::list<unsigned int>::const_iterator i = inputList.begin( ); i != inputList.end( ); i++ )
	{
		printf( "%d, ", ( *i ) );
	}
	printf( "\nOutput list contains: " );
	for( std::list<unsigned int>::const_iterator j = outputList.begin( ); j != outputList.end( ); ++j )
	{
		printf( "%d, ", ( *j ) );
	}

	printf( "\n------------------------------------------------------------\n" );
}

void getUniqueIDsFromList( unsigned int *&inputList, const int &inputListLength, std::list<unsigned int> &outputList )
{
	// returns only unique ids from list
	// example:
	// input: 0 2 0 0 1
	// returns: 0 2 1 // note, ids not sorted, as we don't need that (so far)

	bool idExists = false;

	for( int i = 0; i < inputListLength; ++i )
	{
		// check if id is alreay in output list
		for( std::list<unsigned int>::const_iterator j = outputList.begin( ); j != outputList.end( ); ++j )
		{
			if( inputList[i] == ( *j ) )
			{
				idExists = true;
				break;
			}
		} // for all output ids

		if( idExists == true )
		{
			idExists = false;
		}
		else
		{
			outputList.push_back( inputList[i] );
		}
	} // for all input ids
}

void getUniqueIDsFromList( const std::vector<unsigned int> &inputList, std::list<unsigned int> &outputList )
{
	// returns only unique ids from list
	// example:
	// input: 0 2 0 0 1
	// returns: 0 2 1 // note, ids not sorted, as we don't need that (so far)

	bool idExists = false;

	for( unsigned int i = 0; i < inputList.size( ); ++i )
	{
		// check if id is alreay in output list
		for( std::list<unsigned int>::const_iterator j = outputList.begin( ); j != outputList.end( ); ++j )
		{
			if( inputList[i] == ( *j ) )
			{
				idExists = true;
				break;
			}
		} // for all output ids

		if( idExists == true )
		{
			idExists = false;
		}
		else
		{
			outputList.push_back( inputList[i] );
		}
	} // for all input ids

	/*	// TODO: nur zum testen, wenn es laeuft, raus damit
	    //print output:
	    printf("------------------------------------------------------------\n");
	    printf("Input list contains: ");
	    for (unsigned int i = 0; i < inputList.size(); i++)
	    {
	    printf("%d, ", inputList[i]);
	    }

	    printf("\nOutput list contains: ");
	    for (std::list<unsigned int>::const_iterator j = outputList.begin();
	    j != outputList.end();
	    j++)
	    {
	    printf("%d, ", (*j));
	    }

	    printf("\n------------------------------------------------------------\n");
	    */
}

//--------------------------------------------------------------------------------------
// getID_BinaryToInteger
//--------------------------------------------------------------------------------------
void getID_BinaryToInteger( const std::list<unsigned int> &inputList, unsigned int &roomCombinationListID )
{
	roomCombinationListID = 0;

	for( std::list<unsigned int>::const_iterator i = inputList.begin( ); i != inputList.end( ); i++ )
	{
		roomCombinationListID = (unsigned int)( roomCombinationListID + pow( 2.0f, (int)( *i ) ) );
	}
}

unsigned int getID_BinaryToInteger( const std::list<unsigned int> &inputList )
{
	if( inputList.size( ) == 1 )
		return 1;

	unsigned int roomCombinationListID = 0;

	for( std::list<unsigned int>::const_iterator i = inputList.begin( ); i != inputList.end( ); ++i )
	{
		roomCombinationListID = (unsigned int)( roomCombinationListID + pow( 2.0f, (int)( *i ) ) );
	}

	return roomCombinationListID;
}


//--------------------------------------------------------------------------------------
// storeCurrentSubPowerSet
//--------------------------------------------------------------------------------------
void storeCurrentSubPowerSet( const std::list<unsigned int> &my_power_set,   // IN
                              std::list<unsigned int> &valid_power_set_ids ) // OUT
{
	//	printf("\nids: ");

	for( std::list<unsigned int>::const_iterator i = my_power_set.begin( ); i != my_power_set.end( ); ++i )
	{
		//		printf("%d", (*i));
	}
	unsigned int tmp_id = getID_BinaryToInteger( my_power_set );
	// we don't need index 0
	if( tmp_id != 0 )
	{
		valid_power_set_ids.push_back( tmp_id );
	}
	//	printf("\n");
}

//--------------------------------------------------------------------------------------
// buildSubPowerSets (for recursive calls)
//--------------------------------------------------------------------------------------
void buildSubPowerSets( unsigned int &counter, unsigned int startIndex, const std::list<unsigned int> &my_power_set, std::list<unsigned int> &my_helper,
                        std::list<unsigned int> &valid_power_set_ids )
{
	storeCurrentSubPowerSet( my_helper, valid_power_set_ids );

	counter++;

	std::list<unsigned int>::const_iterator i = my_power_set.begin( );

	unsigned int counter2 = startIndex;

	// verschieben des zeigers auf den startIndex
	for( unsigned int k = 0; k < startIndex; k++ )
	{
		i++;
	}
	// for( unsigned int i = startIndex; i < my_power_set.size(); ++i )
	for( i; i != my_power_set.end( ); ++i )
	{
		counter2++;
		my_helper.push_back( ( *i ) );
		buildSubPowerSets( counter, counter2, my_power_set, my_helper, valid_power_set_ids );
		my_helper.pop_back( );
	}
}

//--------------------------------------------------------------------------------------
// buildSubPowerSets (initial)
//--------------------------------------------------------------------------------------
void buildSubPowerSets( const std::list<unsigned int> &my_power_set, std::list<unsigned int> &valid_power_set_ids )
{
	std::list<unsigned int> tmp_power_set;
	unsigned int my_counter = 1;
	buildSubPowerSets( my_counter, 0, my_power_set, tmp_power_set, valid_power_set_ids );
}

//--------------------------------------------------------------------------------------
// Interpolation
//--------------------------------------------------------------------------------------
void Interpolation( std::vector<float> &x, const std::vector<float> &y, const std::vector<float> &xDomainScale, const std::vector<float> &yDomainScale,
                    const unsigned int IntType )
{
	// Ausgabe auf richtige Gr��e skalieren
	x.resize( xDomainScale.size( ) );

	float m              = 0.0f;
	unsigned int counter = 0;                                  // Z�hlvariable
	unsigned int xSize   = (unsigned int)xDomainScale.size( ); // Gr��e x-Array
	unsigned int ySize   = (unsigned int)yDomainScale.size( ); // Gr��e y-Array

	if( IntType == 0 ) // Halteglied
	{
		for( unsigned int i = 0; i < xSize; i++ )
		{
			if( xDomainScale[i] <= yDomainScale[counter] )
			{
				x[i] = y[counter];
			}
			else if( ( xDomainScale[i] > yDomainScale[counter] ) && ( counter < ( ySize - 1 ) ) )
			{
				counter++;
				x[i] = y[counter];
			}
			else
			{
				x[i] = y[counter];
			}
		}
	}
	else if( IntType == 1 ) // lineare Interpolation
	{
		for( unsigned int i = 0; i < xSize; i++ )
		{
			// Frequenzindex unter der Untergrenze, einfaches Halteglied
			if( xDomainScale[i] < yDomainScale[0] )
			{
				x[i] = y[0];
			}
			// Frequenzindex �ber Obergrenze, einfaches Halteglied
			else if( xDomainScale[i] > yDomainScale[ySize - 1] )
			{
				x[i] = y[ySize - 1];
			}
			else // sonst: lineare Interpolation
			{
				//// trick um nullen im sp�ten bereich zu vermeiden...
				//// ####################################################################################################
				// if (y[counter+1] == 0)
				//	y[counter+1] = y[counter]/2;
				//// ####################################################################################################
				//// soll hier wirklich das input array �berschrieben werden??????

				m    = ( y[counter + 1] - y[counter] ) / ( yDomainScale[counter + 1] - yDomainScale[counter] );
				x[i] = y[counter] + m * ( xDomainScale[i] - yDomainScale[counter] );


				if( ( xDomainScale[i] >= yDomainScale[counter] ) && ( counter < ( ySize - 2 ) ) )
					counter++;
			}
		}
	}
	else if( IntType == 2 ) // Spline Interpolation
	{
		// TODO: eventuell noch die Erweiterung der St�tzstellen einf�hren um ein
		//		�bersprechen an den Grenzen zu vermeiden.
		float *ypp = NULL;
		// determine needed matrix for spline interpolation and store it in ypp
		ypp = spline_cubic_set( ySize, const_cast<float *>( &yDomainScale[0] ), const_cast<float *>( &y[0] ), 0, y[0], 0, y[ySize - 1] );

		// interpolate all required nodes
		float fDummy;
		for( unsigned int i = 0; i < xSize; i++ )
			x[i] = spline_cubic_val( ySize, const_cast<float *>( &yDomainScale[0] ), const_cast<float *>( &y[0] ), ypp, xDomainScale[i], &fDummy, &fDummy );

		delete[] ypp;
	}
	else
	{
		printf( "Fehler!! Diese Interpolationsmethode gibt es nicht!!" );
	}
}


//--------------------------------------------------------------------------------
// Insert-Routinen
//--------------------------------------------------------------------------------
void Insert( const std::vector<float> &pfSource, std::vector<float> &pfDest, const float fOffset, const unsigned int uiLength )
{
	unsigned int uiStart = (unsigned int)floorf( std::max( 0.0f, fOffset ) + 0.5f ); // rounding
	for( unsigned int j = 0; j < uiLength; ++j )
		pfDest[uiStart + j] += pfSource[j];
}

void Insert( const float *pfSource, std::vector<float> &pfDest, const float fOffset, const unsigned int uiLength )
{
	// assert( fOffset > 0.0f );
	unsigned int uiStart = (unsigned int)floorf( std::max( 0.0f, fOffset ) + 0.5f ); // rounding
	for( unsigned int j = 0; j < uiLength; ++j )
		pfDest[uiStart + j] += pfSource[j];
}

void Insert_scaled( const std::vector<float> &pfSource, std::vector<float> &pfDest, const float fOffset, const unsigned int uiLength, const float scaleFactor )
{
	unsigned int uiStart = (unsigned int)floorf( std::max( 0.0f, fOffset ) + 0.5f ); // rounding
	for( unsigned int j = 0; j < uiLength; ++j )
		pfDest[uiStart + j] += pfSource[j] * scaleFactor;
}

void Insert_scaled( const std::vector<float> &pfSource, std::vector<float> &pfDest, const unsigned int fOffset, const unsigned int uiLength, const float scaleFactor )
{
	for( unsigned int j = 0; j < uiLength; ++j )
		pfDest[fOffset + j] += pfSource[j] * scaleFactor;
}

void Insert_scaled( const float *pfSource, std::vector<float> &pfDest, const unsigned int fOffset, const unsigned int uiLength, const float scaleFactor )
{
	for( unsigned int j = 0; j < uiLength; ++j )
		pfDest[fOffset + j] += pfSource[j] * scaleFactor;
}

void Insert_scaled( const float *pfSource, float *pfDest, const unsigned int fOffset, const unsigned int uiNumSamples, const float scaleFactor )
{
	for( unsigned int j = 0; j < uiNumSamples; ++j )
		pfDest[fOffset + j] += pfSource[j] * scaleFactor;
}


void Insert_scaled( const std::vector<float> &pfSource, std::vector<float> &pfDest, const unsigned int insertPosition, const float scaleFactor )
{
	for( unsigned int j = 0; j < pfSource.size( ); ++j )
		pfDest[insertPosition + j] += pfSource[j] * scaleFactor;
}

void Insert_scaled( const float *pfSource, float *pfDest, const int iOffset, const int iNumSamples, const float fScaleFactor )
{
	for( int i = 0; i < iNumSamples; ++i )
		pfDest[iOffset + i] += pfSource[i] * fScaleFactor;
}


//--------------------------------------------------------------------------------------
// Interpolationsroutinen
//--------------------------------------------------------------------------------------

void interpolate( std::vector<float> &result, const std::vector<float> &a, const std::vector<float> &b )
{
	for( unsigned int j = 0; j < result.size( ); j++ )
	{
		result[j] = ( a[j] + b[j] ) / 2;
	}
}

int readPortalParameter( const std::string filename_fullPath, std::vector<float> &transfer_function, std::string &portal_name, std::string &portal_notes,
                         std::string &portal_surface_material )
{
	// check if file is valid (at least the importan information must be present)
	if( ( !INIFileSectionExists( filename_fullPath, "Portal" ) ) || ( !INIFileKeyExists( filename_fullPath, "Portal", "dampingFunction" ) ) ||
	    ( !INIFileKeyExists( filename_fullPath, "Portal", "surfaceMaterial" ) ) )
	{
		return EXIT_FAILURE;
	}

	portal_name             = ( INIFileReadString( filename_fullPath, "Portal", "name", "n/a" ) ).c_str( );
	portal_notes            = ( INIFileReadString( filename_fullPath, "Portal", "notes", "n/a" ) ).c_str( );
	portal_surface_material = ( INIFileReadString( filename_fullPath, "Portal", "surfaceMaterial", "n/a" ) ).c_str( );
	transfer_function       = INIFileReadFloatList( filename_fullPath, "Portal", "dampingFunction", ',' );

	return EXIT_SUCCESS;
}

//--------------------------------------------------------------------------------------
// determineCurrentValidPowerSetIDs
//--------------------------------------------------------------------------------------
void determineCurrentValidPowerSetIDs( const std::list<unsigned int> &currentValidRoomIDs, std::list<unsigned int> &currentValidPowerSetIDs )
{
	// printf("determining current valid ISRoomCombinationList ids...\n");
	currentValidPowerSetIDs.clear( );

	buildSubPowerSets( currentValidRoomIDs,       // in potenzmenge
	                   currentValidPowerSetIDs ); // out; indizes auf die isroomcombination liste

#ifdef _DEBUG
	std::string sOutput;
	sOutput += "current room(s) ";
	for( std::list<unsigned int>::const_iterator i = currentValidRoomIDs.begin( ); i != currentValidRoomIDs.end( ); i++ )
	{
		sOutput += UIntToString( *i );
		sOutput += ", ";
	}
	sOutput += "are accessible in raven world ...";
	ITABASE_INFO( sOutput );

	sOutput = "current valid ids for primary source: image sources/room power set list are: ";
	for( std::list<unsigned int>::const_iterator i = currentValidPowerSetIDs.begin( ); i != currentValidPowerSetIDs.end( ); i++ )
	{
		sOutput += UIntToString( *i );
		sOutput += ", ";
	}
	ITABASE_INFO( sOutput );
#endif
}

//--------------------------------------------------------------------------------------
// writeVector (for debugging)
//--------------------------------------------------------------------------------------
void writeVector( std::vector<float> vfVec, std::string sFileName )
{
	std::string sFilePath = "..\\RavenOutput\\";
	sFilePath.append( sFileName );

	std::ofstream osOutfile( sFilePath.c_str( ) );
	for( unsigned int i = 0; i < vfVec.size( ); ++i )
		osOutfile << roundIt( vfVec[i], 7 ) << std::endl;

	osOutfile.close( );
}
