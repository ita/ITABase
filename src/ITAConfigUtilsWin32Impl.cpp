#include "ITAConfigUtils.h"

/*
 *  Diese Datei enth�lt Implementierungen einiger Funktionen aus den ITAConfigUtils,
 *  welche platform-spezifisch sind und hier mittels Win32-Funktionen umgesetzt werden.
 */

#ifndef USE_SIMPLEINI
#	ifdef _WIN32

#		include <ITAException.h>
#		include <ITAFilesystemUtils.h>
#		include <windows.h>

// Maximale g�ltige L�nge der Zeichenketten von Werten
const unsigned int MAX_VALUE_STRING_LENGTH = 4096;

// Bugfix (fwe): Falls kein Pfad und nur ein Dateinamen angebenen ist, muss .\ vorne angeh�ngt werden
std::string FixPath( const std::string& sPath )
{
	std::string sCorrectedPathWithSystemSeparator = correctPath( sPath );
	if( sCorrectedPathWithSystemSeparator.find_first_of( PATH_SEPARATOR ) != -1 )
		return sCorrectedPathWithSystemSeparator;
	else
		return std::string( "." ) + PATH_SEPARATOR + sPath;
}

std::vector<std::string> INIFileGetSections( const std::string& sINIFilename )
{
	// [fwe: Bugfix 2008-09-09] Sicherstellen das die Datei �berhaupt existiert
	INIFileRequireINIFile( sINIFilename );

	// fwe: Insgesamt 64k Zeichen f�r die Liste der Sektionsnamen
	//      sollte f�r die meisten Anwendungen reichen
	const unsigned int SECTION_NAME_BUFFER_SIZE = 65536;
	char buf[SECTION_NAME_BUFFER_SIZE];
	std::vector<std::string> vsResult;

	if( GetPrivateProfileSectionNamesA( buf, SECTION_NAME_BUFFER_SIZE, FixPath( sINIFilename ).c_str( ) ) > 0 )
	{
		char* p = buf;
		unsigned int l;

		while( ( l = (unsigned int)strlen( p ) ) > 0 )
		{
			vsResult.push_back( p );
			p += l + 1;
		}
	}

	return vsResult;
}

std::vector<std::string> INIFileGetKeys( const std::string& sINIFilename, const std::string& sSection )
{
	// [fwe: Bugfix 2008-09-09] Sicherstellen das die Datei �berhaupt existiert
	INIFileRequireINIFile( sINIFilename );

	// fwe: Insgesamt 64k Zeichen f�r die Liste der Sektionsnamen
	//      sollte f�r die meisten Anwendungen reichen
	const unsigned int SECTION_BUFFER_SIZE = 65536;
	char buf[SECTION_BUFFER_SIZE];
	std::vector<std::string> vsResult;

	if( GetPrivateProfileSectionA( sSection.c_str( ), buf, SECTION_BUFFER_SIZE, FixPath( sINIFilename ).c_str( ) ) > 0 )
	{
		char* p = buf;
		unsigned int l;

		while( ( l = (unsigned int)strlen( p ) ) > 0 )
		{
			std::string s = p;
			// Windows macht einen Fehler und gibt hier auch auskommentierte Schl�ssel zur�ck. Deshalb Filtern:
			if( s[0] != '#' )
			{
				// Gleichheitszeichen suchen
				std::string t = s.substr( 0, s.find_first_of( '=' ) );

				if( !t.empty( ) )
					vsResult.push_back( t );
			}

			p += l + 1;
		}
	}

	return vsResult;
}

std::string INIFileReadString( const std::string& sINIFilename, const std::string& sSection, const std::string& sKey, std::string sDefaultValue )
{
	// Sicherstellen das die INI-Datei existiert
	INIFileRequireINIFile( sINIFilename );

	// Zeichenpuffer
	char buf[MAX_VALUE_STRING_LENGTH];

	GetPrivateProfileStringA( sSection.c_str( ), sKey.c_str( ), sDefaultValue.c_str( ), (LPSTR)buf, (DWORD)MAX_VALUE_STRING_LENGTH, FixPath( sINIFilename ).c_str( ) );

	return std::string( buf );
}

bool INIFileWriteString( const std::string& sINIFilename, const std::string& sSection, const std::string& sKey, const std::string& sValue )
{
	// [fwe 2011-06-14] Improvement: Put a whitespace after the = in the file. Looks much nicer ;-)
	std::string s( " " );
	s.append( sValue );
	return ( WritePrivateProfileStringA( sSection.c_str( ), sKey.c_str( ), s.c_str( ), FixPath( sINIFilename ).c_str( ) ) == TRUE );
}

#	endif // _WIN32
#endif // !USE_SIMPLEINI
