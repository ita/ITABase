#include <ITAException.h>

ITAException::ITAException( int iErrorCode_, std::string sModule_, std::string sReason_ ) : iErrorCode( iErrorCode_ ), sModule( sModule_ ), sReason( sReason_ ) {}

std::string ITAException::ToString( ) const
{
	std::string sError;

	switch( iErrorCode )
	{
		case ITAException::INVALID_PARAMETER:
			sError = "Invalid parameter";
			break;
		case ITAException::NOT_IMPLEMENTED:
			sError = "Not implemented";
			break;
		case ITAException::NETWORK_ERROR:
			sError = "Network error";
			break;
		case ITAException::UNKNOWN:
		default:
			sError = "Unkown error";
	}

	if( sModule.empty( ) == false )
		sError += " in " + sModule;

	if( sReason.empty( ) == false )
		sError += ": " + sReason;

	return sError;
}

std::ostream& operator<<( std::ostream& os, const ITAException& ex )
{
	return os << ex.ToString( );
}
