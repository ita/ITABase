#include <ITAHPT.h>

// Unsichereres aber genaueres Timestamp Register (RDTSC) anstelle der Windows Performance Counters nutzen
//#define USE_INSECURE_RDTSC

// Kein Inline-Assembler unter 64-Bit Windows. RDTSC-Variante deaktivieren
#ifdef PLATFORM_X64
#	ifdef USE_INSECURE_RDTSC
#		undef USE_INSECURE_RDTSC
#	endif
#endif

#include <cmath>

#ifndef _WIN32_WINNT // @todo: remove
#	define _WIN32_WINNT 0x0501
#endif
#include "ITAException.h"

#include <windows.h>

#define RDTSC_MEASUREMENT_LOOPS 1000

static bool _bInitialized = false;
static double _dFrequency;

#ifdef USE_INSECURE_RDTSC

// RDTSC Implementierung

void ITAHPT_init( )
{
	if( _bInitialized )
		return;

	// Den hochauflösenden Windows-Timer zum Ausmessen verwenden
	LARGE_INTEGER fPC;
	if( !QueryPerformanceFrequency( &fPC ) )
		ITA_EXCEPT1( UNKNOWN, "Abfrage der Win32-Timerfrequenz fehlgeschlagen" );

	// Wir nehmen einen kritischen Bereich zur besseren Messung
	CRITICAL_SECTION cs;
	InitializeCriticalSection( &cs );
	EnterCriticalSection( &cs );

	// Messprinzip: Bestimmung der Frequenz über die bekannten
	//              Kenngrößen des Windows Performance Counters
	LARGE_INTEGER t1, t2, t3, t4;
	_dFrequency = 0;
	for( unsigned int i = 0; i < RDTSC_MEASUREMENT_LOOPS; i++ )
	{
		QueryPerformanceCounter( (LARGE_INTEGER*)&t3 );
		__asm {
			rdtsc
				mov t1.LowPart,  eax
				mov t1.HighPart, edx
		}

		// Verschwende etwas Zeit, um die Messung stabiler zu machen

		// Hinweis zur Genauigkeit von Frank Wefers:
		//
		// Auf meinem Pentium-M 1400 MHz ThinkPad betrug bei 10000
		// Verschwedungszyklen (siehe unten) die Abweichung der
		// gemessenen Frequenz (~1395 MHz) <= +/- 2 MHz
		// Dies entspricht einer Messungenauigkeit von ~1/700

		int s = 0;
		for( unsigned int i = 0; i < 10000; i++ )
			s += i;

		QueryPerformanceCounter( (LARGE_INTEGER*)&t4 );
		__asm {
			rdtsc
				mov t2.LowPart,  eax
				mov t2.HighPart, edx
		}

		double d1 = (double)( t2.QuadPart - t1.QuadPart );
		double d2 = (double)( t4.QuadPart - t3.QuadPart );
		_dFrequency += d1 / ( d2 / ( (double)fPC.QuadPart ) );
	}

	LeaveCriticalSection( &cs );
	DeleteCriticalSection( &cs );

	_dFrequency /= RDTSC_MEASUREMENT_LOOPS;
	_bInitialized = true;
}

#else

// Windows Performance Counter Implementierung

void ITAHPT_init( )
{
	if( _bInitialized )
		return;

	// Den hochauflösenden Windows-Timer zum Ausmessen verwenden
	LARGE_INTEGER li;
	if( !QueryPerformanceFrequency( &li ) )
		ITA_EXCEPT1( UNKNOWN, "Abfrage der Win32-Timerfrequenz fehlgeschlagen" );

	_dFrequency   = (double)li.QuadPart;
	_bInitialized = true;
}

#endif

#ifdef USE_INSECURE_RDTSC

// RDTSC Implementierung

ITATimerTicks ITAHPT_now( )
{
	if( !_bInitialized )
		ITAHPT_init( );

	LARGE_INTEGER t;
	__asm {
		rdtsc
			mov t.LowPart,  eax
			mov t.HighPart, edx
	}
	return (ITATimerTicks)t.QuadPart;
}

#else

// Windows Performance Counter Implementierung

ITATimerTicks ITAHPT_now( )
{
	if( !_bInitialized )
		ITAHPT_init( );

	LARGE_INTEGER li;
	QueryPerformanceCounter( &li );
	return (ITATimerTicks)li.QuadPart;
}

#endif

double ITAHPT_frequency( )
{
	return _dFrequency;
}

double ITAHPT_resolution( )
{
	return 1 / _dFrequency;
}

ITATimerTicks toTimerTicks( double t )
{
	return (ITATimerTicks)ceil( t * (double)_dFrequency );
}

double toSeconds( ITATimerTicks n )
{
	return ( (double)n ) / ( (double)_dFrequency );
}

std::string toString( ITATimerTicks n )
{
	// Der Einfachheit und Überlaufsicherheit-halber in 64-Bit rechnen:
	__int64 h, m, s, ms, mus, ns;
	ns  = (__int64)floor( toSeconds( n ) * 1000000000.0 );
	mus = ns / 1000;
	ns %= 1000;
	ms = mus / 1000; // Idee: Wert im 10^(-3) faktorisieren.
	mus %= 1000;     //   ... Modulus (Rest) übrig lassen.
	s = ms / 1000;
	ms %= 1000;
	m = s / 60;
	s %= 60;
	h = m / 60;
	m %= 60;

	char buffer[255];
	sprintf( buffer, "%I64dh%I64dm%I64ds%I64dms%I64dmks%I64dns", h, m, s, ms, mus, ns );
	return std::string( buffer );
}

/*
std::string cleverFourDigits(double x) {
char buf[255];
if (x < 1)
sprintf(buf, "%0.3f", x);
else
if (x < 10)
sprintf(buf, "%0.2f", x);
else
if (x < 100)
sprintf(buf, "%0.1f", x);
else
sprintf(buf, "%0.0f", x);
return buf;
}
*/

std::string convertTimeToHumanReadableString( double dSeconds )
{
	char buf[255];

	// if (dSeconds == 0) return "0 s ";

	if( dSeconds < 0.000001 )
	{ // Kleiner als 1 ms
		// return cleverFourDigits(dSeconds*1000000000) + " ns";
		sprintf( buf, "%0.3f ns", dSeconds * 1000000000 );
		return std::string( buf );
	}

	if( dSeconds < 0.001 )
	{ // Kleiner als 1 ms
		// return cleverFourDigits(dSeconds*1000000) + " us";
		sprintf( buf, "%0.3f us", dSeconds * 1000000 );
		return std::string( buf );
	}

	if( dSeconds < 1 )
	{ // Kleiner als 1 ms
		// return cleverFourDigits(dSeconds*1000) + " ms";
		sprintf( buf, "%0.3f ms", dSeconds * 1000 );
		return std::string( buf );
	}
	/*
	    __int64 iSecs = ceil(dSeconds);
	    double iHSecs = dSeconds - iSecs;

	    if (iSecs < 60) {
	    sprintf(buf, "%0.3f s ", dSeconds);
	    return buf;
	    }
	    */
	sprintf( buf, "%0.3f s", dSeconds );
	return std::string( buf );
}