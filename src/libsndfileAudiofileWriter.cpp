#include "libsndfileAudiofileWriter.h"

#include <ITAException.h>
#include <ITATypes.h>
#include <algorithm>
#include <locale>

// Voreinstellung: Maximale Gr��e des Schreibpuffers (in Bytes)
// TODO: Welches ist eine sinnvolle Vorgabe?
#define WRITE_BUFFER_SIZE 1024 * 1024 // = 1 MB

libsndfileAudiofileWriter::libsndfileAudiofileWriter( const std::string& sFilePath, const ITAAudiofileProperties& props )
{
	m_pfWriteBuffer = 0;
	m_props         = props;

	if( sFilePath.empty( ) )
		ITA_EXCEPT1( INVALID_PARAMETER, "sFilename" );
	if( props.eQuantization == ITAQuantization::ITA_UNKNOWN_QUANTIZATION )
		ITA_EXCEPT1( INVALID_PARAMETER, "Quantisierung muss explizit angegeben werden" );
	if( props.eDomain == ITADomain::ITA_FREQUENCY_DOMAIN )
		ITA_EXCEPT1( INVALID_PARAMETER, "Frequenzbereich nicht unterst�tzt" );

	// Eigenschaften in SF_INFO-Struktur umsetzen
	SF_INFO info;
	info.frames     = 0;
	info.samplerate = (int)props.dSampleRate;
	info.channels   = (int)props.iChannels;
	info.sections   = 0;
	info.seekable   = 0;
	info.format     = 0;

	std::string sSuffix = sFilePath.substr( sFilePath.find_last_of( "." ) + 1 );
	for( size_t i = 0; i < sSuffix.length( ); i++ )
		sSuffix[i] = std::toupper( sSuffix.c_str( )[i], std::locale( ) );

	if( sSuffix == "WAV" )
		info.format |= SF_FORMAT_WAV;
	if( ( sSuffix == "AIF" ) || ( sSuffix == "AIFF" ) )
		info.format |= SF_FORMAT_AIFF;
	if( ( sSuffix == "AU" ) || ( sSuffix == "SND" ) )
		info.format |= SF_FORMAT_AU;
	if( ( sSuffix == "RAW" ) || ( sSuffix == "PCM" ) )
		info.format |= SF_FORMAT_RAW;

	if( info.format == 0 )
		ITA_EXCEPT1( INVALID_PARAMETER, "Dateiformat konnte nicht aus dem Dateinamen bestimmt werden" );

	switch( props.eQuantization )
	{
		case ITAQuantization::ITA_INT16:
			info.format |= SF_FORMAT_PCM_16;
			break;
		case ITAQuantization::ITA_INT24:
			info.format |= SF_FORMAT_PCM_24;
			break;
		case ITAQuantization::ITA_INT32:
			info.format |= SF_FORMAT_PCM_32;
			break;
		case ITAQuantization::ITA_FLOAT:
			info.format |= SF_FORMAT_FLOAT;
			break;
		case ITAQuantization::ITA_DOUBLE:
			info.format |= SF_FORMAT_DOUBLE;
			break;
	}

	// Standard Endian-ness des Dateiformates benutzen
	info.format |= SF_ENDIAN_FILE;

	// Pr�fen ob diese Kombination von Merkmalen funktioniert
	if( !sf_format_check( &info ) )
		ITA_EXCEPT1( INVALID_PARAMETER, "Eigenschaften nicht unterst�tzt oder kompatibel" );

	// Versuchen die Datei zu �ffnen
	if( ( _sf = sf_open( sFilePath.c_str( ), SFM_WRITE, &info ) ) == nullptr )
		ITA_EXCEPT1( IO_ERROR, sf_strerror( _sf ) );

	// Kommentar setzen
	if( !props.sComment.empty( ) )
		if( sf_set_string( _sf, SF_STR_COMMENT, props.sComment.c_str( ) ) != 0 )
			ITA_EXCEPT1( IO_ERROR, "Kommentar konnte nicht gesetzt werden" );

	// Schreibpuffer allozieren (anhand Vorgabe)
	m_iWriteBufferSize = WRITE_BUFFER_SIZE / props.iChannels;
	m_pfWriteBuffer    = new float[m_iWriteBufferSize * props.iChannels];
}

libsndfileAudiofileWriter::~libsndfileAudiofileWriter( )
{
	delete[] m_pfWriteBuffer;
	// Audiodatei schlie�en, falls diese ge�ffnet wurde
	if( _sf != 0 )
		if( sf_close( _sf ) != 0 )
			ITA_EXCEPT1( IO_ERROR, "Audiodatei konnte nicht geschlossen werden" );
}

void libsndfileAudiofileWriter::write( int iLength, std::vector<const float*> vpfSource )
{
	if( iLength < 0 )
		ITA_EXCEPT1( INVALID_PARAMETER, "Given length of audio file below zero, something went wrong beforehand." );

	if( iLength == 0 )
		return;

	// Wenn keine Zeiger im Vektor enthalten sind, ist nichts zu tun!
	if( vpfSource.empty( ) )
		return;

	// Falls mehr Zeiger im Vektor enthalten sind als Kan�le in der Datei,
	// so wird Sicherheitshalber eine Ausnahme geworfen!
	if( vpfSource.size( ) > m_props.iChannels )
		ITA_EXCEPT1( IO_ERROR, "Mehr Elemente im Vektor als Kanaele in der Datei" );

	// Testen: Alle Zeiger im Vektor = Nullzeiger?
	bool bNull = true;
	for( size_t i = 0; i < vpfSource.size( ); i++ )
		if( vpfSource[i] )
			bNull = false;
	if( bNull )
		return;

	// Schreibpuffer mit den notwendigen Nullsamples f�llen
	for( size_t i = 0; i < vpfSource.size( ); i++ )
		if( !vpfSource[i] )
			for( int j = 0; j < std::min( iLength, m_iWriteBufferSize ); j++ )
				m_pfWriteBuffer[j * m_props.iChannels + i] = 0;

	int n = 0; // Anzahl geschriebene Samples
	while( n < iLength )
	{
		// Wieviele Samples werden in diesem Durchgang geschrieben?
		int k = std::min( iLength - n, m_iWriteBufferSize );

		// Daten zusammenstellen (interleaven)
		for( size_t i = 0; i < vpfSource.size( ); i++ )
			if( vpfSource[i] )
			{
				const float* pfData = vpfSource[i];
				for( size_t j = 0; j < k; j++ )
					m_pfWriteBuffer[j * m_props.iChannels + i] = pfData[n + j];
			}

		// Daten schreiben
		sf_count_t frames = (sf_count_t)k;
		if( sf_writef_float( _sf, m_pfWriteBuffer, frames ) != frames )
			ITA_EXCEPT1( IO_ERROR, sf_strerror( _sf ) );

		n += k;
	}
}
