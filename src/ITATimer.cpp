#include <ITATimer.h>

// STL Includes
#include <algorithm>
#include <cmath>

// ITAToolkit Includes
#include <ITAException.h>


ITATimer::ITATimer( double dDuration, bool bPeriodic )
{
	InitializeCriticalSection( &m_cs );
	m_dDuration = dDuration;
	m_bPeriodic = bPeriodic;
	m_bActive   = false;
	m_bDirty    = false;

	m_hWaitableTimer = m_hTerminateEvent = m_hThread = 0;

	// WaitableTimer erzeugen (Manuelles zur�cksetzen
	if( FAILED( m_hWaitableTimer = CreateWaitableTimer( NULL, FALSE, NULL ) ) )
		ITA_EXCEPT1( UNKNOWN, "Unable to create waitable timer" );

	// Terminierungs-Ereignis erzeugen
	if( FAILED( m_hTerminateEvent = CreateEvent( NULL, FALSE, FALSE, NULL ) ) )
		ITA_EXCEPT1( UNKNOWN, "Unable to create event" );

	// Thread erzeugen
	if( FAILED( m_hThread = CreateThread( NULL, 0, &ITATimer::threadProc, this, 0, NULL ) ) )
		ITA_EXCEPT1( UNKNOWN, "Unable to create timer thread" );

	SetThreadPriority( m_hThread, THREAD_PRIORITY_TIME_CRITICAL );
}

ITATimer::~ITATimer( )
{
	EnterCriticalSection( &m_cs );

	if( ( m_hTerminateEvent != 0 ) && ( m_hThread != 0 ) )
	{
		SetEvent( m_hTerminateEvent );
		WaitForSingleObject( m_hThread, 2000 );
	}

	CloseHandle( m_hThread );
	CloseHandle( m_hWaitableTimer );
	CloseHandle( m_hTerminateEvent );
	DeleteCriticalSection( &m_cs );
}

double ITATimer::getDuration( ) const
{
	return m_dDuration;
}

bool ITATimer::isPeriodic( ) const
{
	return m_bPeriodic;
}

void ITATimer::start( )
{
	EnterCriticalSection( &m_cs );
	if( m_bActive )
	{
		LeaveCriticalSection( &m_cs );
		return;
	}

	LARGE_INTEGER liDueTime;
	liDueTime.QuadPart = 0;

	// Dauer in 100-Nanosekunden-Intervallen
	long lPeriod = (long)std::floor( m_dDuration * 1000 );

	if( FAILED( SetWaitableTimer( m_hWaitableTimer, &liDueTime, lPeriod, NULL, NULL, FALSE ) ) )
	{
		LeaveCriticalSection( &m_cs );
		ITA_EXCEPT1( INVALID_PARAMETER, "Unable to set the waitable timer" );
	}

	m_bActive = true;

	LeaveCriticalSection( &m_cs );
}

void ITATimer::stop( )
{
	EnterCriticalSection( &m_cs );
	if( !m_bActive )
	{
		LeaveCriticalSection( &m_cs );
		return;
	}

	if( FAILED( CancelWaitableTimer( m_hWaitableTimer ) ) )
	{
		LeaveCriticalSection( &m_cs );
		ITA_EXCEPT1( INVALID_PARAMETER, "Unable to cancel the waitable timer" );
	}

	m_bActive = false;

	LeaveCriticalSection( &m_cs );
}

bool ITATimer::isActive( )
{
	EnterCriticalSection( &m_cs );
	bool bResult = m_bActive;
	LeaveCriticalSection( &m_cs );
	return bResult;
}

void ITATimer::attach( ITATimerEventHandler* pHandler )
{
	if( pHandler == 0 )
		ITA_EXCEPT1( INVALID_PARAMETER, "Nullpointer passed as handler pointer" );

	// Sicherstellen, das kein Handler mehr als einmal enthalten ist...
	EnterCriticalSection( &m_cs );

	std::vector<ITATimerEventHandler*>::const_iterator cit = std::find( m_vHandlers.begin( ), m_vHandlers.end( ), pHandler );
	if( cit == m_vHandlers.end( ) )
	{
		m_vHandlers.push_back( pHandler );
		m_bDirty = true;
	}

	LeaveCriticalSection( &m_cs );
}

void ITATimer::detach( ITATimerEventHandler* pHandler )
{
	// Der Nullzeiger wird nie enthalten sein!
	if( pHandler == 0 )
		return;

	EnterCriticalSection( &m_cs );

	std::vector<ITATimerEventHandler*>::iterator it = std::find( m_vHandlers.begin( ), m_vHandlers.end( ), pHandler );
	if( it != m_vHandlers.end( ) )
	{
		m_vHandlers.erase( it );
		m_bDirty = true;
	}

	LeaveCriticalSection( &m_cs );
}

void ITATimer::attach( ITATimerCallback* pCallback )
{
	if( pCallback == 0 )
		ITA_EXCEPT1( INVALID_PARAMETER, "Nullpointer passed as callback function pointer" );

	// Sicherstellen, das kein Handler mehr als einmal enthalten ist...
	EnterCriticalSection( &m_cs );

	std::vector<ITATimerCallback*>::const_iterator cit = std::find( m_vCallbacks.begin( ), m_vCallbacks.end( ), pCallback );

	if( cit == m_vCallbacks.end( ) )
	{
		m_vCallbacks.push_back( pCallback );
		m_bDirty = true;
	}

	LeaveCriticalSection( &m_cs );
}

void ITATimer::detach( ITATimerCallback* pCallback )
{
	// Der Nullzeiger wird nie enthalten sein!
	if( pCallback == 0 )
		return;

	EnterCriticalSection( &m_cs );

	std::vector<ITATimerCallback*>::iterator it = std::find( m_vCallbacks.begin( ), m_vCallbacks.end( ), pCallback );
	if( it != m_vCallbacks.end( ) )
	{
		m_vCallbacks.erase( it );
		m_bDirty = true;
	}

	LeaveCriticalSection( &m_cs );
}

void ITATimer::fire( )
{
	/* Idee hier: Es soll nicht �ber die ganze Zeit der Handler/Callback-Aufrufe
	              die Instanz-eigene Critical Section gelockt werden. Deshalb wird
	              mittels dirty-flag beschrieben, ob sich eine �nderung bei den
	              Handlern/Callbacks ergeben hat. Nur im Falle einer �nderung
	              werden die (internen) Handler-/Callback-Container mit den externen
	              synchronisiert. So ist lock-Zeit nur auf die Abfrage der dirty-flag
	              begrenzt.
	              */

	EnterCriticalSection( &m_cs );

	// Satefy lock, that ensures that the timer is not fired after stop() was called!
	if( !m_bActive )
	{
		LeaveCriticalSection( &m_cs );
		return;
	}

	if( m_bDirty )
	{
		// Synchronisieren
		m_vHandlersInternal.clear( );
		// TODO: Das macht Probleme... Warum?
		// std::copy(m_vHandlers.begin(), m_vHandlers.end(), m_vHandlersInternal.begin());
		for( size_t i = 0; i < m_vHandlers.size( ); i++ )
			m_vHandlersInternal.push_back( m_vHandlers[i] );

		m_vCallbacksInternal.clear( );
		// std::copy(m_vCallbacks.begin(), m_vCallbacks.end(), m_vCallbacksInternal.begin());
		for( size_t i = 0; i < m_vCallbacks.size( ); i++ )
			m_vCallbacksInternal.push_back( m_vCallbacks[i] );

		m_bDirty = false;
	}

	LeaveCriticalSection( &m_cs );

	for( std::vector<ITATimerEventHandler*>::iterator it = m_vHandlersInternal.begin( ); it != m_vHandlersInternal.end( ); ++it )
		( *it )->handleTimerEvent( *this );

	for( std::vector<ITATimerCallback*>::iterator it = m_vCallbacksInternal.begin( ); it != m_vCallbacksInternal.end( ); ++it )
		( *it )( *this );
}

DWORD WINAPI ITATimer::threadProc( LPVOID lpParameter )
{
	ITATimer* pInstance = (ITATimer*)lpParameter;
	HANDLE hObjects[2]  = { pInstance->m_hTerminateEvent, pInstance->m_hWaitableTimer };
	do
	{
		if( WaitForMultipleObjects( 2, hObjects, FALSE, INFINITE ) == WAIT_OBJECT_0 )
			return 0;
		pInstance->fire( );
	} while( true );
}
