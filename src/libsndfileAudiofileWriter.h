/*
 * ----------------------------------------------------------------
 *
 *		ITA core libs
 *		(c) Copyright Institute of Technical Acoustics (ITA)
 *		RWTH Aachen University, Germany, 2015-2024
 *
 * ----------------------------------------------------------------
 *				    ____  __________  _______
 *				   //  / //__   ___/ //  _   |
 *				  //  /    //  /    //  /_|  |
 *				 //  /    //  /    //  ___   |
 *				//__/    //__/    //__/   |__|
 *
 * ----------------------------------------------------------------
 *
 */

#ifndef INCLUDE_WATCHER_LIBSNDFILE_AUDIOFILEWRITER
#define INCLUDE_WATCHER_LIBSNDFILE_AUDIOFILEWRITER

#include "ITAAudiofileWriter.h"

#include <sndfile.h>

class libsndfileAudiofileWriter : public ITAAudiofileWriter
{
public:
	libsndfileAudiofileWriter( const std::string& sFilePath, const ITAAudiofileProperties& props );
	~libsndfileAudiofileWriter( );

	void write( int iLength, std::vector<const float*> vpfSource );

private:
	ITAAudiofileProperties m_props; //!@ Properties
	SNDFILE* _sf;                   //!@ SndFile handle
	int m_iWriteBufferSize;         //!@ Size of buffer
	float* m_pfWriteBuffer;         //!@ Write buffer (channel-interleaved)
};

#endif // INCLUDE_WATCHER_LIBSNDFILE_AUDIOFILEWRITER
