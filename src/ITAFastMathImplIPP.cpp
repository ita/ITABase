#include <ITAException.h>
#include <ITAFastMath.h>
#include <assert.h>
#include <ipp.h>
#include <malloc.h>
#include <memory.h>
#include <stdio.h>
#include <stdlib.h>
#include <xmmintrin.h>

// Makro: Längen auf Vielfaches eines Faktor zu erweitern
#ifdef FM_XSIZE
#	undef FM_XSIZE
#endif
#ifdef FM_NOEXT
#	define FM_XSIZE( V, F )
#else
#	define FM_XSIZE( V, F ) V = F * ( V % F ? ( V / F ) + 1 : V / F );
#endif

bool bInit           = false;
unsigned int uiFlags = 0;

void fm_init( )
{
	if( bInit )
		return;

	if( ippInit( ) != ippStsNoErr )
		ITA_EXCEPT1( UNKNOWN, "Failed to initialize IPP library" );

	IppCpuType cpu = ippGetCpuType( );
	if( cpu | ippCpuSSE )
		uiFlags |= FM_SSE;
	if( cpu | ippCpuSSE3 )
		uiFlags |= FM_SSE3;

	bInit = true;
}

unsigned int fm_flags( )
{
	return uiFlags;
}

std::string fm_flags_str( )
{
	if( uiFlags & FM_SSE3 )
		return "IPP optimization supporting SSE, SSE3";

	if( uiFlags & FM_SSE )
		return "IPP optimization supporting SSE";

	return "IPP optimization";
}

#define FM_XSIZE( V, F ) V = F * ( V % F ? ( V / F ) + 1 : V / F );

float* fm_falloc( unsigned int len, bool zeroinit )
{
	FM_XSIZE( len, 4 );
	float* ptr = (float*)ippsMalloc_32f( len );

	assert( ptr != 0 );

	if( zeroinit )
		fm_zero( ptr, len );
	return ptr;
}

void fm_free( float* ptr )
{
	ippsFree( ptr );
}

void fm_set( float* buf, float value, unsigned int count )
{
	ippsSet_32f( value, buf, count );
}

void fm_zero( float* buf, unsigned int count )
{
	fm_set( buf, 0, count );
}

void fm_copy( float* dest, const float* src, unsigned int count )
{
	ippsCopy_32f( src, dest, count );
}

void fm_add( float* dest, const float* summand, unsigned int count )
{
	ippsAdd_32f_I( summand, dest, count );
}

void fm_add( float* dest, const float* summand1, const float* summand2, unsigned int count )
{
	ippsAdd_32f( summand1, summand2, dest, count );
}

void fm_sub( float* dest, const float* src, unsigned int count )
{
	ippsSub_32f_I( src, dest, count );
}

void fm_sub( float* dest, const float* summand1, const float* summand2, unsigned int count )
{
	ippsSub_32f( summand1, summand2, dest, count );
}

void fm_mul( float* dest, float factor, unsigned int count )
{
	ippsMulC_32f_I( factor, dest, count );
}

void fm_cmul( float* dest, const float* factor, unsigned int count )
{
	ippsMul_32fc_I( (const Ipp32fc*)factor, (Ipp32fc*)dest, count );
}


void fm_cmul( float* dest, const float* factor1, const float* factor2, unsigned int count )
{
	ippsMul_32fc( (const Ipp32fc*)factor1, (const Ipp32fc*)factor2, (Ipp32fc*)dest, count );
}

void fm_cmul_x( float* dest, const float* factor1, const float* factor2, unsigned int count )
{
	ippsMul_32fc( (const Ipp32fc*)factor1, (const Ipp32fc*)factor2, (Ipp32fc*)dest, count );
}

void fm_cdiv( float* dest, const float* div, unsigned int count )
{
	ippsDiv_32fc_I( (const Ipp32fc*)div, (Ipp32fc*)dest, count );
}

// Real-valued addition (in-place)
// Semantic: srcdest[i] += src[i]
void fm_add_f32( float* srcdest, const float* src, int count )
{
	ippsAdd_32f_I( (const Ipp32f*)src, (Ipp32f*)srcdest, count );
}

// Real-valued multiplication (in-place)
// Semantic: srcdest[i] *= src[i]
void fm_mul_f32( float* srcdest, const float* src, int count )
{
	ippsMul_32f_I( (const Ipp32f*)src, (Ipp32f*)srcdest, count );
}

// Complex-valued multiply
// Semantic: dest[i] = src1[i]*src2[i]
void fm_cmul_f32( float* dest, const float* src1, const float* src2, int count )
{
	ippsMul_32fc( (const Ipp32fc*)src1, (const Ipp32fc*)src2, (Ipp32fc*)dest, count );
}

// Complex-valued multiply-accumulate
// Semantic: srcdest[i] += src1[i]*src2[i]
void fm_cmuladd_f32( float* srcdest, const float* src1, const float* src2, int count )
{
	ippsAddProduct_32fc( (const Ipp32fc*)src1, (const Ipp32fc*)src2, (Ipp32fc*)srcdest, count );
}

#ifndef FM_NO_AUTOINIT

// Auto-Initialisierer
struct FastMathAutoInitializer
{
	inline FastMathAutoInitializer( ) { fm_init( ); };
};

FastMathAutoInitializer g_oAutoInitializer;

#endif // FM_NO_AUTOINIT

#undef FM_XSIZE
