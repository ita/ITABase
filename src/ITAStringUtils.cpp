#include <ITAConstants.h>
#include <ITAException.h>
#include <ITANumericUtils.h>
#include <ITAStringUtils.h>
#include <algorithm>
#include <assert.h>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <iomanip>
#include <sstream>

#ifdef WIN32
#	include <ITAHPT.h>
#endif

// Diese Makro aktiviert die String-Überprüfungen mittels Regex für String->Typ Konvertierungen
#define VERIFY_STRING_CONVERSIONS


// --------------------------------------------------------------------------

std::string nTimes( std::string s, unsigned int n )
{
	std::string result;
	for( unsigned int i = 0; i < n; i++ )
		result += s;
	return result;
}

std::string BoolToString( bool bValue )
{
	std::stringstream ssStream;
	ssStream << bValue;
	return ssStream.str( );
}

std::string IntToString( int iValue )
{
	std::stringstream ssStream;
	ssStream << iValue;
	return ssStream.str( );
}

std::string UIntToString( unsigned int uiValue )
{
	std::stringstream ssStream;
	ssStream << uiValue;
	return ssStream.str( );
}

std::string LongToString( unsigned int lValue )
{
	std::stringstream ssStream;
	ssStream << lValue;
	return ssStream.str( );
}

std::string Int64ToString( int64_t i64Value )
{
	std::stringstream ssStream;
	ssStream << i64Value;
	return ssStream.str( );
}

std::string UInt64ToString( uint64_t ui64Value )
{
	std::stringstream ssStream;
	ssStream << ui64Value;
	return ssStream.str( );
}

std::string FloatToString( float fValue, int iPrecision )
{
	std::stringstream ssStream;
	if( iPrecision != -1 )
	{
		ssStream.setf( std::ios_base::fixed, std::ios_base::floatfield );
		ssStream.precision( iPrecision );
	}
	ssStream << fValue;
	return ssStream.str( );
}

std::string DoubleToString( double dValue, int iPrecision )
{
	std::stringstream ssStream;
	if( iPrecision != -1 )
	{
		ssStream.setf( std::ios_base::fixed, std::ios_base::floatfield );
		ssStream.precision( iPrecision );
	}
	ssStream << dValue;
	return ssStream.str( );
}

std::string BitmaskToString( int iMask, int iNumBits )
{
	std::stringstream ssStream;
	for( int i = iNumBits - 1; i >= 0; i-- )
		ssStream << ( ( iMask & ( 1 << i ) ) ? "1" : "0" );
	return ssStream.str( );
}

std::string DecibelToString( double dDecibelValue, std::string sMinusInfinity, std::string sUnit )
{
	if( dDecibelValue == ITAConstants::MINUS_INFINITY_D )
		return sMinusInfinity + std::string( " " ) + sUnit;
	else
	{
		std::stringstream ssStream;
		ssStream.setf( std::ios_base::fixed, std::ios_base::floatfield );
		ssStream.precision( 1 );
		// F�r Werte gr��er Null auch immer das Vorzeichen ("+") ausgeben
		if( dDecibelValue > 0 )
			ssStream << "+";
		ssStream << dDecibelValue << " " << sUnit.c_str( );
		return ssStream.str( );
	}
}

std::string IntVecToString( std::vector<int> viValues, std::string sSeparator )
{
	std::string s;
	if( !viValues.empty( ) )
	{
		s += IntToString( viValues.front( ) );
		for( unsigned int i = 1; i < viValues.size( ); i++ )
			s += sSeparator + IntToString( viValues[i] );
	}
	return s;
}

std::string UIntVecToString( std::vector<unsigned int> vuiValues, std::string sSeparator )
{
	std::string s;
	if( !vuiValues.empty( ) )
	{
		s += UIntToString( vuiValues.front( ) );
		for( unsigned int i = 1; i < vuiValues.size( ); i++ )
			s += sSeparator + UIntToString( vuiValues[i] );
	}
	return s;
}

std::string FloatVecToString( std::vector<float> vfValues, int iPrecision, std::string sSeparator )
{
	std::string s;
	if( !vfValues.empty( ) )
	{
		s += FloatToString( vfValues.front( ) );
		for( unsigned int i = 1; i < vfValues.size( ); i++ )
			s += sSeparator + FloatToString( vfValues[i], iPrecision );
	}
	return s;
}

std::string DoubleVecToString( std::vector<double> vdValues, int iPrecision, std::string sSeparator )
{
	std::string s;
	if( !vdValues.empty( ) )
	{
		s += DoubleToString( vdValues.front( ) );
		for( unsigned int i = 1; i < vdValues.size( ); i++ )
			s += sSeparator + DoubleToString( vdValues[i], iPrecision );
	}
	return s;
}

std::string StringVecToString( std::vector<std::string> vsValues, std::string sSeparator )
{
	std::string s;
	if( !vsValues.empty( ) )
	{
		s += vsValues.front( );
		for( unsigned int i = 1; i < vsValues.size( ); i++ )
			s += sSeparator + vsValues[i];
	}
	return s;
}

std::string IntArrayToString( const int* piValues, size_t count, std::string sSeparator )
{
	std::string s;
	if( count > 0 )
	{
		s += IntToString( piValues[0] );
		for( size_t i = 1; i < count; i++ )
			s += sSeparator + IntToString( piValues[i] );
	}
	return s;
}

std::string UIntArrayToString( const unsigned int* puiValues, size_t count, std::string sSeparator )
{
	std::string s;
	if( count > 0 )
	{
		s += UIntToString( puiValues[0] );
		for( size_t i = 1; i < count; i++ )
			s += sSeparator + UIntToString( puiValues[i] );
	}
	return s;
}

std::string FloatArrayToString( const float* pfValues, size_t count, int iPrecision, std::string sSeparator )
{
	std::string s;
	if( count > 0 )
	{
		s += FloatToString( pfValues[0] );
		for( size_t i = 1; i < count; i++ )
			s += sSeparator + FloatToString( pfValues[i], iPrecision );
	}
	return s;
}

std::string ComplexFloatArrayToString( const float* pfValues, size_t count, int iPrecision, std::string sSeparator )
{
	std::string s;
	if( count > 0 )
	{
		for( size_t i = 0; i < count; i++ )
		{
			if( i > 0 )
				s += sSeparator;
			s += FloatToString( pfValues[2 * i], iPrecision );
			if( pfValues[2 * i + 1] < 0 )
				s += FloatToString( pfValues[2 * i + 1], iPrecision ) + "i";
			if( pfValues[2 * i + 1] > 0 )
				s += "+" + FloatToString( pfValues[2 * i + 1], iPrecision ) + "i";
		}
	}
	return s;
}

std::string DoubleArrayToString( const double* pdValues, size_t count, int iPrecision, std::string sSeparator )
{
	std::string s;
	if( count > 0 )
	{
		s += DoubleToString( pdValues[0] );
		for( size_t i = 1; i < count; i++ )
			s += sSeparator + DoubleToString( pdValues[i], iPrecision );
	}
	return s;
}

std::string StringArrayToString( const char** ppcValues, size_t count, std::string sSeparator )
{
	std::string s;
	if( count > 0 )
	{
		s += ppcValues[0];
		for( size_t i = 1; i < count; i++ )
			s += sSeparator + ppcValues[i];
	}
	return s;
}

std::string toLowercase( const std::string& s )
{
	std::string result = s;
	std::transform( result.begin( ), result.end( ), result.begin( ), ( int ( * )( int ) )::tolower );
	return result;
}

std::string toUppercase( const std::string& s )
{
	std::string result = s;
	std::transform( result.begin( ), result.end( ), result.begin( ), ( int ( * )( int ) )::toupper );
	return result;
}

std::string stripSpaces( const std::string& s )
{
	// fwe: Bugfix. Crash unten, im Fall von s=""
	if( s.empty( ) )
		return s;

	/* Neuer Code: Erkennt auch Tabulatoren als Leerraum */
	int n = (int)s.length( );
	int a = 0;
	do
	{
		if( ( s[a] != ' ' ) && ( s[a] != '\t' ) )
			break;
	} while( ++a < n );

	int b = n - 1;
	do
	{
		if( ( s[b] != ' ' ) && ( s[b] != '\t' ) )
			break;
	} while( --b > 0 );

	return s.substr( a, b - a + 1 );
}

std::vector<std::string> splitString( const std::string& s, char cSeparator )
{
	std::vector<std::string> v;
	size_t i, j = 0;
	for( i = 0; i < s.size( ); i++ )
	{
		if( s[i] == cSeparator )
		{
			v.push_back( s.substr( j, i - j ) );
			j = i + 1;
		}
	}

	// push remainder
	if( j < s.size( ) )
		v.push_back( s.substr( j, i - j ) );
	else
		v.push_back( "" ); // String ends with seperator, ergo remainder empty

	return v;
}

std::vector<std::string> splitString( const std::string& s, const std::string& sSeparator )
{
	if( sSeparator.size( ) == 1 )
		return splitString( s, char( sSeparator[0] ) );
	else
		ITA_EXCEPT0( NOT_IMPLEMENTED ); // TODO: Implement for strings
}


std::string ratio_to_db_str_internal( double db, std::string sSuffix )
{
	std::string s;
	char buf[128];
	if( db == ITAConstants::MINUS_INFINITY_D )
		s = "-inf";
	else
	{
		// Auf eine Nachkommastelle runden, Aufrunden bei 0.05 -> 0.1
		db = round( db * 10.0 ) * 0.1;
		if( db == 0 )
			s = "0";
		else
		{
			if( db < 0 )
				sprintf( buf, "%0.1f", db );
			else
				sprintf( buf, "+%0.1f", db );

			s = buf;
		}
	}

	return s + std::string( " dB" ) + sSuffix;
}

std::string ratio_to_db10_str( double r, std::string sSuffix )
{
	return ratio_to_db_str_internal( ratio_to_db10( r ), sSuffix );
}

std::string ratio_to_db20_str( double r, std::string sSuffix )
{
	return ratio_to_db_str_internal( ratio_to_db20( r ), sSuffix );
}

std::string timeToString( const double dSeconds )
{
#ifdef WIN32
	// ITAHPT Implementierung benutzten
	return convertTimeToHumanReadableString( dSeconds );
#else
	// Kein ITAHPT unter Linux, deshalb Code kopiert:
	char buf[255];

	if( dSeconds < 1e-12 )
	{ // Kleiner als 1 fs -> as
		sprintf( buf, "%0.3f as", dSeconds * 1e18 );
		return std::string( buf );
	}

	if( dSeconds < 1e-12 )
	{ // Kleiner als 1 ps -> fs
		sprintf( buf, "%0.3f fs", dSeconds * 1e15 );
		return std::string( buf );
	}

	if( dSeconds < 1e-9 )
	{ // Kleiner als 1 ns -> ps
		sprintf( buf, "%0.3f ps", dSeconds * 1e12 );
		return std::string( buf );
	}

	if( dSeconds < 1e-6 )
	{ // Kleiner als 1 us -> ns
		sprintf( buf, "%0.3f ns", dSeconds * 1e9 );
		return std::string( buf );
	}

	if( dSeconds < 1e-3 )
	{ // Kleiner als 1 ms -> us
		sprintf( buf, "%0.3f us", dSeconds * 1e6 );
		return std::string( buf );
	}

	if( dSeconds < 1e0 )
	{ // Kleiner als 1 s -> ms
		sprintf( buf, "%0.3f ms", dSeconds * 1e3 );
		return std::string( buf );
	}

	if( dSeconds < 60.0f )
	{
		sprintf( buf, "%0.1f s ", dSeconds );
		return std::string( buf );
	}

	/* Da geht noch mehr ...
	if (dSeconds < 60.0f*60.0f) {
	    sprintf(buf, "%i min %0.1f s ", (int) dSeconds, fmodf((float) dSeconds, 60.0f));
	    return std::string(buf);
	}
	*/

	sprintf( buf, "%i min %0.1f s ", (int)dSeconds, fmodf( (float)dSeconds, 60.0f ) );
	return std::string( buf );
#endif // WIN32
}

std::string SubstituteMacro( const std::string& sInput, const std::string& sMacroName, const std::string& sMacroValue )
{
	std::string sOutput( sInput );

	size_t pos = 0;
	if( ( pos = sOutput.find( sMacroName, pos ) ) != std::string::npos )
	{
		// $(sMacroName) -> sMacroValue
		assert( pos >= 2 && pos < sOutput.length( ) );
		sOutput = sInput.substr( 0, pos - 2 ) + sMacroValue + sInput.substr( pos + sMacroName.length( ) + 1, sInput.length( ) - 1 );
	}

	return sOutput;
}
