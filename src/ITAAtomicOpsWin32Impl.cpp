#include <ITAAtomicOps.h>

#ifndef _WIN32_WINNT // @todo: remove
#	define _WIN32_WINNT 0x0501
#endif
#include <windows.h>

typedef union
{
	LONG longValue;
	float floatValue;
} ConvLongFloat;

int atomic_read_int( volatile const int* src )
{
	/*
	 *  Trick: Realisierung mittels CAS!
	 *         Falls der Wert schon 0, dann wird er gegen 0 ausgetauscht...
	 *         Also keine Änderung!
	 *
	 *  Info:  Auf manchen Architekturen ist InterlockedAdd(src, 0) schneller.
	 *         Leider steht InterlockedAdd nur auf Itanium-Arch. zur Verfügung.
	 */
	return InterlockedCompareExchange( (volatile LONG*)src, 0, 0 );
}

long atomic_read_long( volatile const long* src )
{
	/*
	 *  Trick: Realisierung mittels CAS!
	 *         Falls der Wert schon 0, dann wird er gegen 0 ausgetauscht...
	 *         Also keine Änderung!
	 *
	 *  Info:  Auf manchen Architekturen ist InterlockedAdd(src, 0) schneller.
	 *         Leider steht InterlockedAdd nur auf Itanium-Arch. zur Verfügung.
	 */
	return InterlockedCompareExchange( (volatile LONG*)src, 0, 0 );
}

float atomic_read_float( volatile const float* src )
{
	/*
	 *  Trick: Realisierung mittels CAS!
	 *         Falls der Wert schon 0, dann wird er gegen 0 ausgetauscht...
	 *         Also keine Änderung!
	 *
	 *  Info:  Auf manchen Architekturen ist InterlockedAdd(src, 0) schneller.
	 *         Leider steht InterlockedAdd nur auf Itanium-Arch. zur Verfügung.
	 */

	ConvLongFloat c;
	c.longValue = InterlockedCompareExchange( (volatile LONG*)src, 0, 0 );
	return c.floatValue;
}

// inline void* atomic_read_ptr(volatile const void** src) {
void* atomic_read_ptr( volatile const void** src )
{
	/*
	 *  Trick: Realisierung mittels CAS!
	 *         Falls der Wert schon 0, dann wird er gegen 0 ausgetauscht...
	 *         Also keine Änderung!
	 *
	 *  Info:  Auf manchen Architekturen ist InterlockedAdd(src, 0) schneller.
	 *         Leider steht InterlockedAdd nur auf Itanium-Arch. zur Verfügung.
	 */
	return InterlockedCompareExchangePointer( (PVOID volatile*)src, 0, 0 );
}


void atomic_write_int( volatile int* dest, int value )
{
	InterlockedExchange( (volatile LONG*)dest, value );
}

void atomic_write_long( volatile long* dest, long value )
{
	InterlockedExchange( dest, value );
}

void atomic_write_float( volatile float* dest, float value )
{
	ConvLongFloat c;
	c.floatValue = value;
	InterlockedExchange( (volatile LONG*)dest, c.longValue );
}

void atomic_write_ptr( volatile void** dest, void* value )
{
	InterlockedExchangePointer( (PVOID volatile*)dest, value );
}

int atomic_inc_int( volatile int* dest )
{
	return (int)InterlockedIncrement( (volatile LONG*)dest );
}

int atomic_dec_int( volatile int* dest )
{
	return (int)InterlockedDecrement( (volatile LONG*)dest );
}


bool atomic_cas_int( volatile int* dest, int expected_value, int new_value )
{
	return InterlockedCompareExchange( (volatile LONG*)dest, new_value, expected_value ) == expected_value;
}

bool atomic_cas_long( volatile long* dest, long expected_value, long new_value )
{
	return InterlockedCompareExchange( dest, new_value, expected_value ) == expected_value;
}

/*
bool atomic_cas_float(volatile float* dest, float expected_value, float new_value) {
    LONG& e = *((LONG*) &expected_value);
    LONG& n = *((LONG*) &new_value);
    return InterlockedCompareExchange((volatile LONG*) dest, n, e) != e;
}
*/

/*
Problem: Tuts nur auf Windows Vista aufw�rts

bool atomic_cas_double(volatile double* dest, double expected_value, double new_value) {
    LONGLONG& e = *((LONGLONG*) &expected_value);
    LONGLONG& n = *((LONGLONG*) &new_value);
    return InterlockedCompareExchange64((volatile LONG*) dest, n, e) != e;
}
*/

bool atomic_cas_ptr( volatile void** dest, void* expected_value, void* new_value )
{
	return ( InterlockedCompareExchangePointer( (volatile PVOID*)dest, new_value, expected_value ) == expected_value );
}


void atomic_read32( volatile void* src, void* dest )
{
	/*
	 *  Trick: Realisierung mittels CAS!
	 *         Falls der Wert schon 0, dann wird er gegen 0 ausgetauscht...
	 *         Also keine Änderung!
	 *
	 *  Info:  Auf manchen Architekturen ist InterlockedAdd(src, 0) schneller.
	 *         Leider steht InterlockedAdd nur auf Itanium-Arch. zur Verfügung.
	 */


	*( (LONG*)dest ) = InterlockedCompareExchange( (volatile LONG*)src, 0, 0 );
}

void atomic_write32( volatile void* dest, void* new_value )
{
	LONG& lNewValue = *( (LONG*)new_value );
	InterlockedExchange( (volatile LONG*)dest, lNewValue );
}

bool atomic_cas32( volatile void* dest, void* expected_value, void* new_value )
{
	LONG& lExpValue = *( (LONG*)expected_value );
	LONG& lNewValue = *( (LONG*)new_value );
	return ( InterlockedCompareExchange( (volatile LONG*)dest, lNewValue, lExpValue ) == lExpValue );
}
