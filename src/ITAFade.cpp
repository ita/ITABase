#include <ITAConstants.h>
#include <ITAException.h>
#include <ITAFade.h>
#include <cmath>

void Fade( float* pfData, int iFadeLength, int iFadingSign, int iFadeFunction )
{
	Fade( pfData, iFadeLength, iFadingSign, iFadeFunction, 0, iFadeLength );
}

void Fade( float* pfData, int iFadeLength, int iFadingSign, int iFadeFunction, int iOffset, int iLength )
{
	if( iFadeLength == 0 )
		return;

	float alpha;

	switch( iFadeFunction )
	{
		case ITABase::FadingFunction::LINEAR:
		{
			alpha = 1 / (float)iFadeLength;
			if( iFadingSign == ITABase::FadingSign::FADE_IN )
			{
				// Einblenden
				for( int i = 0; i < iLength; i++ )
				{
					// Saubere Werte vor bzw. hinter der Blendphase
					if( ( i + iOffset ) < 0 )
						pfData[i] = 0;
					else if( ( i + iOffset ) < iFadeLength )
						pfData[i] *= alpha * (float)( i + iOffset );
				}
			}
			else // fade out
			{
				// Ausblenden
				for( int i = 0; i < iLength; i++ )
				{
					// Saubere Werte vor bzw. hinter der Blendphase
					if( ( i + iOffset ) >= iFadeLength )
						pfData[i] = 0;
					else if( ( i + iOffset ) >= 0 )
						pfData[i] *= 1 - ( alpha * (float)( i + iOffset ) );
				}
			}

			break;
		}
		case ITABase::FadingFunction::COSINE_SQUARE:
		{
			alpha = ITAConstants::HALF_PI_F / (float)iFadeLength;

			if( iFadingSign == ITABase::FadingSign::FADE_IN )
				for( int i = 0; i < iLength; i++ )
				{
					// Saubere Werte vor bzw. hinter der Blendphase
					if( ( i + iOffset ) < 0 )
						pfData[i] = 0;
					else if( ( i + iOffset ) < iFadeLength )
					{
						float c   = cos( alpha * (float)( i + iOffset ) );
						float csq = (float)( c * c );
						pfData[i] *= ( 1 - csq );
					}
				}
			else // fade out
				for( int i = 0; i < iLength; i++ )
				{
					// Saubere Werte vor bzw. hinter der Blendphase
					if( ( i + iOffset ) >= iFadeLength )
						pfData[i] = 0;
					else if( ( i + iOffset ) >= 0 )
					{
						float c   = cos( alpha * (float)( i + iOffset ) );
						float csq = (float)( c * c );
						pfData[i] *= csq;
					}
				}

			break;
		}
		default:
		{
			ITA_EXCEPT1( INVALID_PARAMETER, "Invalid flags" );
		}
	}
}

void Crossfade( const float* pfSrc1, const float* pfSrc2, float* pfDest, int iFadeLength, int iFadeFunction )
{
	if( iFadeLength == 0 )
		return;

	float alpha;

	switch( iFadeFunction )
	{
		case ITABase::FadingFunction::LINEAR:
		{
			alpha = 1 / (float)iFadeLength;

			for( int i = 0; i < iFadeLength; i++ )
			{
				float c   = alpha * i;
				pfDest[i] = ( 1 - c ) * pfSrc1[i] + c * pfSrc2[i];
			}

			break;
		}
		case ITABase::FadingFunction::COSINE_SQUARE:
		{
			alpha = ITAConstants::HALF_PI_F / (float)iFadeLength;

			for( int i = 0; i < iFadeLength; i++ )
			{
				float c   = cos( alpha * (float)i );
				float csq = (float)( c * c );
				pfDest[i] = csq * pfSrc1[i] + ( 1 - csq ) * pfSrc2[i];
			}

			break;
		}
		default:
		{
			ITA_EXCEPT1( INVALID_PARAMETER, "Invalid flags" );
		}
	}
}
