#include "ITACriticalSection.h"

#ifdef WIN32

#	include "ITACriticalSectionWin32Impl.h"

ITACriticalSection::ITACriticalSection( )
{
	m_pImpl = new ITACriticalSectionWin32Impl;
}

#else // POSIX

#	include "ITACriticalSectionPosixImpl.h"

ITACriticalSection::ITACriticalSection( )
{
	m_pImpl = new ITACriticalSectionPosixImpl;
}

#endif // WIN32 / POSIX


ITACriticalSection::ITACriticalSection( const ITACriticalSection& )
{
	/*
	 *  [fwe 2009-12-15] Bugfix
	 *
	 *  Dieser Copyconstructor wird u.A. von den STL-Containern benutzt,
	 *  wenn diese Objekte direkt referenzieren (bsp: std::vector<Obj> anstelle std::vector<obj*>)
	 *
	 *  Mann kann Critical sections nicht zuweisen, denn sie haben keinen wert.
	 *  Wohl aber eine definierte Identität der erhalten bleiben muss.
	 *  Zuweisungen der Form ITACriticalSection = ITACriticalSection sind also zwecklos.
	 *
	 *  Um die Identität zur Erhalten muss jede Instanz ihr eigenes Win32-Objekt verwalten.
	 */
}

ITACriticalSection::~ITACriticalSection( )
{
	delete m_pImpl;
}

bool ITACriticalSection::tryenter( ) const
{
	// Delegieren
	return m_pImpl->tryenter( );
}

void ITACriticalSection::enter( ) const
{
	m_pImpl->enter( );
}

void ITACriticalSection::leave( ) const
{
	m_pImpl->leave( );
}

ITACriticalSection& ITACriticalSection::operator=( const ITACriticalSection& )
{
	/*
	 *  Mann kann Critical sections nicht zuweisen, denn sie haben keinen wert.
	 *  Wohl aber eine definierte Identität der erhalten bleiben muss.
	 *  Zuweisungen der Form ITACriticalSection = ITACriticalSection sind also zwecklos.
	 *
	 *  Deshalb einfach ignorieren...
	 */

	return *this;
}
