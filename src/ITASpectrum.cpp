﻿#include <ITASpectrum.h>
#include <ITAException.h>
#include <cmath>

CITASpectrum::CITASpectrum( const int nNumBands )
{
	if( nNumBands <= 0 )
		ITA_EXCEPT1( INVALID_PARAMETER, "Spectrum has to have at least one band" );

	m_vfCenterFrequencies.resize( nNumBands );
	m_vfValues.resize( nNumBands );
}

void CITASpectrum::SetName( const std::string& sVolatileName )
{
	m_sName = sVolatileName;
}

std::string CITASpectrum::GetName( ) const
{
	return m_sName;
}

void CITASpectrum::SetValueUnit( const std::string& sUnit )
{
	m_sValueUnit = sUnit;
}

std::string CITASpectrum::GetValueUnit( ) const
{
	return m_sValueUnit;
}

int CITASpectrum::GetNumBands( ) const
{
	return int( m_vfCenterFrequencies.size( ) );
}

const std::vector<float>& CITASpectrum::GetCenterFrequencies( ) const
{
	return m_vfCenterFrequencies;
}

void CITASpectrum::SetCenterFrequencies( const std::vector<float>& vfCenterFrequencies )
{
	if( vfCenterFrequencies.size( ) != m_vfCenterFrequencies.size( ) )
		ITA_EXCEPT1( INVALID_PARAMETER, "Number of bands mismatching, can not set center frequencies" );

	std::copy( vfCenterFrequencies.begin( ), vfCenterFrequencies.end( ), m_vfCenterFrequencies.begin( ) );
}

void CITASpectrum::SetValues( const std::vector<float>& vfValues )
{
	if( vfValues.size( ) != m_vfValues.size( ) )
		ITA_EXCEPT1( INVALID_PARAMETER, "Number of values mismatching, can not set center frequencies" );

	std::copy( vfValues.begin( ), vfValues.end( ), m_vfValues.begin( ) );
}

void CITASpectrum::SetValue( const int iFrequencyBandIndex, const float fValue )
{
	if( iFrequencyBandIndex >= GetNumBands( ) || iFrequencyBandIndex < 0 )
		ITA_EXCEPT1( INVALID_PARAMETER, "Frequency band index out of range" );

	m_vfValues[iFrequencyBandIndex] = fValue;
}

std::vector<float> CITASpectrum::GetValuesCopy( ) const
{
	return m_vfValues;
}

const std::vector<float>& CITASpectrum::GetValues( ) const
{
	return m_vfValues;
}

void CITASpectrum::Multiply( const float fFactor )
{
	for( size_t n = 0; n < m_vfValues.size( ); n++ )
		m_vfValues[n] *= fFactor;
}

void CITASpectrum::Multiply( const CITASpectrum& oOtherSpetrum )
{
	if( GetNumBands( ) != oOtherSpetrum.GetNumBands( ) )
		ITA_EXCEPT1( INVALID_PARAMETER, "Number of bands mismatching, can not multiply spectra" );

	for( size_t n = 0; n < m_vfValues.size( ); n++ )
		m_vfValues[n] *= oOtherSpetrum[int( n )];
}

void CITASpectrum::Add( const float fSummand )
{
	for( size_t n = 0; n < m_vfValues.size( ); n++ )
		m_vfValues[n] += fSummand;
}

void CITASpectrum::Add( const CITASpectrum& oOtherSpectrum )
{
	if( GetNumBands( ) != oOtherSpectrum.GetNumBands( ) )
		ITA_EXCEPT1( INVALID_PARAMETER, "Number of bands mismatching, can not add spectra" );

	for( size_t n = 0; n < m_vfValues.size( ); n++ )
		m_vfValues[n] += oOtherSpectrum[int( n )];
}

void CITASpectrum::Sub( const CITASpectrum& oOtherSpectrum )
{
	if( GetNumBands( ) != oOtherSpectrum.GetNumBands( ) )
		ITA_EXCEPT1( INVALID_PARAMETER, "Number of bands mismatching, can not sub spectra" );

	for( size_t n = 0; n < m_vfValues.size( ); n++ )
		m_vfValues[n] -= oOtherSpectrum[int( n )];
}

void CITASpectrum::Invert( )
{
	for( size_t n = 0; n < m_vfValues.size( ); n++ )
	{
		if( m_vfValues[n] == 0.0f )
			ITA_EXCEPT_INVALID_PARAMETER( "Could not invert spectrum, encountered values of zero" );

		m_vfValues[n] = 1.0f / m_vfValues[n];
	}
}

bool CITASpectrum::CompareEqualValues( const CITASpectrum& oOtherSpectrum, const float fThreshold /*= 10.0e-10 */ ) const
{
	if( GetNumBands( ) != oOtherSpectrum.GetNumBands( ) )
		ITA_EXCEPT1( INVALID_PARAMETER, "Number of bands mismatching, can not compare" );

	for( size_t n = 0; n < m_vfValues.size( ); n++ )
	{
		if( std::fabs( oOtherSpectrum[int( n )] - m_vfValues[n] ) > fThreshold )
			return false;
	}

	return true;
}

const float& CITASpectrum::operator[]( int iFrequencyBandIndex ) const
{
	if( iFrequencyBandIndex >= GetNumBands( ) || iFrequencyBandIndex < 0 )
		ITA_EXCEPT1( INVALID_PARAMETER, "Frequency band index out of range" );

	const float& rV( m_vfValues[iFrequencyBandIndex] );
	return rV;
}

float& CITASpectrum::operator[]( int iFrequencyBandIndex )
{
	if( iFrequencyBandIndex >= GetNumBands( ) || iFrequencyBandIndex < 0 )
		ITA_EXCEPT1( INVALID_PARAMETER, "Frequency band index out of range" );

	float& rV( m_vfValues[iFrequencyBandIndex] );
	return rV;
}

void CITASpectrum::SetValuesExtrapolated( const std::vector<float>& vfProvidedFrequencies, const std::vector<float>& vfProvidedValues, const int iStrategy /* =NONE */ )
{
	if( vfProvidedFrequencies.size( ) != vfProvidedValues.size( ) )
		ITA_EXCEPT1( INVALID_PARAMETER, "Frequency-value count mismatch in interpolation routine" );

	if( vfProvidedFrequencies.size( ) < 1 )
		ITA_EXCEPT1( INVALID_PARAMETER, "At least one frequency-value sample must be provided" );

	if( iStrategy != ITABase::InterpolationFunctions::NONE )
		ITA_EXCEPT1( INVALID_PARAMETER, "Only value repetition at boundaries is supported, yet" );

	if( vfProvidedFrequencies.size( ) > GetNumBands( ) )
		ITA_EXCEPT1( INVALID_PARAMETER, "Provided values must be less than available frequencies to extrapolate range" );

	size_t iExtrapolatedRangeLower = 0;
	size_t iExtrapolatedRangeUpper = m_vfCenterFrequencies.size( );
	for( size_t i = 0; i < m_vfCenterFrequencies.size( ); i++ )
	{
		if( m_vfCenterFrequencies[i] < vfProvidedFrequencies[0] )
		{
			m_vfValues[i]           = vfProvidedValues[0];
			iExtrapolatedRangeLower = i;
		}
		else if( m_vfCenterFrequencies[i] > vfProvidedFrequencies[vfProvidedFrequencies.size( ) - 1] )
		{
			m_vfValues[i] = vfProvidedValues[vfProvidedFrequencies.size( ) - 1];
			if( i < iExtrapolatedRangeUpper )
				iExtrapolatedRangeUpper = i;
		}
		else
		{
			// Assign
			m_vfValues[i] = vfProvidedValues[i - iExtrapolatedRangeLower - 1];
		}
	}
}


CITASpectrum& CITASpectrum::operator*=( const float rhs )
{
	this->Multiply( rhs );
	return *this;
}
CITASpectrum& CITASpectrum::operator*=( const CITASpectrum& rhs )
{
	this->Multiply( rhs );
	return *this;
}
CITASpectrum& CITASpectrum::operator+=( const float rhs )
{
	this->Add( rhs );
	return *this;
}
CITASpectrum& CITASpectrum::operator+=( const CITASpectrum& rhs )
{
	this->Add( rhs );
	return *this;
}
CITASpectrum& CITASpectrum::operator-=( const float rhs )
{
	( *this ) += ( -rhs );
	return *this;
}
CITASpectrum& CITASpectrum::operator-=( const CITASpectrum& rhs )
{
	this->Sub( rhs );
	return *this;
}


std::ostream& operator<<( std::ostream& os, const CITASpectrum& oSpectrum )
{
	std::string sName = "Unnamed spectrum";
	if( !oSpectrum.GetName( ).empty( ) )
		sName = oSpectrum.GetName( );
	os << "[ ITASpectrum ] '" << sName << "' spectrum with " << oSpectrum.GetNumBands( ) << " bands." << std::endl;

	for( int n = 0; n < oSpectrum.GetNumBands( ); n++ )
	{
		if( n > 0 )
			os << ", ";
		os << oSpectrum.GetCenterFrequencies( )[n] << " Hz = " << oSpectrum[n] << " " << oSpectrum.GetValueUnit( );
	}

	return os;
}
