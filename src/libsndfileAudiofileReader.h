// $Id: libsndfileAudiofileReader.h,v 1.3 2006-05-21 12:29:29 wefers Exp $

#ifndef INCLUDE_WATHER_LIBSNDFILE_AUDIOFILEREADER
#define INCLUDE_WATHER_LIBSNDFILE_AUDIOFILEREADER

#include <ITAAudiofileReader.h>
#include <sndfile.h>

class libsndfileAudiofileReader : public ITAAudiofileReader
{
public:
	libsndfileAudiofileReader( const std::string& sFilePath );
	~libsndfileAudiofileReader( );

	void seek( int iOffset );
	void read( int iLength, std::vector<float*> vpfDest );

private:
	SNDFILE* _sf;
	int m_iReadBufferSize;
	float* m_pfReadBuffer;
};

#endif // INCLUDE_WATHER_LIBSNDFILE_AUDIOFILEREADER
