#include <ITAConstants.h>
#include <ITAException.h>
#include <ITAISO9613.h>
#include <ITAThirdOctaveMagnitudeSpectrum.h>
#include <cassert>

using namespace ITABase;


double ISO9613::AtmosphericAbsorptionLevel( double dFrequency, double dDistance, double dTemperature, double dHumidity, double dStaticPressure /*= 101.325*/ )
{
	// ISO 9613-1 Acoustics - Attenuation of sound during propagation outdoors

	// Reference ambient atmospheric pressure (Standard Ambient Atmosphere) [kPa]
	// (Referenzatmosphärendruck Umgebung nach ISO Standard)
	const double p_r = 101.325f;

	// Ambient atmospheric pressure [kPa]
	const double p_a = dStaticPressure;

	// Reference air temperature [K]
	const double T_0 = 273.15 + 20.0;

	// Temperature [K] used for equation (B.3)
	const double T_01 = 273.15 + 0.01;

	// Ambient atmospheric temperature [K]
	const double T = 273.15 + dTemperature;

	// Equations (B.3) and (B.2) of Annex B used for calculation of h in (B.1)
	const double C         = -6.8346 * pow( T_01 / T, 1.261 ) + 4.6151;
	const double p_sat_p_r = pow( (double)10.0f, C );

	// Molar concentration of water vapour [%] (Moleküldichte Wasserdampf)
	// Equation (B.2)
	assert( 0.0f <= dHumidity && dHumidity <= 100.0f );
	const double h = dHumidity * p_sat_p_r / ( p_a / p_r );

	// Oxygen relaxation frequency [Hz]
	// Equation (3)
	const double f_r_o = ( p_a / p_r ) * ( 24.0 + 4.04e4 * h * ( 0.02 + h ) / ( 0.391 + h ) );

	// Nitrogen relaxation frequency [Hz]
	// Equation (4)
	const double f_r_n = ( p_a / p_r ) * pow( T / T_0, -1.0 / 2.0 ) * ( 9.0f + 280.0 * h * exp( -4.710 * ( pow( T / T_0, -( 1 / 3.0 ) ) - 1.0 ) ) );

	// Parts of Equation (5) for the calculation of the attenuation coefficient [dB/m]
	const double dAlpha1 = 8.686 * pow( dFrequency, 2.0 );
	const double dAlpha2 = 1.84e-11 * pow( p_a / p_r, -1.0 ) * pow( T / T_0, 1.0 / 2.0 );
	const double dAlpha3 = pow( T / T_0, -5.0 / 2.0 );
	const double dAlpha4 = 0.01275f * exp( -2239.1 / T ) * pow( f_r_o + pow( dFrequency, 2.0 ) / f_r_o, -1.0 );
	const double dAlpha5 = 0.10680f * exp( -3352.0 / T ) * pow( f_r_n + pow( dFrequency, 2.0 ) / f_r_n, -1.0 );

	// Attenuation coefficient [dB/m], ~f, as assembly of Equation (5) parts
	const double dAlpha = dAlpha1 * ( dAlpha2 + dAlpha3 * ( dAlpha4 + dAlpha5 ) );

	// Resulting atmospheric absorption [dB], ~alpha (~f)
	// Equation (2)
	const double dAirAbsorptionDecibel = dAlpha * dDistance;

	// Attenuation factor in decibel
	return dAirAbsorptionDecibel;
}

double ISO9613::AtmosphericAbsorptionFactor( double dFrequency, double dDistance, double dTemperature, double dHumidity, double dStaticPressure /*= 101.325*/ )
{
	const double dAirAbsorptionDecibel = AtmosphericAbsorptionLevel( dFrequency, dDistance, dTemperature, dHumidity, dStaticPressure );
	const double dAirAbsorptionFactor = pow( 10, -dAirAbsorptionDecibel / 20.0 ); //Factor to be applied to sound pressure
	return dAirAbsorptionFactor;
}

void ISO9613::AtmosphericAbsorption( ITABase::CThirdOctaveDecibelMagnitudeSpectrum& oAttenuationDB, double dDistance, double dTemperature, double dHumidity,
                                     double dStaticPressure /*= 101.325*/ )
{
	for( int i = 0; i < oAttenuationDB.GetNumBands( ); i++ )
	{
		const double dFrequency            = (double)oAttenuationDB.GetCenterFrequencies( )[i];
		const double dAirAbsorptionDecibel = AtmosphericAbsorptionLevel( dFrequency, dDistance, dTemperature, dHumidity, dStaticPressure );
		oAttenuationDB.SetMagnitude( i, (float)dAirAbsorptionDecibel );
	}
}

void ISO9613::AtmosphericAbsorption( ITABase::CThirdOctaveFactorMagnitudeSpectrum& oAttenuationMagnitudes, double dDistance, double dTemperature, double dHumidity,
                                     double dStaticPressure /*= 101.325*/ )
{
	// loop over all frequencies and converting from decibel to a factor between 0 and 1
	for( int i = 0; i < oAttenuationMagnitudes.GetNumBands( ); i++ )
	{
		const double dFrequency           = (double)oAttenuationMagnitudes.GetCenterFrequencies( )[i];
		const double dAirAbsorptionFactor = AtmosphericAbsorptionFactor( dFrequency, dDistance, dTemperature, dHumidity, dStaticPressure );
		oAttenuationMagnitudes.SetMagnitude( i, (float)dAirAbsorptionFactor );
	}
}
