#include <ITAAudioSample.h>
#include <ITAException.h>
#include <cassert>
#include <math.h>
#include <samplerate.h>

CITAAudioSample::CITAAudioSample( const float fSampleRate ) : m_fSampleRate( fSampleRate ) {}

CITAAudioSample::CITAAudioSample( const int iChannels, const int iLength, const float fSampleRate, const bool bZeroInit /*= true */ )
    : ITASampleFrame( iChannels, iLength, bZeroInit )
    , m_fSampleRate( fSampleRate )
{
}

CITAAudioSample::CITAAudioSample( const CITAAudioSample* pSource )
{
	Init( pSource->GetNumChannels( ), pSource->GetLength( ), pSource->GetSampleRate( ) );
	ITASampleFrame::write( pSource, pSource->GetLength( ) );
}

CITAAudioSample::CITAAudioSample( const CITAAudioSample& sfSource )
{
	Init( sfSource.GetNumChannels( ), sfSource.GetLength( ), sfSource.GetSampleRate( ) );
	ITASampleFrame::write( sfSource, sfSource.GetLength( ) );
}

CITAAudioSample::CITAAudioSample( const std::string& sFilePath ) : CITAAudioSample( )
{
	Load( sFilePath );
}

void CITAAudioSample::Init( const int iNumChannels, const int iLength, const float fSampleRate, const bool bZeroInit /*= true */ )
{
	m_fSampleRate = fSampleRate;
	ITASampleFrame::Init( iNumChannels, iLength, bZeroInit );
}

void CITAAudioSample::LoadWithSampleRateConversion( const std::string& sFilePath )
{
	if( m_fSampleRate <= 0.0f )
		ITA_EXCEPT_INVALID_PARAMETER( "Invalid internal audio sample sampling rate" );

	double dSampleRate;
	ITASampleFrame sfRaw;
	sfRaw.Load( sFilePath, dSampleRate );

	Load( sfRaw, float( dSampleRate ) );
}

void CITAAudioSample::Load( const CITAAudioSample& oSource )
{
	Load( oSource, oSource.GetSampleRate( ) );
}

void CITAAudioSample::Load( const std::string& sFilePath )
{
	double dSampleRate;
	ITASampleFrame::Load( sFilePath, dSampleRate );
	m_fSampleRate = float( dSampleRate );
}

float CITAAudioSample::GetSampleRate( ) const
{
	return m_fSampleRate;
}

void CITAAudioSample::Load( const ITASampleFrame& sfSource, const float fSourceSampleRate )
{
	if( fSourceSampleRate <= 0.0f )
		ITA_EXCEPT_INVALID_PARAMETER( "Source sample rate must be greater zero." );

	// Same sample rate
	if( fabs( fSourceSampleRate - m_fSampleRate ) < 1e-20 )
	{
		Init( sfSource.GetNumChannels( ), sfSource.GetLength( ), m_fSampleRate );
		ITASampleFrame::write( sfSource, sfSource.GetLength( ) );
		return;
	}

	const float fSRCRation                     = GetSampleRate( ) / fSourceSampleRate;
	const float fTargetLengthSamples           = sfSource.GetLength( ) * fSRCRation;
	int iTargetLengthSamples                   = (int)floor( fTargetLengthSamples );
	const float fTargetLengthSubsampleFraction = fTargetLengthSamples - float( iTargetLengthSamples );
	// Account for exact match, then the "last" sample is not resampled anymore and can be skipped
	if( fTargetLengthSubsampleFraction == 0.0f )
		iTargetLengthSamples--;
	Init( sfSource.GetNumChannels( ), iTargetLengthSamples, m_fSampleRate );

	const int iConverter = SRC_SINC_MEDIUM_QUALITY;
	for( int i = 0; i < sfSource.GetNumChannels( ); i++ )
	{
		int iSRCError;
		SRC_STATE* pSRCStace =
		    src_new( iConverter, 1, &iSRCError ); // single channel conversion, samplerate expets interleaved. this is not compatible with ITASampleBuffer/Frame
		if( pSRCStace == nullptr )
			ITA_EXCEPT_INVALID_PARAMETER( "Could not create sample rate converter, samplerate error was: " + std::string( src_strerror( iSRCError ) ) );

		SRC_DATA oSRCData;
		oSRCData.data_in       = sfSource[i].GetData( );
		oSRCData.data_out      = ( *this )[i].GetData( );
		oSRCData.input_frames  = sfSource.GetLength( ); // In samplerate, one frame is "one sample" of multi channel audio
		oSRCData.output_frames = iTargetLengthSamples;
		oSRCData.src_ratio     = fSRCRation;
		if( ( iSRCError = src_process( pSRCStace, &oSRCData ) ) != 0 )
			ITA_EXCEPT_INVALID_PARAMETER( "Could not convert sample rate: " + std::string( src_strerror( iSRCError ) ) );

		assert( oSRCData.output_frames_gen == iTargetLengthSamples );

		src_delete( pSRCStace );
	}
}
