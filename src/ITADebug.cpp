#include <ITACriticalSection.h>
#include <ITADebug.h>
#include <stdarg.h>
#include <stdio.h>

#ifdef WIN32
#	include <windows.h>

#	define DEBUG_PRINTF_BUFSIZE 16384

// Lock f�r den Puffer in den Ausgabe-Routinen
static ITACriticalSection g_csDebugPrintf;
static char g_pszDebugPrintfBuf[16384];

#	ifdef _DEBUG

void DEBUG_PRINTF( const char* format, ... )
{
	g_csDebugPrintf.enter( );
	va_list args;
	va_start( args, format );
	vsprintf_s( g_pszDebugPrintfBuf, DEBUG_PRINTF_BUFSIZE, format, args );
	va_end( args );

	OutputDebugStringA( g_pszDebugPrintfBuf );
	g_csDebugPrintf.leave( );
}

#	else

void DEBUG_PRINTF( const char* format, ... )
{
	/* NUR BENUTZEN WENN DEBUG_PRINTF IM RELEASE MODE BEN�TIGT WIRD!!!
	g_csDebugPrintf.enter();
	va_list args;
	va_start(args, format);
	vsprintf_s(g_pszDebugPrintfBuf, DEBUG_PRINTF_BUFSIZE, format, args);
	va_end(args);

	OutputDebugStringA(g_pszDebugPrintfBuf);
	g_csDebugPrintf.leave();
	*/
}

#	endif // _DEBUG

/*
 *  Kommt von hier: http://www.codeproject.com/KB/threads/Name_threads_in_debugger.aspx
 */

typedef struct tagTHREADNAME_INFO
{
	DWORD dwType;     // must be 0x1000
	LPCSTR szName;    // pointer to name (in user addr space)
	DWORD dwThreadID; // thread ID (-1=caller thread)
	DWORD dwFlags;    // reserved for future use, must be zero
} THREADNAME_INFO;

void SetThreadName( long lThreadID, const char* szThreadName )
{
	THREADNAME_INFO info;
	{
		info.dwType     = 0x1000;
		info.szName     = szThreadName;
		info.dwThreadID = (DWORD)lThreadID;
		info.dwFlags    = 0;
	}
	__try
	{
		RaiseException( 0x406D1388, 0, sizeof( info ) / sizeof( DWORD ), (ULONG_PTR*)&info );
	}
	__except( EXCEPTION_CONTINUE_EXECUTION )
	{
	}
}

#endif // Win32

#ifndef WIN32 // Linux oder was auch immer
void DEBUG_PRINTF( const char* format, ... )
{
	va_list args;
	va_start( args, format );
	vprintf( format, args );
	va_end( args );
}
#endif // Linux