#include <ITAEndianness.h>
#include <ITASampleTypeConversion.h>
#include <stddef.h>

void stc_sint16_to_float( float* dest, const short* src, size_t count, int input_stride, int output_stride )
{
	for( size_t i = 0; i < count; i++ )
		dest[i * output_stride] = (float)src[i * input_stride] / 32767.0F;
}

void stc_sint24_to_float( float* dest, const void* src, size_t count, int input_stride, int output_stride )
{
	const unsigned char* p = (const unsigned char*)src;

	union
	{
		int ivalue;
		unsigned char byte[4];
	};

	// TODO: Hier muss noch die Endianness gepr�ft werden.

	if( GetEndianness( ) == ITA_BIG_ENDIAN )
	{
		for( size_t i = 0; i < count; i++ )
		{
			// Big endian
			// TODO: Testen!
			byte[1] = p[0];
			byte[2] = p[1];
			byte[3] = p[2];

			// Eingangswert negativ (h�chstes Bit = 1)?
			if( p[1] & 0x80 )
				byte[0] = 0xFF;
			else
				byte[0] = 0x00;

			dest[i * output_stride] = (float)ivalue / 8388607.0F;

			p = p + input_stride * 3;
		}
	}
	else
	{
		for( size_t i = 0; i < count; i++ )
		{
			// Little endian
			byte[0] = p[0];
			byte[1] = p[1];
			byte[2] = p[2];

			// Input value negative (higher Bit = 1)?
			if( p[2] & 0x80 )
				byte[3] = 0xFF;
			else
				byte[3] = 0x00;

			dest[i * output_stride] = (float)ivalue / 8388607.0F;

			p = p + input_stride * 3;
		}
	}
}

void stc_sint32_to_float( float* dest, const int* src, size_t count, int input_stride, int output_stride )
{
	for( size_t i = 0; i < count; i++ )
		dest[i * output_stride] = (float)src[i * input_stride] / 2147483647.0F;
}

void stc_double_to_float( float* dest, const double* src, size_t count, int input_stride, int output_stride )
{
	for( size_t i = 0; i < count; i++ )
		dest[i * output_stride] = (float)src[i * input_stride];
}

void stc_float_to_sint16( short* dest, const float* src, size_t count, int input_stride, int output_stride )
{
	/* �berlauf-Schutz: �bersteuerte Werte gr��er +/-1.0 werden auf +/-32767 abgebildet.
	                    Dies mag zwar langsam sein, ist aber sicherer! */

	for( size_t i = 0; i < count; i++ )
	{
		if( src[i * input_stride] >= +1.0 )
			dest[i * output_stride] = 32767;
		else if( src[i * input_stride] <= -1.0 )
			dest[i * output_stride] = -32767;
		else
			dest[i * output_stride] = (short)( src[i * input_stride] * 32767.0F );
	}
}

void stc_float_to_sint32( int* dest, const float* src, size_t count, int input_stride, int output_stride )
{
	/* �berlauf-Schutz: �bersteuerte Werte gr��er +/-1.0 werden auf +/-2147483647 abgebildet.
	                    Dies mag zwar langsam sein, ist aber sicherer! */

	for( size_t i = 0; i < count; i++ )
	{
		if( src[i * input_stride] >= +1.0 )
			dest[i * output_stride] = 2147483647;
		else if( src[i * input_stride] <= -1.0 )
			dest[i * output_stride] = -2147483647;
		else
			dest[i * output_stride] = (int)( src[i * input_stride] * 2147483647.0F );
	}
}

void stc_float_to_double( double* dest, const float* src, size_t count, int input_stride, int output_stride )
{
	for( size_t i = 0; i < count; i++ )
		dest[i * output_stride] = (double)src[i * input_stride];
}
