#include "ITAMutex.h"

#include "ITAException.h"

ITAMutex::ITAMutex( )
{
	if( FAILED( hMutex = CreateMutex( NULL, FALSE, NULL ) ) )
		ITA_EXCEPT1( UNKNOWN, "Mutex konnte nicht erzeugt werden" );
}

ITAMutex::~ITAMutex( )
{
	WaitForSingleObject( hMutex, INFINITE );
	CloseHandle( hMutex );
}
