#include "libsndfileAudiofileWriter.h"

#include <ITAAudiofileWriter.h>
#include <ITAException.h>
#include <ITASampleBuffer.h>
#include <ITASampleFrame.h>

ITAAudiofileWriter* ITAAudiofileWriter::create( const std::string& sFilePath, const ITAAudiofileProperties& props )
{
	return new libsndfileAudiofileWriter( sFilePath, props );
}

ITAAudiofileWriter::~ITAAudiofileWriter( ) { };

void ITAAudiofileWriter::write( int iLength, std::vector<float*> vpfSource )
{
	write( iLength, *reinterpret_cast<std::vector<const float*>*>( &vpfSource ) );
}

void ITAAudiofileWriter::write( const ITASampleFrame* psfSource )
{
	if( !psfSource )
		ITA_EXCEPT1( INVALID_PARAMETER, "Nullpointer argument" );

	write( psfSource, psfSource->length( ), 0 );
}

void ITAAudiofileWriter::write( const ITASampleFrame* psfSource, int iNumSamples, int iOffset )
{
	if( !psfSource )
		ITA_EXCEPT1( INVALID_PARAMETER, "Nullpointer argument" );

	if( ( iOffset + iNumSamples ) > psfSource->length( ) )
		ITA_EXCEPT1( INVALID_PARAMETER, "Number of samples and offset exceed sample frame range" );

	// Datenzeiger aufsetzen
	std::vector<const float*> vpfData( psfSource->channels( ) );
	for( int i = 0; i < psfSource->channels( ); i++ )
		vpfData[i] = ( *psfSource )[i].GetData( ) + iOffset;

	// Schreiben mit der Variante std::vector<const float*>
	write( iNumSamples, vpfData );
}

void writeAudiofile( const std::string& sFilePath, const ITAAudiofileProperties& props, std::vector<const float*>& vpfSource )
{
	ITAAudiofileWriter* pWriter = ITAAudiofileWriter::create( sFilePath, props );
	try
	{
		pWriter->write( props.iLength, vpfSource );
	}
	catch( ... )
	{
		delete pWriter;
		throw;
	}
	delete pWriter;
}

void writeAudiofile( const std::string& sFilePath, const ITAAudiofileProperties& props, std::vector<float*>& vpfSource )
{
	writeAudiofile( sFilePath, props, reinterpret_cast<std::vector<const float*>&>( vpfSource ) );
}

void writeAudiofile( std::string sFilename, const float* pfData, unsigned int uiLength, double dSampleRate, ITAQuantization eQuantization )
{
	ITAAudiofileProperties props;
	props.iChannels     = 1;
	props.iLength       = uiLength;
	props.dSampleRate   = dSampleRate;
	props.eDomain       = ITADomain::ITA_TIME_DOMAIN;
	props.eQuantization = eQuantization;
	props.sComment      = "";

	std::vector<const float*> data( 1 );
	data[0] = pfData;

	ITAAudiofileWriter* writer = ITAAudiofileWriter::create( sFilename, props );
	try
	{
		writer->write( props.iLength, data );
	}
	catch( ... )
	{
		delete writer;
		throw;
	}
	delete writer;
}

void writeAudiofile( const std::string& sFilePath, const float* pfDataCh0, const float* pfDataCh1, int iLength, double dSampleRate, ITAQuantization eQuantization )
{
	ITAAudiofileProperties props;
	props.iChannels     = 2;
	props.iLength       = iLength;
	props.dSampleRate   = dSampleRate;
	props.eDomain       = ITADomain::ITA_TIME_DOMAIN;
	props.eQuantization = eQuantization;
	props.sComment      = "";

	std::vector<const float*> data( 2 );
	data[0] = pfDataCh0;
	data[1] = pfDataCh1;

	ITAAudiofileWriter* writer = ITAAudiofileWriter::create( sFilePath, props );
	try
	{
		writer->write( props.iLength, data );
	}
	catch( ... )
	{
		delete writer;
		throw;
	}
	delete writer;
}

void writeAudiofile( const std::string& sFilePath, const ITASampleBuffer* psbData, double dSampleRate, ITAQuantization eQuantization )
{
	ITAAudiofileProperties props;
	props.iChannels     = 1;
	props.iLength       = (unsigned int)psbData->GetLength( );
	props.dSampleRate   = dSampleRate;
	props.eDomain       = ITADomain::ITA_TIME_DOMAIN;
	props.eQuantization = eQuantization;
	props.sComment      = "";

	std::vector<const float*> data( 1 );
	data[0] = psbData->GetData( );

	ITAAudiofileWriter* writer = ITAAudiofileWriter::create( sFilePath, props );
	try
	{
		writer->write( props.iLength, data );
	}
	catch( ... )
	{
		delete writer;
		throw;
	}
	delete writer;
}

void writeAudiofile( const std::string& sFilePath, const ITASampleFrame* psfData, double dSampleRate, ITAQuantization eQuantization )
{
	ITAAudiofileProperties props;
	props.iChannels     = (unsigned int)psfData->channels( );
	props.iLength       = (unsigned int)psfData->length( );
	props.dSampleRate   = dSampleRate;
	props.eDomain       = ITADomain::ITA_TIME_DOMAIN;
	props.eQuantization = eQuantization;
	props.sComment      = "";

	std::vector<const float*> data( psfData->channels( ) );
	for( int i = 0; i < psfData->channels( ); i++ )
		data[i] = const_cast<float*>( ( *psfData )[i].GetData( ) );

	ITAAudiofileWriter* writer = ITAAudiofileWriter::create( sFilePath, props );

	try
	{
		writer->write( props.iLength, data );
	}
	catch( ... )
	{
		delete writer;
		throw;
	}

	delete writer;
}
