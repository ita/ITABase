#include <ITAConstants.h>
#include <ITAException.h>
#include <ITAInterpolation.h>
#include <ITANumericUtils.h>
#include <ITASampleBuffer.h>
#include <cassert>
#include <vector>

// Calculate cubic spline set (second derivatives, ypp) for equidistant data
void spline_cubic_set_equidistant( const int n, const float* y, std::vector<float>& ypp );

// Evaluate cubic spline interpolation point using only base values (y) and second derivatives (ypp) for equidistant data
float spline_cubic_val_equidistant( const float fX, const float* y, const std::vector<float>& ypp );

bool CITASampleLinearInterpolation::Interpolate( const ITASampleBuffer* pInput, const int iInputLength, const int iInputStartOffset, ITASampleBuffer* pOutput,
                                                 const int iOutputLength, const int iOutputOffset /*=0*/ ) const
{
	assert( pOutput->GetLength( ) >= iOutputLength + iOutputOffset );
	assert( pInput->GetLength( ) >= iInputLength + iInputStartOffset );

	if( iOutputLength < 1 )
		ITA_EXCEPT_INVALID_PARAMETER( "Requested output length for interpolation must be at least 1" );
	if( iInputLength < 1 )
		ITA_EXCEPT_INVALID_PARAMETER( "Provided input length for interpolation must be at least 1" );

	// Interpolation ist nur m�glich, wenn der Eingabe-Offset auf dem Eingabepuffer
	// gr��er oder gleich 1 ist, da sonst keine St�tzwerte vorhanden sind.
	assert( iInputStartOffset > 0 );


	// Resamplingfaktor
	float r = iInputLength / (float)iOutputLength;


	// TODO kl�ren ob diese resampling grenzen �berhaupt gelten, theor. gibt es keine einschr�nkung hier!


	// --= Resampling =--

	if( r < 1 ) // Oversampling
	{
		// �ber Ausgabesamples (Laufvariable i �ber den Ausgangspuffer)
		// laufen und interpolierte Werte eintragen, dabei evtl.
		// Samples des �berlappungspuffers am Anfang einbeziehen
		for( int i = 0; i < iOutputLength; i++ )
		{
			// Calculate the position in the input buffer
			float x_input = ( i + 1 ) * r + iInputStartOffset - 1;
			assert( x_input >= 0.0f );

			// Die Position des letzten Samples muss �bereinstimmen mit dem letzten
			// Eingabesample, dann wurde die Zeit erfolgreich eingeholt bzw.
			// abgebremst.

			// Linkes/rechtes Nachbarsample in der Eingabe
			int a = (int)floor( x_input );
			int b = (int)ceil( x_input );

			// Relative Position zwischen den Nachbarsamples in der Eingabe
			float frac = x_input - (float)a;

			// Die Linke Seite liegt immer innerhalb des Ausgangs
			assert( ( a >= 0 ) && ( b < ( iInputLength + iInputStartOffset ) ) );

			// Sample genau getroffen (z.B. a=b=0)
			if( a == b )
			{
				( *pOutput )[i + iOutputOffset] = ( *pInput )[a];
			}
			else
			{
				// Zwischenwert anhand linkem und rechtem St�tzwert ermitteln
				assert( ( b >= 1 ) && ( b < iInputLength + iInputStartOffset ) );

				// Steigung (leicht lesbare Implementierung, wird vom Compiler wegoptimiert)
				float left_val                  = ( *pInput )[a];
				float right_val                 = ( *pInput )[b];
				float m                         = right_val - left_val;
				float val                       = left_val + m * frac;
				( *pOutput )[i + iOutputOffset] = val;
			}
		}
	}
	else if( r > 1 ) // Undersampling
	{
		// �ber Ausgabesamples (Laufvariable i �ber den Ausgangspuffer)
		// laufen und interpolierte Werte eintragen, dabei evtl.
		// Samples des �berlappungspuffers am Anfang einbeziehen
		for( int i = 0; i < iOutputLength; i++ )
		{
			// Position in der Eingabe berechnen
			float x_input = ( i + 1 ) * r + iInputStartOffset - 1;

			// Linkes/rechtes Nachbarsample in der Eingabe
			int a = (int)floor( x_input );
			int b = (int)ceil( x_input );

			// Relative Position zwischen den Nachbarsamples in der Eingabe
			float frac = x_input - (float)a;

			// Die Linke Seite liegt immer innerhalb des Ausgangs
			assert( ( a >= iInputStartOffset - 1 ) && ( b < ( iInputLength + iInputStartOffset ) ) );

			// Sample genau getroffen (z.B. a=b=0)
			if( a == b )
			{
				( *pOutput )[i + iOutputOffset] = ( *pInput )[a];
			}
			else
			{
				// Zwischenwert anhand linkem und rechtem St�tzwert ermitteln
				assert( ( b >= 1 ) && ( b < iInputLength ) );

				// Steigung
				float m                         = ( *pInput )[b] - ( *pInput )[a];
				( *pOutput )[i + iOutputOffset] = ( *pInput )[a] + m * frac;
			}
		}
	}
	else // Kein Resampling; TODO evtl immer resamplen, dieser Fall sollte vor Aufruf der Interpolation bereits abgefangen werden
	{
		// Nur Kopieren
		pOutput->write( pInput, iOutputLength, iInputStartOffset );
	}

	return false;
}

bool CITASampleCubicSplineInterpolation::Interpolate( const ITASampleBuffer* pInput, const int iInputLength, const int iInputStartOffset, ITASampleBuffer* pOutput,
                                                      const int iOutputLength, const int iOutputOffset /*=0*/ ) const
{
	// Eingabe validieren
	assert( iInputStartOffset >= 2 );
	assert( iOutputLength > 0 );

	assert( pOutput->length( ) >= iOutputLength + iOutputOffset );

	// Resamplingfaktor
	float r = iInputLength / (float)iOutputLength;


	// --= Resampling =--

	const float* pInputData = pInput->data( );
	float* pOutputData      = pOutput->data( );

	// Return value
	std::vector<float> vfInputSecondDerivatives( iInputLength + iInputStartOffset );
	spline_cubic_set_equidistant( iInputLength + iInputStartOffset, pInputData, vfInputSecondDerivatives ); // Natural spline interpolation

	for( int i = 0; i < iOutputLength; i++ )
	{
		// Position in the input buffer
		float fXInput = ( i + 1 ) * r + iInputStartOffset - 1;
		// First item (index): fXInput = r - 1
		// Last item (index):  fXInput = iInputLength + iInputStartOffset - 1

		// Spline segment evaluation at given position
		float fYOutput = spline_cubic_val_equidistant( fXInput, pInputData, vfInputSecondDerivatives );

		pOutputData[i + iOutputOffset] = fYOutput;
	}

	return true;
}

bool CITASampleWindowedSincInterpolation::Interpolate( const ITASampleBuffer* pInput, const int iInputLength, const int iInputStartOffset, ITASampleBuffer* pOutput,
                                                       const int iOutputLength, const int iOutputOffset /*=0*/ ) const
{
	// Eingabe validieren
	assert( iInputStartOffset >= m_iWindowSize / 2 );
	assert( iOutputLength > 0 );
	assert( pOutput->length( ) >= iOutputLength + iOutputOffset );

	// Resamplingfaktor
	float r = iInputLength / (float)iOutputLength;


	// --= Resampling =--

	const float* pInputData = pInput->data( );
	float* pOutputData      = pOutput->data( );

	for( int i = 0; i < iOutputLength; i++ )
	{
		// Position in der Eingabe berechnen (relativ zum Anfang mit Offset)
		float fXInput = ( i + 1 ) * r + iInputStartOffset - 1;

		// Fenster anwenden & Si-Rekonstruktion
		pOutputData[i + iOutputOffset] = 0.0f;
		for( int j = (int)ceil( fXInput - m_iWindowSize / 2 ); j < (int)floor( fXInput + m_iWindowSize / 2 ); j++ )
		{
			int iCenterIndex = j;
			assert( iCenterIndex >= 0 && iCenterIndex < iInputLength + m_iWindowSize / 2 ); // �berlauf verhindern

			float fAmplitude = pInputData[iCenterIndex];

			// Relativer Abstand zum aktuellen Lesecursor in der Eingabe
			float fSincArg = fXInput - iCenterIndex;
			assert( std::fabs( fSincArg ) <= (float)m_iWindowSize / 2 ); // Fenster�berlauf verhindern

			float fSincValue = ( fSincArg != 0.0f ) ? ( sin( ITAConstants::PI_F * fSincArg ) / ( ITAConstants::PI_F * fSincArg ) ) : 1.0f;

			// Rect
			// float fWindowValue = 1.0f;

			// Hanning
			// float fWindowValue = 0.5f * (1-cos(2*PI_F*fSincArg/(float) (m_iWindowSize/2)));

			// Hamming
			// float fWindowValue = 0.54f  - 0.46f * cos(2*PI_F*fSincArg/(float) (m_iWindowSize/2));

			// Lanczos
			float a            = m_iWindowSize / 2.0f;
			float fWindowValue = ( fSincArg != 0.0f ) ? ( sin( ITAConstants::PI_F * fSincArg / a ) / ( ITAConstants::PI_F * fSincArg / a ) ) : 1.0f;

			float fContribution = fWindowValue * fAmplitude * fSincValue;

			pOutputData[i + iOutputOffset] += fContribution;
		}
	}

	return true;
}

void spline_cubic_set_equidistant( const int n, const float* y, std::vector<float>& ypp )
{
	assert( n > 2 );

	// Tempor�rer Puffer
	float* a = new float[3 * n - 1];

	// Setup equations, save intermediate values to b
	ypp[0] = 0.0f;
	a[1]   = 1.0f;  // 1+0*3
	a[3]   = -1.0f; // 0+1*3
	for( int i = 1; i < n - 1; i++ )
	{
		ypp[i]               = y[i + 1] - 2 * y[i] + y[i - 1];
		a[2 + ( i - 1 ) * 3] = 1.0f / 6.0f;
		a[1 + i * 3]         = 2.0f / 3.0f;
		a[0 + ( i + 1 ) * 3] = 1.0f / 6.0f;
	}
	ypp[n - 1]           = 0.0f;
	a[2 + ( n - 2 ) * 3] = -1.0f;
	a[1 + ( n - 1 ) * 3] = 1.0f;

	// Lineares Gleichungssystem l�sen
	a[1 + 1 * 3] = 5.0f / 6.0f;
	ypp[1]       = ypp[1] - ypp[0] / 6.0f;

	float xmult;
	for( int i = 2; i < n; i++ )
	{
		xmult        = a[2 + ( i - 1 ) * 3] / a[1 + ( i - 1 ) * 3];
		a[1 + i * 3] = a[1 + i * 3] - xmult * a[0 + i * 3];
		ypp[i]       = ypp[i] - xmult * ypp[i - 1];
	}

	// R�ckw�rts einsetzen
	for( int i = n - 2; 0 <= i; i-- )
		ypp[i] = ( ypp[i] - a[0 + ( i + 1 ) * 3] * ypp[i + 1] ) / a[1 + i * 3];

	delete[] a;
}

float spline_cubic_val_equidistant( const float fX, const float* y, const std::vector<float>& ypp )
{
	assert( ypp.size( ) > fX );

	int iXInt   = (int)std::floor( fX );
	float fFrac = fX - iXInt;

	if( fFrac < ITAConstants::EPS_F_L )
		return y[iXInt];

	float y_interp = y[iXInt] + fFrac * ( ( y[iXInt + 1] - y[iXInt] ) - ( ypp[iXInt + 1] / 6.0f + ypp[iXInt] / 3.0f ) +
	                                      fFrac * ( 0.5f * ypp[iXInt] + fFrac * ( ypp[iXInt + 1] - ypp[iXInt] ) / 6.0f ) );

	return y_interp;
}
