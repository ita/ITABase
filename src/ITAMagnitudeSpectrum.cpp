﻿#include <ITAMagnitudeSpectrum.h>
#include <ITAConstants.h>
#include <limits>
#include <numeric>


ITABase::CMagnitudeSpectrum::CMagnitudeSpectrum( const int nNumBands ) : CITASpectrum( nNumBands ) {}

std::vector<float> ITABase::CMagnitudeSpectrum::GetMagnitudesCopy( ) const
{
	return CITASpectrum::GetValuesCopy( );
}

const std::vector<float>& ITABase::CMagnitudeSpectrum::GetMagnitudes( ) const
{
	return CITASpectrum::GetValues( );
}

void ITABase::CMagnitudeSpectrum::SetMagnitudes( const std::vector<float>& vfMagnitudes )
{
	CITASpectrum::SetValues( vfMagnitudes );
}

void ITABase::CMagnitudeSpectrum::SetMagnitude( const int iFrequencyBandIndex, const float fMagnitudeValue )
{
	CITASpectrum::SetValue( iFrequencyBandIndex, fMagnitudeValue );
}

// ----- ----------- -------------------

ITABase::CDecibelMagnitudeSpectrum::CDecibelMagnitudeSpectrum( const int iNumBands ) : CMagnitudeSpectrum( iNumBands )
{
	CITASpectrum::SetValueUnit( "dB" );
}

ITABase::CDecibelMagnitudeSpectrum::~CDecibelMagnitudeSpectrum( ) {}

void ITABase::CDecibelMagnitudeSpectrum::SetIdentity( )
{
	for( size_t n = 0; n < m_vfValues.size( ); n++ )
		m_vfValues[n] = 0;
}

void ITABase::CDecibelMagnitudeSpectrum::SetZero( )
{
	for( size_t n = 0; n < m_vfValues.size( ); n++ )
		m_vfValues[n] = -std::numeric_limits<float>::infinity( );
}

bool ITABase::CDecibelMagnitudeSpectrum::IsIdentity( ) const
{
	for( size_t n = 0; n < m_vfValues.size( ); n++ )
	{
		if( m_vfValues[n] != 0.0f )
			return false;
	}

	return true;
}

bool ITABase::CDecibelMagnitudeSpectrum::IsZero( ) const
{
	for( size_t n = 0; n < m_vfValues.size( ); n++ )
	{
		if( m_vfValues[n] != -std::numeric_limits<float>::infinity( ) )
			return false;
	}

	return true;
}

// ----- ----------- -------------------

ITABase::CGainMagnitudeSpectrum::CGainMagnitudeSpectrum( const int iNumBands ) : CMagnitudeSpectrum( iNumBands )
{
	CITASpectrum::SetValueUnit( "gain" );
}

ITABase::CGainMagnitudeSpectrum::~CGainMagnitudeSpectrum( ) {}

void ITABase::CGainMagnitudeSpectrum::SetIdentity( )
{
	for( size_t n = 0; n < m_vfValues.size( ); n++ )
		m_vfValues[n] = 1.0f;
}

void ITABase::CGainMagnitudeSpectrum::SetZero( )
{
	for( size_t n = 0; n < m_vfValues.size( ); n++ )
		m_vfValues[n] = 0.0f;
}

bool ITABase::CGainMagnitudeSpectrum::IsIdentity( ) const
{
	for( size_t n = 0; n < m_vfValues.size( ); n++ )
	{
		if( m_vfValues[n] != 1.0f )
			return false;
	}

	return true;
}

bool ITABase::CGainMagnitudeSpectrum::IsZero( ) const
{
	for( size_t n = 0; n < m_vfValues.size( ); n++ )
	{
		if( m_vfValues[n] != 0.0f )
			return false;
	}

	return true;
}
