#include "ITAClock.h"

#ifndef WIN32
#	define HAVE_POSIX
#endif

#include <ITAException.h>
#include <ITAPosixRealtimeClock.h>
#include <ITAWinPCClock.h>

static ITAClock* pDefaultClock = NULL;

ITAClock* ITAClock::getDefaultClock( )
{
	if( !pDefaultClock )
	{
#ifdef WIN32
		// Standard auf Windows-Systemen: Windows Performance Counters
		pDefaultClock = ITAWinPCClock::getInstance( );
#endif
#ifdef HAVE_POSIX
		// Standard on Posix systems: Real-time clock
		pDefaultClock = ITAPosixRealtimeClock::getInstance( );
#endif
		// Kein Standard-Zeitgeber f�r diese Plattform -> Ausnahme!
		if( !pDefaultClock )
			ITA_EXCEPT1( NOT_IMPLEMENTED, "Could not create a default clock instance (activate HAVE_POSIX if available)" );
	}

	return pDefaultClock;
}
