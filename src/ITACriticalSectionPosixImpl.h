#ifndef INCLUDE_WATCHER_ITA_CRITICALSECTION_POSIXIMPL
#define INCLUDE_WATCHER_ITA_CRITICALSECTION_POSIXIMPL

#include "ITACriticalSectionImpl.h"

#include <pthread.h>

/*
 *  Linux implementation by Jonas Stienen
 *
 *  Da es unter Linux nichts Vergleichbares zu den CriticalSections unter Windows gibt,
 *  behelfen wir uns der Mutex Funktionalit�t. Siehe auch:
 *
 *  http://www-128.ibm.com/developerworks/linux/library/l-ipc2lin3.html
 */

class ITACriticalSectionPosixImpl : public ITACriticalSectionImpl
{
public:
	ITACriticalSectionPosixImpl( ) : mutex( PTHREAD_MUTEX_INITIALIZER )
	{
		pthread_mutexattr_init( &mutexattr );
		pthread_mutexattr_settype( &mutexattr, PTHREAD_MUTEX_RECURSIVE_NP );
		pthread_mutex_init( &mutex, &mutexattr );
		pthread_mutexattr_destroy( &mutexattr );
	}

	~ITACriticalSectionPosixImpl( )
	{
		pthread_mutex_lock( &mutex );
		pthread_mutex_destroy( &mutex );
	}

	// TODO: Tests haben ergeben, dass der trylock nicht 0 ausgibt, wenn der kritische Bereich betretbar ist. Was ist da also los?
	bool tryenter( ) const
	{
		// const-wegcasten. Nach aussen soll eine CS const-Member haben.
		return pthread_mutex_trylock( (pthread_mutex_t*)&mutex ) == 0 ? true : false;
	}

	void enter( ) const
	{
		// const-wegcasten. Nach aussen soll eine CS const-Member haben.
		pthread_mutex_lock( (pthread_mutex_t*)&mutex );
	}

	void leave( ) const
	{
		// const-wegcasten. Nach aussen soll eine CS const-Member haben.
		pthread_mutex_unlock( (pthread_mutex_t*)&mutex );
	}

private:
	pthread_mutex_t mutex;         // Mutex
	pthread_mutexattr_t mutexattr; // Mutex attributes
};

#endif // INCLUDE_WATCHER_ITA_CRITICALSECTION_POSIXIMPL
