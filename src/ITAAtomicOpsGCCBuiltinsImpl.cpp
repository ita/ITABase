#include "ITAAtomicOps.h"

#include <stdint.h>

typedef union
{
	int32_t i32Value;
	float floatValue;
} ConvI32Float;

int atomic_read_int( volatile const int* src )
{
	/*
	 *  Trick: Realisierung mittels CAS!
	 *         Falls der Wert schon 0, dann wird er gegen 0 ausgetauscht...
	 *         Also keine Änderung!
	 */
	return __sync_val_compare_and_swap( (int*)src, 0, 0 );
}

long atomic_read_long( volatile const long* src )
{
	/*
	 *  Trick: Realisierung mittels CAS!
	 *         Falls der Wert schon 0, dann wird er gegen 0 ausgetauscht...
	 *         Also keine Änderung!
	 */
	return __sync_val_compare_and_swap( (long*)src, 0, 0 );
}

float atomic_read_float( volatile const float* src )
{
	/*
	 *  Trick: Realisierung mittels CAS!
	 *         Falls der Wert schon 0, dann wird er gegen 0 ausgetauscht...
	 *         Also keine �nderung!
	 *
	 *  Info:  Auf manchen Architekturen ist InterlockedAdd(src, 0) schneller.
	 *         Leider steht InterlockedAdd nur auf Itanium-Arch. zur Verf�gung.
	 */

	ConvI32Float c;
	c.i32Value = __sync_val_compare_and_swap( (volatile int32_t*)src, 0, 0 );
	return c.floatValue;
}

void* atomic_read_ptr( volatile const void** src )
{
	/*
	 *  Trick: Realisierung mittels CAS!
	 *         Falls der Wert schon 0, dann wird er gegen 0 ausgetauscht...
	 *         Also keine Änderung!
	 */
	return __sync_val_compare_and_swap( (void**)src, 0, 0 );
}

void atomic_write_int( volatile int* dest, int value )
{
	__sync_lock_test_and_set( dest, value );
}

void atomic_write_long( volatile long* dest, long value )
{
	__sync_lock_test_and_set( dest, value );
}

void atomic_write_float( volatile float* dest, float value )
{
	ConvI32Float c;
	c.floatValue = value;
	__sync_lock_test_and_set( (volatile int32_t*)dest, c.i32Value );
}

void atomic_write_ptr( volatile void** dest, void* value )
{
	__sync_lock_test_and_set( dest, value );
}

bool atomic_cas_int( volatile int* dest, int expected_value, int new_value )
{
	return __sync_bool_compare_and_swap( dest, expected_value, new_value );
}

/*
bool atomic_cas_double(volatile double* dest, double expected_value, double new_value) {
return __sync_bool_compare_and_swap(dest, expected_value, new_value);
}
*/

bool atomic_cas_ptr( volatile void** dest, void* expected_value, void* new_value )
{
	return __sync_bool_compare_and_swap( dest, expected_value, new_value );
}

void atomic_read32( volatile void* src, void* dest )
{
	*( (int32_t*)dest ) = __sync_val_compare_and_swap( (volatile int32_t*)src, 0, 0 );
}

bool atomic_cas32( volatile void* dest, void* expected_value, void* new_value )
{
	return __sync_bool_compare_and_swap( (volatile int32_t*)dest, *(int32_t*)expected_value, *(int32_t*)new_value );
}

int atomic_inc_int( volatile int* dest )
{
	return __sync_fetch_and_add( dest, 1 );
}

int atomic_dec_int( volatile int* dest )
{
	return __sync_fetch_and_sub( dest, 1 );
}
