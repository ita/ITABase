#include <ITAEndianness.h>

static int iEndianness = -1;

void byteswap_2byte( void*, size_t );
void byteswap_4byte( void*, size_t );
void byteswap_8byte( void*, size_t );
void byteswap_16byte( void*, size_t );
void noswap( void*, size_t );

void ( *le2se_2byte )( void* src, size_t count );
void ( *le2se_4byte )( void* src, size_t count );
void ( *le2se_8byte )( void* src, size_t count );
void ( *le2se_16byte )( void* src, size_t count );
void ( *be2se_2byte )( void* src, size_t count );
void ( *be2se_4byte )( void* src, size_t count );
void ( *be2se_8byte )( void* src, size_t count );
void ( *be2se_16byte )( void* src, size_t count );

void Init( )
{
	char test[2] = { 1, 0 };
	if( *( (short*)test ) == 1 )
	{
		// Little endian - small value byte at beginning
		iEndianness = ITA_LITTLE_ENDIAN;

		le2se_2byte  = &noswap;
		le2se_4byte  = &noswap;
		le2se_8byte  = &noswap;
		le2se_16byte = &noswap;

		be2se_2byte  = &byteswap_2byte;
		be2se_4byte  = &byteswap_4byte;
		be2se_8byte  = &byteswap_8byte;
		be2se_16byte = &byteswap_16byte;
	}
	else
	{
		// Big endian
		iEndianness = ITA_BIG_ENDIAN;

		le2se_2byte  = &byteswap_2byte;
		le2se_4byte  = &byteswap_4byte;
		le2se_8byte  = &byteswap_8byte;
		le2se_16byte = &byteswap_16byte;

		be2se_2byte  = &noswap;
		be2se_4byte  = &noswap;
		be2se_8byte  = &noswap;
		be2se_16byte = &noswap;
	}
}

int GetEndianness( )
{
	if( iEndianness == -1 )
		Init( );
	return iEndianness;
}

// Macro for triangle swap
#define TRI_SWAP( i, j ) \
	h    = p[i];         \
	p[i] = p[j];         \
	p[j] = h;

void byteswap_2byte( void* src, size_t count )
{
	// In:  |  B1  |  B0  |
	// Out: |  B0  |  B1  |

	char* p = (char*)src;
	char h;

	for( size_t i = 0; i < count; i++ )
	{
		TRI_SWAP( 0, 1 );

		p = p + 2;
	}
}

void byteswap_4byte( void* src, size_t count )
{
	// In:  |  B3  |  B2  |  B1  |  B0  |
	// Out: |  B0  |  B1  |  B2  |  B3  |

	char* p = (char*)src;
	char h;

	for( size_t i = 0; i < count; i++ )
	{
		TRI_SWAP( 0, 3 );
		TRI_SWAP( 1, 2 );

		p = p + 4;
	}
}

void byteswap_8byte( void* src, size_t count )
{
	// In:  |  B7  |  B6  |  B5  |  B4  |  B3  |  B2  |  B1  |  B0  |
	// Out: |  B0  |  B1  |  B2  |  B3  |  B4  |  B5  |  B6  |  B7 |

	char* p = (char*)src;
	char h;

	for( size_t i = 0; i < count; i++ )
	{
		TRI_SWAP( 0, 7 );
		TRI_SWAP( 1, 6 );
		TRI_SWAP( 2, 5 );
		TRI_SWAP( 3, 4 );

		p = p + 8;
	}
}

void byteswap_16byte( void* src, size_t count )
{
	char* p = (char*)src;
	char h;

	for( size_t i = 0; i < count; i++ )
	{
		TRI_SWAP( 0, 15 );
		TRI_SWAP( 1, 14 );
		TRI_SWAP( 2, 13 );
		TRI_SWAP( 3, 12 );
		TRI_SWAP( 4, 11 );
		TRI_SWAP( 5, 10 );
		TRI_SWAP( 6, 9 );
		TRI_SWAP( 7, 8 );

		p = p + 16;
	}
}

// Dummy
void noswap( void*, size_t ) {}
