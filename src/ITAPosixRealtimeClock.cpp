#include "ITAPosixRealtimeClock.h"

#include <ITAException.h>

#ifndef WIN32

#	include <time.h>


ITAPosixRealtimeClock* ITAPosixRealtimeClock::m_pInstance = NULL;

ITAPosixRealtimeClock* ITAPosixRealtimeClock::getInstance( )
{
	if( !m_pInstance )
		m_pInstance = new ITAPosixRealtimeClock( );
	return m_pInstance;
}

ITAPosixRealtimeClock::ITAPosixRealtimeClock( )
{
	struct timespec ts;
	int iResult   = clock_getres( CLOCK_REALTIME, &ts );
	m_dResolution = ts.tv_sec;
}

double ITAPosixRealtimeClock::getResolution( ) const
{
	return m_dResolution;
}

double ITAPosixRealtimeClock::getFrequency( ) const
{
	return 1 / m_dResolution;
}

inline double ITAPosixRealtimeClock::getTime( )
{
	struct timespec ts;
	clock_gettime( CLOCK_REALTIME, &ts );
	return ts.tv_sec + ts.tv_nsec / (double)1000000000;
}

#else // WIN32

#	include <cstdlib>

// Dummy implementation - does nothing, but allows clean compiling and linking

ITAPosixRealtimeClock* ITAPosixRealtimeClock::m_pInstance = NULL;

ITAPosixRealtimeClock* ITAPosixRealtimeClock::getInstance( )
{
	ITA_EXCEPT0( NOT_IMPLEMENTED );
	return NULL;
}
ITAPosixRealtimeClock::ITAPosixRealtimeClock( )
{
	ITA_EXCEPT0( NOT_IMPLEMENTED );
}
double ITAPosixRealtimeClock::getResolution( ) const
{
	ITA_EXCEPT0( NOT_IMPLEMENTED );
	return 0;
}
double ITAPosixRealtimeClock::getFrequency( ) const
{
	ITA_EXCEPT0( NOT_IMPLEMENTED );
	return 0;
}
double ITAPosixRealtimeClock::getTime( )
{
	ITA_EXCEPT0( NOT_IMPLEMENTED );
	return 0;
}

#endif // WIN32
