#include <ITAException.h>
#include <ITAMultichannelFiniteImpulseResponse.h>

ITABase::CMultichannelFiniteImpulseResponse::CMultichannelFiniteImpulseResponse( ) : m_dSampleRate( 0 ) {}

ITABase::CMultichannelFiniteImpulseResponse::CMultichannelFiniteImpulseResponse( const int iNumChannels, const int iLength, const double dSampleRate,
                                                                                 const bool bZeroInit /*= true */ )
    : ITASampleFrame( iNumChannels, iLength, bZeroInit )
    , m_dSampleRate( dSampleRate )
{
}

ITABase::CMultichannelFiniteImpulseResponse::CMultichannelFiniteImpulseResponse( const CMultichannelFiniteImpulseResponse* pSource )
{
	Init( pSource->channels( ), pSource->GetLength( ), pSource->GetSampleRate( ) );
	ITASampleFrame::write( pSource, pSource->GetLength( ) );
}

ITABase::CMultichannelFiniteImpulseResponse::CMultichannelFiniteImpulseResponse( const CMultichannelFiniteImpulseResponse& sbSource )
{
	Init( sbSource.GetNumChannels( ), sbSource.GetLength( ), sbSource.GetSampleRate( ) );
	ITASampleFrame::write( sbSource, sbSource.GetLength( ) );
}

void ITABase::CMultichannelFiniteImpulseResponse::Init( const int iNumChannels, const int iLength, const double dSampleRate, const bool bZeroInit /*= true */ )
{
	m_dSampleRate = dSampleRate;
	ITASampleFrame::Init( iNumChannels, iLength, bZeroInit );
}

double ITABase::CMultichannelFiniteImpulseResponse::GetSampleRate( ) const
{
	return m_dSampleRate;
}

double ITABase::CMultichannelFiniteImpulseResponse::GetNyquistFrequency( ) const
{
	return GetSampleRate( ) / 2.0f;
}

void ITABase::CMultichannelFiniteImpulseResponse::LoadFromFile( const std::string& sFilePath )
{
	ITASampleFrame::Load( sFilePath, m_dSampleRate );
}

void ITABase::CMultichannelFiniteImpulseResponse::StoreToFile( const std::string& sFilePath ) const
{
	if( GetNumChannels( ) <= 0 || GetSampleRate( ) <= 0.0f )
		ITA_EXCEPT1( INVALID_PARAMETER, "Trying to store a multichannel FIR filter with no channels or invalid sampling rate" );

	ITASampleFrame::Store( sFilePath, GetSampleRate( ) );
}
