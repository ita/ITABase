#include "libsndfileAudiofileReader.h"

#include <ITAException.h>
#include <ITATypes.h>
#include <algorithm>
#include <sys/stat.h>

// Voreinstellung: Maximale Gr��e des Lesepuffers (in Bytes)
// TODO: Welches ist eine sinnvolle Vorgabe?
#define MAX_READ_BUFFER_SIZE 1024 * 1024 // = 1 MB

libsndfileAudiofileReader::libsndfileAudiofileReader( const std::string& sFilePath )
{
	m_pfReadBuffer = 0;

	// libsndfile kann keinen expliziten "File-Not-Found"-Fehler(code) zur�ckgeben.
	// Da solch eine Ausgabe aber erw�nscht ist, stricken wir uns einen Test von Hand:
	struct stat buf;
	if( stat( sFilePath.c_str( ), &buf ) != 0 )
		ITA_EXCEPT1( FILE_NOT_FOUND, "Datei \"" + sFilePath + "\" nicht gefunden" );

	// Versuchen die Datei zu �ffnen
	SF_INFO info;
	if( !( _sf = sf_open( sFilePath.c_str( ), SFM_READ, &info ) ) )
	{
		/* DEBUG:
		printf("error: %d\n", sf_error(_sf));
		printf("perror: %d\n", sf_perror(_sf));
		printf("strerror: %s\n", sf_strerror(_sf));
		printf("error_number(error): %s\n", sf_error_number(sf_error(_sf)));
		printf("error_number(perror): %s\n", sf_error_number(sf_perror(_sf)));
		*/
		ITA_EXCEPT1( IO_ERROR, sf_error_number( sf_error( _sf ) ) );
	}

	m_iChannels   = info.channels;
	m_dSampleRate = (double)info.samplerate;
	m_iLength     = (int)info.frames;
	m_iCursor     = 0;

	// Datentyp bestimmen
	m_eQuantization = ITAQuantization::ITA_UNKNOWN_QUANTIZATION;
	if( ( info.format & SF_FORMAT_PCM_16 ) == SF_FORMAT_PCM_16 )
		m_eQuantization = ITAQuantization::ITA_INT16;
	if( ( info.format & SF_FORMAT_PCM_24 ) == SF_FORMAT_PCM_24 )
		m_eQuantization = ITAQuantization::ITA_INT24;
	if( ( info.format & SF_FORMAT_PCM_32 ) == SF_FORMAT_PCM_32 )
		m_eQuantization = ITAQuantization::ITA_INT32;
	if( ( info.format & SF_FORMAT_FLOAT ) == SF_FORMAT_FLOAT )
		m_eQuantization = ITAQuantization::ITA_FLOAT;
	if( ( info.format & SF_FORMAT_DOUBLE ) == SF_FORMAT_DOUBLE )
		m_eQuantization = ITAQuantization::ITA_DOUBLE;

	// Dieser Lader unterst�tzt nur den Zeitbereich
	m_eDomain = ITADomain::ITA_TIME_DOMAIN;

	// Kommentar suchen
	const char* pszComment = sf_get_string( _sf, SF_STR_COMMENT );
	m_sComment             = ( pszComment ? pszComment : "" );

	// Lesepuffer allozieren (Maximal die Vorgabe)
	m_iReadBufferSize = std::min( MAX_READ_BUFFER_SIZE / m_iChannels, m_iLength );
	m_pfReadBuffer    = new float[m_iReadBufferSize * m_iChannels];
}

libsndfileAudiofileReader::~libsndfileAudiofileReader( )
{
	delete[] m_pfReadBuffer;
	// Audiodatei schlie�en, falls diese ge�ffnet wurde
	if( _sf != 0 )
		if( sf_close( _sf ) != 0 )
			ITA_EXCEPT1( IO_ERROR, "Audiodatei konnte nicht geschlossen werden" );
}

void libsndfileAudiofileReader::seek( int iOffset )
{
	sf_count_t offset = (sf_count_t)iOffset;
	if( sf_seek( _sf, offset, SEEK_SET ) != offset )
		ITA_EXCEPT1( IO_ERROR, "Seek failed" );
}

void libsndfileAudiofileReader::read( int uiLength, std::vector<float*> vpfDest )
{
	if( uiLength == 0 )
		return;

	// Leseanforderung �ber das Ende der Datei hinaus?
	if( uiLength > ( m_iLength - m_iCursor ) )
		ITA_EXCEPT1( IO_ERROR, "Versuch �ber das Ende der Datei hinaus zu lesen" );

	// Wenn keine Zeiger im Vektor enthalten sind, ist nichts zu tun!
	if( vpfDest.empty( ) )
		return;

	// Falls mehr Zeiger im Vektor enthalten sind als Kan�le in der Datei,
	// so wird Sicherheitshalber eine Ausnahme geworfen!
	if( vpfDest.size( ) > m_iChannels )
		ITA_EXCEPT1( IO_ERROR, "Mehr Elemente im Vektor als Kanaele in der Datei" );

	bool bNull = true; // F�r den Test: Alle Zeiger im Vektor = Nullzeiger?
	int n      = 0;    // Anzahl gelesene Samples

	while( n < uiLength )
	{
		// Wieviele Samples werden in diesem Durchgang gelesen?
		int k = std::min( uiLength - n, m_iReadBufferSize );

		// Daten lesen
		sf_count_t frames = (sf_count_t)k;
		if( sf_readf_float( _sf, m_pfReadBuffer, frames ) != frames )
			ITA_EXCEPT1( IO_ERROR, sf_strerror( _sf ) );

		// Gelesene Daten deinterleaven
		for( int i = 0; i < vpfDest.size( ); i++ )
			if( vpfDest[i] )
			{
				bNull         = false;
				float* pfData = vpfDest[i];
				for( int j = 0; j < k; j++ )
					pfData[n + j] = m_pfReadBuffer[j * m_iChannels + i];
			}

		// Alle enthaltenen Zeiger = Nullzeiger?
		// Dann abbrechen, aber an die Zielstelle seeken (Wichtig!!!)
		if( bNull )
		{
			sf_count_t offset = ( sf_count_t )( m_iCursor + uiLength );
			if( sf_seek( _sf, offset, SEEK_SET ) != offset )
				ITA_EXCEPT1( IO_ERROR, "Seek failed" );
			return;
		}

		n += k;
	}
}
