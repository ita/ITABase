#include <ITATypes.h>

std::string toString( ITAQuantization eQuantization )
{
	switch( eQuantization )
	{
		case ITAQuantization::ITA_INT16:
			return "Integer with 16-Bit";
		case ITAQuantization::ITA_INT20:
			return "Integer with 20-Bit";
		case ITAQuantization::ITA_INT24:
			return "Integer with 24-Bit";
		case ITAQuantization::ITA_INT32:
			return "Integer with 32-Bit";
		case ITAQuantization::ITA_FLOAT:
			return "Floating point with 32-Bit";
		case ITAQuantization::ITA_DOUBLE:
			return "Floating point with 64-Bit";
		default:
			return "Unkown";
	}
}

std::string toString( ITADomain eDomain )
{
	return ( eDomain == ITADomain::ITA_TIME_DOMAIN ? "time domain" : "frequency domain" );
}
