#ifndef INCLUDE_WATCHER_ITA_CRITICALSECTION_WIN32IMPL
#define INCLUDE_WATCHER_ITA_CRITICALSECTION_WIN32IMPL

#include "ITACriticalSectionImpl.h"

#ifndef _WIN32_WINNT // @todo: remove
#	define _WIN32_WINNT 0x0501
#endif
#include <windows.h>

class ITACriticalSectionWin32Impl : public ITACriticalSectionImpl
{
public:
	inline ITACriticalSectionWin32Impl( ) { InitializeCriticalSection( (CRITICAL_SECTION*)&_cs ); };

	inline ~ITACriticalSectionWin32Impl( )
	{
		EnterCriticalSection( (CRITICAL_SECTION*)&_cs );
		DeleteCriticalSection( (CRITICAL_SECTION*)&_cs );
	};

	inline bool tryenter( ) const { return ( TryEnterCriticalSection( (CRITICAL_SECTION*)&_cs ) != 0 ); };

	inline void enter( ) const { EnterCriticalSection( (CRITICAL_SECTION*)&_cs ); };

	inline void leave( ) const { LeaveCriticalSection( (CRITICAL_SECTION*)&_cs ); };

private:
	CRITICAL_SECTION _cs;
};

#endif // INCLUDE_WATCHER_ITA_CRITICALSECTION_WIN32IMPL
