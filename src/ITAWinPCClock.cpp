#include <ITAWinPCClock.h>

#ifdef WIN32

#	include <ITAException.h>

#	ifndef _WIN32_WINNT // @todo: remove
#		define _WIN32_WINNT 0x0501
#	endif
#	include <windows.h>

ITAWinPCClock* ITAWinPCClock::m_pInstance = NULL;

ITAWinPCClock* ITAWinPCClock::getInstance( )
{
	if( !m_pInstance )
		m_pInstance = new ITAWinPCClock( );
	return m_pInstance;
}

ITAWinPCClock::ITAWinPCClock( )
{
	LARGE_INTEGER li;
	if( !QueryPerformanceFrequency( &li ) )
		ITA_EXCEPT1( UNKNOWN, "Failed to query the Windows performance counter frequency" );

	m_dFrequency = (double)li.QuadPart;
}

double ITAWinPCClock::getResolution( ) const
{
	return 1 / m_dFrequency;
}

double ITAWinPCClock::getFrequency( ) const
{
	return m_dFrequency;
}

__forceinline double ITAWinPCClock::getTime( )
{
	LARGE_INTEGER li;
	QueryPerformanceCounter( &li );
	return ( (double)li.QuadPart ) / m_dFrequency;
}

#else // WIN32

// Dummy implementation - does nothing, but allows clean compiling and linking

ITAWinPCClock* ITAWinPCClock::m_pInstance = NULL;

ITAWinPCClock* ITAWinPCClock::getInstance( )
{
	return NULL;
}
ITAWinPCClock::ITAWinPCClock( ) {}
double ITAWinPCClock::getResolution( ) const
{
	return 0;
}
double ITAWinPCClock::getFrequency( ) const
{
	return 0;
}
double ITAWinPCClock::getTime( )
{
	return 0;
}

#endif // WIN32
