#include "ITAAudiofileCommon.h"

#include <cstdio>

void print( const ITAAudiofileProperties& props )
{
	printf( "%s | %0.1f Hz | %i channels | %s | length: %i Samples", toString( props.eDomain ).c_str( ), props.dSampleRate, props.iChannels,
	        toString( props.eQuantization ).c_str( ), props.iLength );

	if( !props.sComment.empty( ) )
		printf( " | Comment: \"%s\"", props.sComment.c_str( ) );

	printf( "\n" );
}
