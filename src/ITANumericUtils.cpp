﻿#include <ITANumericUtils.h>

#include <ITAConstants.h>
#include <ITAException.h>
#include <VistaBase/VistaQuaternion.h>
#include <VistaBase/VistaTransformMatrix.h>
#include <VistaBase/VistaVector3D.h>
#include <cassert>
#include <numeric>

using namespace ITAConstants;

int getExp2( const unsigned int x )
{
	// Algorithmus: Bits überprüfen. Falls zwei Bits 1 sind -> keine 2er-Potenz

	// Triviale Fälle:
	if( x == 0 )
		return -1; // 0 ist keine Zweierpotenz
	if( x == 1 )
		return 0; // 2^0 = 1

	bool s         = false;           // Suchflag
	unsigned int n = sizeof( x ) * 8; // Anzahl der Bits im Datentyp
	unsigned int k = 0;               // Zähler

	// Erstes 1-Bit von rechts nach links (LSB->MSB) suchen
	// (Muss gefunden werden, da x!=0)
	while( ( ( x >> k ) & 1 ) == 0 )
		k++;

	unsigned int l = k; // Exponent speichern

	// Weitersuchen
	if( k < n )
	{
		k++;
		while( !( s = ( ( ( x >> k ) & 1 ) == 1 ) ) && k < n )
			k++;
	}

	return ( s ? -1 : (int)l );
}

bool isPow2( const unsigned int x )
{
	return getExp2( x ) != -1;
}

unsigned int nextPow2( const unsigned int x )
{
	// Trivialer Fall:
	if( x == 0 )
		return 1;

	bool s = false;           // Suchflag
	int n  = sizeof( x ) * 8; // Anzahl der Bits im Datentyp
	int k  = n - 1;           // Zähler

	// Erstes 1-Bit von links nach rechts (MSB->LSB) suchen
	// (Muss gefunden werden, da x!=0)
	while( ( ( x >> k ) & 1 ) == 0 )
		k--;

	unsigned int l = k; // Exponent speichern

	// Weitersuchen
	if( k > 1 )
	{
		k--;
		while( !( s = ( ( ( x >> k ) & 1 ) == 1 ) ) && k > 0 )
			k--;
	}

	// Zweites 1-Bit gefunden? Dann keine Zweierpotenz...
	return ( s ? ( (unsigned int)1 ) << ( l + 1 ) : x );
}

int lwrmul( const int x, const int mul )
{
	int r = x % mul; // Teilungsrest
	return ( r == 0 ? x : ( x > 0 ? x - r : x - r - mul ) );
}

int uprmul( const int x, const int mul )
{
	int r = x % mul; // Teilungsrest
	return ( r == 0 ? x : ( x > 0 ? x + mul - r : x - r ) );
}

unsigned int lwrmulu( const unsigned int x, const unsigned int mul )
{
	return x - ( x % mul );
}

unsigned int uprmulu( const unsigned int x, const unsigned int mul )
{
	unsigned int r = x % mul; // Teilungsrest
	return ( r == 0 ? x : x + mul - r );
}

int uprdiv( const int a, const int b )
{
	// TODO: Testen mit negativen Zahlen!
	int c = a / b;
	if( ( a % b ) > 0 )
		c++;
	return c;
}

unsigned int uprdivu( const unsigned int a, const unsigned int b )
{
	if( b == 0 )
		return 0;

	unsigned int c = a / b;
	if( ( a % b ) > 0 )
		c++;
	return c;
}

float rad2gradf( const float phi )
{
	return phi * 180.0F / PI_F;
}

double rad2grad( const double phi )
{
	return phi * 180.0 / PI_D;
}

float grad2radf( const float phi )
{
	return phi * PI_F / 180.0f;
}

double grad2rad( const double phi )
{
	return phi * PI_D / 180.0f;
}

float correctAngle180( const float phi )
{
	if( fabs( phi ) == 180.0f )
		return phi;

	const float phi_temp = fmodf( phi, 360.0f );
	if( phi_temp < -180.0f )
		return ( phi_temp + 360.0f );
	return ( fmodf( phi_temp + 180.0f, 360.0f ) - 180.0f );
}

float correctAngle360( const float phi )
{
	return fmodf( phi, 360.0f );
}

double db10_to_ratio( const double db )
{
	return ( db == -std::numeric_limits<double>::infinity( ) ? 0.0f : pow( 10.0f, db / 10.0f ) );
}

double db20_to_ratio( const double db )
{
	return ( db == -std::numeric_limits<double>::infinity( ) ? 0.0f : pow( 10.0f, db / 20.0f ) );
}

double ratio_to_db10( const double r )
{
	if( r < 0.0f )
		ITA_EXCEPT1( INVALID_PARAMETER, "Conversion to decibel not possible for negative values" );

	if( r == 0.0f )
		return -std::numeric_limits<double>::infinity( );
	else
		return 10.0f * log10( r );
}

double ratio_to_db20( const double r )
{
	if( r < 0.0f )
		ITA_EXCEPT1( INVALID_PARAMETER, "Conversion to decibel not possible for negative values" );

	if( r == 0.0f )
		return -std::numeric_limits<double>::infinity( );
	else
		return 20.0f * log10( r );
}

float cabsf( const float fReal, const float fImag )
{
	return sqrt( fReal * fReal + fImag * fImag );
}

float canglef( const float re, const float im )
{
	if( re == 0 )
	{
		// Realteil = 0
		if( im == 0 )
			// Komplexe Zahl 0+0i. Winkel nach Konvention 0
			return 0;
		else
			// Rein Imaginäre Zahl, d.h. im/re -> Division durch Null!
			return ( im > 0 ? PI_F / 2.0f : -PI_F / 2.0f );
	}
	else
	{
		if( re > 0 )
			// Re > 0, Im != 0 -> Standardberechnung über Arcustangens.
			return atan( im / re );
		else
			// Re < 0, Im != 0 -> Fallunterscheidungen
			return ( im >= 0 ? atan( im / re ) + PI_F : atan( im / re ) - PI_F );
	}
}

void csabsparg( const float in_re, const float in_im, const float dest_abs, float& out_re, float& out_im )
{
	if( dest_abs < 0.0f )
		ITA_EXCEPT_INVALID_PARAMETER( "Absolute value must be greater or equal zero" );

	// Nur Anpassung der Länge, d.h. Multiplikation mit konstantem
	// reellen Verlängerungsfaktor c = dest_abs / abs(in)

	const float fMag = cabsf( in_re, in_im );
	if( fMag == 0.0f )
	{
		out_re = 0.0f;
		out_im = 0.0f;
	}
	else
	{
		const float c = dest_abs / fMag;
		out_re        = in_re * c;
		out_im        = in_im * c;
	}
}

void csargpabs( const float in_re, const float in_im, const float dest_arg, float& out_re, float& out_im )
{
	// Nur Anpassung des Winkels: (1) Bestimmung des gegebenen
	// Betrags danach (2) Auswerten der Eulerformel

	float l = cabsf( in_re, in_im );
	out_re  = l * cos( dest_arg );
	out_im  = l * sin( dest_arg );
}

/* +----------- RAD -----------+ */
float anglef_proj_0_2PI( const float alpha )
{
	float alpha_temp = fmodf( alpha, 2 * PI_F );
	if( alpha_temp < 0 )
		alpha_temp += 2 * PI_F;
	return alpha_temp;
}

float anglef_proj_NPI_PI( const float alpha )
{
	float x = anglef_proj_0_2PI( alpha - PI_F ) + PI_F;
	return x;
}

float anglef_proj_NPIH_PIH( const float alpha )
{
	float alpha_temp = fmodf( alpha + PI_F / 2.0f, PI_F ) - PI_F / 2.0f;
	if( alpha_temp < -PI_F / 2.0f )
		alpha_temp += PI_F;
	return alpha_temp;
}

float anglef_mindiff_0_2PI( const float alpha, const float beta )
{
	float gamma = anglef_proj_0_2PI( beta ) - anglef_proj_0_2PI( alpha );
	if( gamma >= 0 )
		return ( gamma <= PI_F ? gamma : gamma - 2 * PI_F );
	else
		return ( gamma >= -PI_F ? gamma : gamma + 2 * PI_F );
}

float anglef_mindiff_abs_0_2PI( const float alpha, const float beta )
{
	// TODO: Schnellere Implementierung möglich
	return fabs( anglef_mindiff_0_2PI( alpha, beta ) );
}

float anglef_proj_0_360_DEG( const float alpha )
{
	return rad2gradf( anglef_proj_0_2PI( grad2radf( alpha ) ) );
}

float anglef_proj_N180_180_DEG( const float alpha )
{
	return rad2gradf( anglef_proj_NPI_PI( grad2radf( alpha ) ) );
}

float anglef_proj_N90_90_DEG( const float alpha )
{
	return rad2gradf( anglef_proj_NPIH_PIH( grad2radf( alpha ) ) );
}

float anglef_mindiff_0_360_DEG( const float alpha, const float beta )
{
	return rad2gradf( anglef_mindiff_0_2PI( grad2radf( alpha ), grad2radf( beta ) ) );
}

float anglef_mindiff_abs_0_360_DEG( const float alpha, const float beta )
{
	return rad2gradf( anglef_mindiff_abs_0_2PI( grad2radf( alpha ), grad2radf( beta ) ) );
}


void convertYPR2VU( const float yaw, const float pitch, const float roll, float& vx, float& vy, float& vz, float& ux, float& uy, float& uz )
{
	double vx_, vy_, vz_, ux_, uy_, uz_;
	convertYPR2VU( double( yaw ), double( pitch ), double( roll ), vx_, vy_, vz_, ux_, uy_, uz_ );
	vx = float( vx_ );
	vy = float( vy_ );
	vz = float( vz_ );
	ux = float( ux_ );
	uy = float( uy_ );
	uz = float( uz_ );
}

void convertYPR2VU( const double yaw, const double pitch, const double roll, double& vx, double& vy, double& vz, double& ux, double& uy, double& uz )
{
	/*
	 *  Yaw-pitch-roll (YPR) angles rotation order (referring to OpenGL axis)
	 *
	 *  R := Ry(yaw) * Rx(pitch) * Rz(-roll);
	 *
	 *  Note: Roll accounts to the view axis (-Z), not the Z axis (+Z). Hence inversion.
	 */

	double sy = sin( yaw ), cy = cos( yaw );
	double sp = sin( pitch ), cp = cos( pitch );
	double sr = sin( roll ), cr = cos( roll );

	vx = -sy * cp;
	vy = sp;
	vz = -cy * cp;

	ux = cy * sr + sy * sp * cr;
	uy = cp * cr;
	uz = -sy * sr + cy * sp * cr;
}
void convertVU2YPR( const float vx, const float vy, const float vz, const float ux, const float uy, const float uz, float& yaw, float& pitch, float& roll )
{
	double y, p, r;
	convertVU2YPR( double( vx ), double( vy ), double( vz ), double( ux ), double( uy ), double( uz ), y, p, r );
	yaw   = float( y );
	pitch = float( p );
	roll  = float( r );
}

void convertVU2YPR( const double vx, const double vy, const double vz, const double ux, const double uy, const double uz, double& yaw, double& pitch, double& roll )
{
	const float EPSILON = 0.0001F;

	// DEBUG: Test on normalization and orthogonality
	/*
	double Lv = sqrt( vx*vx + vy*vy + vz*vz );
	double Lu = sqrt( ux*ux + uy*uy + uz*uz );
	double Svu = vx*ux + vy*uy + vz*uz;
	*/

	// Problem: View points into north pole => Gimbal lock between yaw and roll
	if( vy >= ( 1 - EPSILON ) )
	{
		/*
		 *  Solution: Forget about roll. Everything is yaw.
		 *
		 *  Ry(y).Rx(Pi/2).Rz(0).u0 = (sin(y) 0 cos(y))
		 *
		 *  yaw = atan2(ux, uz)
		 *
		 */

		yaw   = atan2( ux, uz );
		pitch = HALF_PI_F;
		roll  = 0;

		return;
	}

	// Problem: View points into south pole => Gimbal lock between yaw and roll
	if( vy <= -( 1 - EPSILON ) )
	{
		/*
		 *  Solution: Forget about roll. Everything is yaw.
		 *
		 *  Ry(y).Rx(Pi/2).Rz(0).u0 = (sin(y) 0 cos(y))
		 *
		 *  yaw = atan2(-ux, -uz)
		 *
		 */

		yaw   = atan2( -ux, -uz );
		pitch = -HALF_PI_F;
		roll  = 0;

		return;
	}

	yaw   = atan2( -vx, -vz );
	pitch = asin( vy );

	/*
	 *  Problem: View does not point into a pole (see above)
	 *           but up-vector lies within horizontal XZ plane.
	 *
	 *  Solution: Roll can only be +90°/-90°.
	 *            Decide by hemisphere which crossprod(v,u) falls.
	 *            Upper hemisphere (y>=0) means -90°
	 *
	 */

	// y-component of cross production v x u
	double zy = vz * ux - vx * uz;

	if( ( uy <= EPSILON ) && ( uy >= -EPSILON ) )
	{
		// y-component of cross production v x u
		// double zy = vz*ux - vx*uz;
		roll = ( zy <= 0 ? HALF_PI_F : -HALF_PI_F );
		return;
	}

	// Hint: cos(pitch) = cos( arcsin(vy) ) = sqrt(1-vy^2)
	double cp = sqrt( 1 - vy * vy );
	roll      = ( zy <= 0 ? acos( uy / cp ) : -acos( uy / cp ) );

	/* debug
	double v_norm = vx*vx + vy*vy + vz*vz;
	double u_norm = ux*ux + uy*uy + uz*uz;
	*/
}


void calculateYPRAnglesFromQuaternion( VistaQuaternion q, double& dRoll, double& dPitch, double& dYaw )
{
	// Quaternion: [qx, qy, qz, qw] as described/defined in VistaQuaternion
	//			  [0,   1,  2,  3]
	// Taken from http://www.tu-berlin.de/fileadmin/fg169/miscellaneous/Quaternions.pdf
	// First convert Quaternion to a DCM, then transform DCM to euler
	double a21 = 2 * ( q[0] * q[1] + q[2] * q[3] );
	;
	double a22 = pow( q[3], 2 ) - pow( q[0], 2 ) + pow( q[1], 2 ) - pow( q[2], 2 );

	double a13 = 2 * ( q[0] * q[2] + q[1] * q[3] );
	double a33 = pow( q[3], 2 ) - pow( q[0], 2 ) - pow( q[1], 2 ) + pow( q[2], 2 );

	double a23 = 2 * ( q[1] * q[2] - q[0] * q[3] );

	// Calculation is based on Roll around z; Yaw around negative x; Pitch around y
	dRoll  = atan2( -a21, a22 );
	dYaw   = -atan2( -a13, a33 );
	dPitch = asin( -a23 );
}


void pow2space( std::vector<int>& dest, const int a, const int b )
{
	int xa = getExp2( a );
	int xb = getExp2( b );
	if( ( xa == -1 ) || ( xb == -1 ) )
		ITA_EXCEPT1( INVALID_PARAMETER, "Interval boundaries must be powers of two" );

	dest.clear( );
	if( xa > xb )
	{
		dest.reserve( xa - xb + 1 );
		for( int x = xa; x >= xb; --x )
			dest.push_back( 1 << x );
	}
	else
	{
		dest.reserve( xb - xa + 1 );
		for( int x = xa; x <= xb; ++x )
			dest.push_back( 1 << x );
	}
}

double theta2elevation( const double dThetaRAD )
{
	if( dThetaRAD < 0 || dThetaRAD > ITAConstants::PI_D )
		ITA_EXCEPT1( INVALID_PARAMETER, "thetaRAD is only valid between 0 and PI" );

	return ITAConstants::PI_D / 2.0f - dThetaRAD;
}

double elevation2theta( const double dElevationRAD )
{
	if( abs( dElevationRAD ) < ITAConstants::PI_D / 2.0f )
		ITA_EXCEPT1( INVALID_PARAMETER, "elevationRAD is only valid between -PI/2 and PI/2" );

	return ITAConstants::PI_D / 2.0f - dElevationRAD;
}

int factorial( const int m )
{
	if( m < 0 )
		ITA_EXCEPT1( INVALID_PARAMETER, "Factorial can only be calculated for a positive integer" );
	return ( m <= 1 ? 1 : m * factorial( m - 1 ) );
}

double SHNormalizeConst( const int m, const int n )
{
	if( m > n )
		ITA_EXCEPT1( INVALID_PARAMETER, "Abs(degree) cannot be greater than order" );

	double dRes = 0;
	if( m % 2 == 1 )
		dRes = -1;
	else
		dRes = 1;

	dRes *= sqrt( ( 2.0f * n + 1 ) * ( 2 - SHKronecker( m ) ) * double( factorial( n - m ) ) / ( 4.0f * PI_F * double( factorial( n + m ) ) ) );
	return dRes;
}

int SHKronecker( const int m )
{
	if( m == 0 )
		return 1;
	else
		return 0;
}

std::vector<double> SHRealvaluedBasefunctions( const double thetaRAD, const double azimuthRAD, const int maxOrder )
{
	std::vector<double> Y;
	int nmax = ( maxOrder + 1 ) * ( maxOrder + 1 );
	Y.resize( nmax );

	Y = SHAssociatedLegendre( maxOrder, cos( thetaRAD ) );

	for( int n = 0; n <= maxOrder; n++ )
	{
		Y[SHDegreeOrder2Linear( 0, n )] *= SHNormalizeConst( 0, n );
		for( int m = 1; m <= n; m++ )
		{
			double Normalizing = SHNormalizeConst( m, n );
			Y[SHDegreeOrder2Linear( m, n )] *= cos( m * azimuthRAD ) * Normalizing;
			Y[SHDegreeOrder2Linear( -m, n )] *= sin( m * azimuthRAD ) * Normalizing;
		}
	}
	return Y;
}

std::vector<double> SHAssociatedLegendre( const int N, const double mu )
{
	std::vector<double> P;
	P.resize( ( N + 1 ) * ( N + 1 ) );
	P[0] = 1.0;
	for( int n = 1; n <= N; n++ )
	{
		P[SHDegreeOrder2Linear( n, n )]     = ( -( 2 * n - 1 ) * P[SHDegreeOrder2Linear( ( n - 1 ), ( n - 1 ) )] * sqrt( 1 - ( mu * mu ) ) );
		P[SHDegreeOrder2Linear( n - 1, n )] = ( ( 2 * n ) - 1 ) * mu * P[SHDegreeOrder2Linear( n - 1, n - 1 )]; // m-ter Grad
		if( n > 1 )
		{
			for( int m = 0; m <= ( n - 2 ); m++ )
			{
				P[SHDegreeOrder2Linear( m, n )] =
				    ( ( 2 * n - 1 ) * mu * P[SHDegreeOrder2Linear( m, n - 1 )] - ( n + m - 1 ) * P[SHDegreeOrder2Linear( m, n - 2 )] ) / ( n - m );
			}
		}
		for( int m = 1; m <= n; m++ )
		{
			P[SHDegreeOrder2Linear( -m, n )] = P[SHDegreeOrder2Linear( m, n )];
		}
	}
	return P;
}

int SHDegreeOrder2Linear( const int m, const int n )
{
	if( abs( m ) > n )
		ITA_EXCEPT1( INVALID_PARAMETER, "Abs(degree) cannot be greater than order" );

	return ( n * n + n + m );
}

std::vector<double> HOARemaxWeights( int iTruncationOrder )
{
	if( iTruncationOrder > 20 )
		ITA_EXCEPT1( INVALID_PARAMETER, "ReMAx decoding only implemented up to Truncatrion Order 20" );

	std::vector<double> vdRemax;
	double dMaxRootTable[21]           = { 0.993752, 0.993129, 0.992407, 0.991565, 0.990575, 0.989401, 0.987993, 0.986284, 0.984183, 0.978229, 0.978229,
                                 0.973907, 0.96816,  0.96029,  0.949108, 0.93247,  0.90618,  0.861136, 0.774597, 0.57735,  0 };
	double dMaxRoot                    = dMaxRootTable[21 - iTruncationOrder - 1];
	std::vector<double> vdAssoLegendre = ( SHAssociatedLegendre( iTruncationOrder, dMaxRoot ) );

	for( int k = 0; k <= iTruncationOrder; k++ )
	{
		vdRemax.push_back( vdAssoLegendre[SHDegreeOrder2Linear( 0, k )] );
	}

	return vdRemax;
}

void SHLinear2DegreeOrder( const int iLinear, int& m, int& n )
{
	if( iLinear < 0 )
		ITA_EXCEPT1( INVALID_PARAMETER, "The linear index has to be higher than one" );

	std::vector<int> nm;
	n = (int)floor( sqrt( (double)iLinear ) );
	m = ( iLinear - n * n - n );
}
