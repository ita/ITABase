#include <ITAException.h>
#include <ITASampleFrame.h>
#include <ITAStopWatch.h>
#include <assert.h>
#include <iostream>

using namespace std;

int main( int argc, char** argv )
{
	ITASampleFrame sf;

	double dSampleRate;
	sf.Load( "OldDiesel.wav", dSampleRate );
	sf.div_scalar( 2 );
	sf.Store( "OldDiesel_out.wav", dSampleRate ); // ITA_FLOAT

	return 0;
}
