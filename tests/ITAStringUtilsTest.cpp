#include <ITAStringUtils.h>
#include <iostream>

using namespace std;

int main( int, char** )
{
	std::vector<int> v = StringToIntVec( "1,2,3,4" );

	for( int n: v )
		cout << n << endl;

	return 0;
}
