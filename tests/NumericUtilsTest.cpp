#include "Eigen/Dense"

#include <ITAConstants.h>
#include <ITANumericUtils.h>
#include <VistaBase/VistaVector3D.h>
#include <math.h>
#include <vector>

using namespace std;

double calcAzimuthDirection( VistaVector3D& view, VistaVector3D& up, VistaVector3D dir )
{
	dir.Normalize( );
	VistaVector3D right  = up.Cross( view );
	const double azimuth = atan2( dir.Dot( right ), dir.Dot( view ) );
	return ( azimuth < 0 ? azimuth + 2 * ITAConstants::PI_D : azimuth );
}

double calcZenithDirection( VistaVector3D& view, VistaVector3D& up, VistaVector3D dir )
{
	dir.Normalize( );
	const double zenith = acos( dir.Dot( up ) );
	return zenith;
}

// ------------------------ Tests start here ------------------------
void testSphericalHarmonics( VistaVector3D& view, VistaVector3D& up, vector<VistaVector3D>& vv3LsPos )
{
	const int N = floor( sqrt( vv3LsPos.size( ) ) - 1 ), hoaChannels = ( N + 1 ) * ( N + 1 );

	Eigen::MatrixXd matY( hoaChannels, vv3LsPos.size( ) );
	Eigen::VectorXd vecMagnitude( hoaChannels );
	vecMagnitude.setZero( );

	for( int i = 0; i < vv3LsPos.size( ); i++ )
	{
		VistaVector3D v3LsPos = vv3LsPos[i];
		double phi = calcAzimuthDirection( view, up, v3LsPos ), theta = calcZenithDirection( view, up, v3LsPos );
		vector<double> Y_i = SHRealvaluedBasefunctions( theta, phi, N );

		for( int j = 0; j < Y_i.size( ); j++ )
		{
			matY( j, i ) = Y_i[j];
			vecMagnitude[j] += Y_i[j] * Y_i[j];
		}
	}
	vecMagnitude /= vv3LsPos.size( );

	if( abs( vecMagnitude.mean( ) - 1.0 / ( 4 * ITAConstants::PI_D ) ) > 1e-3 )
	{
		cout << "Error: Mean magnitude for Williams normalization deviates from 1/(4*pi) by more than 1e-3" << endl;
	}
}

int main( int, char** )
{
	VistaVector3D up( 0, 1, 0 ), view( 0, 0, -1 );

	vector<VistaVector3D> vv3LsPos { // 4-Design, 14 points
		                             VistaVector3D( 0, 0, -1 ),
		                             VistaVector3D( -0.17765, 0.78406, -0.59472 ),
		                             VistaVector3D( 0.76784, -0.23818, -0.59472 ),
		                             VistaVector3D( -0.59019, -0.54588, -0.59472 ),
		                             VistaVector3D( -0.17765, -0.78406, 0.59472 ),
		                             VistaVector3D( -0.59019, 0.54588, 0.59472 ),
		                             VistaVector3D( 0.76784, 0.23818, 0.59472 ),
		                             VistaVector3D( -0.87947, 0.36847, -0.30125 ),
		                             VistaVector3D( 0.75884, 0.57741, -0.30125 ),
		                             VistaVector3D( 0.12063, -0.94588, -0.30125 ),
		                             VistaVector3D( -0.87947, -0.36847, 0.30125 ),
		                             VistaVector3D( 0.12063, 0.94588, 0.30125 ),
		                             VistaVector3D( 0.75884, -0.57741, 0.30125 ),
		                             VistaVector3D( -1.2246e-16, 0, 1 )
	};

	cout << "--- Testing Spherical Harmonics ---" << endl;
	testSphericalHarmonics( view, up, vv3LsPos );

	cout << endl << endl << "--- All tests finished ---";
}
