/*
 * ----------------------------------------------------------------
 *
 *		ITA Base
 *		(c) Copyright Institute of Technical Acoustics (ITA)
 *		RWTH Aachen University, Germany
 *
 * ----------------------------------------------------------------
 *				    ____  __________  _______
 *				   //  / //__   ___/ //  _   |
 *				  //  /    //  /    //  /_|  |
 *				 //  /    //  /    //  ___   |
 *				//__/    //__/    //__/   |__|
 *
 * ----------------------------------------------------------------
 *
 *
 *
 */


#include <ITABase/Math/Spline.h>
#include <ITAException.h>
#include <ITAStopWatch.h>
#include <fstream>
#include <iostream>
#include <random>

using namespace ITABase::Math;

bool BenchmarkPiecewisePolynomial( )
{
	// Create cubic spline polynomials
	const int nSamples = 1000;
	const double dLowerDataBound     = 0;
	const double dUpperDataBound     = 10000;
	std::vector<double> vdSupportingPoints( nSamples );
	std::vector<double> vdDataPoints( nSamples );
	std::uniform_real_distribution<double> unif_dist_data( dLowerDataBound, dUpperDataBound );
	std::default_random_engine random_engine;
	for( int idx = 0; idx < nSamples; idx++ )
	{
		vdSupportingPoints[idx] = double( idx );
		vdDataPoints[idx]       = unif_dist_data( random_engine );
	}
	CPiecewisePolynomial myPoly = CubicSpline( vdSupportingPoints, vdDataPoints );
	
	// Evaluation points
	const int nEvaluations = 10000;
	std::vector<double> vdEvaluationPoints( nEvaluations );
	std::uniform_real_distribution<double> unif_dist_eval( vdSupportingPoints.front( ), vdSupportingPoints.back( ) );
	for( int idx = 0; idx < nEvaluations; idx++ )
	{
		vdEvaluationPoints[idx] = unif_dist_eval( random_engine );
	}

	//myPoly.Evaluate( 0.0, true );
	//myPoly.Evaluate( vdSupportingPoints.back( ), true );
	//myPoly.Evaluate( -1.0, true );
	//myPoly.Evaluate( vdSupportingPoints.back()+1.0, true );
	
	// Actual benchmark
	std::cout << std::endl << "Evaluating run-time for " << nEvaluations << " evaluations of piecewise polynomial:" << std::endl;
	const int nRuns       = 100;
	ITAStopWatch swPPEvaluationWatch = ITAStopWatch( );
	for( int idx = 0; idx < nRuns; idx++ )
	{
		swPPEvaluationWatch.start( );
		for( const double& dEval: vdEvaluationPoints )
			myPoly.Evaluate( dEval );
		swPPEvaluationWatch.stop( );
	}
	std::cout << swPPEvaluationWatch << std::endl;

	return true;
}

bool SpeedTest( )
{
	// the following data is random.
	std::cout << std::endl << "zuerst wird der benchmark mit DOUBLES ausgefuert:" << std::endl;

	std::vector<double> vdSupportingPoints { -10, -9, -8, -7, -6, -5, -4, -3, -2, -1, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
	std::vector<double> vdDataPoints2 { 77, 80, 19, 49, 45, 65, 71, 76, 28, 68, 66, 17, 12, 50, 96, 35, 59, 23, 76, 26, 51 };
	std::vector<double> vdX     = { -10, 1.5, -7.898, 3.3, 3.3, 9.9, -2.5, 8.9, 0.01, -4.3 };
	CPiecewisePolynomial myPoly = CubicSpline( vdSupportingPoints, vdDataPoints2 );
	std::vector<double> result( 10 );
	ITAStopWatch myWatch = ITAStopWatch( );
	std::cout << "Rueckgabe von piecwisePolynomial Objekten" << std::endl;

	for( int k = 0; k < 100; k++ )
	{
		myWatch.start( );
		myPoly = CubicSpline( vdSupportingPoints, vdDataPoints2 );
		myWatch.stop( );
	}
	std::cout << myWatch << std::endl;
	myWatch.reset( );
	std::cout << "Rueckgabe von vector Objekten" << std::endl;

	for( int k = 0; k < 100; k++ )
	{
		myWatch.start( );
		result = CubicSpline( vdSupportingPoints, vdDataPoints2, vdX );
		myWatch.stop( );
	}
	std::cout << myWatch << std::endl;
	myWatch.reset( );
	// JETZT MIT FLOAT
	std::cout << std::endl << "Nun wird das gleiche noch mit FLOATS wiederholt:" << std::endl;

	std::vector<float> vdSupportingPoints_float { -10, -9, -8, -7, -6, -5, -4, -3, -2, -1, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
	std::vector<float> vdDataPoints2_float { 77, 80, 19, 49, 45, 65, 71, 76, 28, 68, 66, 17, 12, 50, 96, 35, 59, 23, 76, 26, 51 };
	std::vector<float> vdX_float      = { float( -10 ), float( 1.5 ),  float( -7.898 ), float( 3.3 ),  float( 3.3 ),
                                     float( 9.9 ), float( -2.5 ), float( 8.9 ),    float( 0.01 ), float( -4.3 ) };
	CPiecewisePolynomial myPoly_float = CubicSpline( vdSupportingPoints_float, vdDataPoints2_float );
	std::vector<float> result_float( 10 );

	std::cout << "Rueckgabe von piecwisePolynomial Objekten" << std::endl;

	for( int k = 0; k < 100; k++ )
	{
		myWatch.start( );
		myPoly_float = CubicSpline( vdSupportingPoints_float, vdDataPoints2_float );
		myWatch.stop( );
	}
	std::cout << myWatch << std::endl;
	myWatch.reset( );
	std::cout << "Rueckgabe von vector Objekten" << std::endl;

	for( int k = 0; k < 100; k++ )
	{
		myWatch.start( );
		result = CubicSpline( vdSupportingPoints_float, vdDataPoints2_float, vdX_float );
		myWatch.stop( );
	}
	std::cout << myWatch << std::endl;

	std::cout << std::endl;
	return true;
}

//! creates one .txt file with the Points a spline function shall be constructed for and one with the cooeficients, this programm has computes. A third contains the
//! coefficenc of the derivate. All can be read by the SplineTest.m into MATLAB.
void Compare2MATLAB( )
{
	// the following data is random.
	std::vector<double> vdSupportingPoints { -10, -9, -8, -7, -6, -5, -4, -3, -2, -1, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
	std::vector<double> vdDataPoints { 77, 80, 19, 49, 45, 65, 71, 76, 28, 68, 66, 17, 12, 50, 96, 35, 59, 23, 76, 26, 51 };
	std::vector<double> vdX         = { -10, 1.5, -7.898, 3.3, 3.3, 9.9, -2.5, 8.9, 0.01, -4.3 };
	CPiecewisePolynomial myPoly     = CubicSpline( vdSupportingPoints, vdDataPoints );
	CPiecewisePolynomial myDerivate = myPoly.Derivation( );

	std::vector<double> vdCoeff = myPoly.Coefficients( );
	std::ofstream myFile;
	myFile.open( "points.txt" );
	for( int k = 0; k < vdDataPoints.size( ); k++ )
		myFile << vdSupportingPoints[k] << ", " << vdDataPoints[k] << std::endl;
	myFile.close( );

	myFile.open( "coeff.txt" );
	for( int k = 0; k < vdDataPoints.size( ) - 1; k++ )
		myFile << vdCoeff[4 * k] << ", " << vdCoeff[4 * k + 1] << ", " << vdCoeff[4 * k + 2] << ", " << vdCoeff[4 * k + 3] << std::endl;
	myFile.close( );

	vdCoeff = myDerivate.Coefficients( );
	myFile.open( "derivate.txt" );
	for( int k = 0; k < vdDataPoints.size( ) - 1; k++ )
		myFile << vdCoeff[3 * k] << ", " << vdCoeff[3 * k + 1] << ", " << vdCoeff[3 * k + 2] << std::endl;
	myFile.close( );
}
//! Checks the general behavior of the code
bool GeneralTest( )
{
	// ALLES mit FLOATS
	// the following data is random.
	const std::vector<float> vdSupportingPoints { -10, -9, -8, -7, -6, -5, -4, -3, -2, -1, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
	const std::vector<float> vdDataPoints { 77, 80, 19, 49, 45, 65, 71, 76, 28, 68, 66, 17, 12, 50, 96, 35, 59, 23, 76, 26, 51 };
	const std::vector<float> vdX = { -10, 1.5f, -7.898f, 3.3f, 3.3f, 9.9f, -2.5f, 8.9f, 0.01f, -4.3f };
	CPiecewisePolynomial myPoly  = CubicSpline( vdSupportingPoints, vdDataPoints );

	std::cout << "-------Starting with FLOAT values-------" << std::endl << std::endl;

	std::cout << "At supportion Point 0, the calculated value should be 66: " << myPoly.Evaluate( float( 0 ) ) << std::endl;
	// should get specific Value
	std::cout << "At random Point x=0.5 the spline should interpolate ~40.9: " << myPoly.Evaluate( float( 0.5 ) ) << std::endl;


	std::vector<float> vfBreakPoints = { 0, 1 };
	std::vector<float> vfCoeffs      = { 1, 1, 1 };
	int iOrder                       = 2;
	CPiecewisePolynomial myPoly2     = CPiecewisePolynomial( vfBreakPoints, vfCoeffs, iOrder );

	// deviation test
	CPiecewisePolynomial myPoly3 = myPoly2.Derivation( );
	std::cout << "Deviation test:" << std::endl;
	if( myPoly3.NumCoefficients( ) == 2 && myPoly3.Coefficients( )[0] == 2 && myPoly3.Coefficients( )[1] == 1 )
		std::cout << "OK";
	else
		std::cout << "ERROR: The deviation of x^2+x+1 should be 2x + 1 But the new polyonom has " << myPoly3.NumCoefficients( )
		          << " coefficients: " << myPoly3.Coefficients( )[0] << " * x + " << myPoly3.Coefficients( )[1];
	std::cout << std::endl << std::endl;

	// exceptions for Piecewise Polynoms
	std::cout << "Testing the Value function with Argument out of it's range, without extrapolation:" << std::endl;
	bool ok = false;
	try
	{
		myPoly2.Evaluate( float( -2 ) );
	}
	catch( ITAException& err )
	{
		ok = true;
		std::cout << err;
	}
	if( !ok )
		std::cout << "ERROR: Expected throw of exception";
	std::cout << std::endl << std::endl;

	std::cout << "Testing the Value function with Arguments out of it's range, with extrapolation:" << std::endl;
	std::cout << "At -2, the calculated value should be -1: " << myPoly2.Evaluate( float( -2 ), true ) << std::endl;
	std::cout << "At 2, the calculated value should be 6: " << myPoly2.Evaluate( float( 2 ), true ) << std::endl;
	std::cout << std::endl;

	// exceptions for spline
	std::cout << "Testing spline with too few Points:" << std::endl;
	ok = false;
	try
	{
		std::vector<float> vfSupportingPoints { -10, -9 };
		std::vector<float> vfDataPoints { 77, 80 };
		myPoly = CubicSpline( vfSupportingPoints, vfDataPoints );
	}
	catch( ITAException& err )
	{
		ok = true;
		std::cout << err;
	}
	if( !ok )
		std::cout << "ERROR: Expected throw of exception";
	std::cout << std::endl << std::endl;


	std::cout << "Testing non matching dimensions:" << std::endl;
	ok = false;
	try
	{
		std::vector<float> vdSupportingPoints { -10, -5, -1, 0, 3 }; // 5points
		std::vector<float> vdDataPoints { 77, 80, 4, 5, -4, -1 };    // 6 Points
		myPoly = CubicSpline( vdSupportingPoints, vdDataPoints );
	}
	catch( ITAException& err )
	{
		ok = true;
		std::cout << err;
	}
	if( !ok )
		std::cout << "ERROR: Expected throw of exception";
	std::cout << std::endl << std::endl;

	std::cout << "Testing with non ascending supportingPoints" << std::endl;
	ok = false;
	try
	{
		std::vector<float> vfSupportingPoints { -10, 5, -1, 0, 3 };
		std::vector<float> vfDataPoints { 77, 80, 4, 5, -4 };
		myPoly = CubicSpline( vfSupportingPoints, vfDataPoints );
	}
	catch( ITAException& err )
	{
		ok = true;
		std::cout << err << std::endl;
	}
	if( !ok )
		std::cout << "ERROR: Expected throw of exception";
	std::cout << std::endl << std::endl;

	//
	//	ALLES MIT DOUBLES
	//

	// the following data is random.
	std::vector<double> vdSupportingPoints_double { -10, -9, -8, -7, -6, -5, -4, -3, -2, -1, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
	std::vector<double> vdDataPoints_double { 77, 80, 19, 49, 45, 65, 71, 76, 28, 68, 66, 17, 12, 50, 96, 35, 59, 23, 76, 26, 51 };
	std::vector<double> vdX_double     = { -10, 1.5, -7.898, 3.3, 3.3, 9.9, -2.5, 8.9, 0.01, -4.3 };
	CPiecewisePolynomial myPoly_double = CubicSpline( vdSupportingPoints_double, vdDataPoints_double );

	std::cout << std::endl << "-------continuing with DOUBLE values-------" << std::endl << std::endl;

	std::cout << "At supportion Point 0, the calculated value should be 66: " << myPoly_double.Evaluate( 0.0 ) << std::endl;
	// should get specific Value
	std::cout << "At random Point x=0.5 the spline should interpolate ~40.9: " << myPoly_double.Evaluate( 0.5 ) << std::endl;


	std::vector<double> vdBreakPoints_double = { 0, 1 };
	std::vector<double> vdCoeff_double       = { 1, 1, 1 };
	int iOrder2                              = 2;
	CPiecewisePolynomial myPoly2_double      = CPiecewisePolynomial( vdBreakPoints_double, vdCoeff_double, iOrder2 );

	// deviation test
	CPiecewisePolynomial myPoly3_double = myPoly2_double.Derivation( );
	std::cout << "The deviation of x^2+x+1 should be: " << myPoly3_double.Coefficients( )[0] << " * x + " << myPoly3_double.Coefficients( )[1] << std::endl;
	std::cout << "so the new polynoial only got " << myPoly3_double.NumCoefficients( ) << " coefficients." << std::endl;

	// exceptions for Piecewise Polynoms
	std::cout << "Testing the Value function with Argument out of it's range, without extrapolation:" << std::endl;
	try
	{
		myPoly2_double.Evaluate( -2.0 );
	}
	catch( ITAException& err )
	{
		std::cout << err << std::endl;
	}
	std::cout << "Testing the Value function with Arguments out of it's range, with extrapolation:" << std::endl;
	std::cout << "At -2, the calculated value should be -1: " << myPoly2_double.Evaluate( -2.0, true ) << std::endl;
	std::cout << "At 2, the calculated value should be 6: " << myPoly2_double.Evaluate( 2.0, true ) << std::endl;
	// exceptions for spline
	std::cout << "Testing spline with to few Points:" << std::endl;
	try
	{
		std::vector<double> vdSupportingPoints { -10, -9 };
		std::vector<double> vdDataPoints { 77, 80 };
		myPoly_double = CubicSpline( vdSupportingPoints, vdDataPoints );
	}
	catch( ITAException& err )
	{
		std::cout << err << std::endl;
	}
	std::cout << "Testing non matching dimensions:" << std::endl;
	try
	{
		std::vector<double> vdSupportingPoints { -10, -5, -1, 0, 3 }; // 5points
		std::vector<double> vdDataPoints { 77, 80, 4, 5, -4, -1 };    // 6 Points
		myPoly_double = CubicSpline( vdSupportingPoints, vdDataPoints );
	}
	catch( ITAException& err )
	{
		std::cout << err << std::endl;
	}
	std::cout << "Testing with non ascending supportingPoints" << std::endl;
	try
	{
		std::vector<double> vdSupportingPoints { -10, 5, -1, 0, 3 };
		std::vector<double> vdDataPoints { 77, 80, 4, 5, -4 };
		myPoly_double = CubicSpline( vdSupportingPoints, vdDataPoints );
	}
	catch( ITAException& err )
	{
		std::cout << err << std::endl;
	}

	//
	// JETZT ALLE PERMUTATIONEN AN ÜBERGABEPARAMETERN FÜR DIE KONSTRUKTOREN TESTEN
	//

	CPiecewisePolynomial mySpline1 = CubicSpline( vdSupportingPoints, vdDataPoints );
	CPiecewisePolynomial mySpline2 = CubicSpline( vdSupportingPoints_double, vdDataPoints );
	CPiecewisePolynomial mySpline3 = CubicSpline( vdSupportingPoints, vdDataPoints_double );
	CPiecewisePolynomial mySpline4 = CubicSpline( vdSupportingPoints_double, vdDataPoints_double );
	std::vector<double> myResult1  = CubicSpline( vdSupportingPoints_double, vdDataPoints_double, vdX_double );
	std::vector<double> myResult2  = CubicSpline( vdSupportingPoints, vdDataPoints, vdX );
	float myResult3                = CubicSpline( vdSupportingPoints, vdDataPoints, float( 1 ) );
	double myResult4               = CubicSpline( vdSupportingPoints_double, vdDataPoints_double, double( 1 ) );
	std::cout << std::endl << "Es wurden erfolgreich alle Konstruktoren getestet" << std::endl;
	return true;
}

//! SplineTest.cpp consists of several Test for Speed, functionality and DataExport to MATLAB, to compare and plot splines
int main( int iNumInArgs, char* pcInArgs[] )
{
	std::cout << "Checking starts" << std::endl;
	std::cout << "Test 1: general Test for functions" << std::endl << std::endl;
	GeneralTest( );
	std::cout << std::endl << "Test 2: Speedtest" << std::endl << std::endl;
	SpeedTest( );

	std::cout << "The next function will write 3 .txt files that need to be imported to MATLAB with the help of SplineTest.m" << std::endl;
	Compare2MATLAB( );

	BenchmarkPiecewisePolynomial( );

	return 0;
}
