cmake_minimum_required (VERSION 3.20 FATAL_ERROR)

project (ITABaseTests)

# ######################################################################################################################

add_executable (ITAStringUtilsTest ITAStringUtilsTest.cpp)
target_link_libraries (ITAStringUtilsTest PUBLIC ITABase::ITABase)

set_property (TARGET ITAStringUtilsTest PROPERTY FOLDER "Tests/ITABase")
set_property (TARGET ITAStringUtilsTest PROPERTY VS_DEBUGGER_WORKING_DIRECTORY ${CMAKE_RUNTIME_OUTPUT_DIRECTORY})

install (TARGETS ITAStringUtilsTest RUNTIME DESTINATION ${CMAKE_INSTALL_BINDIR})

# ######################################################################################################################

add_executable (ConfigUtilsTest ConfigUtilsTest.cpp)
target_link_libraries (ConfigUtilsTest PUBLIC ITABase::ITABase)

set_property (TARGET ConfigUtilsTest PROPERTY FOLDER "Tests/ITABase")
set_property (TARGET ConfigUtilsTest PROPERTY VS_DEBUGGER_WORKING_DIRECTORY ${CMAKE_RUNTIME_OUTPUT_DIRECTORY})

install (TARGETS ConfigUtilsTest RUNTIME DESTINATION ${CMAKE_INSTALL_BINDIR})

file (
	GENERATE
	OUTPUT ${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/ConfigUtilsTest.ini
	INPUT ${CMAKE_CURRENT_LIST_DIR}/ConfigUtilsTest.ini
)
install (FILES ${CMAKE_CURRENT_LIST_DIR}/ConfigUtilsTest.ini TYPE BIN)

# ######################################################################################################################

add_executable (AttenuationISO9613Test AttenuationISO9613Test.cpp)
target_link_libraries (AttenuationISO9613Test PUBLIC ITABase::ITABase)

set_property (TARGET AttenuationISO9613Test PROPERTY FOLDER "Tests/ITABase")
set_property (TARGET AttenuationISO9613Test PROPERTY VS_DEBUGGER_WORKING_DIRECTORY ${CMAKE_RUNTIME_OUTPUT_DIRECTORY})

install (TARGETS AttenuationISO9613Test RUNTIME DESTINATION ${CMAKE_INSTALL_BINDIR})

# ######################################################################################################################

add_executable (ITABaseSampleBufferTest ITABaseSampleBufferTest.cpp)
target_link_libraries (ITABaseSampleBufferTest PUBLIC ITABase::ITABase)

set_property (TARGET ITABaseSampleBufferTest PROPERTY FOLDER "Tests/ITABase")
set_property (TARGET ITABaseSampleBufferTest PROPERTY VS_DEBUGGER_WORKING_DIRECTORY ${CMAKE_RUNTIME_OUTPUT_DIRECTORY})

install (TARGETS ITABaseSampleBufferTest RUNTIME DESTINATION ${CMAKE_INSTALL_BINDIR})

# ######################################################################################################################

if (ITA_BASE_WITH_SNDFILE)

	add_executable (ITABaseSampleFrameTest ITABaseSampleFrameTest.cpp)
	target_link_libraries (ITABaseSampleFrameTest PUBLIC ITABase::ITABase)

	set_property (TARGET ITABaseSampleFrameTest PROPERTY FOLDER "Tests/ITABase")
	set_property (
		TARGET ITABaseSampleFrameTest PROPERTY VS_DEBUGGER_WORKING_DIRECTORY ${CMAKE_RUNTIME_OUTPUT_DIRECTORY}
	)

	install (TARGETS ITABaseSampleFrameTest RUNTIME DESTINATION ${CMAKE_INSTALL_BINDIR})

endif ()

# ######################################################################################################################

if (ITA_BASE_WITH_SAMPLERATE)

	add_executable (ITABaseAudioSampleTest ITABaseAudioSampleTest.cpp)
	target_link_libraries (ITABaseAudioSampleTest PUBLIC ITABase::ITABase)

	set_property (TARGET ITABaseAudioSampleTest PROPERTY FOLDER "Tests/ITABase")
	set_property (
		TARGET ITABaseAudioSampleTest PROPERTY VS_DEBUGGER_WORKING_DIRECTORY ${CMAKE_RUNTIME_OUTPUT_DIRECTORY}
	)

	install (TARGETS ITABaseAudioSampleTest RUNTIME DESTINATION ${CMAKE_INSTALL_BINDIR})

endif ()

# ######################################################################################################################

add_executable (ITABaseSpectrumTests ITABaseSpectrumTests.cpp)
target_link_libraries (ITABaseSpectrumTests PUBLIC ITABase::ITABase)

set_property (TARGET ITABaseSpectrumTests PROPERTY FOLDER "Tests/ITABase")
set_property (TARGET ITABaseSpectrumTests PROPERTY VS_DEBUGGER_WORKING_DIRECTORY ${CMAKE_RUNTIME_OUTPUT_DIRECTORY})

install (TARGETS ITABaseSpectrumTests RUNTIME DESTINATION ${CMAKE_INSTALL_BINDIR})

# ######################################################################################################################

add_executable (SplineTest SplineTest.cpp)
target_link_libraries (SplineTest PUBLIC ITABase::ITABase)

set_property (TARGET SplineTest PROPERTY FOLDER "Tests/ITABase")
set_property (TARGET SplineTest PROPERTY VS_DEBUGGER_WORKING_DIRECTORY ${CMAKE_RUNTIME_OUTPUT_DIRECTORY})

install (TARGETS SplineTest RUNTIME DESTINATION ${CMAKE_INSTALL_BINDIR})

# ######################################################################################################################

if (ITA_BASE_WITH_JSON_SUPPORT)
	add_executable (ITABaseUtilsJSONTest ITABaseUtilsJSONTest.cpp)
	target_link_libraries (ITABaseUtilsJSONTest PUBLIC ITABase::ITABase nlohmann_json::nlohmann_json)

	set_property (TARGET ITABaseUtilsJSONTest PROPERTY FOLDER "Tests/ITABase")
	set_property (TARGET ITABaseUtilsJSONTest PROPERTY VS_DEBUGGER_WORKING_DIRECTORY ${CMAKE_RUNTIME_OUTPUT_DIRECTORY})

	install (TARGETS ITABaseUtilsJSONTest RUNTIME DESTINATION ${CMAKE_INSTALL_BINDIR})
endif ()

# ######################################################################################################################

add_executable (NumericUtilsTest NumericUtilsTest.cpp)
target_link_libraries (NumericUtilsTest PUBLIC ITABase::ITABase Eigen3::Eigen)

set_property (TARGET NumericUtilsTest PROPERTY FOLDER "Tests/ITABase")
set_property (TARGET NumericUtilsTest PROPERTY VS_DEBUGGER_WORKING_DIRECTORY ${CMAKE_RUNTIME_OUTPUT_DIRECTORY})

install (TARGETS NumericUtilsTest RUNTIME DESTINATION ${CMAKE_INSTALL_BINDIR})

# ######################################################################################################################

add_executable (TriangulationTest TriangulationTest.cpp)
target_link_libraries (TriangulationTest PUBLIC ITABase::ITABase)

set_property (TARGET TriangulationTest PROPERTY FOLDER "Tests/ITABase")
set_property (TARGET TriangulationTest PROPERTY VS_DEBUGGER_WORKING_DIRECTORY ${CMAKE_RUNTIME_OUTPUT_DIRECTORY})

install (TARGETS TriangulationTest RUNTIME DESTINATION ${CMAKE_INSTALL_BINDIR})

# ######################################################################################################################

add_subdirectory (VistaTests)
