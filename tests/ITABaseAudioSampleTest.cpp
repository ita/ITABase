#include <ITAAudioSample.h>
#include <ITAException.h>
#include <iostream>

using namespace std;

int main( int, char** )
{
	try
	{
		CITAAudioSample asTTS48kHz( "tts.wav" );
		CITAAudioSample asTTS44kHz;
		asTTS44kHz.Load( asTTS48kHz );
		asTTS44kHz.Store( "tts_44khz.wav" );

		cout << "Converted files." << endl;
	}
	catch( ITAException& err )
	{
		cerr << "File test error: " << err << endl;
	}

	try
	{
		CITAAudioSample as44kfs;
		CITAAudioSample as48kfs( 48000.0f );
		CITAAudioSample as96kfs( 96000.0f );
		CITAAudioSample as128kfs( 128000.0f );

		as44kfs.Init( 2, 2 * 44100, 44.1e3, true );
		as44kfs[0][0]                        = 1.0f;
		as44kfs[0][as44kfs.GetLength( ) - 1] = -1.0f;

		as48kfs.Load( as44kfs );
		as96kfs.Load( as48kfs );
		as128kfs.Load( as96kfs );

		cout << "Converted." << endl;
	}
	catch( ITAException& err )
	{
		cerr << "Generic test error: " << err << endl;
	}

	return 0;
}
