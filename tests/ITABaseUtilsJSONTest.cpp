#include <ITABase/UtilsJSON.h>
#include <ITAThirdOctaveMagnitudeSpectrum.h>

using namespace std;

int main( int nArgsIn, char** pccArgs )
{
	ITABase::CThirdOctaveGainMagnitudeSpectrum oSpectrum;
	oSpectrum.SetName( "test_spectrum" );
	for( int idx = 0; idx < oSpectrum.GetNumBands( ); idx++ )
		oSpectrum.SetMagnitude( idx, idx );
	ITABase::Utils::JSON::Export( &oSpectrum, "test_spectrum.json" );

	ITABase::CStatistics statsTime( "test_stats_time", "s" );
	statsTime.dMax    = 1;
	statsTime.dMean   = 1;
	statsTime.dMin    = 1;
	statsTime.dStdDev = 1;

	ITABase::CStatistics statsPressure( "test_stats_pressure", "Pa" );
	statsPressure.dMax    = 2;
	statsPressure.dMean   = 2;
	statsPressure.dMin    = 2;
	statsPressure.dStdDev = 2;

	std::vector<ITABase::CStatistics> statsVector { statsTime, statsPressure };

	ITABase::Utils::JSON::Export( statsTime, "test_single_stats.json" );
	ITABase::Utils::JSON::Export( statsVector, "test_multiple_stats.json" );

	return 0;
}
