#include <ITAISO9613.h>
#include <VistaBase/VistaVector3D.h>
#include <iostream>

using namespace std;

int main( int nArgsIn, char** pccArgs )
{
	// Test values taken from ISO 9613-1 Table 1


	// Static air pressure [kPa]
	const double dStaticPressure = 101.325;

	// Distance [m]
	const double dDistance = 1000.0;

	// Temperature [�C], frequency [Hz] and relative humidity [%]
	// for different test environments as well as the respective
	// atmospheric-absorption absorption coefficients [dB] of Table 1.

	const double dTemperatureA       = -20.0;
	const double dFrequencyA         = 50.0;
	const double dHumidityA          = 10.0;
	const double dTargetAttenuationA = 5.89e-1;
	double dActualAttenuationA       = ITABase::ISO9613::AtmosphericAbsorptionLevel( dFrequencyA, dDistance, dTemperatureA, dHumidityA, dStaticPressure );

	const double dTemperatureB       = -5.0;
	const double dFrequencyB         = 1000.0;
	const double dHumidityB          = 50.0;
	const double dTargetAttenuationB = 9.68;
	double dActualAttenuationB       = ITABase::ISO9613::AtmosphericAbsorptionLevel( dFrequencyB, dDistance, dTemperatureB, dHumidityB, dStaticPressure );

	const double dTemperatureC       = 0.0;
	const double dFrequencyC         = 315.0;
	const double dHumidityC          = 30.0;
	const double dTargetAttenuationC = 1.69;
	double dActualAttenuationC       = ITABase::ISO9613::AtmosphericAbsorptionLevel( dFrequencyC, dDistance, dTemperatureC, dHumidityC, dStaticPressure );

	const double dTemperatureD       = 20.0;
	const double dFrequencyD         = 6300.0;
	const double dHumidityD          = 15.0;
	const double dTargetAttenuationD = 1.75e2;
	double dActualAttenuationD       = ITABase::ISO9613::AtmosphericAbsorptionLevel( dFrequencyD, dDistance, dTemperatureD, dHumidityD, dStaticPressure );

	const double dTemperatureE       = 35.0;
	const double dFrequencyE         = 125.0;
	const double dHumidityE          = 80.0;
	const double dTargetAttenuationE = 1.95e-1;
	double dActualAttenuationE       = ITABase::ISO9613::AtmosphericAbsorptionLevel( dFrequencyE, dDistance, dTemperatureE, dHumidityE, dStaticPressure );

	const double dTemperatureF       = 50.0;
	const double dFrequencyF         = 10000.0;
	const double dHumidityF          = 100.0;
	const double dTargetAttenuationF = 1.09e2;
	double dActualAttenuationF       = ITABase::ISO9613::AtmosphericAbsorptionLevel( dFrequencyF, dDistance, dTemperatureF, dHumidityF, dStaticPressure );


	// Compare the here calculated values with the values taken from ISO 9613-1 Table 1.
	cout << "Temperature: " << dTemperatureA << " �C\t Relative humidity: " << dHumidityA << " %\t Frequency: " << dFrequencyA
	     << " Hz\t Target attenuation: " << dTargetAttenuationA << " dB\t Calculated attenuation: " << dActualAttenuationA << " dB" << endl;
	cout << "Temperature: " << dTemperatureB << " �C\t Relative humidity: " << dHumidityB << " %\t Frequency: " << dFrequencyB
	     << " Hz\t Target attenuation: " << dTargetAttenuationB << " dB\t Calculated attenuation: " << dActualAttenuationB << " dB" << endl;
	cout << "Temperature: " << dTemperatureC << " �C\t Relative humidity: " << dHumidityC << " %\t Frequency: " << dFrequencyC
	     << " Hz\t Target attenuation: " << dTargetAttenuationC << " dB\t Calculated attenuation: " << dActualAttenuationC << " dB" << endl;
	cout << "Temperature: " << dTemperatureD << " �C\t Relative humidity: " << dHumidityD << " %\t Frequency: " << dFrequencyD
	     << " Hz\t Target attenuation: " << dTargetAttenuationD << " dB\t Calculated attenuation: " << dActualAttenuationD << " dB" << endl;
	cout << "Temperature: " << dTemperatureE << " �C\t Relative humidity: " << dHumidityE << " %\t Frequency: " << dFrequencyE
	     << " Hz\t Target attenuation: " << dTargetAttenuationE << " dB\t Calculated attenuation: " << dActualAttenuationE << " dB" << endl;
	cout << "Temperature: " << dTemperatureF << " �C\t Relative humidity: " << dHumidityF << " %\t Frequency: " << dFrequencyF
	     << " Hz\t Target attenuation: " << dTargetAttenuationF << " dB\t Calculated attenuation: " << dActualAttenuationF << " dB" << endl;
}
