#include <ITAConfigUtils.h>
#include <iostream>

using namespace std;

int main( int nArgsIn, char** pccArgs )
{
	if( nArgsIn > 1 )
		INIFileUseFile( std::string( pccArgs[1] ) );
	else
		INIFileUseFile( "ConfigUtilsTest.ini" );

	auto sections = INIFileGetSections( );

	for( auto section: sections )
	{
		cout << section << endl;

		INIFileUseSection( section );
		auto keys = INIFileGetKeys( section );
		for( auto key: keys )
			cout << " + " << key << "\t'" << INIFileReadString( key ) << "' [STRING] " << endl;
	}

	return 0;
}
