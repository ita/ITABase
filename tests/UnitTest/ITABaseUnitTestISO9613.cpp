/*
 * ----------------------------------------------------------------
 *
 *		ITA Base
 *		(c) Copyright Institute of Technical Acoustics (ITA)
 *		RWTH Aachen University, Germany
 *
 * ----------------------------------------------------------------
 *				    ____  __________  _______
 *				   //  / //__   ___/ //  _   |
 *				  //  /    //  /    //  /_|  |
 *				 //  /    //  /    //  ___   |
 *				//__/    //__/    //__/   |__|
 *
 * ----------------------------------------------------------------
 *
 *
 *
 */
 
#include "ITABaseTestUtils.h"

#include <ITAISO9613.h>
#include <ITABase/ISO9613/ITAISO9613ReferenceValues.h>
#include <ITAThirdOctaveMagnitudeSpectrum.h>
#include <cmath>

bool GainsBelowMaxdBDeviation( float fGain1, float fGain2, float fMaxDB = 1.0f )
{
	const float fDeltaDB = std::abs( 10.0f * std::log10( fGain1 / fGain2 ) );
	return fDeltaDB < fMaxDB;
}

void ISOAirAttenuationTestBase( const ITABase::ISO9613::CReferenceDataSet& oReference, bool bUseGains )
{
	const double dDistance = 100;
	ITABase::CThirdOctaveGainMagnitudeSpectrum oOutputSpectrum;
	ITABase::CThirdOctaveDecibelMagnitudeSpectrum oOutputSpectrumDB;
	ITABase::CThirdOctaveGainMagnitudeSpectrum oTargetSpectrum;
	ITABase::CThirdOctaveDecibelMagnitudeSpectrum oTargetSpectrumDB;

	if( bUseGains )
		oTargetSpectrum   = oReference.GainSpectrum( (float)dDistance );
	else
		oTargetSpectrumDB = oReference.SpectrumDB( (float)dDistance );

	const double dRelativeHumidityPercent     = oReference.RelativeHumidity( );
	const double dStaticPressureKiloPa        = oReference.StaticPressureKiloPa( );
	const double dTemperatureDegreeCentigrade = oReference.TemperatureDegC( );
	if( bUseGains )
		ITABase::ISO9613::AtmosphericAbsorption( oOutputSpectrum, dDistance, dTemperatureDegreeCentigrade, dRelativeHumidityPercent, dStaticPressureKiloPa );
	else
		ITABase::ISO9613::AtmosphericAbsorption( oOutputSpectrumDB, dDistance, dTemperatureDegreeCentigrade, dRelativeHumidityPercent, dStaticPressureKiloPa );

	const int idx50Hz   = 4;
	const int idx250Hz  = 11;
	const int idx2000Hz = 20;
	const int idx3150Hz = 22;
	const int idx6350Hz = 25;
	const int idx10kHz  = 27;
	float fToleranceDB;
	for( int idx = idx50Hz; idx < idx10kHz; idx++ )
	{
		if( idx < idx250Hz )
			fToleranceDB = 0.1f;
		else if( idx < idx2000Hz )
			fToleranceDB = 0.15f;
		else if( idx < idx3150Hz )
			fToleranceDB = 0.25f;
		else if( idx < idx6350Hz )
			fToleranceDB = 0.35f;
		else
			fToleranceDB = 1.0f;

		if( !bUseGains )
			fToleranceDB = std::min( 1.0f, fToleranceDB * 2.0f );

		if (bUseGains)
		{
			REQUIRE( GainsBelowMaxdBDeviation( oOutputSpectrum[idx], oTargetSpectrum[idx], fToleranceDB ) );
		}
		else
		{
			REQUIRE_THAT( oOutputSpectrumDB[idx], Catch::Matchers::WithinAbs( oTargetSpectrumDB[idx], fToleranceDB ) );
		}
	}
}

TEST_CASE( "ISO 9613-1", "[ITABase][ISO9613][AirAttenuation][PrioHigh]" )
{
	auto idxISODataCase = GENERATE( range( ITABase::ISO9613::CReferenceDataSet::MinimumCaseID( ), ITABase::ISO9613::CReferenceDataSet::MaximumCaseID( ) + 1 ) );
	auto oReference     = ITABase::ISO9613::CReferenceDataSet::Create( idxISODataCase );

	SECTION( "Attenuation dB" )
	{
		ISOAirAttenuationTestBase( oReference, false );
	}
	SECTION( "Gain factors" )
	{
		ISOAirAttenuationTestBase( oReference, true );
	}
}