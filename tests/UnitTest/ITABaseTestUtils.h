/*
 * ----------------------------------------------------------------
 *
 *		ITA Base
 *		(c) Copyright Institute of Technical Acoustics (ITA)
 *		RWTH Aachen University, Germany
 *
 * ----------------------------------------------------------------
 *				    ____  __________  _______
 *				   //  / //__   ___/ //  _   |
 *				  //  /    //  /    //  /_|  |
 *				 //  /    //  /    //  ___   |
 *				//__/    //__/    //__/   |__|
 *
 * ----------------------------------------------------------------
 *
 *
 *
 */
 
#ifndef IW_ITA_BASE_UNIT_TEST_UTILS
#define IW_ITA_BASE_UNIT_TEST_UTILS

#include <catch2/catch_test_macros.hpp>
#include <catch2/catch_translate_exception.hpp>
#include <catch2/generators/catch_generators_all.hpp>
#include <catch2/matchers/catch_matchers_all.hpp>
//#include <fakeit.hpp>
//#include <limits>
#include <ITAException.h>


CATCH_TRANSLATE_EXCEPTION( const ITAException& ex )
{
	return ex.ToString( );
}


#endif //IW_ITA_BASE_UNIT_TEST_UTILS
