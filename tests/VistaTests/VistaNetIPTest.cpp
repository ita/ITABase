#include <VistaInterProcComm/Connections/VistaConnectionIP.h>
#include <VistaInterProcComm/IPNet/VistaIPAddress.h>
#include <VistaInterProcComm/IPNet/VistaNetworkInfo.h>
#include <iostream>

using namespace std;

int main( int, char** )
{
	std::vector<VistaNetworkInfo> devs;
	VistaNetworkInfo::EnumVistaNetworkInfo( devs );

	cout << "Your network devices are:" << endl;
	for( size_t i = 0; i < devs.size( ); i++ )
	{
		VistaIPAddress oAddress = devs[i].GetAddress( );
		std::string sHostName;
		oAddress.GetHostName( sHostName );
		cout << "[ " << i << " ]: " << sHostName;
		if( sHostName.compare( 0, 4, "137." ) == 0 || sHostName.compare( 0, 4, "134." ) == 0 )
			cout << " (RWTH)";
		if( sHostName.compare( 0, 11, "137.226.61." ) == 0 )
			cout << " (ITA)";
		cout << endl;
	}

	std::string sHost = "verdi.akustik.rwth-aachen.de";
	int iPort         = 80; // www service is usually available on server
	cout << "Attempting to connect to '" << sHost << "' on port " << iPort << " ... ";
	VistaConnectionIP oVerdiConn( VistaConnectionIP::VistaProtocol::CT_TCP, sHost, iPort ); // blocking connect
	oVerdiConn.GetIsConnected( ) ? cout << "success!" : cout << "failed.";

	cout << endl;

	return 0;
}
