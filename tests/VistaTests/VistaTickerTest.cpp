#include <VistaBase/VistaTimeUtils.h>
#include <VistaInterProcComm/Concurrency/VistaTicker.h>
#include <iostream>

using namespace std;

class TickerCallback : public VistaTicker::AfterPulseFunctor
{
public:
	inline bool operator( )( )
	{
		cout << "Ticker callback called, text is: " << sText << endl;
		return true;
	};

	int nGranularityMS;
	std::string sText;
};

int main( int, char** )
{
	VistaTicker oTicker;

	TickerCallback oCallback;
	oCallback.sText          = "First call";
	oCallback.nGranularityMS = oTicker.GetGranularity( );

	VistaTicker::TriggerContext oTickerContext( oTicker.GetGranularity( ), true );
	oTicker.SetAfterPulseFunctor( &oCallback );
	oTicker.AddTrigger( &oTickerContext );

	oTicker.StartTicker( );

	VistaTimeUtils::Sleep( int( 2 * 1e3 ) );

	oTicker.StopTicker( );
	oTicker.RemTrigger( &oTickerContext );
	oTicker.SetAfterPulseFunctor( NULL ); // Otherwise Ticker will delete callback instance


	oCallback.sText      = "Second call";
	VistaTicker* pTicker = new VistaTicker( 100 );
	pTicker->AddTrigger( new VistaTicker::TriggerContext( 100, true ) );
	pTicker->SetAfterPulseFunctor( &oCallback );
	pTicker->StartTicker( );
	VistaTimeUtils::Sleep( int( 2 * 1e3 ) );
	pTicker->StopTicker( );
	pTicker->SetAfterPulseFunctor( NULL ); // Otherwise Ticker will delete callback instance
	delete pTicker;

	return 0;
}
