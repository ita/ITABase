#include <ITABase/Math/Triangulation.h>
#include <ITAConstants.h>
#include <VistaBase/VistaVector3D.h>
#include <math.h>
#include <stdio.h>
#include <vector>

using namespace std;
using namespace ITABase;
using namespace ITABase::Math;

bool TestDelaunayCondition( const CTriangulation* triangulation )
{
	if( !triangulation->IsDelaunayTriangulation( ) )
	{
		cout << "Error: Predefined Triangulation satisfies Delaunay condition, however \"IsDelaunayTriangulation\" indicates it doesn't." << endl;
		return false;
	}
	else
	{
		cout << "\"IsDelaunayTriangulation\" -- OK" << endl;
		return true;
	}
}

bool TestIntersectedTriangleDetection( const CTriangulation* triangulation )
{
	bool passed = true;
	// define test points
	const vector<VistaVector3D> vv3TestPos {
		VistaVector3D( 0, 0, -1 ),                                                                  // case node match
		VistaVector3D( -0.088826996301291, 0.392029462242860, -0.797359488602036 ),                 // case on triangle edge
		VistaVector3D( 0.193729508443099, 0.453823535457991, -0.631990887330380 ),                  // case inside triangle
		VistaVector3D( 2 * 0.193729508443099, 2 * 0.453823535457991, 2 * -0.631990887330380 ),      // outside hull inside triangle
		VistaVector3D( 0.5 * 0.193729508443099, 0.5 * 0.453823535457991, 0.5 * -0.631990887330380 ) // outside hull inside triangle
	};

	float ab, ac;
	VistaVector3D v3BarycentricCoords( 0, 0, 0 ), v3ReconstructedFromBary( 0, 0, 0 );

	for( size_t i = 0; i < vv3TestPos.size( ); i++ )
	{
		// function under test
		const VistaTriangle* pTriangle = triangulation->GetTriangleIntersectedByPointDirection( vv3TestPos[i], v3BarycentricCoords );

		bool bInside = v3BarycentricCoords[0] > -1.0e-5 && v3BarycentricCoords[1] > -1.0e-5 && v3BarycentricCoords[2] > -1.0e-5;

		if( i == 0 && bInside )
		{
			cout << "Test case 1: Node match -- OK" << endl;
			cout << "Weights - " << v3BarycentricCoords << endl;
			passed = passed && true;
		}
		else if( i == 1 && bInside )
		{
			cout << "Test case 2: Triangle edge -- OK" << endl;
			cout << "Weights - " << v3BarycentricCoords << endl;
			passed = passed && true;
		}
		else if( i == 2 && bInside )
		{
			cout << "Test case 3: Inside triangle -- OK" << endl;
			cout << "Weights - " << v3BarycentricCoords << endl;
			passed = passed && true;
		}
		else if( i == 3 && bInside )
		{
			cout << "Test case 4: Inside triangle, at radius 2 -- OK" << endl;
			cout << "Weights - " << v3BarycentricCoords << endl;
			passed = passed && true;
		}
		else if( i == 4 && bInside )
		{
			cout << "Test case 5: Inside triangle, at radius 0.5 -- OK" << endl;
			cout << "Weights - " << v3BarycentricCoords << endl;
			passed = passed && true;
		}
		else
		{
			cout << "Error in test case " << i + 1 << ":  Found triangle is not intersected by point direction" << endl;
			cout << "Weights - " << v3BarycentricCoords << endl;
			passed = passed && false;
		}
	}
	if( passed )
		cout << "\"GetTriangleIntersectedByPointDirection\" -- OK" << endl;
	return passed;
}

int main( int, char** )
{
	int nTests = 0, nErrors = 0;
	// define points to be triangulated
	const vector<VistaVector3D> vv3LsPos { // 4-Design, 14 points
		                                   VistaVector3D( 0, 0, -1 ),
		                                   VistaVector3D( -0.17765, 0.78406, -0.59472 ),
		                                   VistaVector3D( 0.76784, -0.23818, -0.59472 ),
		                                   VistaVector3D( -0.59019, -0.54588, -0.59472 ),
		                                   VistaVector3D( -0.17765, -0.78406, 0.59472 ),
		                                   VistaVector3D( -0.59019, 0.54588, 0.59472 ),
		                                   VistaVector3D( 0.76784, 0.23818, 0.59472 ),
		                                   VistaVector3D( -0.87947, 0.36847, -0.30125 ),
		                                   VistaVector3D( 0.75884, 0.57741, -0.30125 ),
		                                   VistaVector3D( 0.12063, -0.94588, -0.30125 ),
		                                   VistaVector3D( -0.87947, -0.36847, 0.30125 ),
		                                   VistaVector3D( 0.12063, 0.94588, 0.30125 ),
		                                   VistaVector3D( 0.75884, -0.57741, 0.30125 ),
		                                   VistaVector3D( -1.2246e-16, 0, 1 )
	};
	// define center
	const VistaVector3D v3CenterPos = VistaVector3D( 0, 0, 0 );
	// define reference delaunay triangulation
	const vector<VistaVector3D> vv3TriangulationIndices {
		VistaVector3D( 0, 1, 8 ),   VistaVector3D( 0, 2, 9 ),   VistaVector3D( 0, 3, 7 ),   VistaVector3D( 0, 7, 1 ),  VistaVector3D( 0, 8, 2 ),
		VistaVector3D( 0, 9, 3 ),   VistaVector3D( 1, 5, 11 ),  VistaVector3D( 1, 7, 5 ),   VistaVector3D( 1, 11, 8 ), VistaVector3D( 2, 6, 12 ),
		VistaVector3D( 2, 8, 6 ),   VistaVector3D( 2, 12, 9 ),  VistaVector3D( 3, 4, 10 ),  VistaVector3D( 3, 9, 4 ),  VistaVector3D( 3, 10, 7 ),
		VistaVector3D( 4, 9, 12 ),  VistaVector3D( 4, 12, 13 ), VistaVector3D( 4, 13, 10 ), VistaVector3D( 5, 7, 10 ), VistaVector3D( 5, 10, 13 ),
		VistaVector3D( 5, 13, 11 ), VistaVector3D( 6, 8, 11 ),  VistaVector3D( 6, 11, 13 ), VistaVector3D( 6, 13, 12 )
	};
	const CTriangulation* triangulation = new CTriangulation( vv3LsPos, vv3TriangulationIndices, v3CenterPos );


	// ---------- Start Tests ----------
	cout << setprecision( 2 );
	cout << "\tStarting Tests for Triangulation\t" << endl;
	cout << "------------------------------------------------" << endl << endl;

	cout << "------------ Test 1 ------------" << endl;
	cout << "Check Delaunay condition function with predefined triangulation from matlab" << endl;
	nErrors += TestDelaunayCondition( triangulation ) ? 0 : 1;
	nTests += 1;
	cout << endl;

	cout << "------------ Test 2 ------------" << endl;
	cout << "Check calculation of intersected triangles for different cases" << endl;
	nErrors += TestIntersectedTriangleDetection( triangulation ) ? 0 : 1;
	nTests += 1;
	cout << endl;

	// --------- All Tests finished ----------
	cout << "------------------------------------------------" << endl;
	cout << "All tests finished -- Passed " << nTests - nErrors << " of " << nTests << " tests" << endl;

	return 0;
}
