#include <ITAMagnitudeSpectrum.h>
#include <ITANumericUtils.h>
#include <ITASpectrum.h>
#include <ITAThirdOctaveMagnitudeSpectrum.h>
#include <stdio.h>

using namespace std;
using namespace ITABase;

void test_third_octave_spectrum( );
void test_db_to_ration( );
void test_ratio_to_db( );

int main( int, char** )
{
	test_third_octave_spectrum( );
	cout << "-----------" << endl;

	test_db_to_ration( );
	cout << "-----------" << endl;

	test_ratio_to_db( );
	cout << "-----------" << endl;

	return 0;
}

void test_db_to_ration( )
{
	CThirdOctaveDecibelMagnitudeSpectrum oTOSpectrumConvertToRatio;
	oTOSpectrumConvertToRatio.SetName( "Test unit decibel magnitude spectrum to ratio" );

	oTOSpectrumConvertToRatio.SetIdentity( );
	for( int n = 0; n < oTOSpectrumConvertToRatio.GetNumBands( ); n++ )
		oTOSpectrumConvertToRatio[n] = db10_to_ratio( oTOSpectrumConvertToRatio[n] );

	oTOSpectrumConvertToRatio.SetValueUnit( "(gain)" );
	cout << oTOSpectrumConvertToRatio << endl;

	oTOSpectrumConvertToRatio.SetZero( );
	for( int n = 0; n < oTOSpectrumConvertToRatio.GetNumBands( ); n++ )
		oTOSpectrumConvertToRatio[n] = db10_to_ratio( oTOSpectrumConvertToRatio[n] );

	oTOSpectrumConvertToRatio.SetValueUnit( "(gain)" );
	cout << oTOSpectrumConvertToRatio << endl;
}

void test_ratio_to_db( )
{
	CThirdOctaveFactorMagnitudeSpectrum oTOSpectrumConvertToRatio;
	oTOSpectrumConvertToRatio.SetName( "Test unit factor spectrum to db" );
	oTOSpectrumConvertToRatio.SetValueUnit( "(gain)" );
	oTOSpectrumConvertToRatio.SetZero( ); // all zeros

	for( int n = 0; n < oTOSpectrumConvertToRatio.GetNumBands( ); n++ )
		oTOSpectrumConvertToRatio[n] = ratio_to_db10( oTOSpectrumConvertToRatio[n] ); // zeros to db -> -inf

	oTOSpectrumConvertToRatio.SetValueUnit( "dB" );

	cout << oTOSpectrumConvertToRatio << endl;

	// Convert gains "1"
	oTOSpectrumConvertToRatio.SetIdentity( ); // all ones
	for( int n = 0; n < oTOSpectrumConvertToRatio.GetNumBands( ); n++ )
		oTOSpectrumConvertToRatio[n] = ratio_to_db10( oTOSpectrumConvertToRatio[n] ); // ratio 1 to db -> 0

	oTOSpectrumConvertToRatio.SetValueUnit( "dB" );

	cout << oTOSpectrumConvertToRatio << endl;
}

void test_third_octave_spectrum( )
{
	CThirdOctaveGainMagnitudeSpectrum oTOSpectrumIdent;
	oTOSpectrumIdent.SetName( "Test unit gain magnitude spectrum" );
	oTOSpectrumIdent.SetIdentity( );

	cout << "Third octave gain magnitude spectrum identity:" << endl;
	cout << oTOSpectrumIdent << endl;

	CThirdOctaveGainMagnitudeSpectrum oTOSpectrumZero;
	oTOSpectrumZero.SetName( "Test unit zero spectrum" );
	oTOSpectrumZero.SetZero( );

	cout << "Third octave gain magnitude spectrum zero:" << endl;
	cout << oTOSpectrumZero << endl;
}
