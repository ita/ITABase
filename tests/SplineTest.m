%Plots the desired Spline interpolation for the DataPoint Values at the
%Supporting Point location indicated in the provided .txt file and compares
%the MATLAB spline against the spline created with C++ by reading another
%.txt file. Compares also the first derivate of both splines
%
%Expected file Formats:
%   coeff.txt
%       a1, b1, c1, d1
%       a2, b2, c2, d2
%       a3, b3, c3, d3
%       ...,...,...,...
%   points.txt
%       sup1, data1
%       sup2, data2
%       sup3, data3
%       ... , ...
%   derivate.txt
%       a1, b1, c1
%       a2, b2, c2
%       a3, b3, c3
%       ...,...,...,...
% The SupportingPoints need to be sorted ascending!
% 
% You will find that the spline computed by matlab will overshoot close to
% the bordering sampling points more then the splines from ITA. This comes 
% from using natural Splines(ITA) vs. clamped splines(Matlab). It's
% illustrated in the difference data ploted in both figures.

%% read Data from files
FID = fopen('points.txt');
if FID == -1
    disp('unable to read indicated file');
    return
end
C = textscan(FID,'%f %f','Delimiter',',');
supPoints = C{1};
dataPoints = C{2};
fclose(FID);

%Coeffs
FID = fopen('coeff.txt');
if FID == -1
    disp('unable to read indicated file');
    return
end

coeff = zeros(size(supPoints,1)-1,4);%alocate
tline = fgetl(FID);
k=1;
while ischar(tline)
    C = textscan(tline,'%f','Delimiter',',');
    coeff(k,:) = C{1,1};
    
    tline = fgetl(FID);
    k = k+1;
end
fclose(FID);
FID = fopen('derivate.txt');
if FID == -1
    disp('unable to read indicated file');
    return
end

deriv_coeff = zeros(size(supPoints,1)-1,3);%alocate
tline = fgetl(FID);
k=1;
while ischar(tline)
    C = textscan(tline,'%f','Delimiter',',');
    deriv_coeff(k,:) = C{1,1};
    
    tline = fgetl(FID);
    k = k+1;
end
fclose(FID);
%coeffs of deriv_coeff

%% plot
%spline
myPoly = mkpp(supPoints,coeff);

grain = (supPoints(end)-supPoints(1))/(10*size(supPoints,1)); %how dense shall the plotted points be?
xx = [supPoints(1) : grain : supPoints(end)];

plot(supPoints,dataPoints,'o',xx,spline(supPoints,dataPoints,xx));
hold on
grid on
title 'Splines: Matlab vs. ITA'
plot(xx,ppval(myPoly,xx));
plot(xx,spline(supPoints,dataPoints,xx)-ppval(myPoly,xx));
legend('Sampling Points','Matlab','ITA','Diff');
hold off

%derivation of spline

myDerivate = mkpp(supPoints,deriv_coeff);
matlabDerivate = fnder(spline(supPoints,dataPoints));

grain = (supPoints(end)-supPoints(1))/(10*size(supPoints,1)); %how dense shall the plotted points be?
xx = [supPoints(1) : grain : supPoints(end)];

figure
plot(xx,ppval(matlabDerivate,xx));
hold on
grid on
title '1st derivitate of Splines: Matlab vs. ITA'
plot(xx,ppval(myDerivate,xx));
plot(xx,ppval(matlabDerivate,xx)-ppval(myDerivate,xx));
legend('Matlab','ITA','Diff');
hold off